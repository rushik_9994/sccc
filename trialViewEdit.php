<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/Comments.class.php');
require_once(ROOT_PATH.'/lib/TrialDiseaseType.class.php');
require_once(ROOT_PATH.'/lib/TrialTrialDiseaseTypeJoin.class.php');
require_once(ROOT_PATH.'/lib/TrialPhase.class.php');
require_once(ROOT_PATH.'/lib/TrialCategory.class.php');
require_once(ROOT_PATH.'/lib/TrialType.class.php');
require_once(ROOT_PATH.'/lib/TrialGroup.class.php');
require_once(ROOT_PATH.'/lib/TrialCompanion.class.php');
require_once(ROOT_PATH.'/lib/SiteTrialsOpenJoin.class.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//create required objects
$tdt                 = new TrialDiseaseType;
$ttdtj               = new TrialTrialDiseaseTypeJoin;
$permissions         = new Permissions;
$trial               = new Trial;
$comment             = new Comments;
$trialPhase          = new TrialPhase;
$trialCategory       = new TrialCategory;
$trialType           = new TrialType;
$trialGroup          = new TrialGroup;
$trialCompanion      = new TrialCompanion;
$trialCompanionTrial = new Trial;
$stoj                = new SiteTrialsOpenJoin;
$site                = new Site;

//Set some defaults
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_site_name_0';
    $_REQUEST['order_direction_0'] = 'asc';
}

$allowedTags = '<strong> <b> <i> <em> <u> <br> <p>';

//define required variables
$msg = '';
$permissions->Load_from_action("ViewTrials");
$UserTypeIDs = explode(',', $permissions->UserTypeIDs);

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'default.pagination.size');

//searchFilter setup
$sFilter  = new SearchFilter('trials');
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, 0, $cfg);

//Check permissions
$canEditRows = checkPermissionString('AddEditTrials');
$canViewRows = checkPermissionString('ViewTrials');
if ((!$canEditRows) && (!$canViewRows)) {
    header('location: login.php');  exit;
}

if (!empty($_REQUEST['id'])) { //viewing trial
    $trial->Load_from_key($_REQUEST['id']);
    $ttdtj_keys = $ttdtj->GetKeysWhereOrderBy('TrialKeyID = \'' . $_REQUEST['id'] . '\'',
                                              'TrialDiseaseTypeKeyID',
                                              'ASC');
    $tg_keys    = $trialGroup->GetKeysWhereOrderBy('TrialKeyID = \'' . $_REQUEST['id'] . '\'',
                                                   'TrialGroupKeyID',
                                                   'ASC');
    if (count($tg_keys) > 0) {
        $i = 0;
        foreach ($tg_keys as $TrialGroupKeyID) {
            $trialGroup->Load_from_key($TrialGroupKeyID);
            $trialGroups[$i] = '<tr><td class="left">' . $trialGroup->TrialGroupTitle . '</td><td class="left">' . $trialGroup->TrialGroupDescription . '</td></tr>';
            $i++;
        }
    }
    $tc_keys = $trialCompanion->GetKeysWhereOrderBy('TrialKeyIDOUT', 'TrialKeyIDIN = \'' . $_REQUEST['id'] . '\'', 'TrialKeyIDOUT', 'ASC');
    if (count($tc_keys) > 0){
        $i = 0;
        foreach ($tc_keys as $TrialCompanionKeyID) {
            $trialCompanionTrial->Load_from_key($TrialCompanionKeyID);
            $trialCompanionTrials[$i] = '<tr><td class="left">' . $trialCompanionTrial->TrialProtocolNumber . '</td></tr>';
            $i++;
        }
    }
    $comment_keys = $comment->GetKeysWhereOrderBy('TrialKeyID=' . $comment->SqlQuote($_REQUEST['id']), 'CommentKeyID', 'ASC');

    $comment->Load_from_key($comment_keys[0]);
    $trialPhase->Load_from_key($trial->TrialPhaseKeyID);
    $trialType->Load_from_key($trial->TrialTypeKeyID);
    $trialCategory->Load_from_key($trial->TrialCategoryKeyID);
    $TrialDiseases = '';
    foreach ($ttdtj_keys as $tdt_key) {
        $tdt->Load_from_key($tdt_key);
        $TrialDiseases .= $tdt->TrialDiseaseTypeName . '<br>';
    }
}


?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<meta name="author" content="Cliff Garrett" />
		<title>
			Trial View/Edit
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
		<script type="text/javascript">
        	$(document).ready(function(){
        	});
		</script>
</head>
<body>
    <form id='main_form' name='main_form' method='POST'>
<?php
        print $pg->generateFormVariables($pgInfo);
?>
        <div class="wrapper">
            <div class="logo">
            </div>
            <div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('trials');
                ?>
                <div id="tabs-1" class="ui-tabs-panel">
                    <div class="subhead">
					Trial Add/Edit
<?php               if ($canEditRows) { ?>
                        <a class="button" href="trialAddEdit.php">Add Trial</a>
                        <a id="main-button" class="button" href="trialAddEdit.php?id=<?php echo $_REQUEST['id']; ?>">Edit Trial</a>
<?php               } ?>
                        <a href="trials.php" class="button">Back</a>
                    <?php displaySuccessAndErrors(); ?>
                    </div>
                    <hr />
                    <div class="msg"><?php echo $msg; ?></div>
                    <table border="0" class="fullwidth">
                      <tr>
                        <td><strong>Protocol Number:</strong> <?php echo (!empty($trial->TrialProtocolNumber)) ? $trial->TrialProtocolNumber : ''; ?></td>
                        <td></td>
                        <td><strong>Trial Type:</strong> <?php echo $trialType->TrialTypeText; ?></td>
                        <td></td>
                        <td><strong>Open Date:</strong> <?php echo ((!empty($trial->TrialOpenDate)) && ($trial->TrialOpenDate != '0000-00-00')) ? $trial->TrialOpenDate : ''; ?></td>
                        <td></td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td><strong>CIRB:</strong> <?php echo ($trial->TrialCIRB == 0) ? 'No' : 'Yes'; ?></td>
                        <td>&nbsp;</td>
                        <td><strong>Trial Phase:</strong> <?php echo $trialPhase->TrialPhaseText; ?></td>
                        <td>&nbsp;</td>
                        <td><strong>Close Date:</strong> <?php echo ((!empty($trial->TrialCloseDate)) && ($trial->TrialCloseDate != '0000-00-00')) ? $trial->TrialCloseDate : ''; ?></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><strong>Trial Credit:</strong> <?php echo (!empty($trial->TrialCredit)) ? $trial->TrialCredit : ''; ?></td>
                        <td>&nbsp;</td>
                        <td><strong>Trial Category:</strong> <?php echo $trialCategory->TrialCategoryText; ?></td>
                        <td>&nbsp;</td>
                        <td><strong>Term Date:</strong> <?php echo ((!empty($trial->TrialTerminationDate)) && ($trial->TrialTerminationDate != '0000-00-00')) ? $trial->TrialTerminationDate : ''; ?></td>
                        <td></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td colspan="2"><strong>Trial QoL:</strong> <input type="checkbox" name="trial[TrialQoL]" id="TrialQoL" <?php echo ($trial->TrialQoL == 0) ? '' : 'checked'; ?> disabled="disabled" />&emsp;<strong>Trial QoLC:&nbsp;</strong><?php echo $trial->TrialQoLC; ?></td>
                        <td>
                            <strong>NCT:</strong> <?php echo $trial->TrialNct; ?>
<?php
                            if ((isset($trial->TrialNct)) && ($trial->TrialNct)) {
                                print '&nbsp;&nbsp;<a style="font-size:8pt;" href="https://clinicaltrials.gov/ct2/show/'. urlencode($trial->TrialNct). "\" target='top'>[NCT]</a>\n";
                                print '&nbsp;&nbsp;<a style="font-size:8pt;" href="trialNctData.php?format=xml&trialnct='. urlencode($trial->TrialNct). "\" target='top'>[xml]</a>\n";
                                print '&nbsp;&nbsp;<a style="font-size:8pt;" href="trialNctData.php?format=flat&trialnct='. urlencode($trial->TrialNct). "\" target='top'>[flat]</a>\n";
                            }
?>
                        </td>
                        <td colspan="4"> </td>
                      </tr>
                      <tr>
                        <td><strong>Target Accrual:</strong> <?php echo (!empty($trial->TrialTargetAccrual)) ? $trial->TrialTargetAccrual : ''; ?></td>
                        <td></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><?php echo ($trial->TrialSuspend == 0) ? '' : '<strong>Status:</strong> <span class="emphasis">Suspended</span>'; ?></td>
                        <td>&nbsp;</td>
                        <td></td>
                      </tr>
                      <tr>
                        <td colspan="7">Trial is <?php echo ($trial->TrialReportExclude == true) ? 'EXCLUDED' : 'NOT EXCLUDED'; ?> from Reports</td>
                      </tr>
                    </table>
                    <hr />
                    <table border="0" class="halfwidth floatright">
                      <tr>
                        <td>
                            <div class="formdiv">
                                <div class="clear"></div>
                                <table class="fullwidth zebra sort">
                                    <thead>
                                        <tr><th class="left">Trial Groups</th><th></th></tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($trialGroups as $trialGroupRow)
{
    echo $trialGroupRow;
} ?>
                                  </tbody>
                                </table>
                            </div>
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <div class="formdiv">
                                <div class="clear"></div>
                                <table class="fullwidth zebra sort">
                                    <thead>
                                        <tr><th class="left">Companion Trials</th></tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($trialCompanionTrials as $trialCompanionTrialRow)
{
    echo $trialCompanionTrialRow;
} ?>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                      </tr>
                    </table>
                    <table border="0" class="halfwidth">
                      <tr>
                        <td><strong>Disease Types:<br /></strong><?php echo $TrialDiseases; ?></td>
                      </tr>
                      <tr>
                        <td><br /><strong>Comment:</strong><br /><?php echo $trial->TrialComment; ?></td>
                      </tr>
                      <!-- <tr>
                       <td><br /><strong>Treatment:</strong> <?php echo (!empty($trial->TrialTreatment)) ? $trial->TrialTreatment : ''; ?>
                      </tr> -->
                    </table>

                    <!-- ---------------------------- -->
                    <br />
                    <table border="1" class="fullwidth">
                        <tr>
                            <td>
                                <table border="0" class="fullwidth">
                                    <tr class="thead_unselected">
                                        <td align="center" style="color:white;" colspan="4">
                                            Trial Info
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="1%">&nbsp;</td>
                                        <td width="15%" valign="top">
                                            <div class="vertical-padding-light"><strong>Treatment:&nbsp;</strong></div>
                                        </td>
                                        <td style="font-size:9pt;">
                                            <?php
                                                $trialTreatment = (!empty($trial->TrialTreatment)) ? $trial->TrialTreatment : '';
                                                $trialTreatment = strip_tags($trialTreatment, $allowedTags);
                                                echo $trialTreatment;
                                            ?>
                                            <br />
                                        </td>
                                        <td width="1%">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td width="1%">&nbsp;</td>
                                        <td width="15%" valign="top">
                                            <div class="vertical-padding-light"><strong>Summary:&nbsp;</strong></div>
                                        </td>
                                        <td style="font-size:9pt;">
                                            <?php
                                            $trialSummaryEn = (!empty($trial->TrialSummaryEn)) ? $trial->TrialSummaryEn : '';
                                            $trialSummaryEn = strip_tags($trialSummaryEn);
                                            echo htmlentities($trialSummaryEn);
                                            ?>
                                            <br />
                                        </td>
                                        <td width="1%">&nbsp;</td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <!-- ---------------------------- -->

                    <div class="clear"></div>

                <?php
                // Lookup and display each site within Trial
                $siteList = array();
                $i = 0;
                $site_keys = $stoj->GetKeysWhereOrderBy('SiteKeyID',
                                                        'TrialKeyID = \'' . $_REQUEST['id'] . '\'', 
                                                        'TrialKeyID',
                                                        'ASC',
                                                        $sClauses);
                if (count($site_keys) > 0) {
                    foreach ($site_keys as $SiteKeyID) {
                        $stoj->Load_from_trial_and_site_id($_REQUEST['id'], $SiteKeyID);
                        $site->Load_from_key($stoj->SiteKeyID);
                        $siteList[$i] = '<tr><td class="left">' . $site->SiteName . '&emsp;</td><td class="left">' . $stoj->SiteTrialOpenApprovalDate . '</td><td class="left">' . $stoj->SiteTrialOpenCloseDate . '</td><td class="left">' . $stoj->SiteTrialOpenTerminationDate . '</td><td class="left">' . $stoj->TrialCompCloseDate . '</td></tr>';
                        $i++;
                    }
                }
                ?>
                <hr>
                <div class="vertical-padding-light"><strong>Sites</strong></div><br />
                <?php if (count($siteList) > 0)
                { ?>
                <a name='tb_0'></a>
                <table class="fullwidth zebra sort">
                    <thead>
                        <tr class='thead_unselected'>
<?php
                            print 
                            $pg->generateTheadLine('order_site_name_0',       'Site Name', array('isdefault'=>1)).
                            $pg->generateTheadLine('order_approval_date_0',   'Approval Date').
                            $pg->generateTheadLine('order_close_date_0',      'Close Date').
                            $pg->generateTheadLine('order_termination_date_0','Termination Date').
                            "<th></th>\n";                            
?>
                        </tr>
                        <tbody>
                        <?php foreach ($siteList as $siteRow) {
                            echo $siteRow;
                        } ?>
                        </tbody>
                </table>
                <?php } ?>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
<?php

