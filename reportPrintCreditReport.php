<?php
/**
 * Print the 'Credit Report'
 */

include_once('reportCommonFunctions.php');
include_once('reportPrintFunctions.php');


session_start();
if (empty($_SESSION['UserKeyID'])) {
    header('location: login.php'); exit;
}

//Start our session, extract POST and GET from SESSION
list($connection, $reportInfo) = commonStartNewReport('Credit Report');

$reportInfo['printoutTitle'] = $reportInfo['reportTitle'] .
                               '     '.
                               'from '.$_POST['ReportStartDate'].
                               '   '.
                               'to '.  $_POST['ReportEndDate'];

printLibraryGeneratePrintout($connection, $reportInfo, 'callbackPrintCreditReport');

// echo '<pre>';
// var_dump($reportInfo);
// echo '</pre>';
// exit;

/**
 * Callback function allows us to modify all print behavior
 */
function callbackPrintCreditReport($what, $connection, $reportInfo, $cbInfo) {
    if ($what=='start_the_printout') {
        $reportInfo['totals'] = array('Accruals'=>0, 'Credits'=>0.0, 'Total_Credits'=>0.0);
    } else if ($what=='before_one_row') {

    } else if ($what=='after_one_row') {
        $reportInfo['totals']['Accruals']      += $cbInfo['row']['Accruals'];
        $reportInfo['totals']['Credits']       += $cbInfo['row']['Credits'];
        $reportInfo['totals']['Total_Credits'] += $cbInfo['row']['Total_Credits'];
        
    } else if ($what=='end_the_printout') {
        $lastRow = ((isset($cbInfo['lastRow'])) && (is_array($cbInfo['lastRow']))) ?  $cbInfo['lastRow'] : array();
        $cbInfo['cont']  = "        <tr><td colspan=\"".$reportInfo['columnCount']."\"><hr/></td></tr>\n";

        if (count($lastRow) > 0) {
            $cbInfo['cont'] .= "        <tr>\n";
            foreach($lastRow as $key=>$val) {
                $cbInfo['cont'] .= "            <td><b>".htmlentities($key)."</b></td>\n";
            }
            reset($lastRow);
            $cbInfo['cont'] .= "        </tr>\n";
        }

        //TOTALS  Accrual-Totals and Credit-Totals
        $cbInfo['cont'] .= "        <tr>\n";
        foreach($lastRow as $key=>$val) {
            if ($key=='Protocol') {
                $cbInfo['cont'] .= "            <td><b>TOTALS</b></td>\n";
            } else if ($key=='Accruals') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['Accruals']."</b></td>\n";
            } else if ($key=='Credits') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['Credits']."</b></td>\n";
            } else if ($key=='Total_Credits') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['Total_Credits']."</b></td>\n";
            } else {
                $cbInfo['cont'] .= "            <td>&nbsp;</td>\n";
            }
        }
        reset($lastRow);
        $cbInfo['cont'] .= "        </tr>\n".
                           "    </tbody>\n".
                           "</table>\n";
    }

    return array($reportInfo, $cbInfo);
}


