<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

if ($_GET['status'] == 1) {
    $prefix = 'Un-';
} else {
    $prefix = '';
} ?>
<html>
	<head>
    <style>
	textarea { width: 100%; padding: 10px; border: 3px solid #999;
   	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
	-moz-box-sizing: border-box;    /* Firefox, other Gecko */
	box-sizing: border-box;         /* IE 8+ */
}
</style>
<script>
$(document).ready(function(){
    $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
});
</script>
	</head>
	<body>
        <form name="confirm" id="confirm" method="post" action="<?php echo $_GET['url']; ?>">
        <input type="hidden" name="CompKeyID" value="<?php echo $_GET['id']; ?>" />
        <input type="hidden" name="action" value="<?php echo $_GET['action']; ?>" />
            <div class="colorbox-frame">
                <table class="centered fullwidth outer-margin">
                <?php if ($_GET['status'] != 1)
{ ?>
                    <tr>
                        <td>&emsp;<strong><?php echo $prefix . ucfirst($_GET['action']); ?> Date:</strong></td>
                        <td><input type="text" name="comp[CompTerminationDate]" id="CompTerminationDate" size="32" class="dpick req" value="<?php echo (!empty($comp->CompTerminationDate)) ? $comp->CompTerminationDate : ''; ?>" /></td>
                    </tr>
                <?php } else
{ ?>
                    <tr>
                        <td></td>
                        <td><input type="hidden" name="comp[CompTerminationDate]" size="32" class="" value="" /></td>
                    </tr>
                <?php } ?>
                    <tr>
                        <td class="center extra-padding" class="center" colspan="2"><textarea name="comment" class="confirm-comment req"></textarea></td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="return validate('confirm');$('#confirm').submit();">&emsp;<?php echo $prefix . ucfirst($_GET['action']); ?> Comp&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;Cancel&emsp;</a></td>
                    </tr>
                </table>
            </div>
        </form>
	</body>
</html>
<?php


