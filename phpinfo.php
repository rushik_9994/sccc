<?php
/**
 * @author Cliff Garrett
 * @copyright 2011
 */


//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//Only accessible if an admin is logged in
if ((!isset($_SESSION['UserTypeKeyID'])) || ($_SESSION['UserTypeKeyID']!=1)) {
    print "Access not allowed";
    exit;
}

echo phpinfo();

