<?php
/**
 * Doctor management page
 */

//load required classes
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/vendor/svggraph/SVGGraph.php'); //----!!!

require_once(ROOT_PATH.'/lib/commonBase.php');  //checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

 $graph = new SVGGraph(640, 480);
 $graph->colours = array('red','green','blue');
 $graph->Values(100, 200, 150);
 $graph->Links('/Tom/', '/Dick/', '/Harry/');
 //$graph->Render('BarGraph');
// exit;
 
  //header('content-type: application/xhtml+xml; charset=UTF-8');
  header('content-type: image/svg+xml');
  // $graph = new SVGGraph(...);
  // $graph setup here!
  echo $graph->Fetch('BarGraph', true);
