<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

switch ($_GET['action']) {
    case 'close':
        $date_field_name = "TrialCloseDate";
        break;
    case 'terminate':
        $date_field_name = "TrialTerminationDate";
        break;
    default:
        $date_field_name = "date";
}
if ($_GET['status'] == 1) {
    $prefix = 'Un-';
} else {
    $prefix = '';
} ?>
<html>
	<head>
    <style>
	textarea { width: 100%; padding: 10px; border: 3px solid #999;
   	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
	-moz-box-sizing: border-box;    /* Firefox, other Gecko */
	box-sizing: border-box;         /* IE 8+ */
}
</style>
<script>
$(document).ready(function(){
   $("#TrialCloseDate").jdPicker({date_format:"YYYY-mm-dd"});
   $("#TrialCloseDate").live('click',function(){    console.log('hi');
       $('.date_selector').css({'left':'','right':'15px','bottom':'15px','top':''});  
   });
   $("#TrialTerminateDate").jdPicker({date_format:"YYYY-mm-dd"});
   $("#TrialTerminateDate").live('click',function(){
       $('.date_selector').css({'left':'','right':'15px','bottom':'15px','top':''});  
   });
});
</script>
	</head>
	<body>
        <form name="confirm" id="confirm" method="post" action="<?php echo $_GET['url']; ?>">
        <input type="hidden" name="TrialKeyID" value="<?php echo $_GET['id']; ?>" />
        <input type="hidden" name="<?php echo $date_field_name; ?>" value="<?php echo date('Y-m-d', time()) . ' 00:00:00'; ?>" />
        <input type="hidden" name="action" value="<?php echo $_GET['action']; ?>" />
            <div class="colorbox-frame">
                <table class="centered fullwidth outer-margin">
                <?php if ($_GET['status'] != 1)
{ ?>
                    <tr>
                        <td>&emsp;<strong><?php echo $prefix . ucfirst($_GET['action']); ?> Date:</strong></td>
                        <?php if ($_GET['action'] == 'close')
    { ?>
                        <td><input type="text" name="trial[TrialCloseDate]" id="TrialCloseDate" size="32" class="datepick" value="<?php echo (!empty($trial->TrialCloseDate)) ? $trial->TrialCloseDate : date('Y-m-d', time()) . ' 00:00:00'; ?>" /></td>
                        <?php } elseif ($_GET['action'] == 'terminate')
    { ?>
                        <td><input type="text" name="trial[TrialTerminationDate]" id="TrialTerminationDate" size="32" class="datepick" value="<?php echo (!empty($trial->TrialTerminationDate)) ? $trial->TrialTerminationDate : date('Y-m-d', time()) . ' 00:00:00'; ?>" /></td>
                        <?php } ?>
                    </tr>
                <?php } else
{ ?>
                    <tr>
                        <td></td>
                        <?php if ($_GET['action'] == 'close')
    { ?>
                        <td><input type="hidden" name="trial[TrialCloseDate]" size="32" class="" value="" /></td>
                        <?php } elseif ($_GET['action'] == 'terminate')
    { ?>
                        <td><input type="hidden" name="trial[TrialTerminationDate]" size="32" class="" value="" /></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                    <tr>
                        <td class="center extra-padding" class="center" colspan="2"><textarea name="comment" class="confirm-comment">Comment default here...</textarea></td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$('#confirm').submit();">&emsp;<?php echo $prefix . ucfirst($_GET['action']); ?> Trial&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;Cancel&emsp;</a></td>
                    </tr>
                </table>
            </div>
        </form>
	</body>
</html>
<?php

