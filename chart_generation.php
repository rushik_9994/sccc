<?php
    error_reporting(0);

/**
 * Chart generation page
 *
 * Uses: chosen plugin  https://github.com/harvesthq/chosen/releases/tag/v1.1.0
 */
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

 $connection = new DataBaseMysql();

 // print_r($connection);
 // exit();
//load required classes
include_once(ROOT_PATH.'/reportCommonFunctions.php');
include_once(ROOT_PATH.'/ChartReport.php');
require_once(ROOT_PATH.'/reportDialogFunctions.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Comp.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/TrialType.class.php');
require_once(ROOT_PATH.'/lib/TrialCategory.class.php');
require_once(ROOT_PATH.'/lib/Doctor.class.php');
require_once(ROOT_PATH.'/lib/Patient.class.php');
require_once(ROOT_PATH.'/lib/PatientIdentifier.class.php');


//define required variables
$msg = '';
$reportStartDate = $cfg['fiscalyear.startdate'];     //Beginning of this fiscal year
$reportEndDate   = date('Y-m-d');   

$chart_data = $_SESSION['chart_data'];

$post_Data = $chart_data['post_Data'];
$chart_generate_data = $chart_data['chart_data'];

echo"<pre>";
print_r($chart_data);
exit;
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Report Generation
		</title>
        <style type="text/css" >
          .errorMsg{border:1px solid red; }
          .message{color: red; font-weight:bold; }
        </style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
        <script src="js/jquery.jdpicker.js"></script>
        <script src="js/chosen.jquery.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

        <script src="chart_css_js/code/highcharts.js"></script>
        <script src="chart_css_js/code/modules/series-label.js"></script>
        <script src="chart_css_js/code/modules/exporting.js"></script>
        <script src="chart_css_js/code/modules/export-data.js"></script>
        <script src="chart_css_js/code/modules/accessibility.js"></script>

		<script type="text/javascript">
        	$(document).ready(function(){
        		//$(".ui-tabs").tabs();
                $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', autoSize: true, changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});

                $('.chosen-select').chosen();
        	});
		</script>
		<link rel="stylesheet" href="css/chosen.css">
        <link rel="stylesheet" href="js/jdpicker.css" type="text/css" media="screen" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body id="main">
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('chart_reports');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<?= $post_Data['titleBlock'] ?>

                    <?php
                        if (!count($chart_generate_data)) {
                            echo "<h4 class='text-center'>No chart generated due lack of data</h4";
                        }
                    ?>
				</div>
			</div>
		</div>
	</body>
</html>