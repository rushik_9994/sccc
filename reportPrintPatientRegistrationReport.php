<?php
/**
 * Print the 'Doctor Accruals By Site Report'
 */

include_once('reportCommonFunctions.php');
include_once('reportQueryBuilder.php');
require_once ('lib/Comp.class.php');
require_once ('lib/ReportRowFetcher.class.php');


session_start();
if (empty($_SESSION['UserKeyID'])) { header('location: login.php'); exit; }

$options = array(); //array('display_hidden'=>1); to also display columns with _ keys

//Start our session, extract POST and GET from SESSION
list($connection, $reportInfo) = commonStartNewReport('Patient Registration Report');

//Just run the print-start function so $reportInfo elements get created...
ob_start();
printLibraryGeneratePrintout($connection, $reportInfo, $options, 'patientRegistrationReportCallback');
ob_end_clean();

$reportInfo['groupByField']  = 'x_IRB';
$reportInfo['sliceSize']     = 5000;
$reportInfo['from']          = 0;
$reportInfo['rowcountText']  = "Number Of Patients";
$reportInfo['rowcountField'] = 'Protocol';

$fetcher = new ReportRowFetcher($connection, $reportInfo);

//Define the summation columns (also automatically initializes the 'subsec' array)
$reportInfo['report'] = array();
$reportInfo['report']['Protocol']   = 0;      //Total
$reportInfo['report']['Credits']    = 0;      //Total



while(!$fetcher->done($reportInfo)) {
    list($xrow, $row) = $fetcher->getNextRow($reportInfo);
    //print "<pre>"; print_r($xrow); print "</pre>";
    
    //REPORT START
    if ($xrow['x_isStart']) {
        //<html>....<body>
        prtReportStart($reportInfo, $xrow, $row, $options);
    }
    
    //SITE START
    if ($xrow['x_firstInGroup']) {
        prtSiteStart($reportInfo, $xrow, $row, array('dispField'=>'x_IRB'), $options);
    }
    
    //SITE ROW
    prtSiteRow($reportInfo, $xrow, $row, $options);
    
    //SITE END
    if ($xrow['x_lastInGroup']) {
        //--------- SITE FOOTER (and once the REPORT FOOTER)
        prtSiteEnd($reportInfo, $xrow, $row, array('replaceColumn'=>'IRB_Code'), $options);
        //--------- SITE FOOTER (and once the REPORT FOOTER)   
    }

    //REPORT END
    if ($xrow['x_isEnd']) {
        //</body>...
        prtReportEnd($reportInfo, $xrow, $row, $options);
    }

}//while !$done



function patientRegistrationReportCallback($what, $connection, $reportInfo, $cbInfo) {
    //('rowcell', $key, $val, $cellcode);
    //$tdattribs = '';
    //if ($type=='rowcell') {
    //    if ($key=='Protocol') {
    //        //$tdattribs = ' width="30%" style="color:red"';
    //    }
    //}
    //$cellcode = "            <td $tdattribs>".htmlentities($val)."&nbsp;&nbsp;</td>\n";

    $cellcode  = '';

    return $cellcode;
}




