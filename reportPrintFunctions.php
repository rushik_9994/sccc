<?php
/**
 * Module that actually performs a printout
 *
 */

include_once('cfg/config.php');
include_once('reportCommonFunctions.php');


session_start();
if (empty($_SESSION['UserKeyID']))  {
    header('location: login.php'); exit;
}

/**
 * Generates reports in print format
 */
function printLibraryGeneratePrintout($connection, &$reportInfo, $callbackFunction='', $options=array()) {
    $from        = 0;
    $size        = 5000;
    $isFirst     = 1;
    $cont        = '';
    $absRowIndex = 0;
    $lastRow     = array();
    $lastXrow    = array();

    $reportInfo['debugMode']    = 0;
    $reportInfo['from']         = 0;
	$reportInfo['haveMoreRows'] = 1;
	$reportInfo['isOddRow']     = 0;
	$reportInfo['sliceNumber']  = 0;
	
	if (($callbackFunction) && (is_callable($callbackFunction))) {
	    $reportInfo['reportCallbackFunction'] = $callbackFunction;
	} else {
	    $reportInfo['reportCallbackFunction'] = '';
    }

    $abort = 0;
    while($error=='') {

        //Load a slice
        list($rows, $xrows, $haveMoreRows, $from, $error) = queryLibraryGetReportRowSlice($connection, $reportInfo, $from, $size);


        //Make callback
        $cbInfo       = array('rows'=>$rows, 'xrows'=>$xrows, 'haveMoreRows'=>$haveMoreRows, 
                              'from'=>$from, 'error'=>$error, 'sliceNumber'=>$sliceNumber, 'isFirst'=>$isFirst);
        $cbInfo       = printLibraryMakeCallback('after_loading_slize', $connection, $reportInfo, $cbInfo, $options);
        $rows         = $cbInfo['rows'];
        $xrows        = $cbInfo['xrows'];
        $haveMoreRows = $cbInfo['haveMoreRows'];
        $from         = $cbInfo['from'];
        $error        = $cbInfo['error'];
        $sliceNumber  = $cbInfo['sliceNumber'];
        $isFirst      = $cbInfo['isFirst'];
        
        $reportInfo['sliceNumber']++;


        //Simulate more rows...
        //$totalCount = 0;
        //$simRows = array();
        //for ($j=1; $j<30; $j++) {
        //    for ($i=0,$maxi=count($rows);$i < $maxi; $i++) {
        //        $tmp = array(); //array('totalCount'=>$totalCount);
        //        foreach($rows[$i] as $key=>$val) {
        //            $tmp[$key] = $val;
        //        }
        //        reset($rows[$i]);
        //        $simRows[] = $tmp;
        //        $totalCount++;
        //    }
        //}
        //$rows = $simRows;

        //If we have an error or have no rows (in the first slice) then exit the loop
        if ($error) { printLibraryDieWithError($error); }
        if (count($rows)==0) {
            if ($isFirst) { printLibraryDieWithError('No data rows found for this report'); } else { break; }
        }

        //Start the export (uses the first row for titles)
        if (($isFirst) && (count($rows) > 0)) {
            list($error, $rows[0], $xrows[0]) = printLibraryStartThePrintout($connection, $reportInfo, $rows[0], $xrows[0], $options);
            if ($error) { printLibraryDieWithError($error); }
            $absRowIndex++;
            $isFirst = 0;
        }

        //Export all rows of this slice of data
        for ($i=0,$maxi=count($rows); $i < $maxi; $i++) {
            list($ignore, $abort, $absRowIndex, $rows[$i], $xrows[$i]) = 
                printLibraryRowPreProcessing($connection, $reportInfo, $absRowIndex, $rows[$i], $xrows[$i], $options);
            if ($ignore) { continue; }
            if ($abort)  { break; }

            $error = printLibraryAddOnePrintoutRow($connection, $reportInfo, $absRowIndex, $rows[$i], $xrows[$i], $options);
            $lastRow  = $rows[$i];
            $lastXrow = $xrows[$i];
            $absRowIndex++;
        }

        //Do we have more data? If so then load another slice
        if ($abort)         { break; }
        if (!$haveMoreRows) { break; }

    }//next slice

    if ($absRowIndex > 0) {
       $error = printLibraryEndThePrintout($connection, $reportInfo, $absRowIndex, $lastRow, $lastXrow, $options);
       if ($error) { printLibraryDieWithError($error); }
    }

}


function prtReportStart(&$reportInfo, $xrow=array(), $row=array(), $options=array()) {
    print "<html>\n".
          "    <head>\n".
          "    <title>".htmlentities($reportInfo['reportTitle'])."</title>\n".
          "    <link rel=\"stylesheet\" href=\"css/style.css?t=".time()."\" />\n".
          "    <link rel=\"stylesheet\" href=\"css/print.css?t=".time()."\" />\n".
          "    </head>\n".
          "<body>\n".
          //str_replace('print.css', 'style.css', $reportInfo['reportHtmlHeader']).
          $reportInfo['completeTitle']."<hr><br /><br /><br />\n";
}

function prtSiteStart(&$reportInfo, $xrow=array(), $row=array(), $info=array(), $options=array()) {
    if (!$xrow['x_isStart']) {
        print "<div class=\"page-break\">&nbsp;</div>\n";
    }
    if (!isset($reportInfo['report'])) { $reportInfo['report'] = array(); }
    $reportInfo['subsec'] = array();
    foreach($reportInfo['report'] as $key=>$ignore) {
        $reportInfo['subsec'][$key] = 0;      //Site
    }
    reset($reportInfo['report']);

    print "<!-- SITE START -->\n".
          "<table class=\"fullwidth zebra sort\">\n".
          "    <!-- SITE HEADER START -->\n".
          "    <thead>\n".
          "        <tr>\n".
          "            <td colspan=\"".$reportInfo['columnCount']."\">\n".
          "                <h4>\n".
          "                    ".str_replace('|', '<br />', htmlentities($xrow[$info['dispField']])).
//                               "&nbsp;&nbsp;&nbsp;&nbsp;\n".
          "                    <small>".$reportInfo['reportHeaderTitleDates']."</small>\n".
          "                </h4>\n".
          "            </td>\n".
          "        </tr>\n".
                   $reportInfo['reportHeaderRow'].
          "        <tr><td colspan=\"".$reportInfo['columnCount']."\"><hr></td></tr>\n".
          "    </thead>\n".
          "    <!-- SITE HEADER END -->\n".
          "    <tbody>\n";
}

function prtSiteRow(&$reportInfo, $xrow, $row, $options=array()) {
    if (!isset($reportInfo['report'])) { $reportInfo['report'] = array(); }
    if (!isset($reportInfo['subsec'])) { $reportInfo['subsec'] = array(); }
    foreach($reportInfo['report'] as $key=>$ignore) {
        //Is this a rowcount colum?
        if ((isset($reportInfo['rowcountField'])) && ($reportInfo['rowcountField']==$key)) {
            //Calculate row counts
            $reportInfo['report'][$key] += 1;               //Total (rowcount)
            $reportInfo['subsec'][$key] += 1;               //Site  (rowcount)
        } else {
            //Calculate totals
            $reportInfo['report'][$key] += $row[$key];      //Total (fieldsum)   REPORT-TOTALS
            $reportInfo['subsec'][$key] += $row[$key];      //Site  (fieldsum)   SECTION-TOTALS
            
            //Are we supposed to round and pad with trailing zeroes?
            if ((isset($reportInfo['formatting'][$key])) && (is_array($reportInfo['formatting'][$key]))) {
                $columnFormatting = $reportInfo['formatting'][$key];
                if (isset($columnFormatting['totals.rounding'])) {
                    $rounding = $columnFormatting['totals.rounding'];
                    if ((is_numeric($rounding)) && ($rounding >= 0)) {
                        $reportInfo['subsec'][$key] = number_format(round($reportInfo['subsec'][$key], $rounding), $rounding, '.', '');
                        $reportInfo['report'][$key] = number_format(round($reportInfo['report'][$key], $rounding), $rounding, '.', '');
                    }
                }
            }
        }
    }
    reset($reportInfo['report']);

    print "        <tr>\n";

    $displayHidden = ((isset($options['display_hidden'])) && ($options['display_hidden'])) ? 1 : 0;
    if ($displayHidden) { $row = $xrow; }

    foreach($row as $key=>$val) {
        
        list ($hide, $val, $tdAttribs, $postFix) = determineCellDetails('row', $reportInfo, $xrow, $row, $key, $val, $options);

        if (!$hide) {
            $cellcode = "            <td $tdAttribs>".htmlentities($val)."$postFix</td>\n";
            if (isset($reportInfo['callback'])) {
                $cellcode = $reportInfo['callback']('rowcell', $key, $val, $cellcode);
            }
            print $cellcode;
        }
    }
    reset($row);
    print "        </tr>\n";
}

function prtSiteEnd(&$reportInfo, $xrow=array(), $row=array(), $info=array(), $options=array()) {
    //Determine our summation columns
    $sumCols = array();
    if (!isset($reportInfo['report'])) { $reportInfo['report'] = array(); }
    if (!isset($reportInfo['subsec'])) { $reportInfo['subsec'] = array(); }
    foreach($reportInfo['subsec'] as $key=>$ignore) {
        $sumCols[$key] = 1;
    }
    reset($sumCols);

    //Site-Footer thin line
    if (count($sumCols) > 0) {
        print "        <!-- SITE FOOTER START -->\n".
              "        <tr>".
                           "<td colspan=\"".$reportInfo['columnCount']."\" class=\"thinline\">".
                               "<hr class=\"thinline\"/>".
                           "</td>".
                      "</tr>\n";
        //Site-Footer headers repeated
        print "        <tr>\n";

        $displayHidden = ((isset($options['display_hidden'])) && ($options['display_hidden'])) ? 1 : 0;
        if ($displayHidden) { $row = $xrow; }

        foreach($row as $key=>$val) {
            if ((isset($reportInfo['rowcountField'])) && 
                ($reportInfo['rowcountField']==$key) &&
                (isset($reportInfo['rowcountText']))) {
                print "            <td class=\"subtotals\">".htmlentities($reportInfo['rowcountText'])."</td>\n";    
            } else {
                print "            <td class=\"subtotals\">".htmlentities($key)."</td>\n";
            }
        }
        reset($row);
        print "        </tr>\n";
    
        //Footer Accrual-Totals
        print "        <tr>\n";

        $displayHidden = ((isset($options['display_hidden'])) && ($options['display_hidden'])) ? 1 : 0;
        if ($displayHidden) { $row = $xrow; }

        foreach($row as $key=>$val) {
            if ($key==$info['replaceColumn']) {
                if (isset($reportInfo['replaceColumnWithTitle'])) {
                    print "            <td class=\"subtotals\">".htmlentities($reportInfo['replaceColumnWithTitle'])."</td>\n";
                } else {
                    print "            <td class=\"subtotals\">SITE-TOTALS</td>\n";
                }
            } else if (isset($reportInfo['subsec'][$key])) {
                print "            <td class=\"subtotals\"><b>".$reportInfo['subsec'][$key]."</b></td>\n";
            } else {
                print "            <td class=\"subtotals\">&nbsp;</td>\n";
            }        
        }
        print "        </tr>\n".
              "        <!-- SITE FOOTER END -->\n";
    
        //--------- REPORT FOOTER (once at bottom)
        if ($xrow['x_isEnd']) {
            prtReportTotals($reportInfo, $xrow, $row, $info, $options);
        }
        //--------- REPORT FOOTER (once at bottom)
    }

    //SITE END
    print "    </tbody>\n".
          "</table>\n".
          "<br /><br /><br /><br /><br />\n".
          "<!-- SITE END -->\n";
}

function prtReportTotals(&$reportInfo, $xrow, $row, $info=array(), $options=array()) {
    if (!isset($reportInfo['report'])) { $reportInfo['report'] = array(); }
    if (count($reportInfo['report']) > 0) {
        print "        <!-- REPORT FOOTER START -->\n".
              "        <tr><td colspan=\"".$reportInfo['columnCount']."\" class=\"nobgcolor\">\n".
              "            &nbsp;<br /><br /><br />\n".
              "        </td></tr>\n";
        print "        <tr>\n";

        $displayHidden = ((isset($options['display_hidden'])) && ($options['display_hidden'])) ? 1 : 0;
        if ($displayHidden) { $row = $xrow; }

        foreach($row as $key=>$val) {
            if ($key==$info['replaceColumn']) {
                print "            <td class=\"nobgcolor\"><b>REPORT-TOTALS</b></td>\n";
            } else if (isset($reportInfo['report'][$key])) {
                print "            <td class=\"nobgcolor\"><b>".$reportInfo['report'][$key]."</b></td>\n";
            } else {
                print "            <td class=\"nobgcolor\">&nbsp;</td>\n";
            }        
        }   
        print "        </tr>\n".
              "        <!-- REPORT FOOTER END -->\n";
    }
}


function prtReportEnd($reportInfo, $xrow, $row, $options=array()) {
    print "</body>\n".
          "</html>\n";
}

/**
 * This determines the contents and style of print report cells
 *
 * @param string $callType
 * @param array  $reportInfo (hashref)
 * @param array  $xrow
 * @param array  $row
 * @param string $key
 * @param string $val
 * @param array  $options optional
 */
function determineCellDetails($callType, &$reportInfo, $xrow, $row, $key, $val, $options=array()) {
    global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;

    //print "<pre>"; print_r($reportInfo); print "</pre>"; exit;
    $reportName = str_replace('_report', '', $reportInfo['reportPrefix']);
    $reportName = str_replace('patient_registration', 'patient', $reportName);
    $reportName = str_replace('doctors_accruals', 'doctor', $reportName);
    $reportName = str_replace('doctor_by_site', 'docbysite', $reportName);

    $format     = $reportInfo['format'];
    
    $ourKey     = str_replace('__Remove', '', $key);
    $ourKey     = str_replace('__', ' ', $ourKey);

    $cfgKey     = "$format|$reportName|$ourKey";

    $cellCfg     = (isset($cfg['reportDetails'][$cfgKey])) ? $cfg['reportDetails'][$cfgKey] : '';
    if (!is_array($cellCfg)) {
        $cellCfg = array('bodyAttr' => $cellCfg);
    }
    $maxLen      = ((isset($cellCfg['maxLen'])) && (is_numeric($cellCfg['maxLen']))) ? $cellCfg['maxLen'] : 0;

    $hide = ((isset($cellCfg['hide'])) && ($cellCfg['hide'])) ? 1 : 0;

    if ($callType == 'title') {
        //TITLE CELL
        $headAttr    = (isset($cellCfg['headAttr'])) ? $cellCfg['headAttr'] : '';
        if ((!$headAttr) && (isset($cellCfg['attr']))) {
            $headAttr = $cellCfg['attr'];
        }
        $addDefaults = ((isset($cellCfg['addDefaults'])) && (!$cellCfg['addDefaults'])) ? 0 : 1;
        $tdAttribs   = $headAttr;
        if ($addDefaults) {
            $tdAttribs   = " class=\"left header\" align=\"left\" nowrap=\"nowrap\" $tdAttribs";
        }
        $postFix     = '&nbsp;&nbsp;&nbsp;';
        //$val = $val . " |$hide-----$cfgKey------".json_encode($cellCfg);
    } else {
        //BODY CELL
        $bodyAttr    = (isset($cellCfg['bodyAttr'])) ? $cellCfg['bodyAttr'] : '';
        if ((!$bodyAttr) && (isset($cellCfg['attr']))) {
            $bodyAttr = $cellCfg['attr'];
        }
        $addDefaults = ((isset($cellCfg['addDefaults'])) && (!$cellCfg['addDefaults'])) ? 0 : 1;
        $tdAttribs   = $bodyAttr;
        if ($addDefaults) {
            $tdAttribs   = $tdAttribs." nowrap='nowrap'";
        }

        //Note: A bit of a hack.  We have | translated into <br /> in the DoctorAccrualsBySite report
        //      but in the regular data-rows we need to remove those | again to make the cells look better
        $val = str_replace('|', ' ', $val);
        if (($maxLen) && (strlen($val) > $maxLen)) {
            $val = substr($val, 0, $maxLen).'...';
        }
        //$val = $val . " |$hide-----$cfgKey-------".json_encode($cellCfg);

        $postFix = '&nbsp;&nbsp;';
    }

    return array($hide, $val, $tdAttribs, $postFix);
}


/**
 * Start a printout
 */
function printLibraryStartThePrintout($connection, &$reportInfo, $firstRow=array(), $firstXrow=array(), $options=array()) {
    $error  = '';

    $report    = $reportInfo['report'];
    $format    = $reportInfo['format'];
    $cont      = '';

    $printoutTitle = (isset($reportInfo['printoutTitle'])) ? $reportInfo['printoutTitle'] : $reportInfo['reportTitle'];
    $printoutTitle = htmlentities($printoutTitle);
    $printoutTitle = str_replace(' ', '&nbsp;', $printoutTitle);

    //htmlStart
    $cont  = "";
    $reportInfo['reportHtmlHeader']    = "<html>\n".
                                         "    <head>\n".
                                         "    <title>".htmlentities($reportInfo['reportTitle'])."</title>\n".
                                         "    <link rel=\"stylesheet\" href=\"css/print.css?t=".time()."\" />\n".
                                         "</head>\n".
                                         "<body>\n";
    $reportInfo['reportHeaderTitleRaw'] = $printoutTitle;                                   
    $reportInfo['reportHeaderTitle']    = "    <h3>".$printoutTitle."</h3>\n";
                                          
    $cont .= $reportInfo['reportHtmlHeader'];
    $cont .= $reportInfo['reportHeaderTitle'];
    
    $cont .= "<hr>\n".
             "<div class=\"msg\"></div>\n".
             "<table class=\"fullwidth zebra sort\">\n".
             "    <thead>\n";

    #Printout title row
    $reportInfo['reportHeaderRow'] =      "        <tr class=\"left\">\n";
    $titleRow    = array_keys($firstRow);
    $reportInfo['columnCount'] = 0;

    $displayHidden = ((isset($options['display_hidden'])) && ($options['display_hidden'])) ? 1 : 0;
    if ($displayHidden) { $row = $xrow; }

    foreach($titleRow as $ignore => $title) {
        $key = $title;

        if ($format=='print') {
            //Printreport  'Some__Word' -> 'Some Word'
            $title = htmlentities($title);
            $title = str_replace('__Remove', '', $title);
            $title = str_replace('__', '&nbsp;', $title);
        } else {
            //Excel        'Some__Word' -> 'SomeWord'
            $title = str_replace('__Remove', '', $title);
            $title = str_replace('__', '', $title);
        }

        list ($hide, $title, $tdAttribs, $postFix) = determineCellDetails('title', $reportInfo, $xrow, $titleRow, $key, $title, $options);

        if (!$hide) {
            $reportInfo['reportHeaderRow'] .= "            <th $tdAttribs>".$title."$postFix</th>\n";
            $reportInfo['columnCount']++;
        }
    }                
    $reportInfo['reportHeaderRow'] .=     "        </tr>\n";
    $cont .= $reportInfo['reportHeaderRow'].
             "    </thead>\n";
             "    <tbody>\n";


    //Make callback
    $cbInfo = array('firstRow'=>$firstRow, 'firstXrow'=>$firstXrow, 'cont'=>$cont);
    $cbInfo = printLibraryMakeCallback('start_the_printout', $connection, $reportInfo, $cbInfo, $options);
    $cont   = $cbInfo['cont'];

    print $cont;

    //Non-printables
    $reportInfo['reportHtmlEnd']          = "</body>\n".
                                            "</html>";

    $reportInfo['reportHeaderTitleDates'] = 'from '.$_POST['ReportStartDate'].
                                            '   '.
                                            'to '.  $_POST['ReportEndDate'];

    $reportInfo['completeTitleRaw']       = $reportInfo['reportHeaderTitleRaw'] .
                                            '     '.
                                            $reportInfo['reportHeaderTitleDates'];

    $reportInfo['completeTitle']          = "<h3>".$reportInfo['completeTitleRaw']."</h3>";

    return array($error, $firstRow, $firstXrow);
}



function printLibraryRowPreProcessing($connection, &$reportInfo, $absRowIndex, $row, $xrow, $options=array()) {
    $error  = '';
    $ignore = 0;
    $abort  = 0;

    $cbInfo       = array('row'=>$row, 'xrow'=>$xrow, 'ignore'=>$ignore, 'abort'=>$abort, 'absRowIndex'=>$absRowIndex, 'error'=>$error);
    $cbInfo       = printLibraryMakeCallback('row_preprocessing', $connection, $reportInfo, $cbInfo, $options);
    $row          = $cbInfo['row'];
    $xrow         = $cbInfo['xrow'];
    $ignore       = $cbInfo['ignore'];
    $abort        = $cbInfo['abort'];
    $absRowIndex  = $cbInfo['absRowIndex'];
    $error        = $cbInfo['error'];

    return array($ignore, $abort, $absRowIndex, $row, $xrow);
}


function printLibraryAddOnePrintoutRow($connection, &$reportInfo, &$absRowIndex, $row=array(), $xrow=array(), $options=array()) {
    $error = '';

    $report    = $reportInfo['report'];
    $format    = $reportInfo['format'];
    $debugMode = $reportInfo['debugMode'];
    $cont      = '';

    //Make callback BEFORE (preprocess the row or prepend content)
    $cbInfo       = array('row'=>$row, 'xrow'=>$xrow, 'cont'=>$cont, 'absRowIndex'=>$absRowIndex, 'error'=>$error);
    $cbInfo       = printLibraryMakeCallback('before_one_row', $connection, $reportInfo, $cbInfo);
    $cont         = $cbInfo['cont'];
    $absRowIndex  = $cbInfo['absRowIndex'];
    $error        = $cbInfo['error'];

    $cont .= "        <tr>\n";
    if ($reportInfo['isOddRow']) { $reportInfo['isOddRow']=0; } else { $reportInfo['isOddRow']=1; }
    $tdClass = ($reportInfo['isOddRow']) ?  'odd' : 'even';
    
    $displayHidden = ((isset($options['display_hidden'])) && ($options['display_hidden'])) ? 1 : 0;
    if ($displayHidden) { $row = $xrow; }

    foreach($row as $ignore=>$field) {
        $cont .= "            <td class=\"left $tdClass\" nowrap=\"nowrap\">".htmlentities($field)."&nbsp;</td>\n";
    }
    $cont .= "        </tr>\n";

    //Make callback AFTER (postprocess the row and/or generated content)
    $cbInfo       = array('row'=>$row, 'xrow'=>$xrow, 'cont'=>$cont, 'absRowIndex'=>$absRowIndex, 'error'=>$error);
    $cbInfo       = printLibraryMakeCallback('after_one_row', $connection, $reportInfo, $cbInfo);
    $cont         = $cbInfo['cont'];
    $absRowIndex  = $cbInfo['absRowIndex'];
    $error        = $cbInfo['error'];
    
    print $cont;

    return $error;
}


function printLibraryEndThePrintout($connection, &$reportInfo, &$absRowIndex, $lastRow=array(), $lastXrow=array(), $options=array()) {
    $error = '';

    $report    = $reportInfo['report'];
    $format    = $reportInfo['format'];
    $debugMode = $reportInfo['debugMode'];
    $cont      = '';


    //Make callback
    $cbInfo       = array('cont'=>$cont, 'absRowIndex'=>$absRowIndex, 'lastRow'=>$lastRow, 'lastXrow'=>$lastXrow, 'error'=>$error);
    $cbInfo       = printLibraryMakeCallback('end_the_printout', $connection, $reportInfo, $cbInfo);
    $cont         = $cbInfo['cont'];
    $absRowIndex  = $cbInfo['absRowIndex'];
    $lastRow      = $cbInfo['lastRow'];
    $error        = $cbInfo['error'];

    $cont .= "    </tbody>\n".
             "</table>\n";
    
    print $cont;

    return $error;
}


function printLibraryMakeCallback($what, $connection, &$reportInfo, $cbInfo, $options=array()) {
    if ($reportInfo['reportCallbackFunction']) {
        list($reportInfo, $cbInfo) = call_user_func($reportInfo['reportCallbackFunction'], $what, $connection, $reportInfo, $cbInfo);
    }

    return $cbInfo;
}



/**
 * Dies displaying an error
 *
 * Note: Function does NOT return. Exit is called.
 *
 * @param string $error
 */
function printLibraryDieWithError($error) {
    print "ERROR: $error<br />";
    exit;
}


