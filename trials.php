<?php
error_reporting(0);
/**
 * Patient management page
 */

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/TrialCategory.class.php');
require_once(ROOT_PATH.'/lib/TrialType.class.php');
//require_once(ROOT_PATH.'/lib/TrialDiseaseType.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_protocol_0';
    $_REQUEST['order_direction_0'] = 'asc';
}

//create required objects
$trials             = new Trial;
$trialCategory      = new TrialCategory;
$trialType          = new TrialType;
//$trialDiseaseType = new TrialDiseaseType;

//define required variables
$msg = '';

//Check permissions
$canEditRows = checkPermissionString('AddEditTrials');
$canViewRows = checkPermissionString('ViewTrials');

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'trials.pagination.size');

//searchFilter setup
$sFilter  = new SearchFilter('trials');
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, 0, $cfg);

$TrialKeyIDs   = $trials->GetKeysOrderBy('TrialKeyID', 'ASC', $pgInfo[0]['from'], $pgInfo[0]['limit'], $sClauses);  //pagination added
$totalRowCount = $trials->totalRowCount;

//begin building output
$indexText   = '_0';
$trials_list = //"<a name='tb_0'></a>\n".
               "<table class=\"fullwidth zebra sort\">\n".
               "<thead>\n".
               "    <tr class='thead_unselected'>\n".
               $pg->generateTheadLine('order_protocol'.$indexText,      'Protocol', array('isdefault'=>1)).
               $pg->generateTheadLine('order_trial_category'.$indexText,'Trial Category').
               $pg->generateTheadLine('order_trial_type'.$indexText,    'Trial Type').
               $pg->generateTheadLine('order_trial_cirb'.$indexText,    'CIRB').
               $pg->generateTheadLine('order_open_date'.$indexText,     'Open Date').
               $pg->generateTheadLine('order_close_date'.$indexText,    'Close Date').
               $pg->generateTheadLine('order_term_date'.$indexText,     'Term Date').
               $pg->generateTheadLine('order_suspended'.$indexText,     'Suspended', array('extra'=>"style=\"max-width: 7em;\""));
if (($canViewRows) || ($canEditRows)) {
    $trials_list.= $pg->generateTheadLine('order_action'.$indexText, '&nbsp;&nbsp;&nbsp;Action', array('right'=>1, 'nosort'=>1));
} else {
    $trials_list.= $pg->generateTheadLine('order_blank'.$indexText, '&nbsp;', array('nosort'=>1));
}
$trials_list.= "    </tr>\n".
               "</thead>\n".
               "<tbody>\n";

foreach ($TrialKeyIDs as $key) {
    $trials->Load_from_key($key);

    $trialTypeText = '';
    if (isset($trials->TrialTypeKeyID)) {
        $trialType->Load_from_key($trials->TrialTypeKeyID);
        if (isset($trialType->TrialTypeText)) {
            $trialTypeText = $trialType->TrialTypeText;
        }
    }

    $trialCategoryText = '';
    if ((isset($trials->TrialCategoryKeyID)) && ($trials->TrialCategoryKeyID)){
        $trialCategory->Load_from_key($trials->TrialCategoryKeyID);
        if (isset($trialCategory->TrialCategoryText)) {
            $trialCategoryText = $trialCategory->TrialCategoryText;
        }
    }

    /*
    $trialDiseaseTypeName = '';
    if (isset($trials->TrialTypeKeyID)) {
        $trialDiseaseType->Load_from_key($trials->TrialDiseaseTypeKeyID);
        if (isset($trialDiseaseType->TrialDiseaseTypeName)) {
            $trialDiseaseTypeName = $trialDiseaseType->TrialDiseaseTypeName;
        }
    }*/

    $cirb = ((isset($trials->TrialCIRB)) && ($trials->TrialCIRB=='1')) ? 'TRUE' : 'FALSE';

    //Grey out closed trials
    $escStyle = '';
    $trialIsClosed = (trim(str_replace('0000-00-00', '', $trials->TrialCloseDate)) == '') ? 0 : 1;
    if ((isset($cfg['styleForClosedTrials'])) && ($cfg['styleForClosedTrials'])) {
        $escStyle = ($trialIsClosed) ? " style=\"".$cfg['styleForClosedTrials']."\"" : '';
    }

    $trials_list .= 
        "<tr>\n".
        "    <td class=\"left\"$escStyle>" . $trials->TrialProtocolNumber . "</td>\n".
        "    <td class=\"left\"$escStyle>" . htmlentities($trialCategoryText) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . htmlentities($trialTypeText) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . htmlentities($cirb) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . trim(str_replace('0000-00-00', '', $trials->TrialOpenDate)) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . trim(str_replace('0000-00-00', '', $trials->TrialCloseDate)) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . trim(str_replace('0000-00-00', '', $trials->TrialTerminationDate)) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . ($trials->TrialSuspend=='0'?'No':'Yes') . "</td>\n";
    if ($canEditRows) {
        $trials_list .=
            "    <td class=\"right\"><a href=\"trialViewEdit.php?id=" . urlencode($key) . "\" class=\"fancy\">View/Edit</a></td>\n";
    } elseif ($canViewRows) {
        $trials_list .= 
            "    <td class=\"right\"><a href=\"trialViewEdit.php?id=" . urlencode($key) . "\" class=\"fancy\">View</a></td>\n";
    } else {
        $trials_list .= 
            "    <td class=\"center\">&nbsp;</td></tr>\n";
    }
    $trials_list .= 
        "</tr>\n";
}
$trials_list .= '</tbody>' . "\n";
$trials_list .= '</table>' . "\n";

//add pagination display
$trials_list .= $pg->generatePaginationCode($pgInfo, 0, $trials, $totalRowCount);


?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Trials
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
		<script type="text/javascript">
        	$(document).ready(function(){
        	});
		</script>
	</head>
	<body>
	    <form name='main_form' id='main_form' action='trials.php' method='POST'>
<?php
        //pagination variables
        print $pg->generateFormVariables($pgInfo);
?>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                    print displayTopRightInfo();
                    print displayTabs('trials');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
                    <div class="subhead">
						Trials
<?php                   if ($canEditRows) { ?>
                            <a id="main-button" href="trialAddEdit.php" class="button">Add Trial</a>
<?php                   }
                        displaySuccessAndErrors();
?>
                    </div>
                        <hr />
<?php
                        print $sFilter->generateSearchFilterCode(0);
?>
                        <div class="msg"><?php echo $msg; ?></div>
                        <?php echo $trials_list; ?>
				</div>
			</div>
		</div>
	    </form>
	</body>
</html>
<?php

