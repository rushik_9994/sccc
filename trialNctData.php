<?php
/**
 * Fetch one of the clinicaltrials.gov pages as an xml-file and display it in various formats
 *
 * https://www.morpheusrising.com/sccc/trialNctData.php?trialnct=NCT00000102&format=flatjson
 * https://www.morpheusrising.com/sccc/trialNctData.php?trialnct=NCT00000102&format=flat
 *
 * https://www.morpheusrising.com/sccc/trialNctData.php?trialnct=NCT00000102&format=json
 * https://www.morpheusrising.com/sccc/trialNctData.php?trialnct=NCT00000102&format=xml
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
//list($cfg, $errors, $warnings, $success) = startRequest();

$result = false;
$error  = '';
$array  = array();

$trialNct = ((isset($_REQUEST['trialnct'])) && ($_REQUEST['trialnct'])) ? $_REQUEST['trialnct'] : '';
$format   = ((isset($_REQUEST['format'])) && ($_REQUEST['format'])) ? $_REQUEST['format'] : 'flat';
//print "<br />trialNct='$trialNct' format='$format'"; exit;

$cont = '';
if ($trialNct) {
    $cont = file_get_contents('https://clinicaltrials.gov/ct2/show/' . urlencode($trialNct) . '?displayxml=true');

    if ($cont) {
        $xml   = simplexml_load_string($cont, "SimpleXMLElement", LIBXML_NOCDATA);
        $json  = json_encode($xml);
        $array = json_decode($json,true);
    } else {
        $result = false;
        $error  = 'Could not load raw NTC data. Wrong trialnct?';
    }
} else {
    $result = false;
    $error  = 'trialnct parameter missing';
}


if ($error) {
    $array = array('error' => $error);
    $cont  = "<error>".htmlentities($error)."</error>";
}


if ($format == 'xml') {
    header("Content-Type:text/xml");
    print $cont;
} else if (($format == 'flat') || ($format == 'htmlflat') || ($format == 'flathtml')) {
    $array = flattenAssoc($array);
    print "<table width='100%' border='1'>";
    foreach ($array as $key => $val) {
        print
            "<tr>\n" .
            "<td style='background-color:#DDDDDD;' width='25%' valign='top'>" . htmlentities($key) . "</td>\n" .
            "<td style='background-color:#EEEEEE;' >" . htmlentities($val) . "</td></tr>";
    }
    print "</table>";
} else if ($format == 'json') {
    header("Content-type:application/json");
    print json_encode($array, true);
} else if (($format == 'flatjson') || (($format == 'jsonflat'))) {
    $array = flattenAssoc($array);
    header("Content-type:application/json");
    print json_encode($array, true);
}

