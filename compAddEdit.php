<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Permissions.class.php');
require_once ('lib/Comp.class.php');
require_once ('lib/Site.class.php');
require_once ('lib/States.class.php');
require_once ('lib/Comments.class.php');
require_once ('lib/TrialCompJoin.class.php');
$pageURL = getPageUrl();

//create required objects
$permissions = new Permissions();
$comp        = new Comp();
$site        = new Site();
$comment     = new Comments();
$tcj         = new TrialCompJoin();
$states      = new States();

try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        if (($action == 'update') || ($action == 'insert')) {
            if (!empty($_GET['id'])) {
                $comp->Load_from_key($_GET['id']);
            }
            foreach ($_POST['comp'] as $key => $val) {
                $comp->$key = $val;
            }
            $comp->CompCRAPhone = preg_replace('/\D/', '', $comp->CompCRAPhone);
            $comp->CompCRAZip = preg_replace('/\D/', '', $comp->CompCRAZip);
            $comp->CompCRAState = $_POST['StatesKeyID'];
            if ($comp->CompStatus == 'on') {
                $comp->CompStatus = 1;
            } else {
                $comp->CompStatus = 0;
            }
            if ($action == 'update') {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'update component', $action, $_POST);
                $comp->Save_Active_Row();
                TrackChanges::endUserAction();
            } else {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'add component', $action, $_POST);
                $comp->Save_Active_Row_as_New();
                TrackChanges::endUserAction();
            }
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: compViewEdit.php?id=" . $comp->CompKeyID);
            }
        }

        if ($action == 'terminate') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'terminate component', $action, $_POST);
            $comp->Load_from_key($_POST['CompKeyID']);
            $comp->CompTermination = ($comp->CompTermination == 0) ? 1 : 0;
            $comp->CompTerminationDate = ($comp->CompTermination == 0) ? '' : $_POST['comp']['CompTerminationDate'];
            $comp->Save_Active_Row();
            $comment->Comment = $_POST['comment'];
            $comment->CompKeyID = $_POST['CompKeyID'];
            $comment->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }

        if ($action == 'delete') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete component', $action, $_POST);
            $comp->Delete_row_from_key($_GET['id']);
            //$comment->Delete_row_from_key('CompKeyID', $_GET['id']);
            $tcj_keys = $tcj->GetKeysWhereOrderBy('TrialCompKeyID', 'CompKeyID = \'' . $_GET['id'] . '\'', 'CompKeyID', 'ASC');
            if (count($tcj_keys) > 0) {
                foreach ($tcj_keys as $TrialCompKeyID) {
                    $tcj->Delete_row_from_key('TrialKeyID', $TrialCompKeyID);
                }
            }
            TrackChanges::endUserAction();

            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: components.php");
            }
        }

        //header("location: $pageURL");

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('Encountered '.errorCount().' errors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }//!empty action

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


//define required variables
$msg = '';
$permissions->Load_from_action("AddEditComponents");
$UserTypeIDs = explode(',', $permissions->UserTypeIDs);
if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
    header("location: login.php");
}
$permissions->Load_from_action("DeleteComponents");
$UserTypeIDs = explode(',', $permissions->UserTypeIDs);
if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
    header("location: login.php");
}

if (!empty($_GET['id'])) {
    //editing
    $comp->Load_from_key($_GET['id']);
}
$statesSelect = $states->CreateSelect($comp->CompCRAState);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
		<meta name="author" content="Cliff Garrett" />
		<title>
			Component Add/Edit
		</title>
		<script type="text/javascript" src="js/jquery-1.4.3.min.js"></script>
        <?php require_once ("lib/common.includes.php"); ?>
		<script type="text/javascript">
        	$(document).ready(function(){
        	   $("a.confirm-terminate").colorbox({inline:true, width:'650px'});
               $('select').addClass('fullwidth');
               $('#StatesKeyID').addClass('req');
        	   $("a.confirm-delete").colorbox({inline:true});
               <?php if (!empty($_GET['id']))
{ ?>
               $('#action').val('update');
               <?php } else
{ ?>
               $('#action').val('insert');
               <?php } ?>
               $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;'});
        	});
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                    <?php
                    print displayTopRightInfo();
                    print displayTabs('components');
                    ?>
    				<div id="tabs-1" class="ui-tabs-panel">
    					<form method="post" action="" name="mainform" id="mainform">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="continue" id="continue" />
                        <?php if ($_GET['id'])
{ ?>
                        <input type="hidden" name="comp[CompKeyID]" id="CompKeyID" value="<?php echo $_GET['id']; ?>" />
                        <?php } ?>
                    <div class="subhead">
    					Component Add/Edit
                            <a href="<?php echo (!empty($_GET['id'])) ? 'compViewEdit.php?id=' . $_GET['id'] : 'components.php'; ?>" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
<table border="0" class="fullwidth">
  <tr>
    <td class="right"><strong>Component Name:</strong> </td>
    <td class="right" colspan="4"><input class="fullwidth req" type="text" name="comp[CompName]" id="CompName" value="<?php echo (!empty($comp->CompName)) ? $comp->CompName : ''; ?>" /></td>
    <td><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div></td>
  </tr>
  <tr>
    <td class="right"><strong>CRA Name:</strong> </td>
    <td class="right" colspan="4"><input class="fullwidth req" type="text" name="comp[CompCRAName]" id="CompCode" value="<?php echo (!empty($comp->CompCRAName)) ? $comp->CompCRAName : ''; ?>" /></td>
    <td><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div></td>
  </tr>
  <tr>
    <td class="right"><strong>Address 1:</strong> </td>
    <td class="right" colspan="5"><input class="fullwidth req" type="text" name="comp[CompCRAAddress1]" id="CompAddress1" value="<?php echo (!empty($comp->CompCRAAddress1)) ? $comp->CompCRAAddress1 : ''; ?>" /></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>Address 2:</strong> </td>
    <td colspan="5"><input type="text" class="fullwidth" name="comp[CompCRAAddress2]" id="CompCRAAddress2" value="<?php echo (!empty($comp->CompCRAAddress2)) ? $comp->CompCRAAddress2 : ''; ?>" /></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>City:</strong> </td>
    <td><input class="fullwidth req" type="text" name="comp[CompCRACity]" id="CompCRACity" value="<?php echo (!empty($comp->CompCRACity)) ? $comp->CompCRACity : ''; ?>" /></td>
    <td class="right"><strong>State:</strong></td>
    <td><?php echo $statesSelect; ?></td>
    <td class="right"><strong>Zip:</strong> </td>
    <td><input class="fullwidth zip req" type="text" name="comp[CompCRAZip]" id="CompCRAZip" value="<?php echo (!empty($comp->CompCRAZip)) ? $comp->CompCRAZip : ''; ?>" /></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>Phone Number:</strong> </td>
    <td colspan="2"><input class="fullwidth phone req" type="text" name="comp[CompCRAPhone]" id="CompCRAPhone" value="<?php echo (!empty($comp->CompCRAPhone)) ? $comp->CompCRAPhone : ''; ?>" /></td>
    <td class="right"><strong>Email Address:</strong> </td>
    <td colspan="2"><input class="fullwidth req" type="text" name="comp[CompCRAEmail]" id="CompCRAEmail" value="<?php echo (!empty($comp->CompCRAEmail)) ? $comp->CompCRAEmail : ''; ?>" /></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><input name="comp[CompStatus]" type="checkbox" <?php echo (($comp->CompStatus == 1) || (empty($_GET['id']))) ? 'checked' : ''; ?>/> <strong>Active</strong></td>
    <td class="left"></td>
    <td><?php if (!empty($comp->CompKeyID))
{ ?><a href="#confirm-terminate" class="confirm-terminate"><input type="checkbox" id="compTermination" class="ays-ignore" <?php echo ($comp->CompTermination == 0) ? '' : 'checked'; ?> /></a>&ensp;<strong>Terminate Component</strong><?php } ?></td>
    <td></td>
    <td></td>
  </tr>
</table>
                        <div class="clear"></div>
                        <div class="right formdiv noborder">
                            <button type="button" class="inline" onmouseup="return validate('mainform'); $('#mainform').submit();">Add/Update</button>&emsp;
                            <?php if (!empty($_GET['id']))
{
    $uses = $site->GetKeysWhereOrderBy('CompKeyID = ' . $comp->CompKeyID, 'SiteKeyID', 'ASC');
    if (count($uses) == 0)
    { ?>
                            <a href="#confirm-delete" class="confirm-delete"><button type="button" class="inline">Delete</button></a>
                            <?php }
} ?>
                        </div>
    				</form>
    			</div>
    		</div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete this Component?<br />Doing so will remove all associations with Sites, Trials, Doctors and Patients.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->

            <!-- confirm terminate -->
            <div id="confirm-terminate">
                <table class="centered fullwidth initial">
                    <tr>
                    <?php if ($comp->CompTermination == 0)
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Terminate <?php echo $comp->CompProtocolNumber; ?>?<br />Doing so will prevent updating of this Comp. You must also enter a Terminate Date and Comment for the Comp before updating.</td>
                    <?php } else
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Un-Terminate <?php echo $comp->CompProtocolNumber; ?>?<br />Doing so will enable updating of this Comp. You must also enter a Comment for the Comp before updating</td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><button class="floatnone" onclick="$.colorbox({href:'confirm_comp.php?url=<?php echo $pageURL; ?>&id=<?php echo $comp->CompKeyID; ?>&action=terminate&status=<?php echo $comp->CompTermination; ?>'});">&emsp;Yes&emsp;</button></td>
                        <td class="center extra-padding"><button class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm terminate
            
        </div>
	</body>
</html>
<?php


