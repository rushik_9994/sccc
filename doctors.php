<?php
error_reporting(0);
/**
 * Doctor management page
 */

//initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

require_once(ROOT_PATH.'/lib/Doctor.class.php');
require_once(ROOT_PATH.'/lib/DoctorSpecialty.class.php');
require_once(ROOT_PATH.'/lib/DoctorSiteJoin.class.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_doctors_0';
    $_REQUEST['order_direction_0'] = 'asc';
}

//create required objects
$doctors = new Doctor;
$doctorSpecialty = new DoctorSpecialty;
$doctorSiteJoin = new DoctorSiteJoin;
$sites = new Site;

//define required variables
$msg = '';

//Check permissions
$canEditRows = checkPermissionString('AddEditDoctors');
$canViewRows = checkPermissionString('ViewDoctors');

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'doctors.pagination.size');

//searchFilter setup
$sFilter  = new SearchFilter('doctors');
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, 0, $cfg);

$DoctorKeyIDs  = $doctors->GetKeysOrderBy('DoctorKeyID', 'ASC',
                                          $pgInfo[0]['from'], 
                                          $pgInfo[0]['limit'], 
                                          $sClauses);  //pagination added
$totalRowCount = $doctors->totalRowCount;

//begin building output
$indexText = '_0';
$doctors_list  = "<a name='tb_0'></a>\n".
                 "<table class=\"fullwidth zebra sort\">\n".
                 "<thead>\n".
                 "    <tr class='thead_unselected'>\n".
                 $pg->generateTheadLine('order_doctors'.$indexText, 'Doctors').
                 $pg->generateTheadLine('order_specialty'.$indexText, 'Specialty').
                 $pg->generateTheadLine('order_email'.$indexText, 'Email').
                 $pg->generateTheadLine('order_sites'.$indexText, 'Sites').
                 $pg->generateTheadLine('order_date_created'.$indexText, 'Date Created');
if (($canViewRows) || ($canEditRows)) {
    $doctors_list .= $pg->generateTheadLine('order_action'.$indexText, '&nbsp;&nbsp;&nbsp;Action', array('right'=>1, 'nosort'=>1));
} else {
    $doctors_list .= $pg->generateTheadLine('order_blank'.$indexText, '&nbsp;', array('nosort'=>1));
}
$doctors_list .= "    </tr>\n".
                 "</thead>\n".
                 "<tbody>\n";
foreach ($DoctorKeyIDs as $key) {
    $doctors->Load_from_key($key);
    $doctorSpecialty->Load_from_key($doctors->DoctorSpecialty);
    $thisDoctorSpecialty = $doctorSpecialty->DoctorSpecialtyText;
    $doctorIsActive = $doctors->getDoctorIsActive();
    $doctorSites = $doctorSiteJoin->GetKeysWhereOrderBy('SiteKeyID', 'DoctorKeyID=\'' . $key . '\'', 'DoctorSiteKeyID', 'ASC');
    $thisDoctorSitesArray = array();
    foreach ($doctorSites as $siteKeys) {
        $sites->Load_from_key($siteKeys);
        //++++++ problem here: If site record no longer exists...
        $thisDoctorSitesArray[] = $sites->SiteName;
    }
    $thisDoctorSites = join(', ', $thisDoctorSitesArray);
    $rowStyle        = ($doctorIsActive) ? '' : ' style="color:#AAAAAA;"';
    $namePostfix     = ($doctorIsActive) ? '' : "<div style='display:inline;color:red;font-size:7pt;'> (Inactive)</div>";

    //Grey out closed doctors
    $escStyle = '';
    $doctorIsInactive = ($doctors->DoctorIsActive) ? 0 : 1;
    if ((isset($cfg['styleForClosedDoctors'])) && ($cfg['styleForClosedDoctors'])) {
        $escStyle = ($doctorIsInactive) ? " style=\"".$cfg['styleForClosedDoctors']."\"" : '';
    }

    $doctors_list .= "<tr $rowStyle>\n".
        "    <td class=\"left\"$escStyle>" . $doctors->DoctorLastName . ', ' . $doctors->DoctorFirstName . $namePostfix. "</td>\n".
        "    <td class=\"left\"$escStyle>" . $doctorSpecialty->DoctorSpecialtyText . "</td>\n".
        "    <td class=\"left\"$escStyle>" . trim($doctors->DoctorEmail) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . $thisDoctorSites . "</td>\n".
        "    <td class=\"left\" nowrap=\"nowrap\"$escStyle>" . substr($doctors->TimestampCreate,0,10) . "</td>\n";
    if ($canEditRows) {
        $doctors_list .= "    <td class=\"center\"><a href=\"doctorViewEdit.php?id=" . urlencode($key) . "\">View/Edit</a></td>\n";
    } elseif ($canViewRows) {
        $doctors_list .= "    <td class=\"center\"><a href=\"doctorViewEdit.php?id=" . urlencode($key) . "\">View</a></td>\n";
    } else {
        $doctors_list .= "    <td class=\"center\"$escStyle>&nbsp;</td>\n";
    }
    $doctors_list .= "</tr>\n";
}
$doctors_list .= '</tbody>' . "\n";
$doctors_list .= '</table>' . "\n";

//add pagination display
$doctors_list .= $pg->generatePaginationCode($pgInfo, 0, $doctors, $totalRowCount);


?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Doctors
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
	</head>
	<body>
	    <form name='main_form' id='main_form' action='doctors.php' method='POST'>
<?php
        //pagination variables
        print $pg->generateFormVariables($pgInfo);
?>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('doctors');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<form >
                    <div class="subhead">
						Doctors
<?php                   if ($canEditRows) { ?>
                            <a href="doctorAddEdit.php"><button type="button" class="button">Add Doctor</button></a>
<?php                   }
                        displaySuccessAndErrors();
?>
                    </div>
                        <hr />
<?php
                        $filterCodeOptions = array();
                        $filterCodeOptions['alsoShowInactive'] = (isset($_REQUEST['searchShowInactiveAlso'])) ? $_REQUEST['searchShowInactiveAlso'] : 0;
                        $filterCodeOptions['alsoShowInactiveText'] = 'Also show inactive doctors';
                        print $sFilter->generateSearchFilterCode(0, $filterCodeOptions);
?>
                        <div class="msg"><?php echo $msg; ?></div>
                        <?php echo $doctors_list; ?>
					</form>
				</div>
			</div>
		</div>
	    </form>
	</body>
</html>
<?php


