<?php
/**
 * Ajax page returning JSON to check the trialNct parameter. Will return 'ok' => true or false
 *
 * Example: //https://www.morpheusrising.com/sccc/ajax_ClinicalTrialsGovCheck.php?trialnct=NCT00000102
 */

defined('ROOT_PATH')      || define('ROOT_PATH', realpath(dirname(__FILE__)));
require(ROOT_PATH.'/lib/commonFunctions.php');

$result       = false;
$error        = 'Unknown reason';
$havesummary = false;
$summary      = '';
$url          = '';
$title        = '';

if (isset($_REQUEST['trialnct'])) {
    if ($_REQUEST['trialnct']) {
        $url  = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url  = str_replace('ajax_ClinicalTrialsGovCheck.php', 'trialNctData.php', $url);
        $url  = ((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == 'on')) ? "https://$url" : "http://$url";
        $url .= '&format=jsonflat';

        list($result, $error, $cont, $arr) = fetchNctData($_REQUEST['trialnct']);

        if (count($arr)) {
            if ((isset($arr)) && (is_array($arr))) {
                if ((isset($arr['error'])) && ($arr['error'])) {
                    $error = $arr['error'];
                } else if ((isset($arr['brief_summary']['textblock'])) && ($arr['brief_summary']['textblock'])) {
                    $summary = cleanClinicalTrialsString($arr['brief_summary']['textblock']);
                    $havesummary = ($summary) ? true : false;
                    $result      = true;
                    $error       = '';
                }
                $title = (isset($arr['brief_title'])) ? $arr['brief_title'] : '';
            }
        }
        /*OLD LOGIC
        $url = 'https://clinicaltrials.gov/ct2/show/' . urlencode($_REQUEST['trialnct']);
        $cont = @file_get_contents($url);
        if (($cont === false) || (strpos($cont, '<h1 class="solo_record">') === false)) {
            $result = false;
            $error = "clinicaltrials.gov page ($url) not found";
        } else {
            $result = true;
            $error = '';
        }
        */
    } else {
        $result = true;
        $error = 'trialnct is empty';
    }
} else {
    $error = 'Parameter trialnct not found';
}

$resultArray = array('ok' => $result, 'error' => $error, 'havesummary' => $havesummary, 'summary' => $summary);
$resultArray['url']   = $url;
$resultArray['title'] = ($title) ? 'TITLE: "'.strip_tags($title).'"' : '';

header('Content-Type: application/json;charset=utf-8');
echo (json_encode($resultArray));

