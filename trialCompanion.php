<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Trial.class.php');
require_once ('lib/TrialCompanion.class.php');

//create required objects
$trial          = new Trial;
$trialCompanion = new TrialCompanion;

$terminatedTrials = $trial->GetKeysWhereOrderBy('TrialKeyID', 'WHERE ((TrialTermination=1) OR (TrialClose=1))', 'TrialKeyID', 'ASC');

$existingCompanions = $trialCompanion->GetKeysWhereOrderBy('TrialKeyIDOUT', 'TrialKeyIDIN = ' . $_GET['id'], 'TrialKeyIDOUT', 'ASC');
$existingCompanions[] = $_GET['id'];
$omit = array_merge($terminatedTrials, $existingCompanions);

$omitTrialIDs = join(',', $omit);

$select = $trial->CreateSelectWhere('WHERE TrialKeyID NOT IN (' . $omitTrialIDs . ')');
?>
<html>
	<head>
	</head>
	<body>
        <form name="trialCompanion" id="trialCompanion" method="post" action="<?php echo $_GET['url']; ?>">
            <input type="hidden" name="TrialKeyIDIN" value="<?php echo $_GET['id']; ?>" />
            <input type="hidden" id="companion_action" name="action" value="insert_companion" />
            <div class="extra-padding">
                <table class="centered fullwidth">
                    <tr>
                        <td class="left" colspan="2">Select&nbsp;Trial&nbsp;Companion:<br />&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="left"><?php echo $select; ?></td>
                        <td class="center"><a class="button floatnone" href="javascript:void(0);" onmouseup="if($('#TrialKeyIDOUT').val() != ''){$('#trialCompanion').submit()};">&emsp;Add Companion&emsp;</a></td>
                    </tr>
                </table>
            </div>
        </form>
	</body>
</html>
<?php

