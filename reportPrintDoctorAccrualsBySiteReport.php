<?php
/**
 * Print the 'Doctor Accruals By Site Report'
 */

include_once('reportCommonFunctions.php');
include_once('reportQueryBuilder.php');
require_once ('lib/Comp.class.php');
require_once ('lib/ReportRowFetcher.class.php');


session_start();
if (empty($_SESSION['UserKeyID'])) { header('location: login.php'); exit; }

//Start our session, extract POST and GET from SESSION
list($connection, $reportInfo) = commonStartNewReport('Doctor Accruals By Site Report');

//Just run the print-start function so $reportInfo elements get created...
ob_start();
printLibraryGeneratePrintout($connection, $reportInfo);
ob_end_clean();

$reportInfo['groupByField'] = 'x_IRB';
$reportInfo['sliceSize']    = 5000;
$reportInfo['from']         = 0;

$fetcher = new ReportRowFetcher($connection, $reportInfo);

//Define the summation columns...
// $reportInfo['report']                   = array();
// $reportInfo['report']['TX_Accruals']    = 0;      //Total
// $reportInfo['report']['CC_Accruals']    = 0;      //Total
// $reportInfo['report']['PX_Accruals']    = 0;      //Total
// $reportInfo['report']['Total_Accruals'] = 0;      //Total
$reportInfo['report'] = array('TX_Accruals'=>0, 'CC_Accruals'=>0, 'CCDR_Accruals'=>0, 'PX_Accruals'=>0, 'Total_Accruals'=>0);

while(!$fetcher->done($reportInfo)) {
    list($xrow, $row) = $fetcher->getNextRow($reportInfo);
    //print "<pre>"; print_r($xrow); print "</pre>";
    
    //REPORT START
    if ($xrow['x_isStart']) {
        //<html>....<body>
        prtReportStart($reportInfo, $xrow, $row);
    }
    
    //SITE START
    if ($xrow['x_firstInGroup']) {
        prtSiteStart($reportInfo, $xrow, $row, array('dispField'=>'x_IRB'));
    }
    
    //SITE ROW
    prtSiteRow($reportInfo, $xrow, $row);
    //print "<pre>"; print_r($xrow); print "</pre>"; //exit;
    
    //SITE END
    if ($xrow['x_lastInGroup']) {
        //--------- SITE FOOTER (and once the REPORT FOOTER)
        prtSiteEnd($reportInfo, $xrow, $row, array('replaceColumn'=>'IRB_Code'));
        //--------- SITE FOOTER (and once the REPORT FOOTER)   
    }

    //REPORT END
    if ($xrow['x_isEnd']) {
        //</body>...
        prtReportEnd($reportInfo, $xrow, $row);
    }

}//while !$done



