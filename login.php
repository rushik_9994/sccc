<?php
//Indicate that commonBase should not do the authentication check
define('SKIP_BASE_AUTH_CHECK', 1);

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();


if ((!empty($_POST['UserName'])) && (!empty($_POST['UserPassword']))) {
    $msg = '';
    require_once 'lib/Users.class.php';
    $user = new Users;
    $user->Load_from_username($_POST['UserName']);
    if (md5($_POST['UserPassword']) == $user->UserPassword) {
        session_start();
        $_SESSION['UserKeyID'] = $user->UserKeyID;
        $_SESSION['UserName'] = $user->UserName;
        $_SESSION['UserTypeKeyID'] = $user->UserTypeKeyID;
        $user->UserLogins += 1;
        $user->Save_Active_Row();
        $redirect = ' onload="window.location(\'trials.php\');"';
        header("location: trials.php");
    } else {
        $redirect = '';
    }
} ?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Login
		</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<!-- <body<?php echo $redirect; ?>> -->
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
            <div class="userinfo"><br /></div>
				<div class="ui-tabs-panel">
					<form method="post" action="">
						Login
                        <hr />
                        <!-- <div class="msg"><?php echo $msg; ?></div> -->
                        <table class="centered login">
                            <tr><td class="right">User Name: </td><td class="left"><input type="text" name="UserName" <!-- value="<?php echo $_POST['UserName']; ?>" --> /> </td><td></tr>
                            <tr><td class="right">Password: </td><td class="left"><input type="password" name="UserPassword" /> </td><td></tr>
                            <tr><td colspan="2" class="right"><input type="submit" value="Submit" /></td></tr>
                        </table>
					</form>
				</div>
			</div>
        </div>
	</body>
</html>
<?php

