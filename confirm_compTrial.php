<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/TrialCompJoin.class.php');
require_once ('lib/SiteTrialsOpenJoin.class.php');
require_once ('lib/Site.class.php');

//create required objects
$tcj = new TrialCompJoin;
$stoj = new SiteTrialsOpenJoin;
$site = new Site;

$tcj->Load_from_key($_GET['tcjid']);

if ($_GET['status'] == 1) {
    $prefix = 'Un-';
} else {
    $prefix = '';
} ?>
<html>
	<head>
    <style>
	textarea { width: 100%; padding: 10px; border: 3px solid #999;
   	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
	-moz-box-sizing: border-box;    /* Firefox, other Gecko */
	box-sizing: border-box;         /* IE 8+ */
}
</style>

<script>
$(document).ready(function(){
    $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
    $('#TrialCompCloseDate').datepicker("option", "minDate", "<?php echo $tcj->TrialCompApprovalDate; ?>");
});
</script>
	</head>
	<body>
        <form name="confirm" id="confirm" method="post" action="<?php echo $_GET['url']; ?>">
        <input type="hidden" name="tcj[TrialCompKeyID]" value="<?php echo $_GET['tcjid']; ?>" />
        <input type="hidden" name="tcj[TrialKeyID]" value="<?php echo $_GET['id']; ?>" />
        <input type="hidden" name="action" value="<?php echo $_GET['action']; ?>" />
            <div class="colorbox-frame">
                <table class="centered fullwidth outer-margin">
                <?php if ($_GET['status'] != 1) { ?>
                    <tr>
                        <td>&emsp;<strong><?php echo $prefix . ucfirst($_GET['action']); ?> Date:</strong></td>
                         <td><input type="hidden" name="tcj[TrialCompClose]" size="32" class="" value="1" /><input type="text" name="tcj[TrialCompCloseDate]" id="TrialCompCloseDate" size="32" class="dpick req" value="<?php echo (!empty($tcj->TrialCompCloseDate)) ? $tcj->TrialCompCloseDate : ''; ?>" /></td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td><input type="hidden" name="tcj[TrialCompClose]" size="32" class="" value="0" /></td>
                        <td><input type="hidden" name="tcj[TrialCompCloseDate]" size="32" class="" value="" /></td>
                    </tr>
                <?php } ?>
                <!--
                    <tr>
                        <td class="center extra-padding" class="center" colspan="2"><textarea name="comment" class="confirm-comment req">Comment default here...</textarea></td>
                    </tr>
                -->
                <tr>
                    <td><strong>Also <?php echo strtolower($prefix) . $_GET['action']; ?> these sites:</strong></td>
                </tr>
                <tr>
                    <td>
                      <?php 
                        $site_keys = $site->GetKeysWhereOrderBy('CompKeyID = \'' . $_GET['compid'] . '\'', 'SiteKeyID', 'ASC');

                        // if trial id is set, select from stoj table where trialkeyid = trial id
                        if (isset($_GET['id'])) {
                            $site_keys = $stoj->GetKeysWhereOrderBy('SiteKeyID',  'TrialKeyID = ' . $_GET['id'], 'SiteKeyID', 'ASC');
                        }

                        if (count($site_keys) > 0)
                        {
                            foreach ($site_keys as $SiteKeyID)
                            {
                                $site->Load_from_key($SiteKeyID);
                                // Check if site belongs to component, otherwise skip
                                if ($site->CompKeyID != $_GET['compid']) {
                                    continue;
                                } ?>
                                <input id="<?php echo $site->SiteName ?>" type="checkbox" value="<?php echo $site->SiteKeyID; ?>" name="siteCheck[]"> <label for="<?php echo $site->SiteName ?>"><?php echo $site->SiteName ?></label><br>
                                <?php
                            }
                        ?> 
                        <script type="text/javascript">
                            // $(document).ready(function(){
                            //    $('#select_all').change(function() {
                            //     alert('change');
                            //         if($(this).is(":checked")) {
                            //             $("[name*='siteCheck']").prop('checked', true);
                            //         } else {
                            //             $("[name*='siteCheck']").prop('checked', false);
                            //         }
                            //     });
                            // });

                        function checkAll(bx){                    
                            for (var tbls = document.getElementsByTagName("table"),i=tbls.length; i--; )
                                for (var bxs=tbls[i].getElementsByTagName("input"),j=bxs.length; j--; )
                                   if (bxs[j].type=="checkbox")
                                       bxs[j].checked = bx.checked;
                        }
                        </script>
                        <input id="select_all" onClick="checkAll(this)" type="checkbox"> <label for="select_all">Select All</label> 
                        <?php
                        }
                      ?>
                      </td>
                  </tr>

                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="return validate('confirm');$('#confirm').submit();">&emsp;<?php echo $prefix . ucfirst($_GET['action']); ?> Trial&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;Cancel&emsp;</a></td>
                    </tr>
                </table>
            </div>
        </form>
	</body>
</html>
<?php

