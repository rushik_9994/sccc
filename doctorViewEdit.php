<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Doctor.class.php');
require_once(ROOT_PATH.'/lib/DoctorSpecialty.class.php');
require_once(ROOT_PATH.'/lib/States.class.php');
require_once(ROOT_PATH.'/lib/DoctorSiteJoin.class.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults  (test with this url:  morpheusrising.com/sccc/siteViewEdit.php?id=4)
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_sites_0';
    $_REQUEST['order_direction_0'] = 'asc';
}

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'default.pagination.size');

//searchFilter setup for first results table (Trials)
$sFilter  = new SearchFilter('trials'); //----!!! should not be trials!
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, $cfg);

//create required objects
$permissions     = new Permissions();
$doctor          = new Doctor();
$doctorSpecialty = new DoctorSpecialty();
$states          = new States();
$site            = new Site();
$doctorSite      = new DoctorSiteJoin();

$canEditRows = checkPermissionString('AddEditDoctors');
$canViewRows = checkPermissionString('ViewDoctors');
if ((!$canEditRows) && (!$canViewRows)) {
    header('location: login.php');  exit;
}

$pageURL = getPageUrl();

try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        if (preg_match('/.+_site$/', $action)) {
            $doctorSite->Load_from_key($_POST['dsj']['DoctorSiteKeyID']);
            foreach ($_POST['dsj'] as $key => $val) {
                $doctorSite->$key = $val;
            }
        }
        if ($action == 'insert_site') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'add doctor/site', $action, $_POST);
            $doctorSite->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }
        if ($action == 'update_site') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'update doctor/site', $action, $_POST);
            $doctorSite->Save_Active_Row();
            TrackChanges::endUserAction();
        }
        if ($action == 'delete_site') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete doctor/site', $action, $_POST);
            $doctorSite->Delete_row_from_key('DoctorSiteKeyID', $doctorSite->DoctorSiteKeyID);
            TrackChanges::endUserAction();
        }

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


//define required variables
$msg = '';

//Enforce user permissions or redirect to login.php
$canEditRows = checkPermissionString('AddEditDoctors');
$canViewRows = checkPermissionString('ViewDoctors');
if ((!$canEditRows) && (!$canViewRows)) {
    header('location: login.php');  exit;
}

$doctor->Load_from_key($_GET['id']);

$states->Load_from_key($doctor->DoctorState);
$doctorSpecialty->Load_from_key($doctor->DoctorSpecialty);

$siteList = array();
$dsj_keys = $doctorSite->GetKeysWhereOrderBy('DoctorSiteKeyID',
                                             'DoctorKeyID = \'' . $_GET['id'] . '\'', 
                                             'SiteKeyID', 
                                             'ASC',
                                             $sClauses);
if (count($dsj_keys) > 0) {
    if (checkPermissionString('AddEditDoctors')) {
        foreach ($dsj_keys as $DoctorSiteKeyID) {
            $doctorSite->Load_from_key($DoctorSiteKeyID);
            $site->Load_from_key($doctorSite->SiteKeyID);
            $siteList[$i] = '<tr><td class="left">' . $site->SiteName . '&emsp;</td><td class="left">' . $site->SiteCode . '&emsp;</td><td class="left"><span class="phone">' . $site->SitePhone . '</span>&emsp;</td><td class="left">' . $doctorSite->DoctorSiteOpenDate . '</td><td class="left">' . str_replace('0000-00-00','', $doctorSite->DoctorSiteCloseDate) . '</td><td><a class="colorbox" href="doctorSite.php?id=' . $_GET['id'] . '&sid=' . $site->SiteKeyID . '&dsjid=' . $DoctorSiteKeyID . '&url=' . $pageURL . '&action=update_site">Edit</a></td></tr>';
            $i++;
        }
    } else {
        foreach ($dsj_keys as $DoctorSiteKeyID) {
            $doctorSite->Load_from_key($DoctorSiteKeyID);
            $site->Load_from_key($doctorSite->SiteKeyID);
            $siteList[$i] = '<tr><td class="left">' . $site->SiteName . '&emsp;</td><td class="left">' . $site->SiteCode . '&emsp;</td><td class="left"><span class="phone">' . $site->SitePhone . '</span>&emsp;</td><td class="left">' . $doctorSite->DoctorSiteOpenDate . '</td><td class="left">' . str_replace('0000-00-00','', $doctorSite->DoctorSiteCloseDate) . '</td><td>
            </td></tr>';
            $i++;
        }
    }
}


?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html" />
        <meta name="author" content="Cliff Garrett" />
        <title>
            Doctor View/Edit
        </title>
        <?php require_once ("lib/common.includes.php"); ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('select').addClass('fullwidth');
               $("a.confirm-delete").colorbox({inline:true});
               $("#site-button, a.colorbox").colorbox();
               <?php if (!empty($_GET['id'])) { ?>
               $('#action').val('update');
               <?php } else { ?>
               $('#action').val('insert');
               <?php } ?>
            });
        </script>
    </head>
    <body>
        <form id='main_form' name='main_form' method='POST'>
<?php
        print $pg->generateFormVariables($pgInfo);
?>
        <div class="wrapper">
            <div class="logo">
            </div>
            <div class="ui-tabs">
                    <?php
                    print displayTopRightInfo();
                    print displayTabs('doctors');
                    ?>
                    <div id="tabs-1" class="ui-tabs-panel">
                    <div class="subhead">
                        Doctor View/Edit
<?php                   if ($canEditRows) { ?>
                            <a class="button" href="doctorAddEdit.php">Add Doctor</a>
                            <a id="main-button" class="button" href="doctorAddEdit.php?id=<?php echo $_GET['id']; ?>">Edit Doctor</a>
<?php                   } ?>
                        <a href="doctors.php" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                            <table border="0" class="fullwidth">
                              <tr>
                                <td class="left"><strong>First Name:</strong> <?php echo (!empty($doctor->DoctorFirstName)) ? $doctor->DoctorFirstName : ''; ?></td>
                                <td class="left"><strong>Last Name:</strong> <?php echo (!empty($doctor->DoctorLastName)) ? $doctor->DoctorLastName : ''; ?></td>
                                <td class="left">
                                    <strong>Specialty:</strong> <?php echo $doctorSpecialty->DoctorSpecialtyText; ?>
                                    <div style='float:right;'>
                                        &nbsp;&nbsp;&nbsp;
                                        <strong>Status:</strong> <?php echo $doctor->getDoctorIsActiveDisplay(); ?>
                                    </div>
                                </td>
                              </tr>
                              <tr>
                                <td class="left" colspan="3"></td>
                              </tr>
                              <tr>
                                <td colspan="3" class="left"><strong>Address 1:</strong> <?php echo (!empty($doctor->DoctorAddress1)) ? $doctor->DoctorAddress1 : ''; ?></td>
                              </tr>
                              <tr>
                                <td class="left" colspan="3"></td>
                              </tr>
                              <tr>
                                <td colspan="3" class="left"><strong>Address 2:</strong> <?php echo (!empty($doctor->DoctorAddress2)) ? $doctor->DoctorAddress2 : ''; ?></td>
                              </tr>
                              <tr>
                                <td class="left"><strong>City:</strong> <?php echo (!empty($doctor->DoctorCity)) ? $doctor->DoctorCity : ''; ?></td>
                                <td class="left"><strong>State:</strong> <?php echo $states->StateName; ?></td>
                                <td class="left"><strong>Zip:</strong> <span class="zip"><?php echo (!empty($doctor->DoctorZip)) ? $doctor->DoctorZip : ''; ?></span></td>
                              </tr>
                              <tr>
                                <td class="left" colspan="3"></td>
                              </tr>
                              <tr>
                                <td colspan="3" class="left"><strong>Phone Number:</strong> <span class="phone"><?php echo (!empty($doctor->DoctorPhone)) ? $doctor->DoctorPhone : ''; ?></span></td>
                              </tr>
                              <tr>
                                <td class="left" colspan="3"></td>
                              </tr>
                              <tr>
                                <td colspan="3" class="left"><strong>Email Address:</strong> <?php echo (!empty($doctor->DoctorEmail)) ? $doctor->DoctorEmail : ''; ?></td>
                              </tr>
                            </table>
                            <hr />
                        <div class="clear"></div>
                    <div class="subhead">
<?php               if (checkPermissionString('AddEditSites')) { ?>
                        <a id="site-button" class="button" href="doctorSite.php?id=<?php echo $_GET['id']; ?>&url=<?php echo $pageURL; ?>&action=insert_site">Add Site</a>
<?php               } ?>
                    </div>
                    <div class="vertical-padding-light"><strong>Sites</strong></div><br>
<?php
                    if (count($siteList) > 0) {
?>
                    <table class="fullwidth zebra sort">
                        <thead>
                            <a name='tb_0'></a>
                            <tr class='thead_unselected'>
<?php
                                print
                                $pg->generateTheadLine('order_sites_0',                 'Site Name', array('isdefault'=>1)).
                                $pg->generateTheadLine('order_site_code_0',             'Site Code').
                                $pg->generateTheadLine('order_site_phone_0',            'Site Phone').
                                $pg->generateTheadLine('order_site_join_date_0',        'Join Date').
                                $pg->generateTheadLine('order_site_close_date_0',       'Close Date');
?>
                                <th></th>
                            </tr>
                        </thead>    
                        <tbody>
<?php
                            foreach ($siteList as $siteRow) {
                                print $siteRow;
                            }
?>
                        </tbody>
                    </table>
                </div>
<?php
                }
?>
            </div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete this Doctor?<br />Doing so will remove all associations with  Sites, Trials and Patients.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            
        </div>
    </body>
</html>
<?php



