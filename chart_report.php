<?php
error_reporting(0);
/**
 * Main reporting module
 *
 * Uses: chosen plugin  https://github.com/harvesthq/chosen/releases/tag/v1.1.0
 */

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

 $connection = new DataBaseMysql();

 // print_r($connection);
 // exit();
//load required classes
include_once(ROOT_PATH.'/reportCommonFunctions.php');
include_once(ROOT_PATH.'/ChartReport.php');
require_once(ROOT_PATH.'/reportDialogFunctions.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Comp.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/TrialType.class.php');
require_once(ROOT_PATH.'/lib/TrialCategory.class.php');
require_once(ROOT_PATH.'/lib/Doctor.class.php');
require_once(ROOT_PATH.'/lib/Patient.class.php');
require_once(ROOT_PATH.'/lib/PatientIdentifier.class.php');


//define required variables
$msg = '';
$reportStartDate = $cfg['fiscalyear.startdate'];     //Beginning of this fiscal year
$reportEndDate   = date('Y-m-d');   

?>
<?php

if(isset($_POST['generate_chart'])){

        
    $err_titleBlock = '';
    $err_ReportStartDate = '';
    $err_ReportEndDate = '';
    $err_TrialDiseaseTypeKeyID = '';
    $err_ReportType = '';
    $err_detailsOption = '';
    $err_NciQuota = '';
    $titleBlock = '';
    $chart_data = '';
    $TrialTypeKeyID = '';
    $TrialCategoryKeyID = '';
    $TrialDiseaseTypeKeyID = '';
    $ReportType = '';
    $detailsOption = '';
    $nciQuota = '';


    $titleBlock=trim($_POST["titleBlock"]);
    $ReportStartDate=trim($_POST["ReportStartDate"]);
    $ReportEndDate=trim($_POST["ReportEndDate"]);
    $chart_data=trim($_POST["chart_data"]);
    $TrialTypeKeyID=trim($_POST["TrialTypeKeyID"]);
    $TrialCategoryKeyID=trim($_POST["TrialCategoryKeyID"]);
    $TrialDiseaseTypeKeyID=trim($_POST["TrialDiseaseTypeKeyID"]);
    $ReportType=trim($_POST["ReportType"]);
    $detailsOption=trim($_POST["detailsOption"]);
    $nciQuota=trim($_POST["nciQuota"]);

    if(empty($titleBlock)){
        $err_titleBlock=  "Please enter a title block";
    }    
    if(empty($ReportStartDate)){
        $err_ReportStartDate=  "Please select a report start date";
    }
    if(empty($ReportEndDate)){
        $err_ReportEndDate=  "Please select a report end date";
    }if(empty($TrialDiseaseTypeKeyID)){
        $err_TrialDiseaseTypeKeyID=  "Please select a disease type";
    }  
    if(empty($ReportType)){
        $err_ReportType=  "Please select a report type";
    }
    if($ReportType == 'nci_quota'){
        if(empty($nciQuota)){
            $err_NciQuota = "Please enter a nci quota";
        }
        if(empty($detailsOption)){
            
            $err_detailsOption = "Please select a chart type detail";
        }
    }

    if($err_titleBlock == '' && $err_ReportStartDate == '' && $err_ReportEndDate == '' && $err_TrialDiseaseTypeKeyID == '' && $err_ReportType == '' && $err_NciQuota == '' && $err_detailsOption == ''){

        
        $chart_report = new ChartReport();
        $chart_data = $chart_report->getReportData($_POST);

        header('Location:chart_generation.php');
        exit();
    }
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Report Generation
		</title>
        <style type="text/css" >
          .errorMsg{border:1px solid red; }
          .message{color: red; font-weight:bold; }
        </style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
        <script src="js/jquery.jdpicker.js"></script>
        <script src="js/chosen.jquery.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
		<script type="text/javascript">
        	$(document).ready(function(){
        		//$(".ui-tabs").tabs();
                $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', autoSize: true, changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});

                $('.chosen-select').chosen();
        	});
		</script>
		<link rel="stylesheet" href="css/chosen.css">
        <link rel="stylesheet" href="js/jdpicker.css" type="text/css" media="screen" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body id="main">
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('chart_reports');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<form method="POST"  id="ChartReport">
                        <div class="subhead displayError">
    						Chart Reports
                            <?php displaySuccessAndErrors(); ?>

                            

                        </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                        <div class="subhead">
                            Title
                        </div>
                        <textarea class="ckeditor" name="titleBlock" id="editor1" rows="10" cols="80" >
                             <?=$titleBlock?>
                        </textarea>
                        <span class="text-danger"><?php echo $err_titleBlock; ?></span>
                        <hr/>
                        
                        <div style="margin: 20px 0px 20px 0px;">
                            <label><strong>Start Date:</strong></label>
                            <input type="text" name="ReportStartDate" id="ReportStartDate" style="margin-left: 50px;" size="32" class="dpick req" value="<?=$reportStartDate?>" />
                            <label style="margin-left: 50px;"><strong>End Date:</strong></label>
                            <input type="text" name="ReportEndDate" style="margin-left: 50px;" id="ReportEndDate" size="32" class="dpick req" value="<?=$reportEndDate?>" />
                        </div>
                        <span class="text-danger"><?php echo $err_ReportStartDate; ?></span>
                        <span class="text-danger"><?php echo $err_ReportEndDate; ?></span>
                        <hr/>
                        <div class="subhead">
                                Chart Data
                        </div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-3" >
                                <input type="radio" name="chart_data" value="Credits" checked="checked">Credits
                                <input type="radio" name="chart_data" value="Accruals">Accruals
                            </div>
                            <div class="col-md-4" style="margin-top: -20px">

                                <?php
                                $select = '<select name="TrialTypeKeyID" id="TrialTypeKeyID" class="form-control">'."\n";
                                $select .= '<option value="">Select a Trial Type...</option>'."\n";
                                $result = $connection->RunQuery("SELECT TrialTypeKeyID, TrialTypeText FROM TrialType ORDER BY TrialTypeKeyID");
                                ?>
                                <!-- <select class="form-control" name="TrialTypeKeyID" id="TrialTypeKeyID" value="<?=$TrialTypeKeyID?>">
                                <option value="">Select a Trial Type...</option>
                                 -->   
                                <?php
                                    while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                        $escTrialTypeKeyID = htmlentities($row["TrialTypeKeyID"]);
                                        $escTrialTypeText  = htmlentities($row["TrialTypeText"]);
                                        if ($row["TrialTypeKeyID"] == $TrialTypeKeyID) {
                                             $select .= "<option value=\"$escTrialTypeKeyID\" selected>$escTrialTypeText</option>\n";
                                        } else {
                                             $select .= "<option value=\"$escTrialTypeKeyID\">$escTrialTypeText</option>\n";
                                        }
                                    }
                                    $select .= '</select>'."\n";
                                    echo $select;
                                ?>  
                                <!-- </select> -->
                                <br/>
                                <?php
                                    $select = '<select name="TrialCategoryKeyID" id="TrialCategoryKeyID" class="form-control">'."\n";
                                    $select .= '<option value="">Select a Category..</option>'."\n";
                                    $result = $connection->RunQuery("SELECT TrialCategoryKeyID, TrialCategoryText FROM TrialCategory ORDER BY TrialCategoryKeyID");
                                ?>
                                      <!--  <select class="form-control" name="TrialCategoryKeyID" id="TrialCategoryKeyID" value="<?=$TrialCategoryKeyID?>">
                                       <option value="">Select a Category...</option> -->
                                    <?php
                                        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                            $escTrialCategoryKeyID = htmlentities($row["TrialCategoryKeyID"]);
                                            $escTrialCategoryText  = htmlentities($row["TrialCategoryText"]);
                                            if ($row["TrialCategoryKeyID"] == $TrialCategoryKeyID) {
                                                $select .= "<option value=\"$escTrialCategoryKeyID\" selected>$escTrialCategoryText</option>\n";
                                            } else {
                                                $select .= "<option value=\"$escTrialCategoryKeyID\">$escTrialCategoryText</option>\n";
                                            }
                                        }
                                        $select .= '</select>'."\n";
                                        echo $select;
                                    ?> 

                                    <!-- </select> -->
                                     <br/>
                                <?php
                                    $select = '<select name="TrialDiseaseTypeKeyID" id="TrialDiseaseTypeKeyID" class="form-control">'."\n";
                                    $select .= '<option value="">Select a Desease Type...</option>'."\n";
                                    $result = $connection->RunQuery("SELECT TrialDiseaseTypeName, TrialDiseaseTypeKeyID FROM TrialDiseaseType ORDER BY TrialDiseaseTypeKeyID");
                                ?>
                               <!--  <select class="form-control" name="TrialDiseaseTypeKeyID" id="TrialDiseaseTypeKeyID" value="<?=$TrialDiseaseTypeKeyID?>">
                                    <option value="">Select a Desease Type...</option> -->
                                    <?php
                                        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                            $escTrialDiseaseTypeKeyID = htmlentities($row["TrialDiseaseTypeKeyID"]);
                                            $escTrialDiseaseTypeName  = htmlentities($row["TrialDiseaseTypeName"]);
                                             if ($row["TrialDiseaseTypeKeyID"] == $TrialDiseaseTypeKeyID) {
                                                $select .= "<option value=\"$escTrialDiseaseTypeKeyID\" selected>$escTrialDiseaseTypeName</option>\n";
                                            } else {
                                                $select .= "<option value=\"$escTrialDiseaseTypeKeyID\">$escTrialDiseaseTypeName</option>\n";
                                            }
                                        }
                                         $select .= '</select>'."\n";
                                        echo $select;
                                    ?>  
                                </select>
                                <span class="text-danger"><?php echo $err_TrialDiseaseTypeKeyID; ?></span>
                            </div>    
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4" >
                                <label>Chart Type</label>
                                <select class="form-control" name="ReportType" id="ReportType" value="<?=$ReportType?>">
                                    
                                    <option value="">Select a Report Type...</option>
                                    <option value="per_component">Per Component</option>
                                    <option value="per_month">Per Month</option>
                                    <option value="nci_quota" >VS. NCI Quota</option>
                                </select>
                                <span class="text-danger"><?php echo $err_ReportType; ?></span>
                            </div>
                            <div class="col-md-4">

                                <label>Chart Type Detail</label>
                                <select class="form-control" name="detailsOption" id="detailsOption" value="<?=$detailsOption?>">

                                    <option value="">Select a Detail Option...</option>
                                    <option value="minority">Minority</option>
                                    <option value="aya">Aya</option>
                                    <option value="rural">Rural</option>
                                    <option value="ethnicity">Ethnicity</option>

                                </select>
                                <span class="text-danger"><?php echo $err_detailsOption; ?></span>
                                <br/>
                                <label id="lableNciQuota"  style="display: none;" >NCI Quota</label>
                                <input type="text" name="nciQuota" id="nciQuota" class="form-control" style="display: none;" placeholder="Enter NCI Quota" value="<?=$nciQuota?>">
                                <span class="text-danger"><?php echo $err_NciQuota; ?></span>
                            </div>    
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <!-- <input type="submit" name="submit" id="generate_chart" value="Submit" class="btn btn-success btn-block"> -->
                                <button type="submit" id="generate_chart" name="generate_chart" class="btn btn-success btn-block">Submit</button>
                            </div>
                            <div class="col-md-3">
                                
                                <button type="submit" id="reset_form"  class="btn btn-danger btn-block">Reset</button>
                                <!-- <input type="submit" name="submit" id="reset_form" value="Reset" class="btn btn-danger btn-block"> -->
                            </div>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

<script type="text/javascript">
$(document).ready(function(){

    if($('input[name=chart_data]:checked').val() == 'Accruals'){
      
        $('#ReportType').children("option[value^= nci_quota]").hide();
        $('#ReportType').prop('selectedIndex',0);
    }else{

        $('#ReportType').children("option[value^= nci_quota]").show();
    } 

});
$('#ReportType').change(function(){
    var report_type = $(this).val();

    $('#nciQuota').attr('style','display:none');
    $('#lableNciQuota').attr('style','display:none');
    if(report_type == 'nci_quota'){
        $('#nciQuota').removeAttr('style');
        $('#lableNciQuota').removeAttr('style');
    }
});
$('input[type=radio][name=chart_data]').change(function() {
    
    if (this.value == 'Accruals') {

        $('#ReportType').children("option[value^= nci_quota]").hide();
        $('#ReportType').val(0);
        $('#nciQuota').attr('style','display:none');
        $('#lableNciQuota').attr('style','display:none');
    }
    else {
        $('#ReportType').children("option[value^= nci_quota]").show();
    }
});

$('#reset_form').click(function(){
    document.getElementById("ChartReport").reset();
});

</script>


