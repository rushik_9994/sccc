<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Permissions.class.php');
require_once ('lib/Site.class.php');
require_once ('lib/Comp.class.php');
require_once ('lib/States.class.php');
require_once ('lib/SiteTrialsOpenJoin.class.php');
require_once ('lib/DoctorSiteJoin.class.php');
require_once ('lib/Patient.class.php');
$pageURL = getPageUrl();

//create required objects
$permissions = new Permissions();
$site        = new Site();
$comp        = new Comp();
$states      = new States();
$stoj        = new SiteTrialsOpenjoin();
$dsj         = new DoctorSiteJoin();
$patient     = new Patient();

try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        if (($action == 'update') || ($action == 'insert')) {
            foreach ($_POST['site'] as $key => $val) {
                $site->$key = $val;
            }
            $site->SitePhone = preg_replace('/\D/', '', $site->SitePhone);
            $site->SiteZip = preg_replace('/\D/', '', $site->SiteZip);
            $site->SiteState = $_POST['StatesKeyID'];
            $site->CompKeyID = $_POST['CompKeyID'];
            if ($site->SiteStatus == 'on') {
                $site->SiteStatus = 1;
            } else {
                $site->SiteStatus = 0;
            }
            if ($action == 'update') {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'update site', $action, $_POST);
                $site->Save_Active_Row();
                TrackChanges::endUserAction();
            } else {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'add site', $action, $_POST);
                $site->Save_Active_Row_as_New();
                TrackChanges::endUserAction();
            }
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: siteViewEdit.php?id=" . $site->SiteKeyID);
            }
        }
        if (($action == 'delete') && (!empty($_GET['id']))) {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete site', $action, $_POST);
            $site->Delete_row_from_key($_GET['id']);
            $stoj->Delete_row_from_key('SiteKeyID', $_GET['id']);
            $dsj->Delete_row_from_key('SiteKeyID', $_GET['id']);
            //$comment->Delete_row_from_key('SiteKeyID', $_GET['id']);
            TrackChanges::endUserAction();
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: sites.php");
            }
        }

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


//header("location: $pageURL");

//define required variables
$msg = '';

//Enforce user permissions or redirect to login.php
list($UserTypeIDs) = enforceUserPermissions($permissions);
$canEditRows = checkPermissionString('AddEditSites');
$canViewRows = checkPermissionString('ViewSites');
if ((!$canEditRows) && (!$canViewRows)) {
    header('location: login.php');  exit;
}


if (!empty($_GET['id'])) {
    //editing
    $site->Load_from_key($_GET['id']);
}
$statesSelect = $states->CreateSelect($site->SiteState);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
		<meta name="author" content="Cliff Garrett" />
		<title>
			Site Add/Edit
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
		<script type="text/javascript">
        	$(document).ready(function(){
	           $('select').addClass('fullwidth');
               $('#StatesKeyID').addClass('req');
               $('#CompKeyID').addClass('req');
        	   $("a.confirm-delete").colorbox({inline:true});
               <?php if (!empty($_GET['id']))
{ ?>
               $('#action').val('update');
               <?php } else
{ ?>
               $('#action').val('insert');
               <?php } ?>
              $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
         	});
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('sites');
                ?>
    				<div id="tabs-1" class="ui-tabs-panel">
    					<form method="post" action="" name="mainform" id="mainform">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="continue" id="continue" />
                        <?php if ($_GET['id'])
{ ?>
                        <input type="hidden" name="site[SiteKeyID]" id="SiteKeyID" value="<?php echo $_GET['id']; ?>" />
                        <?php } ?>
                    <div class="subhead">
    					Site Add/Edit
                            <a href="<?php echo (!empty($_GET['id'])) ? 'siteViewEdit.php?id=' . $_GET['id'] : 'sites.php'; ?>" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
<table border="0" class="fullwidth">
  <tr>
    <td class="right"><strong>Site Name:</strong> </td>
    <td class="right" colspan="2"><input class="fullwidth req" type="text" name="site[SiteName]" id="SiteName" value="<?php echo (!empty($site->SiteName)) ? $site->SiteName : ''; ?>" /></td>
    <td class="right"><strong>Site Code:</strong> </td>
    <td class="right"><input class="fullwidth req" type="text" name="site[SiteCode]" id="SiteCode" value="<?php echo (!empty($site->SiteCode)) ? $site->SiteCode : ''; ?>" /></td>
    <td><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div></td>
  </tr>
  <tr>
    <td class="right"><strong>Address 1:</strong> </td>
    <td class="right" colspan="5"><input class="fullwidth req" type="text" name="site[SiteAddress1]" id="SiteAddress1" value="<?php echo (!empty($site->SiteAddress1)) ? $site->SiteAddress1 : ''; ?>" /></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>Address 2:</strong> </td>
    <td colspan="5"><input type="text" class="fullwidth" name="site[SiteAddress2]" id="SiteAddress2" value="<?php echo (!empty($site->SiteAddress2)) ? $site->SiteAddress2 : ''; ?>" /></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>City:</strong> </td>
    <td><input class="fullwidth req" type="text" name="site[SiteCity]" id="SiteCity" value="<?php echo (!empty($site->SiteCity)) ? $site->SiteCity : ''; ?>" /></td>
    <td class="right"><strong>State:</strong></td>
    <td><?php echo $statesSelect; ?></td>
    <td class="right"><strong>Zip:</strong> </td>
    <td><input class="fullwidth req zip" type="text" name="site[SiteZip]" id="SiteZip" value="<?php echo (!empty($site->SiteZip)) ? $site->SiteZip : ''; ?>" /></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>Phone Number:</strong> </td>
    <td colspan="2"><input class="fullwidth phone req" type="text" name="site[SitePhone]" id="SitePhone" value="<?php echo (!empty($site->SitePhone)) ? $site->SitePhone : ''; ?>" /></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>Open Date:</strong> </td>
    <td class="left"><input type="text" name="site[SiteOpenDate]" id="SiteOpenDate" size="32" class="dpick req" value="<?php echo (!empty($site->SiteOpenDate)) ? $site->SiteOpenDate : ''; ?>" /></td>
    <td></td>
    <td><input name="site[SiteStatus]" type="checkbox" <?php echo (($site->SiteStatus == 1) || (empty($_GET['id']))) ? 'checked' : ''; ?>/> <strong> Active</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td class="right"><strong>Component:</strong> </td>
    <td class="left"><?php echo $comp->CreateSelect($site->CompKeyID, 'WHERE CompTermination <> 1'); ?></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
                        <div class="clear"></div>
                        <div class="right formdiv noborder">
                            <button type="button" class="inline" onmouseup="return validate('mainform'); $('#mainform').submit();">Add/Update</button>&emsp;
                            <?php if (!empty($_GET['id']))
{
    $uses1 = $dsj->GetKeysWhereOrderBy('DoctorKeyID', 'SiteKeyID = ' . $site->SiteKeyID, 'DoctorKeyID', 'ASC');
    $uses2 = $patient->GetKeysWhere('PatientKeyID', 'WHERE SiteKeyID = ' . $site->SiteKeyID);
    $uses = array_merge($uses1, $uses2);
    if (count($uses) == 0)
    { ?>
                            <a href="#confirm-delete" class="confirm-delete"><button type="button" class="inline">Delete</button></a>
                            <?php }
} ?>
                        </div>
    				</form>
    			</div>
    		</div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete this Site?<br />Doing so will remove all associations with Components, Trials, Doctors and Patients.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            
        </div>
	</body>
</html>
<?php


/**
 * Enforce user permissions or redirect to login.php
 * @param $permissions
 * @return array array($UserTypeIDs)
 **/
function enforceUserPermissions($permissions) {
    $permissions->Load_from_action("AddEditSites");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }
    $permissions->Load_from_action("DeleteSites");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }
    return array($UserTypeIDs);
}



