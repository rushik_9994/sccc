<?php
/**
 * Helper functions for reporting
 *
 */

require_once('lib/DataBaseMysql.class.php');
require_once('lib/SecurityFunctions.php');
require_once('reportQueryBuilder.php');
require_once('reportPrintFunctions.php');


session_start();
if (empty($_SESSION['UserKeyID'])) { header('location: login.php'); exit; }



function commonStartNewReport($metaTitle='') {
    $connection  = null;
    $reportInfo  = array('debugMode'    => 0,
                         'metaTitle'    => trim($metaTitle),
                         'report'       => '',
                         'reportTitle'  => '',
                         'reportPrefix' => '',
                         'format'       => '',
                         'post'         => array(),
                         'get'          => array(),
                         'sql'          => '',
                         'from'         => -1,
                         'haveMoreRows' => 0,
                         'error'        => '');
   
    $connection = commonGetDbConnection();

    //Get our POST and GET vars
    $post = array();
    if ((isset($_SESSION['post_params'])) && (is_array($_SESSION['post_params']))) {
        $post = $_SESSION['post_params'];
        $_SESSION['post_params'] = '';
        unset($_SESSION['post_params']);
    } else if (isset($_POST)) {
        $post = $_POST;
    }
    $_POST              = $post;
    $reportInfo['post'] = $post;

    $get = array();
    if ((isset($_SESSION['get_params'])) && (is_array($_SESSION['get_params']))) {
        $get = $_SESSION['get_params'];
        $_SESSION['get_params'] = '';
        unset($_SESSION['get_params']);
    } else if (isset($_GET)) {
        $get = $_GET;
    }
    $_GET              = $get;
    $reportInfo['get'] = $post;
    
    list($reportInfo['report'], $reportInfo['reportTitle'], $reportInfo['reportPrefix'], 
         $reportInfo['format'], $reportInfo['error']) = 
         commonGetReportNameAndFormat($post);

    if (!$reportInfo['metaTitle']) {
        $reportInfo['metaTitle'] = $reportInfo['reportTitle'];
    }

    //Now get our report query
    $reportInfo['sql'] = queryLibraryGetSql($format, $connection, $reportInfo);

    return array($connection, $reportInfo);
}



/**
 * Determines what the report this is and what report format is desired
 *
 * @param array $post                 Usually $_POST
 * @return array array($report, $reportTitle, $reportPrefix, $format, $error)
 */
function commonGetReportNameAndFormat($post=array()) {
    $report       = '';
    $reportPrefix = '';
    $reportTitle  = '';
    $format       = '';
    $error        = '';

    foreach($post as $key => $val) {
        if (strpos($key, '_submit_excel')!==false) {
            $format  = 'excel';
            $report  = str_replace('_submit_excel', '', $key);
        } else if (strpos($key, '_submit_html')!==false) {
            $format  = 'html';
            $report  = str_replace('_submit_html', '', $key);
        } else if (strpos($key, '_submit_print')!==false) {
            $format  = 'print';
            $report  = str_replace('_submit_print', '', $key);
        }
    }
    reset($post);

    list($reportPrefix, $reportTitle, $error) = commonDetermineReportTitleAndPrefix($report, $post);

    if ($error=='') {
        if (($format=='') || ($report=='')) { $error = "Invalid report name or report format!"; }
    }

    return array($report, $reportTitle, $reportPrefix, $format, $error);
}


/**
 * Get Title and prefix for a given report
 *
 * @param  string $report
 * @param  array  $post optional          Usually $_POST
 * @return array  array($reportPrefix, $reportTitle, $error)
 */
function commonDetermineReportTitleAndPrefix($report, $post=array()) {
    $reportPrefix ='';
    $reportTitle  = '';
    $error        = '';

    if ($report=='cred') {
        $reportPrefix = 'credits_report';
        $reportTitle  = 'Credits Report';
    } else if ($report=='prr') {
        $reportPrefix = 'patient_registration_report';
        $reportTitle  = 'Patient Registration Report';
    } else if ($report=='dac') {
        $reportPrefix = 'doctors_accruals_report';
        $reportTitle  = 'Doctor Accruals Report';
    } else if ($report=='dacbs') {
        $reportPrefix = 'doctors_accruals_by_site_report';
        $reportTitle  = 'Doctor Accruals by Site Report';
    } else if ($report=='master') {
        $reportPrefix = 'master_report';
        $reportTitle  = 'Master Report';
        $masterReportType = ((isset($post)) && (is_array($post)) && (isset($post['master_report_type']))) ? $post['master_report_type'] : 'none';
        if ($masterReportType=='trials') {
            $reportPrefix .= '_by_trials';
            $reportTitle  .= ' By Trials';
        } else if ($masterReportType=='components') {
            $reportPrefix .= '_by_components';
            $reportTitle  .= ' By Components';
        } else if ($masterReportType=='sites') {
            $reportPrefix .= '_by_sites';
            $reportTitle  .= ' By Sites';
        } else if ($masterReportType=='doctors') {
            $reportPrefix .= '_by_doctors';
            $reportTitle  .= ' By Doctors';
        } else if ($masterReportType=='researchbases') {
            $reportPrefix .= '_by_researchbases';
            $reportTitle  .= ' By Research Bases';
        }

    } else if ($report=='tracking') {
        $reportPrefix = 'tracking_report';
        $reportTitle  = 'Change Tracking Report';

    } else {
        $error = 'Invalid report encountered';
    }

    return array($reportPrefix, $reportTitle, $error);
}


function commonGetDbConnection() {
    $connection = new DataBaseMysql();

    return $connection;
}



function addSelectOption($source, $value, $string) {
    $selected = ((isset($source)) && (isset($value)) && ($source == $value)) ? ' selected' : '';
    $escText  = (substr($string,0,8) == '[[HTML]]') ? substr($string,8) : htmlentities($string);
    return "    <option value=\"$value\"$selected>$escText</option>\n";
}


