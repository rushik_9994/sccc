<?php
/**
 * Report dialog functions
 *
 * Uses: chosen plugin  https://github.com/harvesthq/chosen/releases/tag/v1.1.0
 */

include_once('reportCommonFunctions.php');



session_start();
if (empty($_SESSION['UserKeyID'])) {
    header('location: login.php'); exit;
}


/**
 * First <tr>...</tr> defining the column widths
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @return string $content
 */
function dialogAddColumnWidthDefinitions($dependencies=array()) {
    $content = "\n".<<< __END__
        <tr>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>

            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
            <td class="left" width="5%">&nbsp;</td>
        </tr>
__END__;

    $content = dialogAddIndentation($content, 20);
    return $content;
}

function dialogAddIndentation($content, $ind=0) {
    if ($ind > 0) {
        $ind     = str_repeat(' ', $ind);
        $content = str_replace("\n", "\n$ind", $content);
    }
    return $content;
}


/**
 * Report Section containing ReportStartDate and ReportendDate
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @param  string $reportStartDate optional
 * @param  string $reportEndDate optional
 * @return string $content
 */
function dialogAddStartAndEndDate($dependencies=array(), $reportStartDate='', $reportEndDate='') {
    $content = "\n".<<< __END__
        <tr>
            <td class="left" colspan="3"><strong>Start Date:</strong></td>
            <td class="left" colspan="6"><input type="text" name="ReportStartDate" id="ReportStartDate" size="32" class="dpick req" value="$reportStartDate" /></td>
            <td class="left" colspan="1">&nbsp;</td>
            <td class="left" colspan="3"><strong>End Date:</strong></td>
            <td class="left" colspan="6"><input type="text" name="ReportEndDate" id="ReportEndDate" size="32" class="dpick req" value="$reportEndDate" /></td>
            <td class="left" colspan="1">&nbsp;</td>
        </tr>
        <tr><td class="left" colspan="20"><br /><hr/></td></tr>
__END__;

    $content = dialogAddIndentation($content, 20);
    return $content;
}


/**
 * Report Section containing Credits Report
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @return string $content
 */
function dialogAddCreditsReportDialog($dependencies=array(), $reportStartDate='', $reportEndDate='') {
    $sepline = (isset($dependencies['use_sepline'])) ? '<tr><td colspan="20">&nbsp;</td></tr>' : '';
    $content = "\n".<<< __END__
        <tr>
            <td class="left" colspan="15"><strong><b>Credits Report</b></strong></td>
            <td class="right" colspan="5">
           <!-- <input type="submit" class="smaller" id="cred_submit_html"  name="cred_submit_html"  value="Html">&nbsp;&nbsp; -->
                <input type="submit" class="smaller" id="cred_submit_print" name="cred_submit_print" value="View">&nbsp;&nbsp;
                <input type="submit" class="smaller" id="cred_submit_excel" name="cred_submit_excel" value="Export">
            </td>
        </tr>
        $sepline
        <tr>
            <td class="left" colspan="3">
                <strong>Detail Options:</strong>
            </td>
            <td class="left" colspan="17">
                <input id="cred_sex" name="cred_sex" checked="" type="checkbox"> <strong> Sex</strong>&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="cred_race" name="cred_race" checked="" type="checkbox"> <strong> Race</strong>&nbsp;&nbsp;&nbsp;&nbsp;
                <input id="cred_ethn" name="cred_ethn" checked="" type="checkbox"> <strong> Ethnicity</strong>&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr><td colspan="20"><br /><hr/></td></tr>
__END__;

    $content = dialogAddIndentation($content, 20);
    return $content;
}




/**
 * Report Section containing Patient Registration Report
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @return string $content
 */
function dialogAddPatientRegistrationReportDialog($dependencies=array(), $reportStartDate='', $reportEndDate='', $options=array()) {
    $compClass       = $dependencies['comp_class'];
    $compSelectCode  = $compClass->CreateSelect();
    $compSelectCode  = str_replace(" class=\"button\"", '', $compSelectCode);
    $compSelectCode  = str_replace("CompKeyID", "prr_CompKeyID", $compSelectCode);
    $compSelectCode  = str_replace("<select ", "<select data-placeholder=\"Component\" class=\"chosen-select button\" ", $compSelectCode);
    $compSelectCode  = str_replace('<option value="">Select...</option>',
                                   '<option value="">All Components</option>'
                                 //.'<option value="">All...</option>'
                                   , $compSelectCode);

    $patientClass                = $dependencies['patient_class'];
    $options['defaultText']      = 'All Research-Bases';
    $options['fieldId']          = 'prr_PatientIdentifier';
    $patientIdentifierSelectCode = $patientClass->CreatePatientIdentifierSelect(0, '', $options);
    $patientIdentifierSelectCode = str_replace("<select ", "<select data-placeholder=\"PatientIdentifier\" class=\"chosen-select button\" ", $patientIdentifierSelectCode);
    
    $escChecked                  = '';
    $patientAddAgeColumnsCode    = "<input type=\"checkbox\" id='prr_add_age_columns' name='prr_add_age_columns' $escChecked>".
                                   "&nbsp;&nbsp;<small>Add PatientAge and RegistrationAge report columns</small>";

    $sepline = (isset($dependencies['use_sepline'])) ? '<tr><td colspan="20">&nbsp;</td></tr>' : '';
    $content = "\n".<<< __END__
        <tr>
            <td class="left" colspan="15"><strong><b>Patient Registration Report</b></strong></td>
            <td class="right" colspan="5">
           <!-- <input type="submit" class="smaller" id="prr_submit_html"  name="prr_submit_html"  value="Html">&nbsp;&nbsp; -->
                <input type="submit" class="smaller" id="prr_submit_print" name="prr_submit_print" value="View">&nbsp;&nbsp;
                <input type="submit" class="smaller" id="prr_submit_excel" name="prr_submit_excel" value="Export">
            </td>
        </tr>
        $sepline
        <tr>
            <td class="left" colspan="5">
                <strong>Choose Component:</strong>
            </td>
            <td class="left" colspan="15">
                $compSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                <strong>Choose Research Base:</strong>
            </td>
            <td class="left" colspan="15">
                $patientIdentifierSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                <strong>Add Age Columns:</strong>
            </td>
            <td class="left" colspan="15">
                $patientAddAgeColumnsCode
            </td>
        </tr>
        <tr><td colspan="20"><br /><hr/></td></tr>
__END__;

    $content = dialogAddIndentation($content, 20);
    return $content;
}



/**
 * Report Section containing Doctor Accruals Report
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @return string $content
 */
function dialogAddDoctorAccrualsReportDialog($dependencies=array(), $reportStartDate='', $reportEndDate='') {
    $sepline = (isset($dependencies['use_sepline'])) ? '<tr><td colspan="20">&nbsp;</td></tr>' : '';
    $content = "\n".<<< __END__
        <tr>
            <td class="left" colspan="15"><strong><b>Doctor Accruals Report</b></strong></td>
            <td class="right" colspan="5">
           <!-- <input type="submit" class="smaller" id="dac_submit_html"  name="dac_submit_html"  value="Html">&nbsp;&nbsp; -->
                <input type="submit" class="smaller" id="dac_submit_print" name="dac_submit_print" value="View">&nbsp;&nbsp;
                <input type="submit" class="smaller" id="dac_submit_excel" name="dac_submit_excel" value="Export">
            </td>
        </tr>
        <tr><td colspan="20"><br /><hr/></td></tr>
__END__;

    $content = dialogAddIndentation($content, 20);
    return $content;
}


/**
 * Report Section containing Doctor Accruals By Site Report
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @return string $content
 */
function dialogAddDoctorAccrualsBySiteReportDialog($dependencies=array(), $reportStartDate='', $reportEndDate='') {
    $compClass       = $dependencies['comp_class'];
    $compSelectCode  = $compClass->CreateSelect();
    $compSelectCode  = str_replace(" class=\"button\"", '', $compSelectCode);
    $compSelectCode  = str_replace("CompKeyID", "dacbs_CompKeyID", $compSelectCode);
    $compSelectCode  = str_replace("<select ", "<select data-placeholder=\"Component\" class=\"chosen-select button\" ", $compSelectCode);
    $compSelectCode  = str_replace('<option value="">Select...</option>',
                                   '<option value="">All Components</option>'
                                 //.'<option value="">All...</option>'
                                   , $compSelectCode);

    $sepline = (isset($dependencies['use_sepline'])) ? '<tr><td colspan="20">&nbsp;</td></tr>' : '';
    $content = "\n".<<< __END__
        <tr>
            <td class="left" colspan="15"><strong><b>Doctor Accruals by Site Report</b></strong></td>
            <td class="right" colspan="5">
           <!-- <input type="submit" class="smaller" id="dacbs_submit_html"  name="dacbs_submit_html"  value="Html">&nbsp;&nbsp; -->
                <input type="submit" class="smaller" id="dacbs_submit_print" name="dacbs_submit_print" value="View">&nbsp;&nbsp;
                <input type="submit" class="smaller" id="dacbs_submit_excel" name="dacbs_submit_excel" value="Export">
            </td>
        </tr>
        $sepline
        <tr>
            <td class="left" colspan="5">
                <strong>Choose Component To Output:</strong>
            </td>
            <td class="left" colspan="15">
                $compSelectCode
            </td>
        </tr>
        <tr><td colspan="20"><br /><hr/></td></tr>
__END__;

    $content = dialogAddIndentation($content, 20);
    return $content;
}


/**
 * Report Section containing Master Report
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @return string $content
 */
function dialogAddMasterReportDialog($dependencies=array(), $reportStartDate='', $reportEndDate='') {
    $trialClass      = $dependencies['trial_class'];
    $trialSelectCode = $trialClass->CreateSelect();
    $trialSelectCode = str_replace(" class=\"button\"", '', $trialSelectCode);
    $trialSelectCode = str_replace("TrialKeyIDOUT", "master_TrialKeyID", $trialSelectCode);
    $trialSelectCode = str_replace("<select ", "<select data-placeholder=\"Trial\" class=\"chosen-select button\" ", $trialSelectCode);
    $trialSelectCode = str_replace('<option value="">Select...</option>',
                                   '<option value="">All Trials</option>'
                                   , $trialSelectCode);


    $trialTypeClass      = $dependencies['trial_type_class'];
    $trialTypeSelectCode = $trialTypeClass->CreateSelect();
    $trialTypeSelectCode = str_replace(" class=\"button\"", '', $trialTypeSelectCode);
    $trialTypeSelectCode = str_replace("TrialTypeKeyID", "master_TrialTypeKeyID", $trialTypeSelectCode);
    $trialTypeSelectCode = str_replace("<select ", "<select data-placeholder=\"TrialType\" class=\"chosen-select button\" ", $trialTypeSelectCode);
    $trialTypeSelectCode = str_replace('<option value="">Select...</option>',
                                       '<option value="">All Trial Types</option>'
                                       , $trialTypeSelectCode);

    $trialCategoryClass      = $dependencies['trial_category_class'];
    $trialCategorySelectCode = $trialCategoryClass->CreateSelect();
    $trialCategorySelectCode = str_replace(" class=\"button\"", '', $trialCategorySelectCode);
    $trialCategorySelectCode = str_replace("TrialCategoryKeyID", "master_TrialCategoryKeyID", $trialCategorySelectCode);
    $trialCategorySelectCode = str_replace("<select ", "<select data-placeholder=\"TrialCategory\" class=\"chosen-select button\" ", $trialCategorySelectCode);
    $trialCategorySelectCode = str_replace('<option value="">Select...</option>',
                                           '<option value="">All Trial Categories</option>'
                                           , $trialCategorySelectCode);

    $compClass       = $dependencies['comp_class'];
    $compSelectCode  = $compClass->CreateSelect();
    $compSelectCode  = str_replace(" class=\"button\"", '', $compSelectCode);
    $compSelectCode  = str_replace("CompKeyID", "master_CompKeyID", $compSelectCode);
    $compSelectCode  = str_replace("<select ", "<select data-placeholder=\"Component\" class=\"chosen-select button\" ", $compSelectCode);
    $compSelectCode  = str_replace('<option value="">Select...</option>',
                                   '<option value="">All Components</option>'
                                 //.'<option value="">All...</option>'
                                   , $compSelectCode);

    $siteClass       = $dependencies['site_class'];
    $siteSelectCode  = $siteClass->CreateSelect();
    $siteSelectCode  = str_replace(" class=\"button\"", '', $siteSelectCode);
    $siteSelectCode  = str_replace("SiteKeyID", "master_SiteKeyID", $siteSelectCode);
    $siteSelectCode  = str_replace("<select ", "<select data-placeholder=\"Site\" class=\"chosen-select button\" ", $siteSelectCode);
    $siteSelectCode  = str_replace('<option value="">Select...</option>',
                                   '<option value="">All Sites</option>'
                                //.'<option value="">All...</option>'
                                   , $siteSelectCode);

    $doctorClass       = $dependencies['doctor_class'];
    $doctorSelectCode  = $doctorClass->CreateSelect();
    $doctorSelectCode  = str_replace(" class=\"button\"", '', $doctorSelectCode);
    $doctorSelectCode  = str_replace("DoctorKeyID", "master_DoctorKeyID", $doctorSelectCode);
    $doctorSelectCode  = str_replace("<select ", "<select data-placeholder=\"Doctor\" class=\"chosen-select button\" ", $doctorSelectCode);
    $doctorSelectCode  = str_replace('<option value="">Select...</option>',
                                   '<option value="">All Doctors</option>'
                                //.'<option value="">All...</option>'
                                   , $doctorSelectCode);


    $patientIdentifierClass       = $dependencies['patient_identifier_class'];
    $patientIdentifierSelectCode  = $patientIdentifierClass->CreateSelect();
    $patientIdentifierSelectCode  = str_replace(" class=\"button\"", '', $patientIdentifierSelectCode);
    $patientIdentifierSelectCode  = str_replace("PatientIdentifierKeyID", "master_PatientIdentifierKeyID", $patientIdentifierSelectCode);
    $patientIdentifierSelectCode  = str_replace("<select ", "<select data-placeholder=\"PatientIdentifier\" class=\"chosen-select button\" ", $patientIdentifierSelectCode);
    $patientIdentifierSelectCode  = str_replace('<option value="">Select...</option>',
                                   '<option value="">All Research Bases</option>'
                                   , $patientIdentifierSelectCode);
    $addendum   = (isset($_REQUEST['master_PatientIdentifierAddendum'])) ? $_REQUEST['master_PatientIdentifierAddendum'] : '';
    $birthdate  = (isset($_REQUEST['master_PatientBirthdate']))          ? $_REQUEST['master_PatientBirthdate'] : '';
    $zipcode    = (isset($_REQUEST['master_PatientZipCode']))            ? $_REQUEST['master_PatientZipCode'] : '';
    $urbanRural = (isset($_REQUEST['master_PatientUrbanRural']))         ? $_REQUEST['master_PatientUrbanRural'] : '';
    $urbanRuralSelectCode = "<select name='master_PatientUrbanRural' id='master_PatientUrbanRural'>\n";
    $urbanRuralSelectCode .= "<option value=''>Select of of these options</option>\n";
    $sel = ($urbanRural == 'urban') ? ' selected="selected"' : '';
    $urbanRuralSelectCode .= "<option value='urban'$sel>urban</option>\n";
    $sel = ($urbanRural == 'rural') ? ' selected="selected"' : '';
    $urbanRuralSelectCode .= "<option value='rural'$sel>rural</option>\n";
    $sel = ($urbanRural == 'unknown') ? ' selected="selected"' : '';
    $urbanRuralSelectCode .= "<option value='unknown'$sel>unknown</option>\n";
    $urbanRuralSelectCode .= "</select>\n";
    $patientIdentifierSelectCode .= "&nbsp;&nbsp;&nbsp;Addendum:&nbsp;".
                                    "<input type=\"text\" id=\"master_PatientIdentifierAddendum\" name=\"master_PatientIdentifierAddendum\" value=\"$addendum\">";

    //Patient Age                                
    $escAgeGroupFrom    = (isset($_REQUEST['master_PatientAgeGroupFrom'])) ? $_REQUEST['master_PatientAgeGroupFrom'] : 0;
    $escAgeGroupTo      = (isset($_REQUEST['master_PatientAgeGroupTo']))   ? $_REQUEST['master_PatientAgeGroupTo']   : 0;
    if ((!is_numeric($escAgeGroupFrom)) || ($escAgeGroupFrom < 0))   { $escAgeGroupFrom = 0; }
    if ((!is_numeric($escAgeGroupTo))   || ($escAgeGroupTo < 0))     { $escAgeGroupTo = 0; }
    if ((!is_numeric($escAgeGroupFrom)) || ($escAgeGroupFrom > 150)) { $escAgeGroupFrom = 0; }
    if ((!is_numeric($escAgeGroupTo))   || ($escAgeGroupTo > 150))   { $escAgeGroupTo = 0; }
    if ($escAgeGroupTo < $escAgeGroupFrom) {
        $tmp = $escAgeGroupFrom;
        $escAgeGroupFrom = $escAgeGroupTo;
        $escAgeGroupTo   = $tmp;
    }
    if (!$escAgeGroupFrom) { $escAgeGroupFrom = ''; }
    if (!$escAgeGroupTo)   { $escAgeGroupTo = ''; }
    if (($escAgeGroupFrom) || ($escAgeGroupTo)) {
        unset($_REQUEST['master_PatientAgeGroup']);
    }

    //Registration Age
    $escRegAgeGroupFrom = (isset($_REQUEST['master_PatientRegAgeGroupFrom'])) ? $_REQUEST['master_PatientRegAgeGroupFrom'] : '';
    $escRegAgeGroupTo   = (isset($_REQUEST['master_PatientRegAgeGroupTo']))   ? $_REQUEST['master_PatientRegAgeGroupTo']   : '';
    if ((!is_numeric($escRegAgeGroupFrom)) || ($escRegAgeGroupFrom < 0))   { $escRegAgeGroupFrom = 0; }
    if ((!is_numeric($escRegAgeGroupTo))   || ($escRegAgeGroupTo < 0))     { $escRegAgeGroupTo = 0; }
    if ((!is_numeric($escRegAgeGroupFrom)) || ($escRegAgeGroupFrom > 150)) { $escRegAgeGroupFrom = 0; }
    if ((!is_numeric($escRegAgeGroupTo))   || ($escRegAgeGroupTo > 150))   { $escRegAgeGroupTo = 0; }
    if ($escRegAgeGroupTo < $escRegAgeGroupFrom) {
        $tmp = $escRegAgeGroupFrom;
        $escRegAgeGroupFrom = $escRegAgeGroupTo;
        $escRegAgeGroupTo   = $tmp;
    }
    if (!$escRegAgeGroupFrom) { $escRegAgeGroupFrom = ''; }
    if (!$escRegAgeGroupTo)   { $escRegAgeGroupTo = ''; }
    if (($escRegAgeGroupFrom) || ($escRegAgeGroupTo)) {
        unset($_REQUEST['master_PatientRegAgeGroup']);
    }
    
    $patientAttributeSelectCode   = 
    //---------- Birthday
                                    "Birth Date:&nbsp;&nbsp;".
                                    "<input type=\"text\" id=\"master_PatientBirthdate\" name=\"master_PatientBirthdate\" value=\"$birthdate\">".
                                    "&nbsp;&nbsp;<small>(Format: YYYY-MM-DD  wildcard '*' and comparisons via '&gt;' and '&lt;' allowed)</small><br />".
    //---------- Birthday

    //---------- Age group
                                    "Age Group:&nbsp;".
                                    "<input type=\"text\" id=\"master_PatientAgeGroupFrom\" name=\"master_PatientAgeGroupFrom\" value=\"$escAgeGroupFrom\" placeholder=\"from...\" size=\"5\">".
                                    "&nbsp;&nbsp;to&nbsp;&nbsp;".
                                    "<input type=\"text\" id=\"master_PatientAgeGroupTo\" name=\"master_PatientAgeGroupTo\" value=\"$escAgeGroupTo\" placeholder=\"to...\" size=\"5\">".
                                    "&nbsp;&nbsp;or&nbsp;&nbsp;".
                                    "<select id=\"master_PatientAgeGroup\" name=\"master_PatientAgeGroup\" style=\"font-size:8pt;\">\n";
    $ageGroup = (isset($_REQUEST['master_PatientAgeGroup'])) ? $_REQUEST['master_PatientAgeGroup'] : 0;
    $patientAttributeSelectCode  .= addSelectOption($ageGroup, 0,  "[[HTML]]select a predefined patient age group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").
                                    addSelectOption($ageGroup, 1,  "Age 0 - 10 years").
                                    addSelectOption($ageGroup, 2,  "Age 11 - 20 years").
                                    addSelectOption($ageGroup, 3,  "Age 21 - 30 years").
                                    addSelectOption($ageGroup, 4,  "Age 31 - 40 years").
                                    addSelectOption($ageGroup, 5,  "Age 41 - 50 years").
                                    addSelectOption($ageGroup, 6,  "Age 51 - 60 years").
                                    addSelectOption($ageGroup, 7,  "Age 61 - 70 years").
                                    addSelectOption($ageGroup, 8,  "Age 71 - 80 years").
                                    addSelectOption($ageGroup, 9,  "Age 81 - 90 years").
                                    addSelectOption($ageGroup, 10, "Age 91 - 100 years").
                                    addSelectOption($ageGroup, 11, "Age 101 - 110 years").
                                    addSelectOption($ageGroup, 12, "Age 111 - 120 years").
                                    addSelectOption($ageGroup, 13, "Birthday is missing or invalid").
                                    "</select>\n".
                                    "<br />\n".
    //---------- Age group


    //---------- RegistrationAge group
                                    "Regist.Age:&nbsp;".
                                    "<input type=\"text\" id=\"master_PatientRegAgeGroupFrom\" name=\"master_PatientRegAgeGroupFrom\" value=\"$escRegAgeGroupFrom\" placeholder=\"from...\" size=\"5\">".
                                    "&nbsp;&nbsp;to&nbsp;&nbsp;".
                                    "<input type=\"text\" id=\"master_PatientRegAgeGroupTo\" name=\"master_PatientRegAgeGroupTo\" value=\"$escRegAgeGroupTo\" placeholder=\"to...\" size=\"5\">".
                                    "&nbsp;&nbsp;or&nbsp;&nbsp;".
                                    "<select id=\"master_PatientRegAgeGroup\" name=\"master_PatientRegAgeGroup\" style=\"font-size:8pt;\">\n";
    $regAgeGroup = (isset($_REQUEST['master_PatientRegAgeGroup'])) ? $_REQUEST['master_PatientRegAgeGroup'] : 0;
    $patientAttributeSelectCode  .= addSelectOption($regAgeGroup, 0,  "select a predefined registration age group").
                                    addSelectOption($regAgeGroup, 1,  "Age 0 - 10 years").
                                    addSelectOption($regAgeGroup, 2,  "Age 11 - 20 years").
                                    addSelectOption($regAgeGroup, 3,  "Age 21 - 30 years").
                                    addSelectOption($regAgeGroup, 4,  "Age 31 - 40 years").
                                    addSelectOption($regAgeGroup, 5,  "Age 41 - 50 years").
                                    addSelectOption($regAgeGroup, 6,  "Age 51 - 60 years").
                                    addSelectOption($regAgeGroup, 7,  "Age 61 - 70 years").
                                    addSelectOption($regAgeGroup, 8,  "Age 71 - 80 years").
                                    addSelectOption($regAgeGroup, 9,  "Age 81 - 90 years").
                                    addSelectOption($regAgeGroup, 10, "Age 91 - 100 years").
                                    addSelectOption($regAgeGroup, 11, "Age 101 - 110 years").
                                    addSelectOption($regAgeGroup, 12, "Age 111 - 120 years").
                                    addSelectOption($regAgeGroup, 13, "Registration Date is invalid ").
                                    "</select>\n".
    //---------- RegistrationAge group

    //---------- ZipCode
                                    "<br />Zip Code:&nbsp;&nbsp;&nbsp;<small>&nbsp;</small>".
                                    "<input type=\"text\" id=\"master_PatientZipCode\" name=\"master_PatientZipCode\" value=\"$zipcode\">".
    //---------- ZipCode

    //---------- UrbanRural
                                    "<br />Urban/Rural:&nbsp;<small>&nbsp;</small>$urbanRuralSelectCode";
    //---------- UrbanRural




    $customSelectCode = '';
    for ($i=1; $i <= 3; $i++) {
        $customSelectCode   .= "<select data-placeholder=\"Select a field...\" class=\"chosen-select button\" id=\"custom_field_$i\" name=\"custom_field_$i\">\n".
                               "    <option value=''>Select a field...</option>\n".
                               "    <option value='site_code'>Site Code</option>\n".
                               "    <option value='doctor'>Doctor</option>\n".
                               "    <option value='doctor_id'>Doctor Id</option>\n".
                               "    <option value='trial'>Trial</option>\n".
                               "    <option value='grp'>Group</option>\n".
                               "    <option value='phase'>Phase</option>\n".
                               "    <option value='pat'>Patient</option>\n".
                               "    <option value='pat_id'>Patient Identifier</option>\n".
                               "    <option value='birthdate'>Birthdate</option>\n".
                               "    <option value='sex'>Sex</option>\n".
                               "    <option value='race'>Race</option>\n".
                               "    <option value='multirace'>MultiRacial</option>\n".
                               "    <option value='ethnic'>Ethnicity</option>\n".
                               "    <option value='zip'>Zip Code</option>\n".
                               "    <option value='urbanrural'>Urban/Rural</option>\n".
                               "    <option value='regdate'>Registration Date</option>\n".
                               "    <option value='creadate'>Creation Date</option>\n".
                               "    <option value='upddate'>Update Date</option>\n".
                               "    <option value='trialcred'>Trial Credit</option>\n".
                               "    <option value='qol'>QoL</option>\n".
                               "    <option value='qolc'>QoLC</option>\n".
                             //"    <option value='creditor'>Creditor</option>\n".
                             //"    <option value='txaccruals'>TX_Accruals</option>\n".
                             //"    <option value='ccaccruals'>CC_Accruals</option>\n".
                             //"    <option value='ccdraccruals'>CCDR_Accruals</option>\n".
                             //"    <option value='totalaccruals'>Total_Accruals</option>\n".
                               "</select>\n".
                               "&nbsp;&nbsp;&nbsp;".
                               "<select data-placeholder=\"Condition...\" class=\"chosen-select button\" id=\"custom_cond_$i\" name=\"custom_cond_$i\">\n".
                               "    <option value=''>Condition...</option>\n".
                             //"    <option value='is'>is</option>\n".
                             //"    <option value='is_not'>is not</option>\n".
                               "    <option value='contains'>contains</option>\n".
                               "    <option value='not_contains'>does not contain</option>\n".
                               "    <option value='is_not_like'>is not like ( use * as wildcard)</option>\n".
                               "    <option value='is_like'>is like ( use * as wildcard)</option>\n".
                               "    <option value='is_not_like'>is not like ( use * as wildcard)</option>\n".
                               "    <option value='is_larger'>is larger than</option>\n".
                               "    <option value='is_smaller'>is smaller than</option>\n".
                               "    <option value='is_null'>is NULL</option>\n".
                               "    <option value='is_not_null'>is not NULL</option>\n".
                               "</select>\n".
                               "&nbsp;&nbsp;&nbsp;".
                               "<input type='text' id='custom_value_$i' name='custom_value_$i'><br />";                                      
    }



    $sepline = (isset($dependencies['use_sepline'])) ? '<tr><td colspan="20">&nbsp;</td></tr>' : '';
    $content = "\n".<<< __END__
        <tr>
            <td class="left" colspan="15"><strong><b>Master Report</b></strong></td>
            <td class="right" colspan="5">
            <!-- <input type="submit" class="smaller" id="master_submit_html"  name="master_submit_html"  value="Html">&nbsp;&nbsp; -->
                 <input type="submit" class="smaller" id="master_submit_print" name="master_submit_print" value="View">&nbsp;&nbsp;
                 <input type="submit" class="smaller" id="master_submit_excel" name="master_submit_excel" value="Export">
            </td>
        </tr>
        $sepline
        <tr>
            <td class="left" valign="top" colspan="5">
                <b>Type Of Report:</b>
            </td>
            <td class="left" colspan="15">
                <select data-placeholder="Type Of Report" id="master_report_type" name="master_report_type" class="chosen-select button">
                    <option value="none">No Grouping (Flat List)</option>
                    <option value="trials">Group By Trials</option>
                    <option value="components">Group By Components</option>
                    <option value="sites">Group By Sites</option>
                    <option value="doctors">Group By Doctors</option>
                    <option value="researchbases">Group By Research Base</option>
                </select>
            </td>
        </tr>




        <tr>
            <td class="left" colspan="5">
                Trial Filter:
            </td>
            <td class="left" colspan="15">
                $trialSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                Trial Type Filter:
            </td>
            <td class="left" colspan="15">
                $trialTypeSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                Trial Category Filter:
            </td>
            <td class="left" colspan="15">
                $trialCategorySelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                Component Filter:
            </td>
            <td class="left" colspan="15">
                $compSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                Site Filter:
            </td>
            <td class="left" colspan="15">
                $siteSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                Doctor Filter:
            </td>
            <td class="left" colspan="15">
                $doctorSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" colspan="5">
                Patient Identifier Filter:
            </td>
            <td class="left" colspan="15">
                $patientIdentifierSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" valign="top" colspan="5">
                Patient Attribute Filters:
            </td>
            <td class="left" colspan="15">
                $patientAttributeSelectCode
            </td>
        </tr>
        <tr>
            <td class="left" valign="top" colspan="5">
                Custom Filters:
            </td>
            <td class="left" colspan="15">
                $customSelectCode
            </td>
        </tr>
        <tr><td colspan="20"><br /><hr/></td></tr>
__END__;

    $content = dialogAddIndentation($content, 20);
    return $content;
}


/**
 * Report Section containing Tracking Report
 *
 * @param  array  $dependencies optional         Various needed object instances and other dependencies
 * @return string $content
 */
function dialogAddTrackingReportDialog($dependencies=array(), $reportStartDate='', $reportEndDate='') {
    $content = '';

    $trackingEndTime   = date('Y/m/d');
    $trackingStartTime = date('Y/m/d');

    /*
    $sepline = (isset($dependencies['use_sepline'])) ? '<tr><td colspan="20">&nbsp;</td></tr>' : '';
    $content = "\n".<<< __END__
        <tr>
            <td class="left" colspan="15"><strong><b>Change Tracking Report</b></strong></td>
            <td class="right" colspan="5">
            <!-- <input type="submit" class="smaller" id="master_submit_html"  name="master_submit_html"  value="Html">&nbsp;&nbsp; -->
                 <input type="submit" class="smaller" id="tracking_submit_print" name="tracking_submit_print" value="View">&nbsp;&nbsp;
                 <input type="submit" class="smaller" id="tracking_submit_excel" name="tracking_submit_excel" value="Export">
            </td>
        </tr>
        $sepline
        <tr>
            <td class="left" colspan="3"><strong>Tracking Start:</strong></td>
            <td class="left" colspan="6"><input type="text" name="TrackingStartDate" id="TrackingStartDate" size="32" class="dpick req" value="$trackingStartTime" /> 00:00:00</td>
            <td class="left" colspan="1">&nbsp;</td>
            <td class="left" colspan="3"><strong>Tracking End:</strong></td>
            <td class="left" colspan="6"><input type="text" name="TrackingEndDate" id="TrackingEndDate" size="32" class="dpick req" value="$trackingEndTime" /> 23:59:59</td>
            <td class="left" colspan="1">&nbsp;</td>
        </tr>
        <tr>
            <td class="left" colspan="3"><strong>Username:</strong></td>
            <td class="left" colspan="6"><input type="text" name="TrackingUserName" id="TrackingUserName" size="32" value="" /></td>
            <td class="left" colspan="1">&nbsp;</td>
            <td class="left" colspan="3"><strong>Parent Table:</strong></td>
            <td class="left" colspan="6"><input type="text" name="TrackingParentTable" id="TrackingParentTable" size="32" value="" /></td>
            <td class="left" colspan="1">&nbsp;</td>
        </tr>

    <tr><td colspan="20"><br /><hr/></td></tr>
__END__;
    */

    $content = dialogAddIndentation($content, 20);

    return $content;
}

