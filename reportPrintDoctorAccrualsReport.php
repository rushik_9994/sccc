<?php
/**
 * Print the 'Doctor Accruals Report'
 */

include_once('reportCommonFunctions.php');
include_once('reportPrintFunctions.php');

session_start();
if (empty($_SESSION['UserKeyID'])) {
    header('location: login.php'); exit;
}


//Start our session, extract POST and GET from SESSION
list($connection, $reportInfo) = commonStartNewReport('Doctor Accruals Report');

$reportInfo['printoutTitle'] = $reportInfo['reportTitle'] . 
                               '     '.
                               'from '.$_POST['ReportStartDate'].
                               '   '.
                               'to '.  $_POST['ReportEndDate'];

/*Debug
echo '<pre>';
var_dump($reportInfo);
echo '</pre>';
exit;
*/


printLibraryGeneratePrintout($connection, $reportInfo, 'callbackPrintDoctorAccrualsReport');


/**
 * Callback function allows us to modify all print behavior
 */
function callbackPrintDoctorAccrualsReport($what, $connection, $reportInfo, $cbInfo) {
    if ($what=='start_the_printout') {
        $reportInfo['totals'] = array('TX_Accruals'=>0, 'CC_Accruals'=>0, 'CCDR_Accruals'=>0, 'PX_Accruals'=>0, 'Total_Accruals'=>0);
    } else if ($what=='before_one_row') {

    } else if ($what=='after_one_row') {

        // Replaced the commented lines below with foreach loop
        foreach ($reportInfo['totals'] as $key => $value) {
            $reportInfo['totals'][$key] += $cbInfo['row'][$key];
        }
        // $reportInfo['totals']['TX_Accruals']    += $cbInfo['row']['TX_Accruals'];
        // $reportInfo['totals']['CC_Accruals']    += $cbInfo['row']['CC_Accruals'];
        // $reportInfo['totals']['PX_Accruals']    += $cbInfo['row']['PX_Accruals'];
        // $reportInfo['totals']['Total_Accruals'] += $cbInfo['row']['Total_Accruals'];
        
    } else if ($what=='end_the_printout') {
        $lastRow = ((isset($cbInfo['lastRow'])) && (is_array($cbInfo['lastRow']))) ?  $cbInfo['lastRow'] : array();
        $cbInfo['cont']  = "        <tr><td colspan=\"".$reportInfo['columnCount']."\"><hr/></td></tr>\n";

        if (count($lastRow) > 0) {
            $cbInfo['cont'] .= "        <tr>\n";
            foreach($lastRow as $key=>$val) {
                $cbInfo['cont'] .= "            <td><b>".htmlentities($key)."</b></td>\n";
            }
            reset($lastRow);
            $cbInfo['cont'] .= "        </tr>\n";
        }

        //TOTALS  Accrual-Totals
        $cbInfo['cont'] .= "        <tr>\n";
        foreach($lastRow as $key=>$val) {
            if ($key=='IRB') {
                $cbInfo['cont'] .= "            <td><b>TOTALS</b></td>\n";
            } else if ($key=='TX_Accruals') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['TX_Accruals']."</b></td>\n";
            } else if ($key=='CC_Accruals') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['CC_Accruals']."</b></td>\n";
            } else if ($key=='CCDR_Accruals') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['CCDR_Accruals']."</b></td>\n";
            } else if ($key=='PX_Accruals') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['PX_Accruals']."</b></td>\n";
            } else if ($key=='Total_Accruals') {
                $cbInfo['cont'] .= "            <td><b>".$reportInfo['totals']['Total_Accruals']."</b></td>\n";
            } else {
                $cbInfo['cont'] .= "            <td>&nbsp;</td>\n";
            }
        }
        reset($lastRow);
        $cbInfo['cont'] .= "        </tr>\n".
                           "    </tbody>\n".
                           "</table>\n";
    }

    return array($reportInfo, $cbInfo);
}





