<?php
error_reporting(0);
/**
 * Patient management page
 */

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Comp.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_component_name_0';
    $_REQUEST['order_direction_0'] = 'asc';
}

//create required objects
$components  = new Comp;

//define required variables
$msg = '';

//Check permissions
$canEditRows = checkPermissionString('AddEditComponents');
$canViewRows = checkPermissionString('ViewComponents');

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'components.pagination.size');

//searchFilter setup
$sFilter  = new SearchFilter('components');
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, 0, $cfg);

$ComponentKeyIDs = $components->GetKeysOrderBy('CompKeyID', 
                                               '', 
                                               'CompKeyID', 
                                               'ASC', 
                                               $pgInfo[0]['from'], 
                                               $pgInfo[0]['limit'], 
                                               $sClauses);  //pagination added;
$totalRowCount   = $components->totalRowCount;

//begin building output
$indexText = '_0';
$components_list  = "<a name='tb_0'></a>\n".
                    "<table class=\"fullwidth zebra sort\">\n".
                    "<thead>\n".
                    "    <tr class='thead_unselected'>\n".
                    $pg->generateTheadLine('order_component_name'.$indexText, 'Component Name').
                    $pg->generateTheadLine('order_cra_name'.$indexText, 'CRA Name').
                    $pg->generateTheadLine('order_cra_phone'.$indexText, 'CRA Phone').
                    $pg->generateTheadLine('order_cra_email'.$indexText, 'CRA Email');
if (($canViewRows) || ($canEditRows)) {
    $components_list .= $pg->generateTheadLine('order_action'.$indexText, '&nbsp;&nbsp;&nbsp;Action', array('right'=>1, 'nosort'=>1));
} else {
    $components_list .= $pg->generateTheadLine('order_blank'.$indexText, '&nbsp;', array('nosort'=>1));
}
$components_list .= "    </tr>\n".
                    "</thead>\n".
                    "<tbody>\n";
foreach ($ComponentKeyIDs as $key) {
    $components->Load_from_key($key);

    //Grey out closed components
    $escStyle = '';
    $componentIsClosed = ($components->CompStatus) ? 0 : 1;
    if ((isset($cfg['styleForClosedComponents'])) && ($cfg['styleForClosedEntities'])) {
        $escStyle = ($componentIsClosed) ? " style=\"".$cfg['styleForClosedComponents']."\"" : '';
    }
    
    $div1='';
    $div2='';
    if ((isset($components->CompName)) && (strlen($components->CompName)>50)) {
        $div1 = "<div style='display:inline;font-size:9pt'>";
        $div2 = "</div>";
    }

    $components_list .= "<tr><td class=\"left\"$escStyle>$div1" . $components->CompName . "$div2</td>\n".
        "    <td class=\"left\"$escStyle>" . $components->CompCRAName . "</td>\n".
        "    <td class=\"left\" nowrap=\"nowrap\"><span class=\"phone\" nowrap=\"nowrap\"$escStyle>" . $components->CompCRAPhone . "</span>&nbsp;</td>\n".
        "    <td class=\"left\" nowrap=\"nowrap\"$escStyle>" . $components->CompCRAEmail . "</td>\n";
    if ($canEditRows) {
        $components_list .= "    <td class=\"center\"$escStyle><a href=\"compViewEdit.php?id=" . urlencode($key) . "\">View/Edit</a></td>\n";
    } elseif ($canViewRows) {
        $components_list .= "    <td class=\"center\"$escStyle><a href=\"compViewEdit.php?id=" . urlencode($key) . "\">View</a></td>\n";
    } else {
        $components_list .= "    <td class=\"center\"$escStyle>&nbsp;</td>\n";
    }
    $components_list .= "</tr>\n";
}
$components_list .= '</tbody>' . "\n";
$components_list .= '</table>' . "\n";

//add pagination display
$components_list .= $pg->generatePaginationCode($pgInfo, 0, $components, $totalRowCount);


?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Components
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
	</head>
	<body>
	    <form name='main_form' id='main_form' action='components.php' method='POST'>
<?php
        //pagination variables
        print $pg->generateFormVariables($pgInfo);
?>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('components');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<form >
                    <div class="subhead">
						Components
<?php                   if ($canEditRows) { ?>
                            <a href="compAddEdit.php"><button type="button" class="button">Add Component</button></a>
<?php                   }
                        displaySuccessAndErrors();
?>
                    </div>
                        <hr />
<?php
                        print $sFilter->generateSearchFilterCode(0);
?>
                        <div class="msg"><?php echo $msg; ?></div>
                        <?php echo $components_list; ?>
					</form>
				</div>
			</div>
		</div>
	    </form>
	</body>
</html>
<?php
