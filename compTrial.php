<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Comp.class.php');
require_once ('lib/Trial.class.php');
require_once ('lib/TrialCompJoin.class.php');
require_once ('lib/Site.class.php');
require_once ('lib/SiteTrialsOpenJoin.class.php');

switch ($_GET['action']) {
    case 'insert':
        $prefix = "Add";
        break;
    case 'update':
        $prefix = "Update";
        break;
    default:
        $prefix = "";
}

$pageURL = getPageUrl();

//create required objects
$comp  = new Comp();
$trial = new Trial();
$tcj   = new TrialCompJoin();
$site  = new Site();
$stoj  = new SiteTrialsOpenJoin();

$comp->Load_from_key($_GET['id']);
if (!empty($_GET['tcjid'])) {
    $tcj->Load_from_key($_GET['tcjid']);
}
if (!empty($_GET['tid'])) {
    $trial->Load_from_key($_GET['tid']);
    $selectTrials = $trial->CreateSelectWhere('WHERE TrialKeyID = ' . $_GET['tid'], $_GET['tid']);
} else {

    $used = $tcj->GetKeysWhereOrderBy('TrialKeyID','CompKeyID = '.$_GET['id'], 'TrialKeyID', 'ASC');
    $omit = $trial->GetKeysWhereOrderBy('TrialKeyID', 'WHERE ((TrialTermination=1) OR (TrialClose=1))', 'TrialKeyID', 'ASC');
    $omit = array_merge($omit, $used);
    if (count($omit) > 0) {
        $omitTrialIDs = join(',', $omit);
        $selectTrials = $trial->CreateSelectWhere('WHERE TrialKeyID NOT IN (' . $omitTrialIDs . ')', $_GET['tid']);
    } else {
        $selectTrials = $trial->CreateSelectWhere('', $_GET['tid']);
    }
} ?>
<html>
	<head>
    <script>
    $(document).ready(function(){
        $('#TrialKeyIDOUT').addClass('req');
        $('#TrialKeyIDOUT').attr('name','tcj[TrialKeyID]');
        $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', autoSize: true, changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
    <?php if (!empty($_GET['tid']))
{
    if ($trial->TrialTermination == 1)
    { ?>
                $('#compTrial input, #compTrial select').attr('disabled', true);
                $('.msg').show();
                <?php }
} ?>
    });
    function setNext(){
        var renewDate = $('#TrialCompRecentRenewalDate').val();
        var nextyear = (renewDate.substr(0,4) *1) + 1;
        var nextDate = nextyear + renewDate.substr(4,6);
        $('#TrialCompNextRenewalDate').val(nextDate);
    }
    </script>
	</head>
	<body>
        <form name="compTrial" id="compTrial" method="post" action="<?php echo $_GET['url']; ?>">
            <input type="hidden" name="tcj[CompKeyID]" value="<?php echo $_GET['id']; ?>" />
            <?php if (!empty($_GET['tcjid']))
{ ?>
            <input type="hidden" name="tcj[TrialCompKeyID]" value="<?php echo $_GET['tcjid']; ?>" />
            <?php } ?>
            <?php if (!empty($_GET['tid']))
{ ?>

            <input type="hidden" name="tcj[TrialKeyID]" value="<?php echo $_GET['tid']; ?>" />
            <?php } ?>
            <input type="hidden" id="compTrial_action" name="action" value="<?php echo $_GET['action']; ?>" />
            <div class="extra-padding">
                <div class="extra-padding">
    <?php if (!empty($_GET['tid']))
{
    if ($trial->TrialTermination == 1)
    { ?>
                    <div class="msg">Trial Protocol Number <?php echo $trial->TrialProtocolNumber; ?> is terminated and inaccessible.</div>
                <?php }
} ?>
                    <table class="centered fullwidth" style="width: 680px;">
                        <tr>
                          <td class="left"><strong>Trial:</strong><?php echo $selectTrials; ?></td>
                          <td class="left"><strong>Approval Date:</strong></td>
                          <td class="left"><input type="text" name="tcj[TrialCompApprovalDate]" id="TrialCompApprovalDate" size="32" class="dpick req" value="<?php echo (!empty($tcj->TrialCompApprovalDate)) ? $tcj->TrialCompApprovalDate : ''; ?>" /></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="left">
                          <?php if (!isset($_GET['tid'])) { ?>
                          <br><strong style="padding-bottom:8px;display:block">Add Component's Sites to Trial:</strong>
                          <?php } else { ?>
                          <br><strong style="padding-bottom:8px;display:block">Apply changes to these sites:</strong>
                          <?php } ?>

                          <?php 
                            $site_keys = $site->GetKeysWhereOrderBy('CompKeyID = \'' . $_GET['id'] . '\'', 'SiteKeyID', 'ASC');

                            // if trial id is set, select from stoj table where trialkeyid = trial id
                            if (isset($_GET['tid'])) {
                                $site_keys = $stoj->GetKeysWhereOrderBy('SiteKeyID',  'TrialKeyID = ' . $_GET['tid'], 'SiteKeyID', 'ASC');
                            }

                            if (count($site_keys) > 0)
                            {
                                foreach ($site_keys as $SiteKeyID)
                                {
                                    $site->Load_from_key($SiteKeyID);
                                    // Check if site belongs to component, otherwise skip
                                    if ($site->CompKeyID != $_GET['id']) {
                                        continue;
                                    } ?>
                                    <input id="<?php echo $site->SiteName ?>" type="checkbox" value="<?php echo $SiteKeyID; ?>" name="siteCheck[]"> <label for="<?php echo $site->SiteName ?>"><?php echo $site->SiteName ?></label><br>
                                    <?php
                                }
                            ?> 
                            <script type="text/javascript">
                                $(document).ready(function(){
                                   $('#select_all').change(function() {
                                        if($(this).is(":checked")) {
                                            $("[name*='siteCheck']").prop('checked', true);
                                        } else {
                                            $("[name*='siteCheck']").prop('checked', false);
                                        }
                                    });
                                });
                            </script>
                            <input id="select_all" type="checkbox"> <label for="select_all">Select All</label> 
                            <?php
                            }
                          ?>
                          </td>
                          <td class="left"><strong>Recent Renewal Date:</strong></td>
                          <td class="left"><input onchange="setNext();" type="text" name="tcj[TrialCompRecentRenewalDate]" id="TrialCompRecentRenewalDate" size="32" class="dpick" value="<?php echo (!empty($tcj->TrialCompRecentRenewalDate)) ? $tcj->TrialCompRecentRenewalDate : ''; ?>" /></td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="left"></td>
                          <td class="left"><strong>Next Renewal Date:</strong> 
                          </td>
                          <td class="left"><input type="text" name="tcj[TrialCompNextRenewalDate]" id="TrialCompNextRenewalDate" size="32" class="dpick" value="<?php echo (!empty($tcj->TrialCompNextRenewalDate)) ? $tcj->TrialCompNextRenewalDate : ''; ?>" /></td>
                          <td></td>
                        </tr>
                        <tr>
                            <td class="left"></td>
                            <td class="left"><?php if (!empty($tcj->TrialCompKeyID))
{ ?><strong>Close Date:</strong><?php } ?></td>
                            <td class="left"><?php if (!empty($tcj->TrialCompKeyID))
{ ?><?php if ($tcj->TrialCompClose == 1)
    { ?><input type="text" name="tcj[TrialCompCloseDate]" id="TrialCompCloseDate" size="32" class="dpick" value="<?php echo (!empty($tcj->TrialCompCloseDate)) ? $tcj->TrialCompCloseDate : ''; ?>" /><?php }
} ?></td>
                            <td><?php if (!empty($tcj->TrialCompKeyID))
{ ?><input type="checkbox"  onmouseup="$('div.extra-padding').hide();$('#confirm-close').show();window.parent.$.colorbox.resize();"/>&ensp;<?php echo ($tcj->TrialCompClose == 0) ? 'Close' : 'Unclose'; ?> Trial<?php } ?></td>
                        </tr>
    <?php if (($trial->TrialTermination != 1) || (empty($_GET['tid'])))
{ ?>
                        <tr>
                            <td class="right extra-padding" colspan="4"><a class="button floatnone" href="javascript:void(0);" onmouseup="return validate('compTrial');$('#compTrial').submit();">&emsp;Add/Update&emsp;</a>
                            <?php if (!empty($_GET['tcjid']))
    { ?>
                            <a class="button floatnone" href="javascript:void(0);" onmouseup="$('div.extra-padding').hide();$('#confirm_delete').show();window.parent.$.colorbox.resize();" >&emsp;Delete&emsp;</a></td>
                            <?php } ?>
                        </tr>
                <?php } ?>
                    </table>
                </div>
            </div>
           <!-- confirm delete -->
            <div id="confirm_delete" class="colorboxes">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete <?php echo $trial->TrialProtocolNumber; ?> for this Component?<br />Doing so will remove all associations with this Component and it's Sites, Patients and Doctors.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$('#compTrial_action').val('delete');$('#compTrial').submit();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            <!-- confirm close -->
            <div id="confirm-close" class="colorboxes">
                <table class="centered fullwidth initial">
                    <tr>
                    <?php if ($tcj->TrialCompClose == 0)
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Close <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will prevent updating of this Trial. You must also enter a Close Date and Comment for the Trial before updating.</td>
                    <?php } else
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Un-Close <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will enable updating of this Trial.</td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="$.colorbox({href:'confirm_compTrial.php?url=<?php echo $_GET['url']; ?>&id=<?php echo $tcj->TrialKeyID; ?>&compid=<?php echo $_GET['id'] ?>&tcjid=<?php echo $tcj->TrialCompKeyID; ?>&action=close&status=<?php echo $tcj->TrialCompClose; ?>'});">&emsp;Yes&emsp;</button></td>
                        <!-- <td class="center extra-padding"><button type="button" class="floatnone" onclick="$('#compTrial_action').val('close');$('#compTrial').submit();">&emsp;Yes&emsp;</button></td> -->
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm close -->            
         </form>
	</body>
</html>
<?php

