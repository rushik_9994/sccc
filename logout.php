<?php

//Indicate that commonBase should not do the authentication check
define('SKIP_BASE_AUTH_CHECK', 1);

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//logout
session_start();
session_destroy();
header("location: login.php");

