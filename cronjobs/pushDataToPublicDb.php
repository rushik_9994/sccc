<?php
/**
 * Purpose: Gather private data and clinicaltrials.gov data and push it to the Wordpress public DB
 *
 * Invoke: This SHOULD be invoked as a cronjob, however I don't have CLI on SCOR DEV, so
 *         this will also be invokable via the WEB (on DEV only!)
 *
 *         https://dev.morpheusrising.net/sccc/cronjobs/pushDataToPublicDb.php
 */

global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));
require_once(ROOT_PATH . '/cfg/config.php');
require_once(ROOT_PATH . '/lib/commonFunctions.php');

require_once(ROOT_PATH . '/lib/EzSql.php');

//in production only... make sure we cannot be called as a a WEB call
ensureCliExecutionOnly();

set_time_limit(0);
$startTime = time();
$error = exportScorDataToPublicDb();
if ($error) {
    die($error);
} else {
    print "OK (".(time() - $startTime).")\n";
}



/**
 * Export the SCOR data to the NEW WP public2 database
 * @return string
 */
function exportScorDataToPublicDb() {
    global $cfg;
    $error = '';
    try {
        //INPUT-DB
        $dsn = sprintf('mysql:host=%s;dbname=%s', $cfg['scordb']['host'], $cfg['scordb']['dbname']);
        $scorEzSql = new EzSql($dsn, $cfg['scordb']['user'], $cfg['scordb']['pass'], array(), $cfg['scordb']);

        //OUTPUT-DB
        $dsn = sprintf('mysql:host=%s;dbname=%s', $cfg['newPublicDb']['host'], $cfg['newPublicDb']['dbname']);
        $newPublicEzSql = new EzSql($dsn, $cfg['newPublicDb']['user'], $cfg['newPublicDb']['pass'], array(), $cfg['newPublicDb']);

        $cmd =
            'mysqldump'.
            ' -u "'. $cfg['scordb']['user'] . '"'.
            ' --password="'.$cfg['scordb']['pass']. '"'.
            ' -h '.$cfg['scordb']['host'] .
            ' --no-data'.
            ' ' . $cfg['scordb']['dbname'].
            ' 2>&1';
        $output = shell_exec($cmd);
        if (!$output) {
            $output = '';
        }
        $lines  = explode(';', $output);

        $jobs = array(
            array('table' => 'Comp'),
            array('table' => 'Doctor'),
            array('table' => 'DoctorSiteJoin', 'keyField' => 'DoctorSiteKeyID'),
            array('table' => 'DoctorSpecialty'),
            array('table' => 'Site'),
            array('table' => 'SiteTrialsOpenJoin', 'keyField' => 'SiteTrialOpenKeyID'),
            array('table' => 'States'),
            array('table' => 'Trial'),
            array('table' => 'TrialCategory'),
            array('table' => 'TrialCompanion'),
            array('table' => 'TrialCompJoin', 'keyField' => 'TrialCompKeyID'),
            array('table' => 'TrialDiseaseType'),
            array('table' => 'TrialGroup'),
            array('table' => 'TrialPhase'),
            array('table' => 'TrialTrialDiseaseTypeJoin', 'keyField' => 'TrialTrialDiseaseTypeKeyID'),
            array('table' => 'TrialType'),
        );

        //Supplement $jobs with DROP TABLE and CREATE TABLE queries
        $jobs = addTableCreateQueriesToAllJobs($lines, $jobs);

        foreach($jobs as $job) {
            copyTableToPublicDb($scorEzSql, $newPublicEzSql, $job);
        }

    } catch (\Exception $e) {
        $error = $e->getMessage();
    }

    return $error;
}

/**
 * Go through all given jobs, make sure we have a CREATE TABLE statement and add that to $jobs
 *
 * @param  array $lines
 * @param  array $jobs
 * @return array $jobs
 * @throws Exception
 */
function addTableCreateQueriesToAllJobs($lines, $jobs) {
    for ($i = 0, $maxi = count($jobs); $i < $maxi; $i++) {
        $job = $jobs[$i];
        $found = 0;
        $tableName = $job['table'];
        $lookingFor = 'CREATE TABLE `' . $tableName . '` (';
        foreach ($lines as $line) {
            if (($line) && (strpos($line, $lookingFor) !== false)) {
                $keyField = (isset($job['keyField'])) ? $job['keyField'] : '';
                if (!$keyField) {
                    $keyField = $tableName . 'KeyID';
                }
                if (strpos($line, '`' . $keyField . '`') !== false) {
                    $found = 1;
                    $jobs[$i]['dropTable'] = "DROP TABLE IF EXISTS `$tableName`";
                    $jobs[$i]['createTable'] = $line;
                    break;
                }
            }
        }
        if (!$found) {
            throw new \Exception("Create table query not found for table '$tableName'");
        }
    }

    return $jobs;
}

/**
 * Drop/Create the table and insert all rows in the target database
 *
 * @param EzSql $scorEzSql
 * @param EzSql $newPublicEzSql
 * @param array $job
 * @throws Exception
 */
function copyTableToPublicDb(EzSql $scorEzSql, EzSql $newPublicEzSql, array $job) {
    $tableName   = $job['table'];
    $keyField    = (isset($job['keyField'])) ? $job['keyField'] : '';
    $dropTable   = $job['dropTable'];
    $createTable = $job['createTable'];

    //Now drop and re-create the table
    if (($dropTable) && ($createTable)) {
        $newPublicEzSql->anyquery($dropTable);
        $error = $newPublicEzSql->getError();
        if ($error) {
            throw new \Exception("DROP TABLE failed on $tableName");
        }

        $newPublicEzSql->anyquery($createTable);
        $error = $newPublicEzSql->getError();
        if ($error) {
            throw new \Exception("CREATE TABLE failed on $tableName");
        }
    }

    //And finally fetch source-rows in slices and insert them into the target DB
    $from = 0;
    while (1) {
        $sql = "SELECT * FROM `$tableName` LIMIT $from, 100";
        $rows = $scorEzSql->select($sql);

        if (count($rows) == 0) {
            break;
        }
        $from += count($rows);
        foreach ($rows as $row) {
            $sql = "INSERT INTO `$tableName` ";
            $sql1 = '';
            $sql2 = '';
            foreach ($row as $key => $val) {
                $sql1 .= ", `$key`";
                $sql2 .= ", " . $newPublicEzSql->quote($val);
            }
            $sql1 = trim($sql1, ', ');
            $sql2 = trim($sql2, ', ');
            $sql .= "($sql1) VALUES ($sql2)";

            $newPublicEzSql->insert($sql);
        }
        //print "<br />$from .... $tableName";
    }//wend
}

