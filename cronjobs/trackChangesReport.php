<?php
/**
 * Purpose: Export tracked changes during the last n days
 *
 * Invoke: This SHOULD be invoked as a cronjob, however I don't have CLI on SCOR DEV, so
 *         this will also be invokable via the WEB (on DEV only!)
 *
 *         If invoked via browser then you MUST supply 'pass'='1pass4web'
 *
 *         https://dev.morpheusrising.net/sccc/cronjobs/trackChangesReport.php
 *         https://dev.morpheusrising.net/sccc/cronjobs/trackChangesReport.php?days=1&pass=1pass4web
 *         https://dev.morpheusrising.net/sccc/cronjobs/trackChangesReport.php?hour=24&pass=1pass4web&format=php&nodetails=1
 */

set_time_limit(0);

global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));
require_once(ROOT_PATH . '/cfg/config.php');
require_once(ROOT_PATH . '/lib/commonFunctions.php');
require_once(ROOT_PATH . '/lib/EzSql.php');

//in production only... make sure we cannot be called as a a WEB call
ensureCliExecutionOnly();


//Collect and validate parameters
$error = '';
$par = getAppParameters();

$days = (isset($par['days'])) ? $par['days'] : 0;
if (isset($par['day'])) {
    $days = $par['day'];
    $par['days'] = $days;
}
$hours = (isset($par['hours'])) ? $par['hours'] : 0;
if (isset($par['hour'])) {
    $hours = $par['hour'];
    $par['hours'] = $hours;
}

if ($par['isCli'] == 0) {
    if ((!isset($par['pass'])) || ($par['pass'] != '1pass4web')) {
        $error = "Mandatory parameter 'pass' missing or invalid";
    }
}

if ($hours) {
    if ((!is_numeric($hours)) || ($hours < 1)) {
        $error = "Parameter 'hours' is invalid";
    }
} else {
    if ((!is_numeric($days)) || ($days < 1)) {
        $error = "Mandatory parameter 'days' missing or invalid";
    }
}
if (($error) || (count($par) == 1) || (isset($par['h']) || ($par['help']))) {
    showHelpScreenAndDie($par, $error);
}

printTrackedChanges($par);
exit;


/**
 * Print all tracked changes
 * @param $par
 */
function printTrackedChanges($par) {
    global $cfg;
    $days  = (isset($par['days']))  ? $par['days'] : 0;
    $hours = (isset($par['hours'])) ? $par['hours'] : 0;
    $format = (isset($par['format'])) ? $par['format'] : '';
    $noDetails = (isset($par['nodetails'])) ? 1 : 0;

    try {
        //INPUT-DB
        $dsn = sprintf('mysql:host=%s;dbname=%s', $cfg['scordb']['host'], $cfg['scordb']['dbname']);
        $scorEzSql = new EzSql($dsn, $cfg['scordb']['user'], $cfg['scordb']['pass'], array(), $cfg['scordb']);

        $timeClause = ($hours) ? " INTERVAL $hours HOUR" : " INTERVAL $days DAY";
        $sql =
            "SELECT *\n".
            "FROM   TrackInfo\n".
            "WHERE  EventTime >= DATE_SUB(NOW(), $timeClause)\n".
            "ORDER BY TrackInfoID ASC\n".
            "LIMIT 50000";
        //print "<pre>".htmlentities($sql)."</pre>";
        $rows = $scorEzSql->select($sql);
        if (count($rows) > 499990) {
            throw new \Exception('Too many rows to export!');
        }
        if ($noDetails) {
            $remove = array('TrackInfoID', 'EventID', 'NumChanges');
            for ($i=0,$maxi=count($rows); $i < $maxi; $i++) {
                foreach($remove as $key) {
                    unset($rows[$i][$key]);
                }
            }
        }

        if ($format == 'json') {
            if (defined('JSON_PRETTY_PRINT')) {
                print json_encode($rows, JSON_PRETTY_PRINT);
            } else {
                print json_encode($rows);
            }
            print "\n";
        } else if ($format == 'php') {
            if (isWebCall()) {
                print "<pre>"; print htmlentities(print_r($rows, true)); print "</pre>";
            } else {
                print_r($rows);
            }
        }

    } catch (\Exception $e) {
        print "\nERROR: ".$e->getMessage()."\n";
    }
}


/**
 * Display help screen and die
 * @param $par
 * @param $error
 */
function showHelpScreenAndDie($par, $error) {
    $errorPrefix = ($error) ? "\nERROR: ".htmlentities($error)."\n\n" : '';
    $cont = <<< __END__

==============================================================================$errorPrefix
USAGE: ./trackChangesReport.php -days=N

PURPOSE:              Export tracked changes that happened during the last 1,2,... days

PARAMETERS:
--days=N              Number of days to read back (1=last_24_hours, 2=last_48_hours, etc.)
--hours=N             Number of hours to read back (either days or hours must be given)
--format=name         The string 'name' can be php, json
--nodetails           If given then some technical details are removed

EXAMPLES (CLI and WEB):
./trackChangesReport.php --days=1 --format=php
./trackChangesReport.php?hour=24&pass=[SUPPRESSED]&format=php&nodetails=1
==============================================================================

__END__;

    if (isWebCall()) {
        $cont = str_replace("\r", '', $cont);
        $cont = str_replace("\n", "<br />\n", $cont);
    }

    print $cont;
    exit;
}


