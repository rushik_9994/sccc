<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Comp.class.php');
require_once(ROOT_PATH.'/lib/States.class.php');
require_once(ROOT_PATH.'/lib/Doctor.class.php');
require_once(ROOT_PATH.'/lib/DoctorSiteJoin.class.php');
require_once(ROOT_PATH.'/lib/DoctorSpecialty.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/SiteTrialsOpenJoin.class.php');
require_once(ROOT_PATH.'/lib/Comments.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults  (test with this url:  http://www.morpheusrising.net/sccc/siteViewEdit.php?id=4)
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_trial_protocol_0';
    $_REQUEST['order_direction_0'] = 'asc';
}
if ((!isset($_REQUEST['order_field_1'])) || (!$_REQUEST['order_field_1'])) {
    $_REQUEST['order_field_1']     = 'order_doctors_1';
    $_REQUEST['order_direction_1'] = 'asc';
}

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'siteViewEdit.pagination.size');

//searchFilter setup for first results table (Trials)
$sFilter  = new SearchFilter('trials'); //----!!! should not be trials!
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, $cfg);


//Enforce user permissions or redirect to login.php
$canEditRows = checkPermissionString('AddEditSites');
$canViewRows = checkPermissionString('ViewSites');
if ((!$canEditRows) && (!$canViewRows)) {
    header('location: login.php');  exit;
}

//create required objects
$site            = new Site();
$comp            = new Comp();
$states          = new States();
$doctor          = new Doctor();
$doctorSite      = new DoctorSiteJoin();
$doctorSpecialty = new DoctorSpecialty();
$trial           = new Trial();
$stoj            = new SiteTrialsOpenJoin();
$comment         = new Comments();

$pageURL = getPageUrl();

try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        //      if((preg_match('/.+_trial$/',$action))||(preg_match('/*close/',$action))||(preg_match('/*terminate/',$action))){
        if (!empty($_POST['stoj']['SiteTrialOpenKeyID'])) {
            $stoj->Load_from_key($_POST['stoj']['SiteTrialOpenKeyID']);
        }
        foreach ($_POST['stoj'] as $key => $val) {
            $stoj->$key = $val;
        }
        // }
        if ($action == 'insert_trial') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'add trial to site', $action, $_POST);
            $stoj->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }
        if ($action == 'update_trial') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'update trial on site', $action, $_POST);
            $stoj->Save_Active_Row();
            TrackChanges::endUserAction();
        }
        if ($action == 'delete_trial') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete trial from site', $action, $_POST);
            $stoj->Delete_row_from_key('SiteTrialOpenKeyID', $stoj->SiteTrialOpenKeyID);
            TrackChanges::endUserAction();
        }

        if ($action == 'close') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'close site', $action, $_POST);
            $stoj->SiteTrialOpenClose = ($stoj->SiteTrialOpenClose == 1) ? 0 : 1;
            $stoj->Save_Active_Row();
            $comment->TrialKeyID = $stoj->TrialKeyID;
            $comment->Comment = $_POST['comment'];
            $comment->SiteKeyID = $stoj->SiteKeyID;
            $comment->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }

        if ($action == 'terminate') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'terminate site', $action, $_POST);
            $stoj->SiteTrialOpenTermination = ($stoj->SiteTrialOpenTermination == 1) ? 0 : 1;
            $stoj->Save_Active_Row();
            $comment->TrialKeyID = $stoj->TrialKeyID;
            $comment->Comment = $_POST['comment'];
            $comment->SiteKeyID = $stoj->SiteKeyID;
            $comment->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


//define required variables
$msg = '';

$site->Load_from_key($_GET['id']);
$comp->Load_from_key($site->CompKeyID);
$states->Load_from_key($site->SiteState);

$trialList = array();
$tsj_keys = $stoj->GetKeysWhereOrderBy('SiteTrialOpenKeyID',
                                       'SiteKeyID = \'' . $_GET['id'] . '\'',
                                       'SiteKeyID',
                                       'ASC',
                                       $sClauses);
$i = 0;
if (count($tsj_keys) > 0) {
    if (checkPermissionString('AddEditSites')) {
        foreach ($tsj_keys as $stoj_key) {
            $stoj->Load_from_key($stoj_key);
            $trial->Load_from_key($stoj->TrialKeyID);
            $trialList[$i] = '<tr><td class="left">' . $trial->TrialProtocolNumber . '&emsp;</td><td class="left">' . $stoj->SiteTrialOpenApprovalDate . '</td><td class="left">' . str_replace('0000-00-00','', $stoj->SiteTrialOpenCloseDate) . '</td><td class="left">' . str_replace('0000-00-00','', $stoj->SiteTrialOpenTerminationDate) . '</td><td class="left"><a class="colorbox" href="siteTrial.php?id=' . $_GET['id'] . '&stojid=' . $stoj->SiteTrialOpenKeyID . '&tid=' . $stoj->TrialKeyID . '&compid=' . $site->CompKeyID . '&action=update_trial&url=' . $pageURL . '">Edit</a></td></tr>';
            $i++;
        }
    } else {
        foreach ($tsj_keys as $stoj_key) {
            $stoj->Load_from_key($stoj_key);
            $trial->Load_from_key($stoj->TrialKeyID);
            $trialList[$i] = '<tr><td class="left">' . $trial->TrialProtocolNumber . '&emsp;</td><td class="left">' . $stoj->SiteTrialOpenApprovalDate . '</td><td class="left">' . str_replace('0000-00-00','', $stoj->SiteTrialOpenCloseDate) . '</td><td class="left">' . str_replace('0000-00-00','', $stoj->SiteTrialOpenTerminationDate) . '</td><td class="left">
            </td></tr>';
            $i++;
        }
    }
}

$doctorList = array();
$dsj_keys = $doctorSite->GetKeysWhereOrderBy('DoctorKeyID',
                                             'SiteKeyID = \'' . $_GET['id'] . '\'',
                                             'SiteKeyID',
                                             'ASC',
                                             $sClauses);
$i = 0;
if (count($dsj_keys) > 0) {
    foreach ($dsj_keys as $DoctorSiteKeyID) {
        $doctor->Load_from_key($DoctorSiteKeyID);
        $doctorSpecialty->Load_from_key($doctor->DoctorSpecialty);
        $doctorList[$i] = '<tr><td class="left">' . $doctor->DoctorFirstName . ', ' . $doctor->DoctorLastName . '&emsp;</td><td class="left">' . $doctorSpecialty->DoctorSpecialtyText . '&emsp;</td><td class="left">' . $doctor->DoctorEmail . '</td></tr>';
        $i++;
    }
}



?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html" />
        <meta name="author" content="Cliff Garrett" />
        <title>
            Site View/Edit
        </title>
        <?php require_once ("lib/common.includes.php"); ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $('select').addClass('fullwidth');
               $("a.confirm-delete").colorbox({inline:true});
               $("#site-button, a.colorbox").colorbox();
               <?php if (!empty($_GET['id']))
{ ?>
               $('#action').val('update');
               <?php } else
{ ?>
               $('#action').val('insert');
               <?php } ?>
            });
        </script>
    </head>
    <body>
        <form id='main_form' name='main_form' method='POST'>
<?php
        print $pg->generateFormVariables($pgInfo);
?>
        <div class="wrapper">
            <div class="logo">
            </div>
            <div class="ui-tabs">
                    <?php
                    print displayTopRightInfo();
                    print displayTabs('sites');
                    ?>
                    <div id="tabs-1" class="ui-tabs-panel">
                        <div class="subhead">
                            Site View/Edit
<?php                       if ($canEditRows) { ?>
                                <a class="button" href="siteAddEdit.php">Add Site</a>
                                <a id="main-button" class="button" href="siteAddEdit.php?id=<?php echo $_GET['id']; ?>">Edit Site</a>
<?php                       } ?>
                            <a href="sites.php" class="button">Back</a>
                            <?php displaySuccessAndErrors(); ?>
                        </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                        <table border="0" class="fullwidth">
                            <tr>
                                <td class="left"><strong>Site Name:</strong> <?php echo (!empty($site->SiteName)) ? $site->SiteName : ''; ?></td>
                                <td class="left"><strong>Site Code:</strong> <?php echo (!empty($site->SiteCode)) ? $site->SiteCode : ''; ?></td>
                                <td class="left"><strong>Component:</strong> <?php echo (!empty($comp->CompKeyID)) ? $comp->CompName : ''; ?></td>
                            </tr>
                            <tr>
                                <td class="left" colspan="2"><strong>Address 1:</strong> <?php echo (!empty($site->SiteAddress1)) ? $site->SiteAddress1 : ''; ?></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="left" colspan="2"><strong>Address 2:</strong> <?php echo (!empty($site->SiteAddress2)) ? $site->SiteAddress2 : ''; ?></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="left"><strong>City:</strong> <?php echo (!empty($site->SiteCity)) ? $site->SiteCity : ''; ?></td>
                                <td class="left"><strong>State:</strong> <?php echo $states->StateName; ?></td>
                                <td class="left"><strong>Zip:</strong> <span class="zip"><?php echo (!empty($site->SiteZip)) ? $site->SiteZip : ''; ?></span></td>
                            </tr>
                            <tr>
                                <td class="left"><strong>Phone Number:</strong> <span class="phone"><?php echo (!empty($site->SitePhone)) ? $site->SitePhone : ''; ?></span></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="left"><strong>Open Date:</strong> <?php echo (!empty($site->SiteOpenDate)) ? $site->SiteOpenDate : date('Y-m-d', time()); ?></td>
                                <td></td>
                                <td><strong>Status:</strong> <?php echo ($site->SiteStatus == 1) ? 'Active' : 'Inactive'; ?></td>
                            </tr>
                        </table>
                        <br /><hr />
                        <div class="clear"></div>
                        <div class="subhead">
<?php
                            if (checkPermissionString('AddEditSites')) {
?>
                                <a id="site-button" class="button" href="siteTrial.php?id=<?php echo $_GET['id']; ?>&compid=<?php echo $site->CompKeyID; ?>&url=<?php echo $pageURL; ?>&action=insert_trial">Add Trial</a>               
<?php
                            } 
?>
                        </div>
                        <div class="vertical-padding-light"><strong>Trials</strong></div><br>
<?php
                        if (count($trialList) > 0){
?>
                            <table class="fullwidth zebra sort">
                                <thead>
                                    <a name='tb_0'></a>
                                    <tr class='thead_unselected'>
<?php
                                        print
                                        $pg->generateTheadLine('order_trial_protocol_0',                 'Trial Protocol', array('isdefault'=>1)).
                                        $pg->generateTheadLine('order_recent_renewal_date_0',            'Recent Renewal Date').
                                        $pg->generateTheadLine('order_close_date_0',                     'Close Date').
                                        $pg->generateTheadLine('order_term_date_0',                      'Term Date').
                                        $pg->generateTheadLine('order_action_0',                         'Action', array('nosort'=>1));
?>
                                    </tr>
                                <tbody>
<?php
                                foreach ($trialList as $trialRow) {
                                    echo $trialRow;
                                }
?>
                                </tbody>
                            </table>
<?php
                        }
?>
                        <br /><hr />
                        <div class="vertical-padding-light"><strong>Doctors</strong></div><br>
<?php
                        if (count($doctorList) > 0) {
?>
                            <a name="tb_1"></a>
                            <table class="fullwidth zebra sort">
                                <thead>
                                    <tr class='thead_unselected'>
<?php
                                        print 
                                        $pg->generateTheadLine('order_doctors_1',   'Doctor Names', array('isdefault'=>1)).
                                        $pg->generateTheadLine('order_specialty_1', 'Specialty').
                                        $pg->generateTheadLine('order_email_1',     'Email');                                    
?>
                                    </tr>
                                <tbody>
<?php
                                    foreach ($doctorList as $doctorRow) {
                                        echo $doctorRow;
                                    }
?>
                                </tbody>
                            </table>
<?php
                        } 
?>
                    </div>
                </div>
            </div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete this Doctor?<br />Doing so will remove all associations with  Sites, Trials and Patients.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            
        </div>
        </form>
    </body>
</html>
<?php
