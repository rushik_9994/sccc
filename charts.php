<?php
/**
 * Main reporting module
 *
 * Uses: chosen plugin  https://github.com/harvesthq/chosen/releases/tag/v1.1.0
 */

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
include_once(ROOT_PATH.'/reportCommonFunctions.php');
require_once(ROOT_PATH.'/reportDialogFunctions.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Comp.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/TrialType.class.php');
require_once(ROOT_PATH.'/lib/TrialCategory.class.php');
require_once(ROOT_PATH.'/lib/Doctor.class.php');
require_once(ROOT_PATH.'/lib/Patient.class.php');
require_once(ROOT_PATH.'/lib/PatientIdentifier.class.php');

//create required objects
$sites                  = new Site;
$permissions            = new Permissions;
$compClass              = new Comp;
$trialClass             = new Trial;
$trialTypeClass         = new TrialType;
$trialCategoryClass     = new TrialCategory;
$doctorClass            = new Doctor;
$patientClass           = new Patient;
$patientIdentifierClass = new PatientIdentifier;

//define required variables
$msg = '';

//which Sites does the User have access to?
$permissions->Load_from_action("ViewSites");
$UserTypeIDs = explode(',', $permissions->UserTypeIDs);
$SiteKeyIDs  = $sites->GetKeysOrderBy('SiteKeyID', 'ASC');
//print "SiteKeys: <pre>"; print_r($SiteKeyIDs); print "</pre>";

//$reportStartDate = date('Y-m-d');
$reportStartDate = $cfg['fiscalyear.startdate'];     //Beginning of this fiscal year
$reportEndDate   = date('Y-m-d');                    //today

//begin building output
$reports_list = "\n".<<< __END__
                        <table class="fullwidth">
                            <tbody>
__END__;

$dependencies  = array();
//$dependencies['use_sepline'] = 1;   //If define then a separation line is added after report title

$dependencies['comp_class']               = $compClass;
$dependencies['trial_class']              = $trialClass;
$dependencies['trial_type_class']         = $trialTypeClass;
$dependencies['trial_category_class']     = $trialCategoryClass;
$dependencies['site_class']               = $sites;
$dependencies['doctor_class']             = $doctorClass;
$dependencies['patient_class']            = $patientClass;
$dependencies['patient_identifier_class'] = $patientIdentifierClass;

$reports_list .= dialogAddColumnWidthDefinitions($dependencies);
$reports_list .= dialogAddStartAndEndDate($dependencies, $reportStartDate, $reportEndDate);
$reports_list .= dialogAddCreditsReportDialog($dependencies);
$reports_list .= dialogAddPatientRegistrationReportDialog($dependencies);
$reports_list .= dialogAddDoctorAccrualsReportDialog($dependencies);
$reports_list .= dialogAddDoctorAccrualsBySiteReportDialog($dependencies);
$reports_list .= dialogAddMasterReportDialog($dependencies);

$reports_list .= "\n".<<< __END__
                            </tbody>
                        </table>
__END__;


?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Report Generation
		</title>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
        <script src="js/jquery.jdpicker.js"></script>
        <script src="js/chosen.jquery.js"></script>
		<script type="text/javascript">
        	$(document).ready(function(){
        		//$(".ui-tabs").tabs();
                $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', autoSize: true, changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});

                $('.chosen-select').chosen();
        	});
		</script>
		<link rel="stylesheet" href="css/chosen.css">
        <link rel="stylesheet" href="js/jdpicker.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('charts');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<form action="reportDispatcher.php" method="POST" target="_blank">
                        <div class="subhead">
    						Charts And Graphs Generation
                            <?php displaySuccessAndErrors(); ?>
                        </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                        <?php echo $reports_list; ?>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
<?php


