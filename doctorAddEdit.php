<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Permissions.class.php');
require_once ('lib/Doctor.class.php');
require_once ('lib/DoctorSiteJoin.class.php');
require_once ('lib/DoctorSpecialty.class.php');
require_once ('lib/States.class.php');
require_once ('lib/Patient.class.php');
$pageURL = getPageUrl();

//create required objects
$permissions     = new Permissions();
$doctor          = new Doctor();
$doctorSite      = new DoctorSiteJoin();
$doctorSpecialty = new DoctorSpecialty();
$states          = new States();
$patient         = new Patient();

try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        if (($action == 'update') || ($action == 'insert')) {
            foreach ($_POST['doctor'] as $key => $val) {
                $doctor->$key = $val;
                $doctor->DoctorPhone = preg_replace('/\D/', '', $doctor->DoctorPhone);
                $doctor->DoctorZip = preg_replace('/\D/', '', $doctor->DoctorZip);
            }
            $doctor->DoctorState = $_POST['StatesKeyID'];
            if ($action == 'update') {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'update doctor', $action, $_POST);
                $doctor->Save_Active_Row();
                TrackChanges::endUserAction();
            } else {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'add doctor', $action, $_POST);
                $doctor->Save_Active_Row_as_New();
                TrackChanges::endUserAction();
            }
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: doctorViewEdit.php?id=" . $doctor->DoctorKeyID);
            }
        }

        if ($action == 'delete') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete doctor', $action, $_POST);
            $doctor->Delete_row_from_key($_GET['id']);
            $doctorSite->Delete_row_from_key('DoctorKeyID', $_GET['id']);
            TrackChanges::endUserAction();
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: doctors.php");
            }
        }
        //header("location: $pageURL");

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }//!empty action

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


//define required variables
$msg = '';

//Enforce user permissions or redirect to login.php
list($UserTypeIDs) = enforceUserPermissions($permissions);

if (!empty($_GET['id'])) {
    //editing
    $doctor->Load_from_key($_GET['id']);
}
$statesSelect = $states->CreateSelect($doctor->DoctorState);
$doctorSpecialtySelect = $doctorSpecialty->CreateSelect($doctor->DoctorSpecialty);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
		<meta name="author" content="Cliff Garrett" />
		<title>
			Doctor Add/Edit
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
		<script type="text/javascript">
        	$(document).ready(function(){
			   $('select').addClass('fullwidth');
               $('#DoctorSpecialtyKeyID').addClass('req');
        	   $("a.confirm-delete").colorbox({inline:true});
               <?php if (!empty($_GET['id']))
{ ?>
               $('#action').val('update');
               <?php } else
{ ?>
               $('#action').val('insert');
               <?php } ?>
        	});
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                    <?php
                    print displayTopRightInfo();
                    print displayTabs('doctors');
                    ?>
    				<div id="tabs-1" class="ui-tabs-panel">
    					<form method="post" action="" name="mainform" id="mainform">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="continue" id="continue" />
                        <?php if ($_GET['id'])
{ ?>
                        <input type="hidden" name="doctor[DoctorKeyID]" id="DoctorKeyID" value="<?php echo $_GET['id']; ?>" />
                        <?php } ?>
                    <div class="subhead">
    					Doctor Add/Edit
                            <a href="<?php echo (!empty($_GET['id'])) ? 'doctorViewEdit.php?id=' . $_GET['id'] : 'doctors.php'; ?>" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                            <table border="0" class="fullwidth">
                              <tr>
                                <td class="right"><strong>First Name:</strong> </td>
                                <td class="right"><input class="fullwidth req" type="text" name="doctor[DoctorFirstName]" id="DoctorFirstName" value="<?php echo (!empty($doctor->DoctorFirstName)) ? $doctor->DoctorFirstName : ''; ?>" /></td>
                                <td class="right"><strong>Last Name:</strong> </td>
                                <td class="right"><input class="fullwidth req" type="text" name="doctor[DoctorLastName]" id="DoctorFirstName" value="<?php echo (!empty($doctor->DoctorLastName)) ? $doctor->DoctorLastName : ''; ?>" /></td>
                                <td class="right"><strong>Specialty:</strong> </td>
                                <td class="right"><?php echo $doctorSpecialtySelect; ?></td>
                                <td><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div></td>
                              </tr>
                              <tr>
                                <td class="right"><strong>Address 1:</strong> </td>
                                <td class="right" colspan="5"><input class="fullwidth" type="text" name="doctor[DoctorAddress1]" id="DoctorAddress1" value="<?php echo (!empty($doctor->DoctorAddress1)) ? $doctor->DoctorAddress1 : ''; ?>" /></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td class="right"><strong>Address 2:</strong> </td>
                                <td colspan="5"><input type="text" class="fullwidth" name="doctor[DoctorAddress2]" id="DoctorAddress2" value="<?php echo (!empty($doctor->DoctorAddress2)) ? $doctor->DoctorAddress2 : ''; ?>" /></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td class="right"><strong>City:</strong> </td>
                                <td><input class="fullwidth" type="text" name="doctor[DoctorCity]" id="DoctorCity" value="<?php echo (!empty($doctor->DoctorCity)) ? $doctor->DoctorCity : ''; ?>" /></td>
                                <td class="right"><strong>State:</strong></td>
                                <td><?php echo $statesSelect; ?></td>
                                <td class="right"><strong>Zip:</strong> </td>
                                <td><input class="fullwidth zip" type="text" name="doctor[DoctorZip]" id="DoctorZip" value="<?php echo (!empty($doctor->DoctorZip)) ? $doctor->DoctorZip : ''; ?>" /></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td class="right"><strong>Phone Number:</strong> </td>
                                <td colspan="2"><input class="fullwidth phone" type="text" name="doctor[DoctorPhone]" id="DoctorPhone" value="<?php echo (!empty($doctor->DoctorPhone)) ? $doctor->DoctorPhone : ''; ?>" /></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td class="right"><strong>Email Address:</strong> </td>
                                <td colspan="2"><input class="fullwidth email" type="text" name="doctor[DoctorEmail]" id="DoctorEmail" value="<?php echo (!empty($doctor->DoctorEmail)) ? $doctor->DoctorEmail : ''; ?>" /></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td class="right"><strong>Status:</strong> </td>
                                <td colspan="2">
<?php
                                    $doctorIsActive = $doctor->getDoctorIsActive();
                                    print "<select class=\"DoctorIsActiveSelect\" name=\"doctor[DoctorIsActive]\" id=\"doctor[DoctorIsActive]\" onChange=\"onIsActiveChange()\">\n";
                                    $sel = ($doctorIsActive)  ? 'selected' : '';
                                    print "    <option value='1' $sel>Doctor Is Active</option>\n";
                                    $sel = (!$doctorIsActive) ? 'selected' : '';
                                    print "    <option value='0' $sel>Doctor Is Inactive</option>\n";
                                    print "</select>\n";

                                    print "<script  type='text/javascript'>\n".
                                          "function onIsActiveChange() {\n".
                                          "    var dropdownField = $(\".DoctorIsActiveSelect\");\n".
                                          "    if (dropdownField) {\n".
                                          "        var doctorIsActive = dropdownField.val();\n".
                                          "        if (doctorIsActive == 1) {\n".
                                          //"            dropdownField.attr('style', 'background-color:lightgreen;');\n".
                                          //"            dropdownField.css('background-color', 'lightgreen');\n".
                                          "            dropdownField.css('background-color', '#d9e6e9');\n".
                                          "            dropdownField.css('color', 'black');\n".
                                          "        } else {\n".
                                          "            dropdownField.attr('style', 'background-color:red;color:white;');\n".
                                          "            dropdownField.css('background-color', 'red');\n".
                                          "            dropdownField.css('color', 'white');\n".
                                          "        }\n".
                                          "    }\n".
                                          "}\n".
                                          "onIsActiveChange()\n".
                                          "</script>";
?>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                            </table>
                        <div class="clear"></div>
                        <div class="right formdiv noborder">
                            <button type="button" class="inline" onmouseup="return validate('mainform'); $('#mainform').submit();">Add/Update</button>&emsp;
                            <?php if (!empty($_GET['id']))
{
    $uses = $patient->GetKeysWhere('PatientKeyID', 'WHERE DoctorKeyID=' . $doctor->DoctorKeyID);
    if (count($uses) == 0)
    { ?>
                            <a href="#confirm-delete" class="confirm-delete"><button type="button" class="inline">Delete</button></a>
                            <?php }
} ?>
                        </div>
    				</form>
    			</div>
    		</div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete this Doctor?<br />Doing so will remove all associations with  Sites, Trials and Patients.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            
        </div>
	</body>
</html>
<?php


/**
 * Enforce user permissions or redirect to login.php
 *
 * @param $permissions
 * @return array array($USerTypeIDs)
 */
function enforceUserPermissions($permissions) {
    $permissions->Load_from_action("AddEditDoctors");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }
    $permissions->Load_from_action("DeleteDoctors");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }

    return array($UserTypeIDs);
}

