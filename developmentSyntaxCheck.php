<?php

print "Internal use only!"; exit;

ob_start();
passthru("find ./ -type f -name \*.php");
$lines = ob_get_contents();
ob_end_clean();

$lines = str_replace("\r", '', $lines);
$arr   = explode("\n", $lines);

foreach($arr as $fl) {
    $fl = trim($fl);
    if ($fl) {
        ob_start();
        passthru("php -l $fl");
        $result = ob_get_contents();
        ob_end_clean();

        if (strpos($result, 'No syntax error') !==false) {
            print "<div style='font-size:7pt;color:blue;'>".htmlentities($result)."</div>";
        } else {
            print "<div style='font-size:12pt;color:red;font-weight:bold;'>".htmlentities($result)."</div>";
        }
    }
}

//passthru("find ./ -type f -name \*.php -exec php -l {} \; | grep �Errors parsing � ");
