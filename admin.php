<?php
/**
 * Patient management page
 */

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');       //startRequest, checkSchema, session_start, check authentication etc.
require_once(ROOT_PATH.'/lib/admin/adminBase.php');  // includes all admin functions
list($cfg, $errors, $warnings, $success) = startRequest();

//Only sysAdmins get to see this tab (if cfg['showAdminTab'] is set to 1)
if (!showSysAdminTab()) {
    //header('Location:trials.php');
    exit;
}

if (count($_POST) > 0) {
    processAdminForm();
}

?>
    <!DOCTYPE HTML>
    <html>
    <head>
        <meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <title>
            Admin
        </title>
        <?php require_once ("lib/common.includes.php"); ?>
        <script type="text/javascript">
            $(document).ready(function(){
            });
        </script>
    </head>
    <body>
    <form name='main_form' id='main_form' action='admin.php' method='POST'>
        <div class="wrapper">
            <div class="logo">
            </div>
            <div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('admin');
                ?>
                <div id="tabs-1" class="ui-tabs-panel">
                    <div class="subhead">
                        Admin
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                    <hr />
                    <br />
                    <?php
                        displayAdminForm();
                    ?>
                </div>
            </div>
        </div>
    </form>
    </body>
    </html>
<?php

/**
 * Display the admin checkboxes
 */
function displayAdminForm() {
    global $cfg;

    if ((isset($cfg['adminProcessPatientsUrbanRural'])) && ($cfg['adminProcessPatientsUrbanRural'])) {
        $name = htmlentities('adminProcessPatientsUrbanRural');
        $legend = htmlentities("Process Patient ZipCodes (Unknown/Rural/Urban settings)");
        print "<input type='checkbox' name='$name' id='$name'>&nbsp;&nbsp;$legend<br />";
    }

    if (0) {
        $name   = htmlentities('adminProcessClinicalTrialsGovImport');
        $legend = htmlentities("Test import from ClinicalTrials.Gov");
        print
            "<input type='checkbox' name='$name' id='$name'>&nbsp;&nbsp;$legend".
            "&nbsp;&nbsp;&nbsp;&nbsp;".
            "<input type='checkbox' name='flatArrayMode' id='flatArrayMode' checked='checked'> Display as flat array".
            "<br />";
    }

    if (0) {
        $name = htmlentities('adminProcessWpOneTimeDataImport');
        $legend = htmlentities("One Time Import Of Fields From WP public");
        print
            "<input type='checkbox' name='$name' id='$name'>&nbsp;&nbsp;$legend".
            "&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' name='dryRunMode' id='dryRunMode' checked='checked'> Dry run mode only".
            "<br />";
    }

    print "<br /><br /><input type='submit' value='Submit'>";
}

/**
 * Handle the selection of the admin checkboxes
 */
function processAdminForm() {
    global $cfg, $errors, $warnings, $success;

    $errors   = array();
    $warnings = array();
    $success  = '';
    if (!isSysAdmin()) {
        exit;
    }

    $allSuccessMessages = array();
    $ignoreAdminRights  = 0;
    foreach ($_POST as $key => $val) {
        if ((substr($key, 0, 5) == 'admin') && ($val == 'on')) {
            if  (is_callable($key)) {
                if ((isset($cfg[$key])) && ($cfg[$key])) {
                    //adminProcessPatientsUrbanRural()
                    //adminProcessWpOneTimeDataImport
                    call_user_func($key, $ignoreAdminRights);
                    if ($success) {
                        $allSuccessMessages[] = $success;
                    }
                } else {
                    $errors[] = "Cannot call admin function '$key' because it is not in the config";
                }
            } else {
                $errors[] = "Cannot call admin function '$key'";
            }
        }
    }

    if (count($allSuccessMessages)) {
        //If we have any success messages and also errors or warnings then move the successes into the warnings array
        if ((count($errors)) || (count($warnings))) {
            foreach ($allSuccessMessages as $msg) {
                $warnings[] = $msg;
                $success    = '';
            }
        } else {
            $success = $allSuccessMessages;
        }
    }
}

