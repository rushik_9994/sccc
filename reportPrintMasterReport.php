<?php
/**
 * Print the 'Master Report'
 */

include_once('reportCommonFunctions.php');
include_once('reportQueryBuilder.php');
require_once ('lib/ReportRowFetcher.class.php');

session_start();
if (empty($_SESSION['UserKeyID'])) { header('location: login.php'); exit; }

//Start our session, extract POST and GET from SESSION
list($connection, $reportInfo) = commonStartNewReport('Master Report');

/*previous
printLibraryGeneratePrintout($connection, $reportInfo);
*/


//Just run the print-start function so $reportInfo elements get created...
ob_start();
printLibraryGeneratePrintout($connection, $reportInfo);
ob_end_clean();

$reportInfo['groupByField'] = 'x_GROUPKEY';
$reportInfo['replaceColumnWithTitle'] = "SECTION-TOTALS";
$reportInfo['sliceSize']    = 5000;
$reportInfo['from']         = 0;

$fetcher = new ReportRowFetcher($connection, $reportInfo);

//Define the summation columns... (Totals, SECTION-TOTALS, REPORT-TOTALS)
// $reportInfo['report']                   = array();
// $reportInfo['report']['TX_Accruals']    = 0;      //Total
// $reportInfo['report']['CC_Accruals']    = 0;      //Total
// $reportInfo['report']['PX_Accruals']    = 0;      //Total
// $reportInfo['report']['Total_Accruals'] = 0;      //Total
$reportInfo['report'] = array('TX_Accruals'    => 0,
                              'CC_Accruals'    => 0,
                              'PX_Accruals'    => 0,
                              'Total_Accruals' => 0,
                              'Trial__Credit'  => 0.0,
                              'Trial__Value'  => 0.0,
                              'QoL'            => 0,
                              'QoLC'           => 0,
                              );
$reportInfo['formatting'] = array('Trial__Credit'  => array('totals.rounding' => 5));
$reportInfo['formatting'] = array('Trial__Value'  => array('totals.rounding' => 5));

while(!$fetcher->done($reportInfo)) {
    list($xrow, $row) = $fetcher->getNextRow($reportInfo);
    //print "<pre>"; print_r($xrow); print "</pre>";
    
    //REPORT START
    if ($xrow['x_isStart']) {
        //<html>....<body>
        prtReportStart($reportInfo, $xrow, $row);
    }
    
    //SITE START
    if ($xrow['x_firstInGroup']) {
        prtSiteStart($reportInfo, $xrow, $row, array('dispField'=>'x_GROUPKEY'));
    }
    
    //SITE ROW
    prtSiteRow($reportInfo, $xrow, $row);
    //print "<pre>"; print_r($xrow); print "</pre>"; //exit; //debug

    //SITE END
    if ($xrow['x_lastInGroup']) {
        //--------- SITE FOOTER (and once the REPORT FOOTER)
        prtSiteEnd($reportInfo, $xrow, $row, array('replaceColumn'=>'Site'));
        //--------- SITE FOOTER (and once the REPORT FOOTER)   
    }

    //REPORT END
    if ($xrow['x_isEnd']) {
        //</body>...
        prtReportEnd($reportInfo, $xrow, $row);
    }

}//while !$done





