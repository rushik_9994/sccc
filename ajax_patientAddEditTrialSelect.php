<?php
error_reporting(0);
//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest(1);

$patientAjaxDebugMode = ((isset($cfg)) && (is_array($cfg)) &&
                         (isset($cfg['debug.patientAjaxDebugMode'])) &&
                         ($cfg['debug.patientAjaxDebugMode'] > 0))
                            ?  $cfg['debug.patientAjaxDebugMode']  :  0;

//load required classes
require_once ('lib/SiteTrialsOpenJoin.class.php');
require_once ('lib/Trial.class.php');

//create required objects
$trialSite = new SiteTrialsOpenJoin;
$trial     = new Trial;

try {
    //------------------- transaction start
    $errors     = array();
    $connection = new DataBaseMysql();
    $connection->BeginTransaction();
    //------------------- transaction start

    $callingSubEvent = 'ajaxcall';
    $site = (isset($_REQUEST['site'])) ? $_REQUEST['site'] : '';
    if ((!$site) && (isset($_REQUEST['ajaxSiteKeyID']))) { $site = $_REQUEST['ajaxSiteKeyID']; }

    $selected = $_REQUEST['selected'];
    if ((!empty($site)) || ($site != '')) {
        $trialIDsArray = $trialSite->GetKeysWhereOrderBy('TrialKeyID', 'SiteKeyID = \'' . $site . '\' and SiteTrialOpenTermination = 0', 'TrialKeyID', 'ASC');
        foreach($trialIDsArray as $key){
            $trial->Load_from_key($key);
            if($trial->TrialTermination != 1){
                $new_array[] = $key;
            }
        }
        $trialIDsArray = $new_array;
        $trialIDs = join(',', $trialIDsArray);
        if ($trialIDs == ',') {
            $trialIDs = '';
            $select = '<select name="TrialKeyIDOUT" id="TrialKeyIDOUT" class="halfwidth req">'."\n".
                      "<option value=\"\">Site has no protocols!</option>\n".
                      "</select>";
        } else {
            if (trim($trialIDs)=='') {
                $select = '<select name="TrialKeyIDOUT" id="TrialKeyIDOUT" class="halfwidth req">'."\n".
                          "<option value=\"\">Site has no protocols!</option>\n".
                          "</select>";
            } else {
                $trialIDs = 'WHERE TrialKeyID IN (' . $trialIDs . ')';
                $select = $trial->CreateSelectSelected($selected, $trialIDs);
            }
        }

    } else {
        $select = '<span class="msg" >Select Site First <span class="arrow">&#8593;</span>';
    }

    //------------------- transaction commit or throw exception
    //Commit all database changes or roll our transaction back
    if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
    if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
    if (isset($connection)) { $connection->CommitTransaction(); }
    //------------------- transaction commit or throw exception

} catch (\Exception $e) {
    $publicMessage = $e->getMessage();
    $f1 = strpos($publicMessage, '|||');
    if ($f1 !== false) {
        $publicMessage = substr($publicMessage, 0, $f1);
    }
    $select = '<span class="msg" >ERROR: '. htmlentities($publicMessage).'</span>';
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}

echo $select;

