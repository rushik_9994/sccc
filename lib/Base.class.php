<?php
/*
 * Base class for the various 'model' classes inside /lib
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/DataBaseMysql.class.php');
require_once(ROOT_PATH.'/cfg/config.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/TrackChanges.php');


/*
 * Base class for the various 'model' classes inside /lib
 */
class BaseClass {

    /**
     * DataBaseMysql.class instance
     * @var object
     */
    protected $connection;

    /**
     *
     */
    protected $limitFrom = 0;

    /**
     * @var int
     */
    protected $limitSlice = 100;


    /**
     * BaseClass constructor.
     */
    public function __construct() {
        $this->connection = new DataBaseMysql();
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * Example:
     * $where = "TrialKeyID = '$quIdValue'"
     * $comment_keys = $comment->GetKeysWhereOrderBy($where, 'CommentKeyID', 'ASC');
     *
     * @param string $tableName                 Name of the table
     * @param string $primeKeyField             Name of the table's primary key ID field  (list of these will be returned0
     * @param string $compareField              One of the fieldnames of the table
     * @param string $compareCondition          One of the SQL comparison operators (usually '=')
     * @param mixed  $compareValue              Key value (usually integer ID but could also be some other primary value)
     * @param string $column                    Used for sorting (order by $column $ascDec)
     * @param string $order                     'ASC' or 'DESC'
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysByFieldConditionAndValue($tableName, $primeKeyField, $compareField, $compareCondition, $compareValue, $column, $order)
    {
        $quTableName     = trim($this->SqlQuote($tableName), "'");
        if (strpos($quTableName, "`") === false) {
            $quTableName = "`$quTableName`";
        }
        $quPrimeKeyField     = trim($this->SqlQuote($primeKeyField), "'");
        if (strpos($quPrimeKeyField, "`") === false) {
            $quPrimeKeyField = "`$quPrimeKeyField`";
        }

        $quCompareField     = trim($this->SqlQuote($compareField), "'");
        if (strpos($quCompareField, "`") === false) {
            $quCompareField = "`$quCompareField`";
        }
        $quCompareCondition = trim($this->SqlQuote($compareCondition), "'");
        $quCompareValue     = (isset($compareValue)) ? trim($this->SqlQuote($compareValue), "'") : 'NULL';
        $quColumn           = trim($this->SqlQuote($column), "'");
        $quOrder            = (strtoupper($order) == 'DESC') ? 'DESC' : 'ASC';

        $quWhere = "$quCompareField $quCompareCondition $quCompareValue ";

        $sql = "SELECT $quPrimeKeyField FROM $quTableName WHERE $quWhere ORDER BY $quColumn $quOrder";
        $result = $this->connection->RunQuery($sql);

        $keys = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (isset($row[$column])) {
                $keys[] = $row[$column];
            }
        }

        return $keys;
    }


    /**
     * Create an SQL-quoted IN clause (where something IN (THISRESULTSTRING)
     * @param array $keyArray
     * @return string
     */
    public function createInClauseList($keyArray=array())
    {
        if (!isset($this->connection)) {
            $this->connection = new DataBaseMysql();
        }

        if ((!isset($keyArray)) || (!is_array($keyArray)) || (count($keyArray) == 0)) {
            return '-1';
        } else {
            for ($i=0,$maxi=count($keyArray); $i < $maxi; $i++) {
                $keyArray[$i] = $this->connection->Quote($keyArray[$i]);
            }
            return join(',', $keyArray);
        }
    }

    /**
     * A smart quote function. If string then 'quotedstring' (string surrounded by quotes). If null then NULL.
     *
     * @param  string $string
     * @return string $sqlUsableString        Either 'quotedstring' or NULL
     */
    public function SqlQuote($string)
    {
        if (!isset($this->connection)) {
            $this->connection = new DataBaseMysql();
        }
        return $this->connection->SqlQuote($string);
    }

    /**
     * A simple quote function (calling the DB-Class' function). Does NOT surround the string with singlequotes!
     *
     * @param string $string
     * @return string $string
     */
    public function Quote($string = '')
    {
        if (!isset($this->connection)) {
            $this->connection = new DataBaseMysql();
        }
        return $this->connection->Quote($string);
    }

    /**
     * Performs an insert (including change tracking)
     *
     * @param $sql
     * @param $className
     * @param $functionName
     * @param $tableName
     * @param $primeKeyFieldName
     * @return mixed
     * @throws Exception
     */
    protected function executeInsert($sql, $className, $functionName, $tableName, $primeKeyFieldName)
    {
        TrackChanges::trackBeforeInsert($this->connection, $tableName, $primeKeyFieldName);

        $this->connection->RunQuery($sql);
        $insertId = $this->connection->LastID();

        //Make sure our insert worked
        $error = mysqli_error($this->connection->conn);
        if (($error) || (!$insertId)) {
            $error = ($error) ? "insert failed|||($error)" : 'insert failed';
            $error = $className . " $error";
        }
        TrackChanges::trackAfterInsert($this->connection, $tableName, $primeKeyFieldName, $insertId);

        if (($error) && (shouldWeRaiseAnException($className, $functionName, array('message' => $error)))) {
            throw new \Exception($error);
        }

        return $insertId;
    }

    /**
     * Performs an update (including change tracking)
     *
     * @param string $sql
     * @param string $className
     * @param string $functionName
     * @param string $tableName
     * @param string $primeKeyFieldName
     * @param mixed  $primeKeyFieldValue
     * @return mixed
     * @throws Exception
     */
    protected function executeUpdate($sql, $className, $functionName, $tableName, $primeKeyFieldName, $primeKeyFieldValue)
    {
        TrackChanges::trackBeforeUpdate($this->connection, $tableName, $primeKeyFieldName, $primeKeyFieldValue);

        $this->connection->RunQuery($sql);

        //Make sure our update worked
        $error = mysqli_error($this->connection->conn);
        if ($error) {
            $error = $className . " failed to update|||($error)";
        }

        TrackChanges::trackAfterUpdate($this->connection, $tableName, $primeKeyFieldName, $primeKeyFieldValue);

        if (($error) && (shouldWeRaiseAnException($className, $functionName, array('message' => $error)))) {
            throw new \Exception($error);
        }
    }


    protected function executeDelete($connection, $className, $functionName, $tableName, $primeKeyFieldName, $primeKeyFieldValue)
    {
        TrackChanges::trackBeforeDelete($connection, $tableName, $primeKeyFieldName, $primeKeyFieldValue);

        $quTableName          = trim($connection->SqlQuote($tableName), "'");
        $quPrimeKeyFieldName  = trim($connection->SqlQuote($primeKeyFieldName), "'");
        $quPrimeKeyFieldValue = trim($connection->SqlQuote($primeKeyFieldValue), "'");

        $sql = "DELETE FROM `$quTableName` WHERE `$quPrimeKeyFieldName` = '$quPrimeKeyFieldValue'";
        $connection->RunQuery($sql);

        //Make sure our insert worked
        $error = mysqli_error($this->connection->conn);
        if ($error) {
            $error = ($error) ? "delete failed|||($error)" : 'delete failed';
            $error = $className . " $error";
        }

        TrackChanges::trackAfterDelete($connection, $tableName, $primeKeyFieldName, $primeKeyFieldValue);

        if (($error) && (shouldWeRaiseAnException($className, $functionName, array('message' => $error)))) {
            throw new \Exception($error);
        }
    }

}

