<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */
 
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


/**
 * Patient class
 */
class Patient extends BaseClass {

    public $PatientKeyID; //int(10)
    public $SiteKeyID; //int(10)
    public $DoctorKeyID; //int(10)
    public $TrialKeyID; //int(10)
    public $TrialGroupKeyID; //int(10)
    public $TimestampCreate; //datetime
    public $TimestampUpdate; //timestamp
    public $PatientFirstInit; //varchar(250)
    public $PatientMiddleInit; //varchar(250)
    public $PatientLastInit; //varchar(250)
    public $PatientBirthDate; //datetime
    public $PatientSex; //varchar(250)
    public $PatientRace; //varchar(250)
    public $PatientEthnicity; //varchar(250)
    public $PatientMultiRacial; //tinyint(1)
    public $PatientZipCode; //int(9)
    public $PatientUrbanRural; //varchar(50)
    public $PatientIdentifier; //varchar(250)
    public $PatientIdentifierAddendum; //varchar(250)
    public $PatientRegistrationDate; //dastetime
    public $PatientTrialCredit; //int(10)
    public $PatientCreditor; //varchar(250)
    public $PatientQoLC; //varchar(250)
    public $PatientQoL; //tinyint(1)

    //Pagination variables
    public $showPagination     = 0;
    public $showFirstButton    = 0;
    public $showPreviousButton = 0;
    public $showNextButton     = 0;
    public $legendText         = '';
    public $totalRowCount      = 0;


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_Patient($SiteKeyID,$DoctorKeyID,$TrialKeyID,$TrialGroupKeyID,$TimestampCreate,$TimestampUpdate,$PatientFirstInit,$PatientMiddleInit,$PatientLastInit,$PatientBirthDate,$PatientSex,$PatientRace,$PatientEthnicity,$PatientMultiRacial,$PatientZipCode,$PatientIdentifier,$PatientRegistrationDate,$PatientTrialCredit, $PatientZipSatus)
    {
        $this->SiteKeyID                 = $SiteKeyID;
        $this->DoctorKeyID               = $DoctorKeyID;
        $this->TrialKeyID                = $TrialKeyID;
        $this->TrialGroupKeyID           = $TrialGroupKeyID;
        $this->TimestampCreate           = $TimestampCreate;
        $this->TimestampUpdate           = $TimestampUpdate;
        $this->PatientFirstInit          = $PatientFirstInit;
        $this->PatientMiddleInit         = $PatientMiddleInit;
        $this->PatientLastInit           = $PatientLastInit;
        $this->PatientBirthDate          = $PatientBirthDate;
        $this->PatientSex                = $PatientSex;
        $this->PatientRace               = $PatientRace;
        $this->PatientEthnicity          = $PatientEthnicity;
        $this->PatientMultiRacial        = $PatientMultiRacial;
        $this->PatientZipCode            = $PatientZipCode;
        $this->PatientIdentifier         = $PatientIdentifier;
        $this->PatientIdentifierAddendum = $PatientIdentifierAddendum;
        $this->PatientRegistrationDate   = $PatientRegistrationDate;
        $this->PatientTrialCredit        = $PatientTrialCredit;
        $this->PatientCreditor           = $PatientCreditor;
        $this->PatientQoLC               = $PatientQoLC;
        $this->PatientQoL                = $PatientQoL;
        $this->PatientUrbanRural          = $PatientUrbanRural;
    }*/


    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $sql = "SELECT *\n".
               "FROM   `Patient`\n".
               "WHERE  `PatientKeyID` = ". $this->SqlQuote($key_row);
        $result  = $this->connection->RunQuery($sql);

        $found = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->PatientKeyID              = $row["PatientKeyID"];
            $this->SiteKeyID                 = $row["SiteKeyID"];
            $this->DoctorKeyID               = $row["DoctorKeyID"];
            $this->TrialKeyID                = $row["TrialKeyID"];
            $this->TrialGroupKeyID           = $row["TrialGroupKeyID"];
            $this->TimestampCreate           = $row["TimestampCreate"];
            $this->TimestampUpdate           = $row["TimestampUpdate"];
            $this->PatientFirstInit          = $row["PatientFirstInit"];
            $this->PatientMiddleInit         = $row["PatientMiddleInit"];
            $this->PatientLastInit           = $row["PatientLastInit"];
            $this->PatientBirthDate          = $row["PatientBirthDate"];
            $this->PatientSex                = $row["PatientSex"];
            $this->PatientRace               = $row["PatientRace"];
            $this->PatientEthnicity          = $row["PatientEthnicity"];
            $this->PatientMultiRacial        = $row["PatientMultiRacial"];
            $this->PatientZipCode            = $row["PatientZipCode"];
            $this->PatientUrbanRural         = (isset($row["PatientUrbanRural"])) ? $row["PatientUrbanRural"] : 'unknown';
            $this->PatientIdentifier         = $row["PatientIdentifier"];
            $this->PatientIdentifierAddendum = $row["PatientIdentifierAddendum"];
            $this->PatientRegistrationDate   = $row["PatientRegistrationDate"];
            $this->PatientTrialCredit        = $row["PatientTrialCredit"];
            $this->PatientCreditor           = $row["PatientCreditor"];
            $this->PatientQoLC               = $row["PatientQoLC"];
            $this->PatientQoL                = $row["PatientQoL"];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Patient', 'PatientKeyID', $key_row);
    }


    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
	{
	    $sql   = '';
	    $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE `Patient` SET\n".
                "    `SiteKeyID`=".                 $this->SqlQuote($this->SiteKeyID) .",\n".
                "    `DoctorKeyID`=".               $this->SqlQuote($this->DoctorKeyID) .",\n".
                "    `TrialKeyID`=".                $this->SqlQuote($this->TrialKeyID) .",\n".
                "    `TrialGroupKeyID`=".           $this->SqlQuote($this->TrialGroupKeyID) .",\n".
                "    `PatientFirstInit`=".          $this->SqlQuote($this->PatientFirstInit) .",\n".
                "    `PatientMiddleInit`=".         $this->SqlQuote($this->PatientMiddleInit) .",\n".
                "    `PatientLastInit`=".           $this->SqlQuote($this->PatientLastInit) .",\n".
                "    `PatientBirthDate`=".          $this->SqlQuote($this->PatientBirthDate) .",\n".
                "    `PatientSex`=".                $this->SqlQuote($this->PatientSex) .",\n".
                "    `PatientRace`=".               $this->SqlQuote($this->PatientRace) .",\n".
                "    `PatientEthnicity`=".          $this->SqlQuote($this->PatientEthnicity) .",\n".
                "    `PatientMultiRacial`=".        $this->SqlQuote($this->PatientMultiRacial) .",\n".
                "    `PatientZipCode`=".            $this->SqlQuote($this->PatientZipCode) .",\n".
                "    `PatientUrbanRural`=".          $this->SqlQuote($this->PatientUrbanRural) .",\n".
                "    `PatientIdentifier`=".         $this->SqlQuote($this->PatientIdentifier) .",\n".
                "    `PatientIdentifierAddendum`=". $this->SqlQuote($this->PatientIdentifierAddendum) .",\n".
                "    `PatientRegistrationDate`=".   $this->SqlQuote($this->PatientRegistrationDate) .",\n".
                "    `PatientTrialCredit`=".        $this->SqlQuote($this->PatientTrialCredit) .",\n".
                "    `PatientQoL`=".                $this->SqlQuote($this->PatientQoL) .",\n".
                "    `PatientQoLC`=".               $this->SqlQuote($this->PatientQoLC) .",\n".
                "    `PatientCreditor`=".           $this->SqlQuote($this->PatientCreditor) ."\n".
                "WHERE `PatientKeyID`=". $this->SqlQuote($this->PatientKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Patient','PatientKeyID', $this->PatientKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO `Patient` (`SiteKeyID`, `DoctorKeyID`, `TrialKeyID`, `TrialGroupKeyID`,\n".
                "                       `PatientFirstInit`, `PatientMiddleInit`, `PatientLastInit`,\n".
                "                       `PatientBirthDate`, `PatientSex`, `PatientRace`,\n".
                "                       `PatientEthnicity`, `PatientMultiRacial`, `PatientZipCode`,\n".
                "                       `PatientUrbanRural`,".
                "                       `PatientIdentifier`, `PatientIdentifierAddendum`,\n".
                "                       `PatientRegistrationDate`, `PatientTrialCredit`,\n".
                "                       `PatientQoL`, `PatientQoLC`, `PatientCreditor`) VALUES (\n".
                $this->SqlQuote($this->SiteKeyID). ",\n".
                $this->SqlQuote($this->DoctorKeyID). ",\n".
                $this->SqlQuote($this->TrialKeyID). ",\n".
                $this->SqlQuote($this->TrialGroupKeyID). ",\n".
                $this->SqlQuote($this->PatientFirstInit). ",\n".
                $this->SqlQuote($this->PatientMiddleInit). ",\n".
                $this->SqlQuote($this->PatientLastInit). ",\n".
                $this->SqlQuote($this->PatientBirthDate). ",\n".
                $this->SqlQuote($this->PatientSex). ",\n".
                $this->SqlQuote($this->PatientRace). ",\n".
                $this->SqlQuote($this->PatientEthnicity). ",\n".
                $this->SqlQuote($this->PatientMultiRacial). ",\n".
                $this->SqlQuote($this->PatientZipCode). ",\n".
                $this->SqlQuote($this->PatientUrbanRural). ",\n".
                $this->SqlQuote($this->PatientIdentifier). ",\n".
                $this->SqlQuote($this->PatientIdentifierAddendum). ",\n".
                $this->SqlQuote($this->PatientRegistrationDate). ",\n".
                $this->SqlQuote($this->PatientTrialCredit). ",\n".
                $this->SqlQuote($this->PatientQoL). ",\n".
                $this->SqlQuote($this->PatientQoLC). ",\n".
                $this->SqlQuote($this->PatientCreditor).')';
            $this->PatientKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Patient','PatientKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }


    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order optional                 Default is 'ASC'
     * @param int    $from optional                  Default is 0   (for LIMIT $from,...)
     * @param int    $limit optional                 Default is 0   (for LIMIT ...,$limit)
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($column, $order='ASC', $from=0, $limit=0, $sClauses=array())
    {
        $keys = array(); $i = 0;

        if ((!isset($order)) || (!is_string($order))) { $order='ASC'; }
        $order = strtoupper($order);
        if (($order!='ASC') && ($order!='DESC')) { $order='ASC'; }

        $column = $this->connection->Quote($column);

        //Support SearchFilter
        //Get additional joins and where clauses as supplied by the searchFilter class
        //print "<pre>".print_r($sClauses,true)."</pre>";
        if ((!isset($sClauses)) || (!is_array($sClauses))) { $sClauses=array(); }
        if ((!isset($sClauses['joins'])) || (!is_array($sClauses['joins'])))   { $sClauses['joins']=array(); }
        if ((!isset($sClauses['where'])) || (!is_array($sClauses['where'])))   { $sClauses['where']=array(); }
        if ((!isset($sClauses['debug'])) || (!is_numeric($sClauses['debug']))) { $sClauses['debug']=0; }
        $joins    = '';
        $whereAnd = '';
        $orderIndex            = (isset($sClauses['order_index']))       ? $sClauses['order_index'] : '_0';
        $needTrial             = ((isset($sClauses['order_field'.$orderIndex])) && ($sClauses['order_field'.$orderIndex]=='order_protocol_number'.$orderIndex)) ? 1 : 0;
        $needPatientIdentifier = ((isset($sClauses['order_field'.$orderIndex])) && ($sClauses['order_field'.$orderIndex]=='order_identifier'.$orderIndex)) ? 1 : 0;
        foreach($sClauses['joins'] as $join=>$ignore) {
            if ((strpos($join, ' Trial ')!==false) ||  (strpos($join, '`Trial`')!==false)) { $needTrial=0; }
            if (strpos($join, 'PatientIdentifier')!==false) { $needPatientIdentifier=0; }
            $joins .= $join;
        }
        reset($sClauses['joins']);
        if ($needTrial) {
            $joins .= "\nLEFT JOIN Trial ON Patient.TrialKeyID=Trial.TrialKeyID\n";
        }
        if ($needPatientIdentifier) {
            $joins .= "\nLEFT JOIN PatientIdentifier ON Patient.PatientIdentifier=PatientIdentifier.PatientIdentifierKeyID\n";
        }
        if ($joins) {
            $joins = trim($joins)."\n";
        }
        
        foreach($sClauses['where'] as $ignore=>$whr) {
            $whereAnd .= $whr;
        }
        reset($sClauses['where']);
        if ($whereAnd) { $whereAnd = trim($whereAnd)."\n"; }

        //COUNT ROWS:  Build the query to just count our results
        $sql = "SELECT count(*) as cnt\n".
               "FROM   `Patient`\n".
               $joins.
               "WHERE  `Patient`.`PatientKeyID` > 0\n".      //Todo: HACK!!! Fix this 'select-all-patients' nonsense!
               $whereAnd;
        $result = $this->connection->RunQuery($sql);
        $this->totalRowCount = 0;
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (isset($row['cnt'])) {
                $this->totalRowCount = $row['cnt'];
                break;
            }
        }

        //GET KEYS:  Build the query to actually get our Keys
        $sql = "SELECT `PatientKeyID`\n".
               "FROM   `Patient`\n".
               $joins.
               "WHERE  `Patient`.`PatientKeyID` > 0\n".      //Todo: HACK!!! Fix this 'select-all-patients' nonsense!
               $whereAnd;

        $orderBy = "ORDER BY `".$this->connection->Quote($column)."` $order";
        $orderIndex     = (isset($sClauses['order_index']))                 ? $sClauses['order_index'] : '_0';
        $orderField     = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }
        if ($orderField) {
            $mainSort = "CONCAT(Patient.PatientFirstInit,Patient.PatientMiddleInit,Patient.PatientLastInit)";
            if ($orderField == 'order_initials'.$orderIndex) {
                $orderBy = "ORDER BY  $mainSort $orderDirection";
            } else if ($orderField == 'order_protocol_number'.$orderIndex) {
                $orderBy = "ORDER BY Trial.TrialProtocolNumber $orderDirection, $mainSort ASC";                
            } else if ($orderField == 'order_identifier'.$orderIndex) {
                $orderBy = "ORDER BY CONCAT(PatientIdentifier.PatientIdentifierText,Patient.PatientIdentifierAddendum) $orderDirection, $mainSort ASC";
            } else if ($orderField == 'order_zip_code'.$orderIndex) {
                $orderBy = "ORDER BY Patient.PatientZipCode $orderDirection, $mainSort ASC";
            } else if ($orderField == 'order_urban_rural'.$orderIndex) {
                $orderBy = "ORDER BY Patient.PatientUrbanRural $orderDirection, Patient.PatientZipCode $orderDirection, $mainSort ASC";
            } else if ($orderField == 'order_birthdate'.$orderIndex) {
                $orderBy = "ORDER BY Patient.PatientBirthDate $orderDirection, $mainSort ASC";
            } else if ($orderField == 'order_reg_date'.$orderIndex) {
                $orderBy = "ORDER BY Patient.PatientRegistrationDate $orderDirection, $mainSort ASC";
            }
        } 
        $sql .= $orderBy;

        //DebugDisplay for SearchFilter
        if ((0) || ($sClauses['debug'])) {
            print "<br /><pre style='font-size:8pt;color:black;'>$sql</pre>";
            print "<pre style='font-size:8pt;color:blue;'>".print_r($sClauses,true)."</pre>";
            if ($sClauses['debug'] > 1) { exit; }
        }

        //Calculate our final SELECT query with limit clause (if $limit>0)
        if ((!isset($from))  || (!is_numeric($from))  || ($from  < 0)) { $from  = 0; }
        if ((!isset($limit)) || (!is_numeric($limit)) || ($limit < 0)) { $limit = 0; }
        if (($from > 0 ) && ($limit==0)) {
            //get pagination limit
            $pg = new Pagination();
            $limit = $pg->getPaginationSize('patients.pagination.size');
        }
        $this->showPagination     = (($from) || ($limit)) ? 1 : 0;
        $this->showFirstButton    = ($from > 0)  ?  1  : 0;
        $this->showPreviousButton = ($from > 0)  ?  1  : 0;
        $this->showNextButton     = 0;
        $this->legendText         = ($limit > 0) ? "Displaying Records ".($from+1)." to ".($from+$limit) : '';
        $this->limitFrom          = $from;
        $this->limitSlice         = $limit;
        $sql .= (($from) || ($limit))  ?  "\nLIMIT $from,".($limit+1) : '';

        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (($limit) && ($i >= $limit)) {
                $this->showNextButton = 1;
                break;
            }
            $keys[$i] = $row["PatientKeyID"];
            $i++;
            if ($limit > 0) { $this->legendText = "Displaying Records ".($from+1)." to ".($from+$i); }
        }

        if (count($keys)==0) { $this->showPagination=0; }

        return $keys;
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * IMPORTANT:  CALLER is responsible for quoting the $where clause!!!  Todo: fix caller!
     *
     * @param string $key
     * @param string $where
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysWhere($key, $where)
    {
        $keys = array(); $i = 0;

        $key = $this->connection->Quote($key);

        $x = strtoupper(trim($where));
        if (($x) && (substr($x,0,6) != 'WHERE ')) { $where = "WHERE $where"; }

        $result = $this->connection->RunQuery("SELECT `".$this->connection->Quote($key)."`\n".
                                              "FROM   `Patient`\n".
                                              "$where"
                                             );
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["PatientKeyID"];
            $i++;
        }
        return $keys;
    }

    /**
     * Calculate the UrbanRural field
     * @param  int $zipCode
     * @param  string $registrationDate
     * @return mixed
     * @throws Exception
     */
    public function calculateUrbanRural($zipCode=0, $registrationDate='')
    {
        if (!isset($this->connection)) { print "ERROR: DB connection missing"; exit; }
        if ($zipCode) {
            $zipCode = $this->PatientZipCode;
        }
        $zipCode = substr(trim($zipCode), 0, 5);

        if (!$registrationDate) {
            $registrationDate = $this->PatientRegistrationDate;
        }
        $registrationDate = trim($registrationDate);

        $sql =
            "SELECT ZipCodes.UrbanRural\n".
            "FROM   ZipCodes\n".
            "WHERE  ZipCodes.ZipCode=".$this->connection->SqlQuote($zipCode)."\n".
            "AND    ZipCodes.ValidFrom <= ". $this->connection->SqlQuote($registrationDate)."\n".
            "ORDER BY ZipCodes.ValidFrom DESC\n".
            "LIMIT 1";
        $result = $this->connection->RunQuery($sql);
        $row = $result->fetch_array(MYSQLI_ASSOC);
        if (($row) && (is_array($row)) && (isset($row['UrbanRural']))) {
            return $row['UrbanRural'];
        } else {
            return 'unknown';
        }
    }

    /**
     * @return null|int $PatientKeyID
     */
    public function getPatientKeyID()
    {
        return $this->PatientKeyID;
    }

    /**
     * @return null|int $SiteKeyID
     */
    public function getSiteKeyID()
    {
        return $this->SiteKeyID;
    }

    /**
     * @return null|int $DoctorKeyID
     */
    public function getDoctorKeyID()
    {
        return $this->DoctorKeyID;
    }

    /**
     * @return null|int $TrialKeyID
     */
    public function getTrialKeyID()
    {
        return $this->TrialKeyID;
    }

    /**
     * @return null|int $TrialGroupKeyID
     */
    public function getTrialGroupKeyID()
    {
        return $this->TrialGroupKeyID;
    }

    /**
     * @return null|string $TimestampCreate
     */
    public function getTimestampCreate()
    {
        return $this->TimestampCreate;
    }

    /**
     * @return null|string $TimestampUpdate
     */
    public function getTimestampUpdate()
    {
        return $this->TimestampUpdate;
    }

    /**
     * @return null|string $PatientFirstInit
     */
    public function getPatientFirstInit()
    {
        return $this->PatientFirstInit;
    }

    /**
     * @return null|string $PatientMiddleInit
     */
    public function getPatientMiddleInit()
    {
        return $this->PatientMiddleInit;
    }

    /**
     * @return null|string $PatientLastInit
     */
    public function getPatientLastInit()
    {
        return $this->PatientLastInit;
    }

    /**
     * @return null|string $PatientBirthDate
     */
    public function getPatientBirthDate()
    {
        return $this->PatientBirthDate;
    }

    /**
     * @return null|string $PatientSex
     */
    public function getPatientSex()
    {
        return $this->PatientSex;
    }

    /**
     * @return null|string $PatientRace
     */
    public function getPatientRace()
    {
        return $this->PatientRace;
    }

    /**
     * @return null|string $PatientEthnicity
     */
    public function getPatientEthnicity()
    {
        return $this->PatientEthnicity;
    }

    /**
     * @return null|int $PatientMultiRacial
     */
    public function getPatientMultiRacial()
    {
        return $this->PatientMultiRacial;
    }

    /**
     * @return null|int $PatientZipCode
     */
    public function getPatientZipCode()
    {
        return $this->PatientZipCode;
    }

    /**
     * @return null|string $PatientUrbanRural
     */
    public function getPatientUrbanRural()
    {
        return $this->PatientUrbanRural;
    }

    /**
     * @return null|string $PatientIdentifier
     */
    public function getPatientIdentifier()
    {
        return $this->PatientIdentifier;
    }

    /**
     * @return null|string $PatientRegistrationDate
     */
    public function getPatientRegistrationDate()
    {
        return $this->PatientRegistrationDate;
    }

    /**
     * @return null|int $PatientTrialCredit
     */
    public function getPatientTrialCredit()
    {
        return $this->PatientTrialCredit;
    }

    /**
     * @param int $PatientKeyID
     */
    public function setPatientKeyID($PatientKeyID)
    {
        $this->PatientKeyID = $PatientKeyID;
    }

    /**
     * @param int $SiteKeyID
     */
    public function setSiteKeyID($SiteKeyID)
    {
        $this->SiteKeyID = $SiteKeyID;
    }

    /**
     * @param int $DoctorKeyID
     */
    public function setDoctorKeyID($DoctorKeyID)
    {
        $this->DoctorKeyID = $DoctorKeyID;
    }

    /**
     * @param int $TrialKeyID
     */
    public function setTrialKeyID($TrialKeyID)
    {
        $this->TrialKeyID = $TrialKeyID;
    }

    /**
     * @param int $TrialGroupKeyID
     */
    public function setTrialGroupKeyID($TrialGroupKeyID)
    {
        $this->TrialGroupKeyID = $TrialGroupKeyID;
    }

    /**
     * @param string $TimestampCreate
     */
    public function setTimestampCreate($TimestampCreate)
    {
        $this->TimestampCreate = $TimestampCreate;
    }

    /**
     * @param string $TimestampUpdate
     */
    public function setTimestampUpdate($TimestampUpdate)
    {
        $this->TimestampUpdate = $TimestampUpdate;
    }

    /**
     * @param string $PatientFirstInit
     */
    public function setPatientFirstInit($PatientFirstInit)
    {
        $this->PatientFirstInit = $PatientFirstInit;
    }

    /**
     * @param string $PatientMiddleInit
     */
    public function setPatientMiddleInit($PatientMiddleInit)
    {
        $this->PatientMiddleInit = $PatientMiddleInit;
    }

    /**
     * @param string $PatientLastInit
     */
    public function setPatientLastInit($PatientLastInit)
    {
        $this->PatientLastInit = $PatientLastInit;
    }

    /**
     * @param string $PatientBirthDate
     */
    public function setPatientBirthDate($PatientBirthDate)
    {
        $this->PatientBirthDate = $PatientBirthDate;
    }

    /**
     * @param string $PatientSex
     */
    public function setPatientSex($PatientSex)
    {
        $this->PatientSex = $PatientSex;
    }

    /**
     * @param string $PatientRace
     */
    public function setPatientRace($PatientRace)
    {
        $this->PatientRace = $PatientRace;
    }

    /**
     * @param string $PatientEthnicity
     */
    public function setPatientEthnicity($PatientEthnicity)
    {
        $this->PatientEthnicity = $PatientEthnicity;
    }

    /**
     * @param int $PatientMultiRacial
     */
    public function setPatientMultiRacial($PatientMultiRacial)
    {
        $this->PatientMultiRacial = $PatientMultiRacial;
    }

    /**
     * @param int $PatientZipCode
     */
    public function setPatientZipCode($PatientZipCode)
    {
        $this->PatientZipCode = $PatientZipCode;
    }

    /**
     * @param string $PatientUrbanRural
     */
    public function setPatientUrbanRural($PatientUrbanRural)
    {
        $this->PatientUrbanRural = $PatientUrbanRural;
    }

    /**
     * @param string $PatientIdentifier
     */
    public function setPatientIdentifier($PatientIdentifier)
    {
        $this->PatientIdentifier = $PatientIdentifier;
    }

    /**
     * @param string $PatientRegistrationDate
     */
    public function setPatientRegistrationDate($PatientRegistrationDate)
    {
        $this->PatientRegistrationDate = $PatientRegistrationDate;
    }

    /**
     * @param int $PatientTrialCredit
     */
    public function setPatientTrialCredit($PatientTrialCredit)
    {
        $this->PatientTrialCredit = $PatientTrialCredit;
    }

    /**
     * Returns a select field and assigns selected
     *
     * IMPORTANT:  CALLER is responsible for quoting the $where clause!!!  Todo: fix caller!
     *
     * @param int $selected
     * @param string $where optional
     * @param array $options optional
     * @return string $select
     * @throws \Exception
     */
    public function CreatePatientIdentifierSelect($selected=0, $where='', $options=array())
    {
        //Gather some variables...
        if ((!isset($options)) || (!is_array($options))) { $options=array(); }
        $fieldId     = (isset($options['fieldId']))     ? $options['fieldId']                   : 'PatientIdentifier';
        $defaultText = (isset($options['defaultText'])) ? htmlentities($options['defaultText']) : 'Select...';
        if ((!isset($where)) || (!is_string($where)) || (!$where)) {
            $where = "WHERE  `PatientIdentifierKeyID` > 0";
        } else {
            $where = trim($where);
        }
        if ((!isset($selected)) || (!is_numeric($selected)) || ($selected < 1)) { $selected = 0; }

        //Build our query...
        $select  = "\n".
                   "<select name=\"$fieldId\" id=\"$fieldId\">\n".
                   "<option value=\"\">$defaultText</option>\n";

        //Run the query and create the dropdown...
        $result = $this->connection->RunQuery("SELECT *\n".
                                              "FROM   `PatientIdentifier`\n".
                                              "$where\n".
                                              "ORDER BY `PatientIdentifierText` ASC");

        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escPatientIdentifierKeyID = htmlentities($row["PatientIdentifierKeyID"]);
            $escPatientIdentifierText  = htmlentities($row['PatientIdentifierText']);
            $quSel = ($row["PatientIdentifierKeyID"] == $selected) ? ' selected' : '';
            $select .= "<option value=\"$escPatientIdentifierKeyID\" $quSel>$escPatientIdentifierText</option>\n";
        }
        $select .= "</select>\n";

        return $select;
    }

    /**
     * Calculate the age of a patient.
     *
     * Returns either a numerical value or a display-string (default)
     *
     * @param  int $getAgeDisplay optional           Defaults to 1
     * @param  string $birthDateOverride optional     (YYYY-MM-DD) default=''. If given this birthday is used instead.
     * @return int|string $age|$ageDisplay
     * @throws \Exception
     */
    public function getCurrentAge($getAgeDisplay=1, $birthDateOverride='')
    {
        $birthDate  = (isset($this->PatientBirthDate)) ? $this->PatientBirthDate : '';
        if ($birthDateOverride) {
            $birthDate = $birthDateOverride;
        }

        if (($birthDate) && ($birthDate != '0000-00-00')) {
            $quBirthdate = $this->SqlQuote($birthDate);
            $sql    = "SELECT ROUND((DATEDIFF(CAST(NOW() AS DATE), CAST($quBirthdate AS DATE)) / 365) - 0.5, 0) AS age";
            //$sql  = "SELECT TIMESTAMPDIFF(YEAR, $quBirthdate, CURDATE()) AS age";
            $result = $this->connection->RunQuery($sql);
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $age = ((isset($row)) && (is_array($row)) && (isset($row['age'])) && (is_numeric($row['age']))) ? $row['age'] : 0;
        } else {
            $age = 0;
        }

        return $this->_getAgeReturnValue($age, $getAgeDisplay);
    }

    /**
     * Calculate the age of a patient at registration time
     *
     * Returns either a numerical value or a display-string (default)
     *
     * @param  int $getAgeDisplay optional           Defaults to 1
     * @param  string $birthDateOverride optional     (YYYY-MM-DD) default=''. If given this birthday is used instead.
     * @param  string $regDateOverride optional      (YYYY-MM-DD) default=''. If given this regdate is used instead.
     * @return int|string $age|$ageDisplay
     * @throws \Exception
     */
    public function getAgeAtRegistration($getAgeDisplay=1, $birthDateOverride='', $regDateOverride='')
    {
        $birthDate  = (isset($this->PatientBirthDate))        ? $this->PatientBirthDate : '';
        if ($birthDateOverride) {
            $birthDate = $birthDateOverride;
        }

        $regDate    = (isset($this->PatientRegistrationDate)) ? $this->PatientRegistrationDate : '';
        if ($regDateOverride) {
            $regDate = $regDateOverride;
        }

        if (($birthDate) && ($birthDate != '0000-00-00') && ($regDate) && ($regDate != '0000-00-00')) {
            $quBirthdate = $this->SqlQuote($birthDate);
            $quRegdate   = $this->SqlQuote($regDate);
            $sql    = "SELECT ROUND((DATEDIFF(CAST($quRegdate AS DATE), CAST($quBirthdate AS DATE)) / 365) - 0.5, 0) AS age";
            //$sql  = "SELECT TIMESTAMPDIFF(YEAR, $quBirthdate, $quRegdate) AS age";
            $result = $this->connection->RunQuery($sql);
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $age = ((isset($row)) && (is_array($row)) && (isset($row['age'])) && (is_numeric($row['age']))) ? $row['age'] : 0;
        } else {
            $age = 0;
        }

        return $this->_getAgeReturnValue($age, $getAgeDisplay, $regDate, $birthDate);
    }

    /**
     * Calculates the return value based on age and the getAgeDisplay flag
     *
     * Returns either a numerical value or a display-string (default)
     *
     * @param  int $age                          in numerical years
     * @param  int $getAgeDisplay optional       Defaults to 1
     * @param  string $firstDate optional
     * @param  string $secondDate optional
     * @return int|string $age|$ageDisplay
     */
    protected function _getAgeReturnValue($age, $getAgeDisplay=1, $firstDate='', $secondDate='')
    {
        if ($getAgeDisplay) {
            if ($age < 0) {
                $ageDisplay = 'ERROR: Invalid (future) birthdate';
                if (($firstDate) || ($secondDate)) {
                    $ageDisplay .= ' (';
                    if (!isset($firstDate))  { $ageDisplay .= 'NULL'; }   else { $ageDisplay .= $firstDate; }
                    if (!isset($secondDate)) { $ageDisplay .= ', NULL'; } else { $ageDisplay .= ", $secondDate"; }
                    $ageDisplay .= ')';
                }
                $ageDisplay .= '!';
            } else if ($age == 0) {
                $ageDisplay = 'Age unavailable';
            } else if ($age == 1) {
                $ageDisplay = '1 year';
            } else {
                $ageDisplay = "$age years";
            }
            return $ageDisplay;
        } else {
            return $age;
        }
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->PatientKeyID)) ? $this->PatientKeyID : null);  //vld: Patient.PatientKeyID
            if ($validationMessage = validateField($val,'PatientKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->SiteKeyID)) ? $this->SiteKeyID : null);  //vld: Patient.SiteKeyID
            if ($validationMessage = validateField($val,'SiteKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->DoctorKeyID)) ? $this->DoctorKeyID : null);  //vld: Patient.DoctorKeyID
            if ($validationMessage = validateField($val,'DoctorKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyID)) ? $this->TrialKeyID : null);  //vld: Patient.TrialKeyID
            if ($validationMessage = validateField($val,'TrialKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialGroupKeyID)) ? $this->TrialGroupKeyID : null);  //vld: Patient.TrialGroupKeyID
            if ($validationMessage = validateField($val,'TrialGroupKeyID','nullok;zeroorpositiveint',__CLASS__)) { break; } //----!!! remove nullok

            //public $TimestampCreate; //datetime     //vld: Patient.TimestampCreate
            //public $TimestampUpdate; //timestamp    //vld: Patient.TimestampUpdate

            $first  = (isset($this->PatientFirstInit))  ? $this->PatientFirstInit : '';  //vld: Patient.PatientFirstInit
            $middle = (isset($this->PatientMiddleInit)) ? $this->PatientMiddleInit : ''; //vld: Patient.PatientMiddleInit
            $last   = (isset($this->PatientLastInit))   ? $this->PatientLastInit : '';   //vld: Patient.PatientLastInit
            if (strlen($first.$middle.$last) < 2) {
                $validationMessage = 'First, Middle and Last initials together must be at least 1 character long'; break;
            }

            $val = ((isset($this->PatientBirthDate)) ? $this->PatientBirthDate : null);  //vld: Patient.PatientBirthDate
            if ($validationMessage = validateField($val, 'PatientBirthDate', 'zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->PatientSex)) ? $this->PatientSex : null);  //vld: Patient.PatientSex
            if ($validationMessage = validateField($val,'PatientSex','positiveint',__CLASS__)) { break; }
            if (($this->PatientSex < 1) || ($this->PatientSex > 2)) {
                $validationMessage = 'PatientSex must be 1 or 2'.getJsonVal($this->PatientSex); break;
            }

            $val = ((isset($this->PatientRace)) ? $this->PatientRace : null);  //vld: Patient.PatientRace
            if ($validationMessage = validateField($val,'PatientRace','nullok;zeroorpositiveint',__CLASS__)) { break; }

            $val = ((isset($this->PatientEthnicity)) ? $this->PatientEthnicity : null);  //vld: Patient.PatientEthnicity
            if ($validationMessage = validateField($val,'PatientEthnicity','zeroorpositiveint',__CLASS__)) { break; } //----!!! zero allowed?

            $val = ((isset($this->PatientMultiRacial)) ? $this->PatientMultiRacial : null);  //vld: Patient.PatientMultiRacial
            if ($validationMessage = validateField($val,'PatientMultiRacial','nullok;0or1',__CLASS__)) { break; }

            //public $PatientZipCode; //int(9) //vld: Patient.PatientZipCode

            $val = ((isset($this->PatientIdentifier)) ? $this->PatientIdentifier : null);  //vld: Patient.PatientIdentifier
            if ($validationMessage = validateField($val,'PatientIdentifier','zeroorpositiveint',__CLASS__)) { break; } //----!!! zero allowed?

            //public $PatientIdentifierAddendum; //varchar(250)  //vld: Patient.PatientIdentifierAddendum

            $val = ((isset($this->PatientRegistrationDate)) ? $this->PatientRegistrationDate : null);  //vld: Patient.PatientRegistrationDate
            if ($validationMessage = validateField($val, 'PatientRegistrationDate', 'zerodateok;date',__CLASS__)) { break; } //----!!! 0000-00-00 alowed?

            if ((isset($this->PatientBirthDate)) && ($this->PatientBirthDate) && ($this->PatientBirthDate != '0000-00-00')) {
                if ((isset($this->PatientRegistrationDate)) && ($this->PatientRegistrationDate) && ($this->PatientRegistrationDate != '0000-00-00')) {
                    if ($this->PatientRegistrationDate <= $this->PatientBirthDate) {
                        $validationMessage = "PatientRegistrationDate cannot be before PatientBirthdate";
                        break;
                    }
                }
            }

            if ((!is_numeric($this->PatientTrialCredit)) || ($this->PatientTrialCredit < 0.0)) { //vld: Patient.PatientTrialCredit
                $validationMessage = 'PatientTrialCredit must be zero or a positive number'; break;
            }

            $val = ((isset($this->PatientCreditor)) ? $this->PatientCreditor : null);  //vld: Patient.PatientCreditor
            if ($validationMessage = validateField($val,'PatientCreditor','zeroorpositiveint',__CLASS__)) { break; } //----!!! zero allowed?

            $val = ((isset($this->PatientQoLC)) ? $this->PatientQoLC : null);  //vld: Patient.PatientQoLC
            if ($validationMessage = validateField($val,'PatientQoLC','numeric',__CLASS__)) { break; } //----!!! zero allowed?

            $val = ((isset($this->PatientQoL)) ? $this->PatientQoL : null);  //vld: Patient.PatientQoL
            if ($validationMessage = validateField($val,'PatientQoL','nullok;0or1',__CLASS__)) { break; } //----!!! zero allowed?

        } while(false);

        return $validationMessage;
    }

}

