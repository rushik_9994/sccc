<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 *
 * Create Date: 2-05-2014
 *
 * Version of MYSQL_to_PHP: 1.1
 *
 * License: LGPL
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');



/**
 * Trial class similar to a model class
 */
class Trial extends BaseClass {

    public $TrialKeyID; //int(10)
    public $TrialProtocolNumber; //varchar(250)
    public $TrialCIRB; //tinyint(1)
    public $TimestampCreate; //datetime
    public $TimestampUpdate; //timestamp
    public $TrialCategoryKeyID; //int(10)
    public $TrialTypeKeyID; //int(10)
    public $TrialComment; //varchar(250)
    public $TrialOpenDate; //datetime
    public $TrialSuspend; //tinyint(1)
    public $TrialCloseDate; //datetime
    public $TrialClose; //tinyint(1)
    public $TrialTerminationDate; //datetime
    public $TrialTermination; //tinyint(1)
    public $TrialReportExclude; //tinyint(1)
    public $TrialTitle; //varchar(250)
    public $TrialTargetAccrual; //int(10)
    public $TrialTreatment;        //varchar(255) shared with public site
    public $TrialPhaseKeyID; //int(10)
    public $TrialCredit; //int(10)
    public $TrialQoL; //tinyint(1)
    public $TrialQoLC; //varchar(250)
    public $TrialNct; //varchar(100)
    public $TrialSummaryEn; //longtext
    public $TrialSummaryEs; //longtext
    public $TrialSummaryLast; //longtext

    //Pagination variables
    public $showPagination     = 0;
    public $showFirstButton    = 0;
    public $showPreviousButton = 0;
    public $showNextButton     = 0;
    public $legendText         = '';
    public $totalRowCount      = 0;


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_Trial($TrialProtocolNumber,$TimestampCreate,$TimestampUpdate,$TrialCategoryKeyID,$TrialTypeKeyID,$TrialComment,$TrialOpenDate,$TrialSuspend,$TrialCloseDate,$TrialClose,$TrialTerminationDate,$TrialTermination,$TrialReportExclude,$TrialTitle,$TrialTargetAccrual,$TrialTreatment,$TrialPhaseKeyID,$TrialCredit, $TrialCIRB, $TrialQoL, $TrialQoLC, $TrialNct, $TrialSummaryEn, $TrialSummaryEs, $TrialSummaryLast)
    {
        $this->TrialProtocolNumber   = $TrialProtocolNumber;
        $this->TrialCIRB             = $TrialCIRB;
        $this->TimestampCreate       = $TimestampCreate;
        $this->TimestampUpdate       = $TimestampUpdate;
        $this->TrialCategoryKeyID    = $TrialCategoryKeyID;
        $this->TrialTypeKeyID        = $TrialTypeKeyID;
        $this->TrialComment          = $TrialComment;
        $this->TrialOpenDate         = $TrialOpenDate;
        $this->TrialSuspend          = $TrialSuspend;
        $this->TrialCloseDate        = $TrialCloseDate;
        $this->TrialClose            = $TrialClose;
        $this->TrialTerminationDate  = $TrialTerminationDate;
        $this->TrialTermination      = $TrialTermination;
        $this->TrialReportExclude    = $TrialReportExclude;
        $this->TrialTitle            = $TrialTitle;
        $this->TrialTargetAccrual    = $TrialTargetAccrual;
        $this->TrialTreatment        = $TrialTreatment;
        $this->TrialPhaseKeyID       = $TrialPhaseKeyID;
        $this->TrialCredit           = $TrialCredit;
        $this->TrialQoL              = $TrialQoL;
        $this->TrialQoLC             = $TrialQoLC;
        $this->TrialNct              = $TrialNct;
        $this->TrialSummaryEn        = $TrialSummaryEn;
        $this->TrialSummaryEs        = $TrialSummaryEs;
        $this->TrialSummaryLast      = $TrialSummaryLast;
    }*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name;
     *
     * @param string|int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $result = $this->connection->RunQuery("SELECT Trial.*\n".
                                              "FROM   `Trial`\n".
                                              "WHERE  `TrialKeyID`=". $this->SqlQuote($key_row)
                                             );

        $found = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->TrialKeyID            = $row["TrialKeyID"];
            $this->TrialProtocolNumber   = $row["TrialProtocolNumber"];
            $this->TrialCIRB             = $row["TrialCIRB"];
            $this->TimestampCreate       = $row["TimestampCreate"];
            $this->TimestampUpdate       = $row["TimestampUpdate"];
            $this->TrialCategoryKeyID    = $row['TrialCategoryKeyID'];
            $this->TrialTypeKeyID        = $row["TrialTypeKeyID"];
            $this->TrialComment          = $row["TrialComment"];
            $this->TrialOpenDate         = $row["TrialOpenDate"];
            $this->TrialSuspend          = $row["TrialSuspend"];
            $this->TrialCloseDate        = $row["TrialCloseDate"];
            $this->TrialClose            = $row["TrialClose"];
            $this->TrialTerminationDate  = $row["TrialTerminationDate"];
            $this->TrialTermination      = $row["TrialTermination"];
            $this->TrialReportExclude    = $row["TrialReportExclude"];
            $this->TrialTitle            = $row["TrialTitle"];
            $this->TrialTargetAccrual    = $row["TrialTargetAccrual"];
            $this->TrialTreatment        = $row["TrialTreatment"];
            $this->TrialPhaseKeyID       = $row["TrialPhaseKeyID"];
            $this->TrialCredit           = $row["TrialCredit"];
            $this->TrialQoL              = $row["TrialQoL"];
            $this->TrialQoLC             = $row["TrialQoLC"];
            $this->TrialNct              = $row['TrialNct'];
            $this->TrialSummaryEn        = $row['TrialSummaryEn'];
            $this->TrialSummaryEs        = $row['TrialSummaryEs'];
            $this->TrialSummaryLast      = $row['TrialSummaryLast'];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param string|int $key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Trial', 'TrialKeyID', $key_row);
    }

    /**
     * Update the active row table on table
     * @throws \Exception
     */
    public function Save_Active_Row()
    {
        $sql = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            //Save the record
            TrackChanges::trackBeforeUpdate($this->connection, 'Trial', 'TrialKeyID', $this->TrialKeyID);
            $sql =
                "UPDATE `Trial` SET\n".
                "    `TrialProtocolNumber`=".      $this->SqlQuote($this->TrialProtocolNumber) .",\n".
                "    `TrialCIRB`=".                $this->SqlQuote($this->TrialCIRB) .",\n".
                "    `TrialCategoryKeyID`=".       $this->SqlQuote($this->TrialCategoryKeyID) .",\n".
                "    `TrialTypeKeyID`=".           $this->SqlQuote($this->TrialTypeKeyID) .",\n".
                "    `TrialComment`=".             $this->SqlQuote($this->TrialComment) .",\n".
                "    `TrialOpenDate`=".            $this->SqlQuote($this->TrialOpenDate) .",\n".
                "    `TrialSuspend`=".             $this->SqlQuote($this->TrialSuspend) .",\n".
                "    `TrialCloseDate`=".           $this->SqlQuote($this->TrialCloseDate) .",\n".
                "    `TrialClose`=".               $this->SqlQuote($this->TrialClose) .",\n".
                "    `TrialTerminationDate`=".     $this->SqlQuote($this->TrialTerminationDate) .",\n".
                "    `TrialTermination`=".         $this->SqlQuote($this->TrialTermination) .",\n".
                "    `TrialReportExclude`=".       $this->SqlQuote($this->TrialReportExclude) .",\n".
                "    `TrialTitle`=".               $this->SqlQuote($this->TrialTitle) .",\n".
                "    `TrialTargetAccrual`=".       $this->SqlQuote($this->TrialTargetAccrual) .",\n".
                "    `TrialTreatment`=".           $this->SqlQuote($this->TrialTreatment) .",\n".
                "    `TrialPhaseKeyID`=".          $this->SqlQuote($this->TrialPhaseKeyID) .",\n".
                "    `TrialCredit`=".              $this->SqlQuote($this->TrialCredit) .",\n".
                "    `TrialQoL`=".                 $this->SqlQuote($this->TrialQoL) .",\n".
                "    `TrialQoLC`=".                $this->SqlQuote($this->TrialQoLC) .",\n".
                "    `TrialNct`=".                 $this->SqlQuote($this->TrialNct).",\n".
                "    `TrialSummaryEn`=".           $this->SqlQuote($this->TrialSummaryEn).",\n".
                "    `TrialSummaryEs`=".           $this->SqlQuote($this->TrialSummaryEs).",\n".
                "    `TrialSummaryLast`=".         $this->SqlQuote($this->TrialSummaryLast)."\n".
                "WHERE `TrialKeyID`=".  $this->SqlQuote($this->TrialKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Trial','TrialKeyID', $this->TrialKeyID);

        } catch (\Exception $e) {
            $error = $e->getMessage();
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO `Trial` (`TrialProtocolNumber`, `TrialCIRB`, `TrialCategoryKeyID`, `TrialTypeKeyID`,\n".
                "                     `TrialComment`, `TrialOpenDate`, `TrialSuspend`,\n".
                "                     `TrialCloseDate`, `TrialClose`, `TrialTerminationDate`,\n".
                "                     `TrialTermination`, `TrialReportExclude`, `TrialTitle`,\n".
                "                     `TrialTargetAccrual`, `TrialTreatment`, `TrialPhaseKeyID`,\n".
                "                     `TrialCredit`, `TrialQoL`, `TrialQoLC`, `TrialNct`,\n".
                "                     `TrialSummaryEn`, `TrialSummaryEs`, `TrialSummaryLast`) VALUES (\n".
                $this->SqlQuote($this->TrialProtocolNumber). ",\n".
                $this->SqlQuote($this->TrialCIRB). ",\n".
                $this->SqlQuote($this->TrialCategoryKeyID). ",\n".
                $this->SqlQuote($this->TrialTypeKeyID). ",\n".
                $this->SqlQuote($this->TrialComment). ",\n".
                $this->SqlQuote($this->TrialOpenDate). ",\n".
                $this->SqlQuote($this->TrialSuspend). ",\n".
                $this->SqlQuote($this->TrialCloseDate). ",\n".
                $this->SqlQuote($this->TrialClose). ",\n".
                $this->SqlQuote($this->TrialTerminationDate). ",\n".
                $this->SqlQuote($this->TrialTermination). ",\n".
                $this->SqlQuote($this->TrialReportExclude). ",\n".
                $this->SqlQuote($this->TrialTitle). ",\n".
                $this->SqlQuote($this->TrialTargetAccrual). ",\n".
                $this->SqlQuote($this->TrialTreatment). ",\n".
                $this->SqlQuote($this->TrialPhaseKeyID). ",\n".
                $this->SqlQuote($this->TrialCredit). ",\n".
                $this->SqlQuote($this->TrialQoL). ",\n".
                $this->SqlQuote($this->TrialQoLC). ",\n".
                $this->SqlQuote($this->TrialNct).",\n".
                $this->SqlQuote($this->TrialSummaryEn).",\n".
                $this->SqlQuote($this->TrialSummaryEs).",\n".
                $this->SqlQuote($this->TrialSummaryLast).
                ")";
            $this->TrialKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Trial','TrialKeyID');

        } catch (\Exception $e) {
            $error = $e->getMessage();
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order optional                 Default is 'ASC'
     * @param int    $from optional                  Default is 0   (for LIMIT $from,...)
     * @param int    $limit optional                 Default is 0   (for LIMIT ...,$limit)
     * @param array  $sClauses
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($column, $order='ASC', $from=0, $limit=0, $sClauses=array())
    {
        $keys = array();

        $i = 0;
        if ((!isset($order)) || (!is_string($order))) { $order='ASC'; }
        $order = strtoupper($order);
        if (($order!='ASC') && ($order!='DESC')) { $order='ASC'; }

        if ($column=='TrialKeyID') { $column="Trial`.`TrialKeyID"; } //Note: this will be surrounded by `...`
        $column = $this->connection->Quote($column);

        //Support SearchFilter
        //Get additional joins and where clauses as supplied by the searchFilter class
        //print "<pre>".print_r($sClauses,true)."</pre>";
        if ((!isset($sClauses)) || (!is_array($sClauses))) { $sClauses=array(); }
        if ((!isset($sClauses['joins'])) || (!is_array($sClauses['joins'])))   { $sClauses['joins']=array(); }
        if ((!isset($sClauses['where'])) || (!is_array($sClauses['where'])))   { $sClauses['where']=array(); }
        if ((!isset($sClauses['debug'])) || (!is_numeric($sClauses['debug']))) { $sClauses['debug']=0; }
        $joins    = '';
        $whereAnd = '';
        $needTrialType        = 1;
        $needTrialCategory    = 1;
        //$needTrialDiseaseType = 1;
        foreach($sClauses['joins'] as $join=>$ignore) {
            $joins .= $join;
            if (strpos($join, 'TrialType')!==false)        { $needTrialType=0; }
            if (strpos($join, 'TrialCategory')!==false)    { $needTrialCategory=0; }
          //if (strpos($join, 'TrialDiseaseType')!==false) { $needTrialDiseaseType=0; }
        }
        reset($sClauses['joins']);
        if ($joins) {
            $joins = trim($joins)."\n";
        }
        if ($needTrialType) {
            $joins .= "\nLEFT JOIN TrialType ON Trial.TrialTypeKeyID=TrialType.TrialTypeKeyID";
        }
        if ($needTrialCategory) {
            $joins .= "\nLEFT JOIN TrialCategory ON Trial.TrialCategoryKeyID=TrialCategory.TrialCategoryKeyID";
        }
        if ($joins) { $joins = trim($joins)."\n"; }

        foreach($sClauses['where'] as $ignore=>$whr) {
            $whereAnd .= $whr;
        }
        reset($sClauses['where']);
        if ($whereAnd) { $whereAnd = trim($whereAnd)."\n"; }

        //COUNT ROWS:  Build the query to just count our results
        $sql = "SELECT count(*) as cnt\n".
               "FROM   `Trial`\n".
               $joins.
               "WHERE  `Trial`.`TrialKeyID` > 0\n".
               $whereAnd;

        $result = $this->connection->RunQuery($sql);
        $this->totalRowCount = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (isset($row['cnt'])) {
                $this->totalRowCount = $row['cnt'];
                break;
            }
        }

        //GET KEYS:  Build the query to actually get our Keys
        $sql = "SELECT `Trial`.`TrialKeyID`\n".
               "FROM   `Trial`\n".
               $joins.
               "WHERE  `Trial`.`TrialKeyID` > 0\n".
               $whereAnd;

        //Support table column sorting 'order_field_0' (thead)...
        $orderBy = "ORDER BY `".$this->connection->Quote($column)."` $order\n";
        $orderIndex     = (isset($sClauses['order_index']))       ? $sClauses['order_index'] : '_0';
        $orderField     = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }

        if ($orderField) {
            if ($orderField == 'order_protocol'.$orderIndex) {
                $orderBy = "ORDER BY Trial.TrialProtocolNumber $orderDirection";
            } else if ($orderField == 'order_open_date'.$orderIndex) {
                $orderBy = "ORDER BY Trial.TrialOpenDate $orderDirection, Trial.TrialProtocolNumber ASC";
            } else if ($orderField == 'order_close_date'.$orderIndex) {
                $orderBy = "ORDER BY Trial.TrialCloseDate $orderDirection, Trial.TrialProtocolNumber ASC";
            } else if ($orderField == 'order_term_date'.$orderIndex) {
                $orderBy = "ORDER BY Trial.TrialTerminationDate $orderDirection, Trial.TrialProtocolNumber ASC";
            } else if ($orderField == 'order_suspended'.$orderIndex) {
                $orderBy = "ORDER BY Trial.TrialSuspend $orderDirection, Trial.TrialProtocolNumber ASC";
            } else if ($orderField == 'order_trial_type'.$orderIndex) {
                $orderBy = "ORDER BY TrialType.TrialTypeText $orderDirection, Trial.TrialProtocolNumber ASC";
            } else if ($orderField == 'order_trial_category'.$orderIndex) {
                $orderBy = "ORDER BY TrialCategory.TrialCategoryText $orderDirection, Trial.TrialProtocolNumber ASC";
            } else if ($orderField == 'order_trial_cirb'.$orderIndex) {
                $orderBy = "ORDER BY Trial.TrialCIRB $orderDirection, Trial.TrialProtocolNumber ASC";
            }
        } 
        $sql .= $orderBy;

        //Calculate our final SELECT query with limit clause (if $limit>0)
        if ((!isset($from))  || (!is_numeric($from))  || ($from  < 0)) { $from  = 0; }
        if ((!isset($limit)) || (!is_numeric($limit)) || ($limit < 0)) { $limit = 0; }

        if (($from > 0 ) && ($limit==0)) {
            //get pagination limit
            $pg = new Pagination();
            $limit = $pg->getPaginationSize('trials.pagination.size');
        }
        $this->showPagination     = (($from) || ($limit)) ? 1 : 0;
        $this->showFirstButton    = ($from > 0)  ?  1  : 0;
        $this->showPreviousButton = ($from > 0)  ?  1  : 0;
        $this->showNextButton     = 0;
        $this->legendText         = '';
        $this->limitFrom          = $from;
        $this->limitSlice         = $limit;
        $sql .= (($from) || ($limit))  ?  "\nLIMIT $from,".($limit+1) : '';

        //DebugDisplay for SearchFilter
        if ((0) || ($sClauses['debug'])) {
            print "<br /><pre style='font-size:8pt;color:black;'>$sql</pre>";
            print "<pre style='font-size:8pt;color:blue;'>".print_r($sClauses,true)."</pre>";
            if ($sClauses['debug'] > 1) { exit; }
        }

        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (($limit) && ($i >= $limit)) {
                $this->showNextButton = 1;
                break;
            }
            $keys[$i] = $row["TrialKeyID"];
            $i++;
            if ($limit > 0) { $this->legendText = "Displaying Records ".($from+1)." to ".($from+$i); }
        }

        if (count($keys)==0) { $this->showPagination=0; }

        return $keys;
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * IMPORTANT:  CALLER is responsible for quoting the $where clause!!!  Todo: fix caller!
     *
     * @param string $key
     * @param string $where
     * @param string $column
     * @param string $order
     * @param int    $from optional                  Default is 0   (for LIMIT $from,...)
     * @param int    $limit optional                 Default is 0   (for LIMIT ...,$limit)
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysWhereOrderBy($key, $where, $column, $order, $from=0, $limit=0)
    {
        $keys = array(); $i = 0;

        $key    = $this->connection->Quote($key);
        $column = $this->connection->Quote($column);

        $x = strtoupper(trim($where));
        if (($x) && (substr($x,0,6) != 'WHERE ')) { $where = "WHERE $where"; }
        if (!$x) { $where = "WHERE `Trial`.`TrialKeyID` > 0"; }

        if ((!isset($order)) || (!is_string($order))) { $order='ASC'; }
        $order = strtoupper($order);
        if (($order!='ASC') && ($order!='DESC')) { $order='ASC'; }

        $sql = "SELECT `$key`\n".
               "FROM   `Trial`\n".
               "$where\n".
               "ORDER BY `".$this->connection->Quote($column)."` $order";

        //Calculate our final SELECT query with limit clause (if $limit>0)
        if ((!isset($from))  || (!is_numeric($from))  || ($from  < 0)) { $from  = 0; }
        if ((!isset($limit)) || (!is_numeric($limit)) || ($limit < 0)) { $limit = 0; }
        if (($from > 0 ) && ($limit==0)) {
            //get pagination limit
            $pg = new Pagination();
            $limit = $pg->getPaginationSize('trials.pagination.size');
        }
        $this->showPagination     = (($from) || ($limit)) ? 1 : 0;
        $this->showFirstButton    = ($from > 0)  ?  1  : 0;
        $this->showPreviousButton = ($from > 0)  ?  1  : 0;
        $this->showNextButton     = 0;
        $this->legendText         = '';
        $sql .= (($from) || ($limit))  ?  "\nLIMIT ".$this->connection->Quote($from).','.($limit+1) : '';
                     
        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (($limit) && ($i >= $limit)) {
                $this->showNextButton = 1;
                break;
            }
            $keys[$i] = $row["TrialKeyID"];
            $i++;
            if ($limit > 0) { $this->legendText = "Displaying Records ".($from+1)." to ".($from+$i); }
        }

        if (count($keys)==0) { $this->showPagination=0; }

        return $keys;
    }

    /**
     * Returns a select field and assigns selected
     *
     * @param  string|int $selected
     * @return string $htmlCode
     * @throws \Exception
     */
    public function CreateSelect($selected='')
    {
        $select = '<select name="TrialKeyIDOUT" id="TrialKeyIDOUT" class="button">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
        $result = $this->connection->RunQuery("SELECT `TrialKeyID`, `TrialProtocolNumber`\n".
                                              "FROM   `Trial`\n".
                                              "ORDER BY `TrialProtocolNumber`");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialKeyID          = htmlentities($row["TrialKeyID"]);
            $escTrialProtocolNumber = htmlentities($row["TrialProtocolNumber"]);
            if ($row["TrialKeyID"] == $selected) {
                continue; //+++++++++ might be a problem!
            } else {
                $select .= "<option value=\"$escTrialKeyID\">$escTrialProtocolNumber</option>\n";
            }
        }
        $select .= '</select>'."\n";
        return $select;
    }

    /**
     * Returns a select field and assigns selected
     *
     * IMPORTANT:  CALLER is responsible for quoting the $where clause!!!  Todo: fix caller!
     *
     * @param  string|int $selected
     * @param  string $where
     * @return string $htmlCode
     * @throws \Exception
     */
    public function CreateSelectSelected($selected, $where)
    {
        $select = '<select name="TrialKeyIDOUT" id="TrialKeyIDOUT" class="halfwidth" >'."\n";
        $select .= '<option value="">Select...</option>'."\n";

        $x = strtoupper(trim($where));
        if (($x) && (substr($x,0,6) != 'WHERE ')) { $where = "WHERE $where"; }

        $result = $this->connection->RunQuery("SELECT `TrialKeyID`, `TrialProtocolNumber`\n".
                                              "FROM   `Trial`\n".
                                              "$where\n".
                                              "ORDER BY `TrialProtocolNumber`");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialKeyID          = htmlentities($row["TrialKeyID"]);
            $escTrialProtocolNumber = htmlentities($row["TrialProtocolNumber"]);

            if ($row["TrialKeyID"] == $selected){
                $select .= "<option value=\"$escTrialKeyID\" selected>$escTrialProtocolNumber</option>\n";
            } else {
                $select .= "<option value=\"$escTrialKeyID\">$escTrialProtocolNumber</option>\n";
            }
        }
        $select .= '</select>'."\n";
        return $select;
    }


    /**
     * Returns a select field and assigns selected
     *
     * IMPORTANT:  CALLER is responsible for quoting the $where clause!!!  Todo: fix caller!
     *
     * @param string $where
     * @param string|int $selected
     * @return string $select
     * @throws \Exception
     */
    public function CreateSelectWhere($where, $selected)
    {
        $select = '<select name="TrialKeyIDOUT" id="TrialKeyIDOUT" class="button" >'."\n";
        $select .= '<option value="">Select...</option>'."\n";
        $result = $this->connection->RunQuery("SELECT `TrialKeyID`, `TrialProtocolNumber`\n".
                                              "FROM   `Trial`\n".
                                              "$where\n".
                                              "ORDER BY `TrialProtocolNumber`");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialKeyID          = htmlentities($row["TrialKeyID"]);
            $escTrialProtocolNumber = htmlentities($row["TrialProtocolNumber"]);
            if ($row["TrialKeyID"] == $selected){
                $select .= "<option value=\"$escTrialKeyID\" selected>$escTrialProtocolNumber</option>\n";
            } else {
                $select .= "<option value=\"$escTrialKeyID\">$escTrialProtocolNumber</option>\n";
            }
        }
        $select .= '</select>'."\n";
        return $select;
    }

    /**
     * @return null|int $TrialKeyID
     */
    public function getTrialKeyID()
    {
        return $this->TrialKeyID;
    }

    /**
     * @return null|string $TrialProtocolNumber
     */
    public function getTrialProtocolNumber()
    {
        return $this->TrialProtocolNumber;
    }

    /**
     * @return null|string $TimestampCreate
     */
    public function getTimestampCreate()
    {
        return $this->TimestampCreate;
    }

    /**
     * @return null|string $TimestampUpdate
     */
    public function getTimestampUpdate()
    {
        return $this->TimestampUpdate;
    }

    /**
     * @return null|int $TrialCategoryKeyID
     */
    public function getTrialCategoryKeyID()
    {
        return $this->TrialCategoryKeyID;
    }

    /**
     * @return null|int $TrialTypeKeyID
     */
    public function getTrialTypeKeyID()
    {
        return $this->TrialTypeKeyID;
    }

    /**
     * @return null|string $TrialComment
     */
    public function getTrialComment()
    {
        return $this->TrialComment;
    }

    /**
     * @return null|string $TrialOpenDate
     */
    public function getTrialOpenDate()
    {
        return $this->TrialOpenDate;
    }

    /**
     * @return null|int $TrialSuspend
     */
    public function getTrialSuspend()
    {
        return $this->TrialSuspend;
    }

    /**
     * @return null|string $TrialCloseDate
     */
    public function getTrialCloseDate()
    {
        return $this->TrialCloseDate;
    }

    /**
     * @return null|int $TrialClose
     */
    public function getTrialClose()
    {
        return $this->TrialClose;
    }

    /**
     * @return null|string $TrialTerminationDate
     */
    public function getTrialTerminationDate()
    {
        return $this->TrialTerminationDate;
    }

    /**
     * @return null|int $TrialTermination
     */
    public function getTrialTermination()
    {
        return $this->TrialTermination;
    }

    /**
     * @return null|int $TrialReportExclude
     */
    public function getTrialReportExclude()
    {
        return $this->TrialReportExclude;
    }

    /**
     * @return null|string $TrialTitle
     */
    public function getTrialTitle()
    {
        return $this->TrialTitle;
    }

    /**
     * @return null|int $TrialTargetAccrual
     */
    public function getTrialTargetAccrual()
    {
        return $this->TrialTargetAccrual;
    }

    /**
     * @return null|int $TrialTreatment
     */
    public function getTrialTreatment()
    {
        return $this->TrialTreatment;
    }

    /**
     * @return null|int $TrialPhaseKeyID
     */
    public function getTrialPhaseKeyID()
    {
        return $this->TrialPhaseKeyID;
    }

    /**
     * @return null|int $TrialCredit
     */
    public function getTrialCredit()
    {
        return $this->TrialCredit;
    }

    /**
     * @param int $TrialKeyID
     */
    public function setTrialKeyID($TrialKeyID)
    {
        $this->TrialKeyID = $TrialKeyID;
    }

    /**
     * @param string $TrialProtocolNumber
     */
    public function setTrialProtocolNumber($TrialProtocolNumber)
    {
        $this->TrialProtocolNumber = $TrialProtocolNumber;
    }

    /**
     * @param string $TimestampCreate
     */
    public function setTimestampCreate($TimestampCreate)
    {
        $this->TimestampCreate = $TimestampCreate;
    }

    /**
     * @param string $TimestampUpdate
     */
    public function setTimestampUpdate($TimestampUpdate)
    {
        $this->TimestampUpdate = $TimestampUpdate;
    }

    /**
     * @param int $TrialCategoryKeyID
     */
    public function setTrialCategoryKeyID($TrialCategoryKeyID)
    {
        $this->TrialCategoryKeyID = $TrialCategoryKeyID;
    }

    /**
     * @param int $TrialTypeKeyID
     */
    public function setTrialTypeKeyID($TrialTypeKeyID)
    {
        $this->TrialTypeKeyID = $TrialTypeKeyID;
    }

    /**
     * @param string $TrialComment
     */
    public function setTrialComment($TrialComment)
    {
        $this->TrialComment = $TrialComment;
    }

    /**
     * @param string $TrialOpenDate
     */
    public function setTrialOpenDate($TrialOpenDate)
    {
        $this->TrialOpenDate = $TrialOpenDate;
    }

    /**
     * @param int $TrialSuspend
     */
    public function setTrialSuspend($TrialSuspend)
    {
        $this->TrialSuspend = $TrialSuspend;
    }

    /**
     * @param string $TrialCloseDate
     */
    public function setTrialCloseDate($TrialCloseDate)
    {
        $this->TrialCloseDate = $TrialCloseDate;
    }

    /**
     * @param int $TrialClose
     */
    public function setTrialClose($TrialClose)
    {
        $this->TrialClose = $TrialClose;
    }

    /**
     * @param string $TrialTerminationDate
     */
    public function setTrialTerminationDate($TrialTerminationDate)
    {
        $this->TrialTerminationDate = $TrialTerminationDate;
    }

    /**
     * @param string $TrialTermination
     */
    public function setTrialTermination($TrialTermination)
    {
        $this->TrialTermination = $TrialTermination;
    }

    /**
     * @param int $TrialReportExclude
     */
    public function setTrialReportExclude($TrialReportExclude)
    {
        $this->TrialReportExclude = $TrialReportExclude;
    }

    /**
     * @param string $TrialTitle
     */
    public function setTrialTitle($TrialTitle)
    {
        $this->TrialTitle = $TrialTitle;
    }

    /**
     * @param int $TrialTargetAccrual
     */
    public function setTrialTargetAccrual($TrialTargetAccrual)
    {
        $this->TrialTargetAccrual = $TrialTargetAccrual;
    }

    /**
     * @param string $TrialTreatment
     */
    public function setTrialTreatment($TrialTreatment)
    {
        $this->TrialTreatment = $TrialTreatment;
    }

    /**
     * @param int $TrialPhaseKeyID
     */
    public function setTrialPhaseKeyID($TrialPhaseKeyID)
    {
        $this->TrialPhaseKeyID = $TrialPhaseKeyID;
    }

    /**
     * @param int $TrialCredit
     */
    public function setTrialCredit($TrialCredit)
    {
        $this->TrialCredit = $TrialCredit;
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        global $callingEvent;

        //Validate
        do {
            if ((!isset($this->TrialTypeKeyID)) || (!$this->TrialTypeKeyID) || (!is_numeric($this->TrialTypeKeyID))) {
                $validationMessage = 'TrialType cannot be empty';
                break;  //++++++++ newly added
            }

            $val = ((isset($this->TrialKeyID)) ? $this->TrialKeyID : null);  //vld: Trial.TrialKeyID
            if ($validationMessage = validateField($val,'TrialKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialProtocolNumber)) ? $this->TrialProtocolNumber : null);  //vld: Trial.TrialProtocolNumber
            if ($validationMessage = validateField($val,'TrialProtocolNumber','nestring',__CLASS__)) { break; }

            $val = ((isset($this->TrialCIRB)) ? $this->TrialCIRB : null);  //vld: Trial.TrialCIRB
            if ($validationMessage = validateField($val,'TrialCIRB','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->TimestampCreate)) ? $this->TimestampCreate : null);  //vld: Trial.TimestampCreate
            if ($validationMessage = validateField($val,'TimestampCreate','nullok;emptyok;zerodatetimeeok;datetime',__CLASS__)) { break; }

            $val = ((isset($this->TimestampUpdate)) ? $this->TimestampUpdate : null);  //vld: Trial.TimestampUpdate
            if ($validationMessage = validateField($val,'TimestampUpdate','nullok;emptyok;zerodatetimeeok;datetime',__CLASS__)) { break; }

            $val = ((isset($this->TrialCategoryKeyID)) ? $this->TrialCategoryKeyID : null);  //vld: Trial.TrialCategoryKeyID
            if ($validationMessage = validateField($val,'TrialCategoryKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialTypeKeyID)) ? $this->TrialTypeKeyID : null);  //vld: Trial.TrialTypeKeyID
            if ($validationMessage = validateField($val,'TrialTypeKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialComment)) ? $this->TrialComment : null);  //vld: Trial.TrialComment
            if ($validationMessage = validateField($val,'TrialComment','nullok;emptyok;nestring',__CLASS__)) { break; }

            $val = ((isset($this->TrialOpenDate)) ? $this->TrialOpenDate : null);  //vld: Trial.TrialOpenDate
            if ($validationMessage = validateField($val,'TrialOpenDate','zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->TrialSuspend)) ? $this->TrialSuspend : null);  //vld: Trial.TrialSuspend
            if ($validationMessage = validateField($val,'TrialSuspend','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->TrialCloseDate)) ? $this->TrialCloseDate : null);  //vld: Trial.TrialCloseDate
            if ($validationMessage = validateField($val,'TrialCloseDate','nullok;zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->TrialClose)) ? $this->TrialClose : null);  //vld: Trial.TrialClose
            if ($validationMessage = validateField($val,'TrialClose','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->TrialTerminationDate)) ? $this->TrialTerminationDate : null);  //vld: Trial.TrialTerminationDate
            $rulePrefix = ((isset($callingEvent)) && ($callingEvent == 'trialAddEdit')) ? 'nullok;' : '';
            if ($validationMessage = validateField($val,'TrialTerminationDate',$rulePrefix.'zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->TrialTermination)) ? $this->TrialTermination : null);  //vld: Trial.TrialTermination
            if ($validationMessage = validateField($val,'TrialTermination','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->TrialReportExclude)) ? $this->TrialReportExclude : null);  //vld: Trial.TrialReportExclude
            if ($validationMessage = validateField($val,'TrialReportExclude','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->TrialTitle)) ? $this->TrialTitle : null);  //vld: Trial.TrialTitle
            if ($validationMessage = validateField($val,'TrialTitle','nullok;emptyok;nestring',__CLASS__)) { break; } //----!!! emptyok?

            $val = ((isset($this->TrialTargetAccrual)) ? $this->TrialTargetAccrual : null); //vld: TrialTargetAccrual
            if ($validationMessage = validateField($val,'TrialTargetAccrual','zeroorpositiveint',__CLASS__)) { break; } //----!!! emptyok?

            $val = ((isset($this->TrialPhaseKeyID)) ? $this->TrialPhaseKeyID : null);  //vld: Trial.TrialPhaseKeyID
            if ($validationMessage = validateField($val,'TrialPhaseKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialCredit)) ? $this->TrialCredit : null);  //vld: Trial.TrialCredit
            if ($validationMessage = validateField($val,'TrialCredit','zeroorpositivenumber',__CLASS__)) { break; }

            $val = ((isset($this->TrialQoL)) ? $this->TrialQoL : null);  //vld: Trial.TrialQoL
            if ($validationMessage = validateField($val,'TrialQoL','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->TrialQoLC)) ? $this->TrialQoLC : null);  //vld: Trial.TrialQoLC
            if ($validationMessage = validateField($val,'TrialQoLC','nullok;zeroorpositivenumber',__CLASS__)) { break; }

            $val = ((isset($this->TrialNct)) ? $this->TrialNct : '');  //vld: Trial.TrialNct
            if ($validationMessage = validateField($val,'TrialNct','nullok;emptyok;nestring;',__CLASS__)) { break; }  //++++++ removed: nctcheck

            $val = ((isset($this->TrialSummaryEn)) ? $this->TrialSummaryEn : '');  //vld: Trial.TrialSummaryEn
            if ($validationMessage = validateField($val,'TrialSummaryEn','nullok;emptyok;nestring;',__CLASS__)) { break; }

            $val = ((isset($this->TrialSummaryEs)) ? $this->TrialSummaryEs : '');  //vld: Trial.TrialSummaryEs
            if ($validationMessage = validateField($val,'TrialSummaryEs','nullok;emptyok;nestring;',__CLASS__)) { break; }

            $val = ((isset($this->TrialSummaryLast)) ? $this->TrialSummaryLast : '');  //vld: Trial.TrialSummaryLast
            if ($validationMessage = validateField($val,'TrialSummaryLast','nullok;emptyok;nestring;',__CLASS__)) { break; }

            //Shared with the public site
            $allowedTags = '<strong> <b> <i> <em> <u> <br> <p>';

            $val = ((isset($this->TrialTreatment)) ? $this->TrialTreatment : null);  //vld: Trial.TrialTreatment
            if ($validationMessage = validateField($val,'TrialTreatment','nullok;emptyok;nestring;checktags;',__CLASS__)) { break; }
            if (isset($this->TrialTreatment)) {
                $this->TrialTreatment = strip_tags($this->TrialTreatment, $allowedTags);
            }

        } while (false);

        return $validationMessage;
    }

}

