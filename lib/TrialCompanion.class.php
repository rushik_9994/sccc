<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialCompanion extends BaseClass {

	public $TrialCompanionKeyID; //int(10)
	public $TrialKeyIDIN; //int(10)
	public $TrialKeyIDOUT; //int(10)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialCompanion($TrialKeyIDIN,$TrialKeyIDOUT){
		$this->TrialKeyIDIN = $TrialKeyIDIN;
		$this->TrialKeyIDOUT = $TrialKeyIDOUT;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM TrialCompanion WHERE TrialCompanionKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->TrialCompanionKeyID = $row["TrialCompanionKeyID"];
			$this->TrialKeyIDIN = $row["TrialKeyIDIN"];
			$this->TrialKeyIDOUT = $row["TrialKeyIDOUT"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param string $key
     * @param mixed $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key, $key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialCompanion', $key, $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE TrialCompanion SET\n".
                "TrialKeyIDIN = ".             $this->SqlQuote($this->TrialKeyIDIN).",\n".
                "TrialKeyIDOUT = ".            $this->SqlQuote($this->TrialKeyIDOUT)."\n".
                "WHERE TrialCompanionKeyID = ".$this->SqlQuote($this->TrialCompanionKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialCompanion','TrialCompanionKeyID', $this->TrialCompanionKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO TrialCompanion (TrialKeyIDIN, TrialKeyIDOUT) VALUES (\n".
                $this->SqlQuote($this->TrialKeyIDIN).",\n".
                $this->SqlQuote($this->TrialKeyIDOUT).')';
            $this->TrialCompanionKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialCompanion','TrialCompanionKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $key
     * @param  string $where
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysWhereOrderBy($key, $where, $column, $order)
    {
		$keys = array();

		//+++++where not quoted
        $quKey    = trim($this->connection->SqlQuote($key), "'");
        $quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT $quKey FROM TrialCompanion WHERE $where ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row[$key];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialCompanionKeyID FROM TrialCompanion ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialCompanionKeyID"];
            $i++;
        }
	    return $keys;
	}

	/**
	 * @return null|int $TrialCompanionKeyID
	 */
	public function getTrialCompanionKeyID()
    {
		return $this->TrialCompanionKeyID;
	}

	/**
	 * @return null|int $TrialKeyIDIN
	 */
	public function getTrialKeyIDIN()
    {
		return $this->TrialKeyIDIN;
	}

	/**
	 * @return null|int $TrialKeyIDOUT
	 */
	public function getTrialKeyIDOUT()
    {
		return $this->TrialKeyIDOUT;
	}

	/**
	 * @param int $TrialCompanionKeyID
	 */
	public function setTrialCompanionKeyID($TrialCompanionKeyID){
		$this->TrialCompanionKeyID = $TrialCompanionKeyID;
	}

	/**
	 * @param int $TrialKeyIDIN
	 */
	public function setTrialKeyIDIN($TrialKeyIDIN)
    {
		$this->TrialKeyIDIN = $TrialKeyIDIN;
	}

	/**
	 * @param int $TrialKeyIDOUT
	 */
	public function setTrialKeyIDOUT($TrialKeyIDOUT)
    {
		$this->TrialKeyIDOUT = $TrialKeyIDOUT;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->TrialCompanionKeyID)) ? $this->TrialCompanionKeyID : null);  //vld: TrialCompanion.TrialCompanionKeyID
            if ($validationMessage = validateField($val,'TrialCompanionKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyIDIN)) ? $this->TrialKeyIDIN : null);  //vld: TrialCompanion.TrialKeyIDIN
            if ($validationMessage = validateField($val,'TrialKeyIDIN','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyIDOUT)) ? $this->TrialKeyIDOUT : null);  //vld: TrialCompanion.TrialKeyIDOUT
            if ($validationMessage = validateField($val,'TrialKeyIDOUT','positiveint',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

