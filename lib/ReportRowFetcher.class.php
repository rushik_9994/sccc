<?php
/*
 * Author: Reinhard Hopperger -- office@hope-soft.com
 *
 * Create Date: 9-07-2014
 *
 */

Class ReportRowFetcher
{
    /**
     * Database connection to use
     */
    protected $_connection              = null;

    /**
     * ReportInfo array ref
     */
    protected $_reportInfo              = array();

    /**
     * Last error string
     */
    protected $_error                   = '';

    /**
     * Database connection to use
     */
    protected $_sql                     = '';

    /**
     * Fieldname to use groupBy logic on (can be empty string)
     */
    protected $_groupByField            = '';

    /**
     * Last group value
     */
    protected $_lastGroupValue          = '';
    
    /**
     * Current group value
     */
    protected $_thisGroupValue          = '';
    
    /**
     * Next group value
     */
    protected $_nextGroupValue          = '';

    /**
     * From for limit clause
     */
    protected $_from                   = 0;

    /**
     * Slice size for limit clause
     */
    protected $_sliceSize               = 5000;

    /**
     * Slice rows
     */
    protected $_xrows                   = array();
    
    /**
     * The xrow just after the current slice (if any)
     */
    protected $_nextXrow                = array();

    /**
     * Current index within slice
     */
    protected $_xrowIndex               = 0;

    /**
     * Absolute index within the whole set
     */
    protected $_absIndex                = 0;

    /**
     * Flag: Have more rows (at least one more row to be processed after this slice)
     */
    protected $_haveMoreRows            = 0;

    /**
     * Is there another slice waiting after the current one?
     */
     protected $_haveMoreSlicesToDo     = 1;

    /**
     * Flag: Is first row
     */
    protected $_firstInAll              = 1;


    /**
     * Constructor
     *
     * @param object $connection
     * @param string $sql                 Needs to contain the string '-- (LIMIT_CLAUSE)' somewhere
     */
    public function __construct($connection, &$reportInfo) {
        $this->_connection     = $connection;
        $this->_reportInfo     = $reportInfo;
        $this->_sql            = (isset($reportInfo['sql']))            ?  $reportInfo['sql']  : '';
        $this->_groupByField   = (isset($reportInfo['groupByField']))   ?  $reportInfo['groupByField'] : '';
        $this->_sliceSize      = (isset($reportInfo['sliceSize']))      ?  $reportInfo['sliceSize'] : 5000;
        $this->_from           = (isset($reportInfo['from']))           ?  $reportInfo['from'] : 0;
        $this->_lastGroupValue = (isset($reportInfo['lastGroupValue'])) ?  $reportInfo['lastGroupValue'] : '';
        $this->_nextGroupValue = (isset($reportInfo['nextGroupValue'])) ?  $reportInfo['nextGroupValue'] : '';
        $this->_nextXrow       = array();

        $this->_loadNextSlice();
    }

    /**
     * Get the next row if possible
     *
     * @param  array ref $reportInfo
     * @return array     array($xrow, $row)
     */
    public function getNextRow(&$reportInfo)
    {
        $xrow    = array();
        $row     = array();

        //Do we need to load a new slice?
        if (!isset($this->_xrows[$this->_xrowIndex])) {
            $this->_loadNextSlice($reportInfo);
        }

        //Do we have our xrowIndex in our xrows now?
        if (isset($this->_xrows[$this->_xrowIndex])) {
            $xrow = $this->_xrows[$this->_xrowIndex];
            
            //Add group information to the xrow
            $xrow['x_firstInAll'] = $this->_firstInAll;
            $this->_firstInAll  = 0;

            $xrow['x_lastInAll']  = 0;
            if (!$this->_haveMoreSlicesToDo) {
                if ($this->_xrowIndex == (count($this->_xrows) - 1)) {
                    $xrow['x_lastInAll'] = 1;
                }
            }

            //Basic group values
            $xrow['x_lastGroupValue'] = $this->_lastGroupValue;

            $this->_thisGroupValue    = (isset($xrow[$this->_groupByField])) ?  $xrow[$this->_groupByField] : '';
            $xrow['x_thisGroupValue'] = $this->_thisGroupValue;
            $this->_lastGroupValue    = $this->_thisGroupValue;

            $nxt = (isset($this->_xrows[($this->_xrowIndex + 1)])) 
                       ? $this->_xrows[($this->_xrowIndex + 1)] : $this->_nextXrow;
            $xrow['x_nextGroupValue'] = (isset($nxt[$this->_groupByField])) ?  $nxt[$this->_groupByField] : '';

            //Decision flags
            $xrow['x_firstInGroup'] = ($xrow['x_lastGroupValue'] != $xrow['x_thisGroupValue'])     ? 1 : 0;
            $xrow['x_lastInGroup']  = ($xrow['x_nextGroupValue'] != $xrow['x_thisGroupValue'])     ? 1 : 0;
            $xrow['x_isStart']      = ($xrow['x_lastGroupValue']=='')                              ? 1 : 0;
            $xrow['x_isEnd']        = ($xrow['x_nextGroupValue']=='')                              ? 1 : 0;

            $row  = $this->_removeXColumnsInRow($xrow);
            $this->_xrowIndex++;
        }

        return array($xrow, $row);
    }

    /**
     * Returns a 0/1 flag indicating if we have run through the whole set or not
     *
     * @param  array ref $reportInfo
     * @return int       $done
     */
    public function done(&$reportInfo)
    {
        if (!isset($this->_xrows))     { $this->_xrows = array(); }
        if (!isset($this->_xrowIndex)) { $this->_xrowIndex = 0; }
        $done = ((isset($this->_xrows[$this->_xrowIndex])) || ($this->_haveMoreSlicesToDo))  ?  0 : 1;
        return $done;
    }

    protected function _loadNextSlice(&$reportInfo)
    {
        $reportInfo['from']         = $this->_from;
    	$reportInfo['haveMoreRows'] = $this->_haveMoreRows;
        $reportInfo['sliceSql']     = $this->_sql;

        $this->_xrows              = array();
        $this->_xrowAfterSlice     = array();
        $this->_haveMoreSlicesToDo = 0;
        $this->_xrowIndex          = 0;
        $this->_nextXrow           = array();

        $limitClause           = 'LIMIT '.$this->_from.','.($this->_sliceSize + 1)."\n";
        $sql = str_replace("-- (LIMIT_CLAUSE)", $limitClause, $this->_sql);
        //print "<pre style='font-size:8pt;color:red;'>$sql</pre>";

    	$result = $this->_connection->RunQuery($sql);
    	$tmprows = array();
    	while($row = $result->fetch_array(MYSQLI_ASSOC)) {
    	    $tmprows[] = $row;
    	}
    	for ($r=0,$maxr=count($tmprows); $r < $maxr; $r++) {
    	    $row = $tmprows[$r];
    	    if (count($this->_xrows) < $this->_sliceSize) {
    	        $this->_xrows[]      = $row;
    	        $this->_from++;
    	    } else {
    	        //We have equal or more than the requested 'size' number of rows...
    	        $this->_haveMoreSlicesToDo = 1;
    	        $this->_nextXrow     = $row;
    	        break;
    	    }
    	}

        $reportInfo['from']         = $this->_from;
    	$reportInfo['haveMoreRows'] = $this->_haveMoreRows;
        $reportInfo['sliceSql']     = $this->_sql;
    }


    public function debugStatusDisplay()
    {
        print "<br />FETCHER: xrowIndex=".$this->_xrowIndex." from=".$this->_from." _haveMoreSlicesToDo=".$this->_haveMoreSlicesToDo;
    }

    /**
     * Strip all x_... columns from a given xrow
     *
     * @param  array $xrow
     * @return array $row
     */
    protected function _removeXColumnsInRow($xrow=array())
    {
        $row = array();
        foreach($xrow as $key=>$val) {
            if (substr($key,0,2) != 'x_') {
                $row[$key] = $val;
            }
        }
        reset($xrow);
        return $row;
    }


}

