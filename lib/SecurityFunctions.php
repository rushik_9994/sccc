<?php

require_once(ROOT_PATH.'/lib/commonBase.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');


/**
 * Check one of the permissions strings in the 'Permissions' table (will ALWAYS return 1 for Admins!)
 *
 * @param string $permissionString
 * @return int $hasPermission
 */
function checkPermissionString($permissionString='') {
    $hasPermission = 0;

    $permissions = new Permissions();
    $permissions->Load_from_action($permissionString);
    $permissionsResult = ((isset($permissions->UserTypeIDs)) && ($permissions->UserTypeIDs)) ? $permissions->UserTypeIDs : '';
    $loggedInUserTypeKeyID = getLoggedInUserTypeKeyId();

    if ($loggedInUserTypeKeyID == 1) {
        //ADMINS always have permission for anything
        $allTabs = ',AddEditTrials,AddEditComponents,AddEditSites,AddEditDoctors,AddEditPatients,';
        if (strpos($allTabs, ",$permissionString,") !== false) {
            $hasPermission = 1;
        }
    } else {
        //NON-ADMINS need specific permissions in the 'Permissions' table
        if ($permissionsResult) {
            $UserTypeIDs = explode(',', $permissionsResult);
            $hasPermission = (in_array($loggedInUserTypeKeyID, $UserTypeIDs)) ? 1 : 0;
        } else {
            //Non-Admins are allowed 'View...' rights on all tabs
            $allTabs = ',ViewTrials,ViewComponents,ViewSites,ViewDoctors,ViewPatients,';
            if (strpos($allTabs, ",$permissionString,") !== false) {
                $hasPermission = 1;
            }
        }
    }

    return $hasPermission;
}

/**
 * Returns the SESSION[UserTypeKeyID] if any. If it doesn't exist, returns 3
 *
 * @return int $loggedInUserTypeKeyID
 */
function getLoggedInUserTypeKeyId() {
    return ((isset($_SESSION['UserTypeKeyID'])) && ($_SESSION['UserTypeKeyID'])) ? $_SESSION['UserTypeKeyID'] : 3;
}

/**
 * Returns 1 if the logged in user has UserTypeKeyID = 1
 * @return int
 */
function isAdminUser() {
    return (getLoggedInUserTypeKeyId() == 1) ? 1 : 0;
}

/**
 * Prints an escaped string
 * @param $string
 * @return mixed
 */
function safePrint($string) {
    return print escapeString($string);
}


/**
 * Escapes a string.
 *
 * @param string  $string     The value to be escaped
 * @return string $string
 */
function escapeString($string) {
    if (!isset($string)) {
        $string = '';
    } else {
        $string = (string) $string;
    }
    if ((is_callable('mb_detect_encoding)')) && (is_callable('mb_detect_order'))) {
        $string = iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8", $string);
        $string = htmlspecialchars($string, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8');
    } else {
        $string = htmlspecialchars($string);
    }

    return $string;
}

