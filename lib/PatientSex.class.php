<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class PatientSex extends BaseClass {

	public $PatientSexKeyID; //int(2)
	public $PatientSexText; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_PatientSex($PatientSexText){
		$this->PatientSexText = $PatientSexText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM PatientSex WHERE PatientSexKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->PatientSexKeyID = $row["PatientSexKeyID"];
			$this->PatientSexText = $row["PatientSexText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'PatientSex', 'PatientSexKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE PatientSex SET\n".
                "PatientSexText = ".        $this->SqlQuote($this->PatientSexText)."\n".
                "WHERE PatientSexKeyID = ". $this->SqlQuote($this->PatientSexKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'PatientSex','PatientSexKeyID', $this->PatientSexKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO PatientSex (PatientSexText) VALUES (\n".
                $this->SqlQuote($this->PatientSexText).')';
            $this->PatientSexKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'PatientSex','PatientSexKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT PatientSexKeyID FROM PatientSex ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["PatientSexKeyID"];
            $i++;
        }
    	return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="patient[PatientSex]" id="PatientSexKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT PatientSexKeyID, PatientSexText FROM PatientSex ORDER BY PatientSexKeyID");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escPatientSexKeyID = htmlentities($row["PatientSexKeyID"]);
            $escPatientSexText  = htmlentities($row["PatientSexText"]);

            if ($row["PatientSexKeyID"] == $selected) {
                 $select .= "<option value=\"$escPatientSexKeyID\" selected>$escPatientSexText</option>\n";
            } else {
                $select .= "<option value=\"$escPatientSexKeyID\">$escPatientSexText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $PatientSexKeyID
	 */
	public function getPatientSexKeyID()
    {
		return $this->PatientSexKeyID;
	}

	/**
	 * @return null|string $PatientSexText
	 */
	public function getPatientSexText()
    {
		return $this->PatientSexText;
	}

	/**
	 * @param int $PatientSexKeyID
	 */
	public function setPatientSexKeyID($PatientSexKeyID)
    {
		$this->PatientSexKeyID = $PatientSexKeyID;
	}

	/**
	 * @param string $PatientSexText
	 */
	public function setPatientSexText($PatientSexText)
    {
		$this->PatientSexText = $PatientSexText;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->PatientSexKeyID)) ? $this->PatientSexKeyID : null);  //vld: PatientSex.PatientSexKeyID
            if ($validationMessage = validateField($val,'PatientSexKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->PatientSexText)) ? $this->PatientSexText : null);  //vld: PatientSex.PatientSexText
            if ($validationMessage = validateField($val,'PatientSexText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

