<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class PatientRace extends BaseClass {

	public $PatientRaceKeyID; //int(2)
	public $PatientRaceText; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_PatientRace($PatientRaceText){
		$this->PatientRaceText = $PatientRaceText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("SELECT * FROM PatientRace WHERE PatientRaceKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->PatientRaceKeyID = $row["PatientRaceKeyID"];
			$this->PatientRaceText = $row["PatientRaceText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'PatientRace', 'PatientRaceKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE PatientRace SET\n".
                "PatientRaceText = ".        $this->SqlQuote($this->PatientRaceText)."\n".
                "WHERE PatientRaceKeyID = ". $this->SqlQuote($this->PatientRaceKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'PatientRace','PatientRaceKeyID', $this->PatientRaceKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO PatientRace (PatientRaceText) VALUES (\n".
                $this->SqlQuote($this->PatientRaceText).')';
            $this->PatientRaceKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'PatientRace','PatientRaceKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT PatientRaceKeyID FROM PatientRace ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["PatientRaceKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="patient[PatientRace]" id="PatientRaceKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT PatientRaceKeyID, PatientRaceText FROM PatientRace ORDER BY PatientRaceKeyID");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escPatientRaceKeyID  = htmlentities($row["PatientRaceKeyID"]);
            $escPatientRaceText   = htmlentities($row["PatientRaceText"]);
            if ($row["PatientRaceKeyID"] == $selected) {
                 $select .= "<option value=\"$escPatientRaceKeyID\" selected>$escPatientRaceText</option>\n";
            } else {
                $select .= "<option value=\"$escPatientRaceKeyID\">$escPatientRaceText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $PatientRaceKeyID
	 */
	public function getPatientRaceKeyID()
    {
		return $this->PatientRaceKeyID;
	}

	/**
	 * @return null|string $PatientRaceText
	 */
	public function getPatientRaceText()
    {
		return $this->PatientRaceText;
	}

	/**
	 * @param int $PatientRaceKeyID
	 */
	public function setPatientRaceKeyID($PatientRaceKeyID)
    {
		$this->PatientRaceKeyID = $PatientRaceKeyID;
	}

	/**
	 * @param string $PatientRaceText
	 */
	public function setPatientRaceText($PatientRaceText)
    {
		$this->PatientRaceText = $PatientRaceText;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->PatientRaceKeyID)) ? $this->PatientRaceKeyID : null);  //vld: PatientRace.PatientRaceKeyID
            if ($validationMessage = validateField($val,'PatientRaceKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->PatientRaceText)) ? $this->PatientRaceText : null);  //vld: PatientRace.PatientRaceText
            if ($validationMessage = validateField($val,'PatientRaceText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

