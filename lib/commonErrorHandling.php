<?php
/**
 * Error handling functions
 */

global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));
require_once(ROOT_PATH . '/cfg/config.php');
require_once(ROOT_PATH . '/lib/DataBaseMysql.class.php');
require_once(ROOT_PATH . '/lib/commonFunctions.php');


/**
 * Decides if we should raise an exception
 *
 * @param string $className
 * @param string $functionName
 * @param array $options
 * @return int
 */
function shouldWeRaiseAnException($className='', $functionName='', $options=array()) {
    global $cfg, $callingEvent, $callingSubEvent, $callingClass, $callingFunction;

    $ignore = 0;

    $callingClass    = $className;
    $callingFunction = $functionName;

    //Calculate our exceptionString
    $exceptionEvent  = 'ignore.';
    $exceptionEvent .= (isset($callingEvent))    ? $callingEvent : '';
    $exceptionEvent .= (isset($callingSubEvent)) ? '.'.$callingSubEvent : '';
    $exceptionEvent .= (isset($callingClass))    ? '.'.$callingClass.'_class' : '';
    $exceptionEvent .= (isset($callingFunction)) ? '.'.$callingFunction.'()' : '';

    //Ignore Load_from_key events that are triggered by empty keys
    if ($functionName == 'Load_from_key') {
        if (array_key_exists('key', $options)) {
            if ((!isset($options['key'])) || (!$options['key'])) {
                $exceptionEvent .= '.haveEmptyKey';
                $ignore = 1;
            }
        }
    }

    //Ignore events from eventStrings listed in config[ignore...]
    if (array_key_exists($exceptionEvent, $cfg)) {
        $ignore = 1;
    }

    //If we're not ignoring this then...
    if (!$ignore) {
        if ((!isset($options)) || (!is_array($options))) {
            $options = array();
        }

        //Get our options:  function + class + function&class + options
        $cfgOptions = array();
        if (($functionName) && (isset($cfg["handleException_{$functionName}"]))) {
            $cfgOptions = $cfg["handleException_$functionName"];
        }
        if (($className) && (isset($cfg["handleException_{$className}"]))) {
            $cfgOptions = $cfg["handleException_$className"];
        }
        if (($className) && ($functionName) && (isset($cfg["handleException_{$className}_{$functionName}"]))) {
            $cfgOptions = $cfg["handleException_{$className}_{$functionName}"];
        }
        $options = array_merge($cfgOptions, $options);

        $ignore = ((isset($options['ignore'])) && ($options['ignore'])) ? 1 : 0;
    }

    return ($ignore) ? 0 : 1;
}

/**
 * Handle an exception
 *
 * @param Exception $e
 * @param string $className
 * @param string $functionName
 * @param array $options
 * @param array $data
 * @throws \Exception
 */
function handleException(\Exception $e, $className='', $functionName='', $options=array(), $data=array())
{
    global $cfg, $callingEvent, $callingSubEvent, $callingClass, $callingFunction;

    if ((!isset($options)) || (!is_array($options))) {
        $options = array();
    }
    $options['callingEvent']    = (isset($callingEvent))    ? $callingEvent : '';
    $options['callingSubEvent'] = (isset($callingSubEvent)) ? $callingSubEvent : '';
    $options['callingClass']    = (isset($callingClass))    ? $callingClass.'_class' : '';
    $options['callingFunction'] = (isset($callingFunction)) ? $callingFunction.'()' : '';
    $options['exceptionEvent']  = 'ignore.'.$options['callingEvent'].'.'.$options['callingSubEvent'];
    if ($options['callingClass']) {
        $options['exceptionEvent'] .= '.'.$options['callingClass'];
    }
    if ($options['callingFunction']) {
        $options['exceptionEvent'] .= '.'.$options['callingFunction'];
    }

    //Get our options:  function + class + function&class + options
    $cfgOptions = array();
    if (($functionName) && (isset($cfg["handleException_{$functionName}"]))) {
        $cfgOptions = $cfg["handleException_$functionName"];
    }
    if (($className) && (isset($cfg["handleException_{$className}"]))) {
        $cfgOptions = $cfg["handleException_$className"];
    }
    if (($className) && ($functionName) && (isset($cfg["handleException_{$className}_{$functionName}"]))) {
        $cfgOptions = $cfg["handleException_{$className}_{$functionName}"];
    }
    $options = array_merge($cfgOptions, $options);

    //Consider any data we might have been given to see if we should downgrade to 'asWarning'
    $ignore           = ((isset($options['ignore']))           && ($options['ignore'])) ? 1 : 0;
    $asWarning        = ((isset($options['asWarning']))        && ($options['asWarning'])) ? 1 : 0;
    $asWarningIfEmpty = ((isset($options['asWarningIfEmpty'])) && ($options['asWarningIfEmpty'])) ? 1 : 0;

    if (!$ignore) {
        $isEmpty = 0;
        if (!isset($data['key'])) {
            $isEmpty = 1;
        } else if ((empty($data['key'])) || (!$data['key'])) {
            $isEmpty = 1;
        }
        if (($asWarningIfEmpty) && ($isEmpty)) {
            $asWarning = $options['asWarning'] = 1;
        }

        logException($e, $options);

        if ((!$asWarning) && (isTransactionActive())) {
            throw $e;
        }
    }
}

/**
 * Log an exception to the SystemStatus table
 *
 * @param  Exception $e
 * @return string $publiclyConsumableString
 */
function logException(\Exception $e, $options=array())
{
    global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent, $callingClass, $callingFunction;

    if ((!isset($options)) || (!is_array($options))) {
        $options = array();
    }
    $asWarning = ((isset($options['asWarning'])) && ($options['asWarning'])) ? 1 : 0;
    $exceptionDisplayDetailedInformation = ((isset($cfg)) && (isset($cfg['error.displayDetailedInformation']))) ? $cfg['error.displayDetailedInformation'] : 0;
    $warningDisplayDetailedInformation   = ((isset($cfg)) && (isset($cfg['warning.displayDetailedInformation'])))   ? $cfg['warning.displayDetailedInformation'] : 0;

    //Suppress display of errors and warnings for non-admin users?
    $suppressMessageDisplay = 0;
    $hideErrorsFromNonAdmins   = ((isset($cfg)) && (isset($cfg['error.hideErrorsFromNonAdmins']))) ? $cfg['error.hideErrorsFromNonAdmins'] : 0;
    $hideWarningsFromNonAdmins = ((isset($cfg)) && (isset($cfg['warning.hideWarningsFromNonAdmins']))) ? $cfg['warning.hideWarningsFromNonAdmins'] : 0;
    $userPrivilegeType = getUserPrivilegeType();
    if ($userPrivilegeType != 1) {
        if (($asWarning) && ($hideWarningsFromNonAdmins)) {
            $suppressMessageDisplay = 1;
        }
        if ((!$asWarning) && ($hideErrorsFromNonAdmins)) {
            $suppressMessageDisplay = 1;
        }
    }

    $message = $e->getMessage();
    $f1 = strpos($message, '|||');
    if ($f1 !== false) {
        $publicMessage = ' (' . trim(substr($message, 0, $f1)) . ') ';
    } else {
        $publicMessage = '';
    }

    $message = 'Exception: ' . $message;
    $file = $e->getFile();
    $line = $e->getLine();
    $location = '';
    if ($file) {
        $location .= $file;
        if ($line) {
            $location .= '[' . $line . ']';
        }
    }

    $details = array();
    $details['exceptionEvent'] = (isset($options['exceptionEvent'])) ? $options['exceptionEvent'] : '';
    if (!$details['exceptionEvent']) {
        if ((isset($callingEvent)) && ($callingEvent) && (isset($callingSubEvent)) && ($callingSubEvent)) {
            $details['exceptionEvent'] = "ignore.$callingEvent.$callingSubEvent";
        }
        if ($options['callingClass']) {
            $details['exceptionEvent'] .= '.'.$options['callingClass'];
        }
        if ($options['callingFunction']) {
            $details['exceptionEvent'] .= '.'.$options['callingFunction'];
        }
    }

    $details['message']  = $e->getMessage();
    $details['location'] = $location;
    $details['sessionUserName'] = ((isset($_SESSION)) && (isset($_SESSION['UserName']))) ? $_SESSION['UserName'] : '';
    $server = (isset($_SERVER)) ? $_SERVER : array();
    $details['requestRemoteAddr'] = (isset($server['REMOTE_ADDR'])) ? $server['REMOTE_ADDR'] : '';
    $details['requestRequestMethod'] = (isset($server['REQUEST_METHOD'])) ? $server['REQUEST_METHOD'] : '';
    $details['requestRequestUri'] = (isset($server['REQUEST_URI'])) ? $server['REQUEST_URI'] : '';
    $details['requestQueryString'] = (isset($server['QUERY_STRING'])) ? $server['QUERY_STRING'] : '';
    $details['requestGet'] = (isset($_GET)) ? $_GET : array();
    $details['requestPost'] = (isset($_POST)) ? $_POST : array();
    $details['exception'] = $e->__toString();

    $severity = ($asWarning) ? 3 : 4;
    $rowType = ($asWarning) ? 'warning' : 'exception';
    $id = logSystemStatus($message, $severity, $rowType, htmlentities(print_r($details, true)));

    $textPrefix = ($asWarning) ? 'A warning ' : 'An exception ';
    $retVal = $textPrefix . $publicMessage;

    $retVal .= ($id) ? "was logged under SystemStatusId[$id]" : 'occurred but failed to be logged';

    if (!$suppressMessageDisplay) {
        if (!$asWarning) {
            $errors[] = $retVal;
            $success = '';
            if ($exceptionDisplayDetailedInformation) {
                $errors[] = 'HTML:<pre style="font-size:8pt;font-weight:normal;">' . htmlentities(print_r($details, true)) . '</pre>';
            }
        } else {
            $warnings[] = $retVal;
            if ($warningDisplayDetailedInformation) {
                $warnings[] = 'HTML:<pre style="font-size:7pt;font-weight:normal;">' . htmlentities(print_r($details, true)) . '</pre>';
            }
        }
    }

    return $retVal;
}


/**
 * Log a message to the SystemStatus table
 *
 * @param $message
 * @param int $severity
 * @param string $rowType
 * @param string $details
 * @return int $insertId
 */
function logSystemStatus($message, $severity=0, $rowType='', $details='') {
    $insertId = 0;
    try {
        $connection = new DataBaseMysql();
        if (!$rowType) {
            if ($severity == 0) {
                $rowType = 'info';
            } else if ($severity == 1) {
                $rowType = 'warning';
            } else if ($severity == 2) {
                $rowType = 'error';
            }
        }
        $quMessage  = $connection->SqlQuote($message);
        $quDetails  = $connection->SqlQuote($details);
        $quSeverity = $connection->SqlQuote($severity);
        $quRowType  = $connection->SqlQuote($rowType);

        $sql =
            "INSERT INTO `SystemStatus`(severity, rowtype, message, details) VALUES (\n" .
            "$quSeverity, $quRowType, $quMessage, $quDetails)";
        $connection->RunQuery($sql);

        $insertId = $connection->LastID();
    } catch (\Exception $e) {
        //ignore all exceptions here
    }

    return $insertId;
}



/**
 * Initialize success and errors
 *
 * Uses global $errors and $success
 * @return array array($errors, $warnings, $success)
 */
function initSuccessAndErrors() {
    global $errors, $warnings, $success;
    $errors   = array();
    $warnings = array();
    $success  = '';

    return array($errors, $warnings, $success);
}



/**
 * Add an error to our error list
 *
 * Uses global $errors
 */
function addError($error) {
    global $errors, $warnings, $success;

    if (!isset($errors)) {
        $errors = array();
    }
    $errors[] = $error;
    $success  = '';
}


/**
 * Set $success (but only if we don't have any errors)
 *
 * Uses global $errors and $success
 */
function setSuccess($message) {
    global $errors, $warnings, $success;

    if (!isset($errors)) {
        $errors = array();
    }
    if (!isset($warnings)) {
        $warnings = array();
    }
    if (!haveAnyErrors()) {
        $success = $message;
    }
}


/**
 * Do we have any errors?
 *
 * Uses global $errors
 * @return int $haveErrors
 */
function haveAnyErrors() {
    global $errors;
    return (((isset($errors)) && (count($errors) > 0)) ? 1 : 0);
}

/**
 * Do we have any errors?
 *
 * Uses global $errors
 * @return int $numErrors
 */
function errorCount() {
    global $errors;
    return (((isset($errors)) && (count($errors))) ? count($errors) : 0);
}

