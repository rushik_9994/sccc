$(document).ready(function(){
    $(".sort").tablesorter({dateFormat : "yyyymmdd"});
    $(".phone").mask("(999) 999-9999");
    $(".zip").mask("99999?-9999");
    $(".dpick").mask("9999-99-99");
    $('span.phone').each(function(){
        var phonenumber = $(this).text();
        if(phonenumber.length == 10){
            var phone = '('+phonenumber.substring(0,3)+') '+phonenumber.substring(3,6)+'-'+phonenumber.substring(6,10);
        }
        $(this).text(phone);
    });
    $('span.zip').each(function(){
        var zipcode = $(this).text();
        if(zipcode.length > 5){
            var zip = zipcode.substring(0,5)+'-'+zipcode.substring(5,9);
        }
        $(this).text(zip);
    });
    $('input.zip').live('blur', function(){
        var zip = $(this).val().replace(/\D/g,'');
        if((zip.length > 5)&&(zip.length < 9)){
            alert('Zip codes must be either 5 or 9 digits.');
            $(this).val('');
        }
    });
});

function trashMe(){
    $('#action').val('delete');
    if($('#mainform')){
        $('#mainform').submit();
    }
    if($('#trialform')){
        $('#trialform').submit();
    }
}

function validate(form){
    if($('#action').val()!='delete'){
        var pass = true;
        var msg = '';
        $('#'+form+' .req').each(function(){
            if($(this).val() == ''){
                $(this).css('background','yellow');
                pass = false;
            }else{
                $(this).css('background','#E9D9DF');           
            }        
        })
        
        if(form == 'trialform'){
            if($('input.TrialDiseaseType:checked').length < 1){
                msg = 'At least one Disease Type must be checked.'
                $('div.checkboxes').css('background', 'yellow')
                pass = false;
            }else{
                $('div.checkboxes').css('background', 'none')
            }
        }
        
        if(pass == false){
            alert('Please complete all required fields as indicated. '+msg);
            return false;
        }else{
            if($('#PatientCreditor')){
                $('#PatientCreditor').attr('disabled', false);
            }
            $('#'+form).submit();
        }
    }
}
