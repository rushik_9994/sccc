<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialCompJoin extends BaseClass {

    public $TrialCompKeyID; //int(10)
    public $TrialKeyID; //int(10)
    public $CompKeyID; //int(10)
    public $TimestampCreate; //datetime
    public $TimestampUpdate; //timestamp
    public $TrialCompRecentRenewalDate; //varchar(250)
    public $TrialCompNextRenewalDate; //varchar(250)
    public $TrialCompApprovalDate; //varchar(250)
    public $TrialCompCloseDate; //varchar(250)
    public $TrialCompClose; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialCompJoin($TrialKeyID,$CompKeyID,$TimestampCreate,$TimestampUpdate,$TrialCompRecentRenewalDate,$TrialCompNextRenewalDate,$TrialCompApprovalDate,$TrialCompCloseDate,$TrialCompClose){
        $this->TrialKeyID                 = $TrialKeyID;
        $this->CompKeyID                  = $CompKeyID;
        $this->TimestampCreate            = $TimestampCreate;
        $this->TimestampUpdate            = $TimestampUpdate;
        $this->TrialCompRecentRenewalDate = $TrialCompRecentRenewalDate;
        $this->TrialCompNextRenewalDate   = $TrialCompNextRenewalDate;
        $this->TrialCompApprovalDate      = $TrialCompApprovalDate;
        $this->TrialCompCloseDate         = $TrialCompCloseDate;
        $this->TrialCompClose             = $TrialCompClose;
    }*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $result = $this->connection->RunQuery("SELECT *\n".
                                              "FROM   `TrialCompJoin`\n".
                                              "WHERE  `TrialCompKeyID`=".$this->SqlQuote($key_row)
                                             );

        $found = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->TrialCompKeyID             = $row["TrialCompKeyID"];
            $this->TrialKeyID                 = $row["TrialKeyID"];
            $this->CompKeyID                  = $row["CompKeyID"];
            $this->TimestampCreate            = $row["TimestampCreate"];
            $this->TimestampUpdate            = $row["TimestampUpdate"];
            $this->TrialCompRecentRenewalDate = $row["TrialCompRecentRenewalDate"];
            $this->TrialCompNextRenewalDate   = $row["TrialCompNextRenewalDate"];
            $this->TrialCompApprovalDate      = $row["TrialCompApprovalDate"];
            $this->TrialCompCloseDate         = $row["TrialCompCloseDate"];
            $this->TrialCompClose             = $row["TrialCompClose"];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param string $key
     * @param int$key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key, $key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialCompJoin', $key, $key_row);
    }

    /**
     * Update the active row table on table
     * @throws \Exception
     */
    public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE `TrialCompJoin` SET\n".
                "      `TrialKeyID`=".                $this->SqlQuote($this->TrialKeyID).",\n".
                "      `CompKeyID`=".                 $this->SqlQuote($this->CompKeyID).",\n".
                "      `TimestampCreate`=".           $this->SqlQuote($this->TimestampCreate).",\n".
                "      `TimestampUpdate`=".           $this->SqlQuote($this->TimestampUpdate).",\n".
                "      `TrialCompRecentRenewalDate`=".$this->SqlQuote($this->TrialCompRecentRenewalDate).",\n".
                "      `TrialCompNextRenewalDate`=".  $this->SqlQuote($this->TrialCompNextRenewalDate).",\n".
                "      `TrialCompApprovalDate`=".     $this->SqlQuote($this->TrialCompApprovalDate).",\n".
                "      `TrialCompCloseDate`=".        $this->SqlQuote($this->TrialCompCloseDate).",\n".
                "      `TrialCompClose`=".            $this->SqlQuote($this->TrialCompClose)."\n".
                "WHERE `TrialCompKeyID`=". $this->SqlQuote($this->TrialCompKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialCompJoin','TrialCompKeyID', $this->TrialCompKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }


    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO `TrialCompJoin` (`TrialKeyID`, `CompKeyID`, `TimestampCreate`,\n".
                "                             `TimestampUpdate`, `TrialCompRecentRenewalDate`,\n".
                "                             `TrialCompNextRenewalDate`,\n".
                "                             `TrialCompApprovalDate`, `TrialCompCloseDate`,\n".
                "                             `TrialCompClose`) VALUES (\n".
                $this->SqlQuote($this->TrialKeyID).",\n".
                $this->SqlQuote($this->CompKeyID).",\n".
                $this->SqlQuote($this->TimestampCreate).",\n".
                $this->SqlQuote($this->TimestampUpdate).",\n".
                $this->SqlQuote($this->TrialCompRecentRenewalDate).",\n".
                $this->SqlQuote($this->TrialCompNextRenewalDate).",\n".
                $this->SqlQuote($this->TrialCompApprovalDate).",\n".
                $this->SqlQuote($this->TrialCompCloseDate).",\n".
                $this->SqlQuote($this->TrialCompClose).')';
            $this->TrialCompKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialCompJoin','TrialCompKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($column, $order)
    {
        $keys = array();

        $quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

        $i = 0;
        $result = $this->connection->RunQuery("SELECT `TrialCompKeyID`\n".
                                              "FROM   `TrialCompJoin`\n".
                                              "ORDER BY `TrialCompJoin`.`$quColumn` $quOrder"
                                             );
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialCompKeyID"];
            $i++;
        }
        return $keys;
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $key
     * @param string $where
     * @param string $column
     * @param string $order
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysWhereOrderBy($key, $where, $column, $order, $sClauses=array())
    {
        $keys = array();

        $i = 0;
        $x = strtoupper(trim($where));
        if (substr($x,0,6) == 'WHERE ') { $where = substr($where,6); }

        if (count($sClauses) > 0) {
            $sql = "SELECT TrialCompJoin.`".$this->connection->Quote($key)."`\n".
                   "FROM   `TrialCompJoin`\n";
        } else {
            $sql = "SELECT `".$this->connection->Quote($key)."`\n".
                   "FROM   `TrialCompJoin`\n";
        }
               

        //Support table column sorting 'order_field_0' (thead)...
        $joins          = '';
        $orderBy        = "ORDER BY `TrialCompJoin`.`".$this->connection->Quote($column)."` $order";
        $orderIndex     = (isset($sClauses['order_index']))                 ? $sClauses['order_index'] : '_0';
        if ((isset($sClauses['order_last_clicked'])) && ($sClauses['order_last_clicked'])) {
            $orderIndex = $sClauses['order_last_clicked'];
        }
        $orderField     = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }

        if ($orderField) {
            if ($orderField == 'order_trial_protocol'.$orderIndex) {
                $joins   = "LEFT JOIN Trial ON TrialCompJoin.TrialKeyID=Trial.TrialKeyID\n";
                $orderBy = "ORDER BY Trial.TrialProtocolNumber $orderDirection";

            } else if ($orderField == 'order_approval_date'.$orderIndex) {
                $orderBy = "ORDER BY TrialCompJoin.TrialCompApprovalDate $orderDirection";

            } else if ($orderField == 'order_recent_renewal_date'.$orderIndex) {
                $orderBy = "ORDER BY TrialCompJoin.TrialCompNextRenewalDate $orderDirection";

            } else if ($orderField == 'order_next_renewal_date'.$orderIndex) {
                $orderBy = "ORDER BY TrialCompJoin.TrialCompNextRenewalDate $orderDirection";

            } else if ($orderField == 'order_close_date'.$orderIndex) {
                $orderBy = "ORDER BY TrialCompJoin.TrialCompCloseDate $orderDirection";
            }
        }
        $sql .= $joins.
                "WHERE  $where\n".
                $orderBy;

        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row[$key];
            $i++;
        }
        return $keys;
    }

    /**
     * Returns a select field and assigns selected
     *
     * @param  string $where
     * @param  int $selected
     * @return string $select
     * @throws \Exception
     */
    public function CreateSelectWhere($where, $selected){
        $select = '<select name="TrialKeyIDOUT" id="TrialKeyIDOUT" class="button" >'."\n";
        $select .= '<option value="">Select...</option>'."\n";

        $x = strtoupper(trim($where));
        if (substr($x,0,6) != 'WHERE ') { $where = "WHERE   $where"; }

        $sql = "SELECT `Trial`.`TrialKeyID`, `Trial`.`TrialProtocolNumber`\n".
               "FROM   `TrialCompJoin`\n".
               "INNER JOIN `Trial` ON `Trial`.`TrialKeyID`=`TrialCompJoin`.`TrialKeyID`\n".
               "$where\n".
               "ORDER BY `Trial`.`TrialProtocolNumber`";
        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialKeyID          = htmlentities($row["TrialKeyID"]);
            $escTrialProtocolNumber = htmlentities($row["TrialProtocolNumber"]);
            if ($row["TrialKeyID"] == $selected) {
                 $select .= "<option value=\"$escTrialKeyID\" selected>$escTrialProtocolNumber</option>\n";
            } else {
                $select .= "<option value=\"$escTrialKeyID\">$escTrialProtocolNumber</option>\n";
            }
        }
        $select .= '</select>'."\n";
        return $select;
    }

    /**
     * @return null|int $TrialCompKeyID
     */
    public function getTrialCompKeyID()
    {
        return $this->TrialCompKeyID;
    }

    /**
     * @return null|int $TrialKeyID
     */
    public function getTrialKeyID()
    {
        return $this->TrialKeyID;
    }

    /**
     * @return null|int $CompKeyID
     */
    public function getCompKeyID()
    {
        return $this->CompKeyID;
    }

    /**
     * @return null|string $TimestampCreate
     */
    public function getTimestampCreate()
    {
        return $this->TimestampCreate;
    }

    /**
     * @return null|string $TimestampUpdate
     */
    public function getTimestampUpdate()
    {
        return $this->TimestampUpdate;
    }

    /**
     * @return null|string $TrialCompRecentRenewalDate
     */
    public function getTrialCompRecentRenewalDate()
    {
        return $this->TrialCompRecentRenewalDate;
    }

    /**
     * @return null|string $TrialCompNextRenewalDate
     */
    public function getTrialCompNextRenewalDate()
    {
        return $this->TrialCompNextRenewalDate;
    }

    /**
     * @return null|string $TrialCompApprovalDate
     */
    public function getTrialCompApprovalDate()
    {
        return $this->TrialCompApprovalDate;
    }

    /**
     * @return null|string $TrialCompCloseDate
     */
    public function getTrialCompCloseDate()
    {
        return $this->TrialCompCloseDate;
    }

    /**
     * @return null|string $TrialCompClose
     */
    public function getTrialCompClose()
    {
        return $this->TrialCompClose;
    }

    /**
     * @param int $TrialCompKeyID
     */
    public function setTrialCompKeyID($TrialCompKeyID)
    {
        $this->TrialCompKeyID = $TrialCompKeyID;
    }

    /**
     * @param int $TrialKeyID
     */
    public function setTrialKeyID($TrialKeyID)
    {
        $this->TrialKeyID = $TrialKeyID;
    }

    /**
     * @param int $CompKeyID
     */
    public function setCompKeyID($CompKeyID)
    {
        $this->CompKeyID = $CompKeyID;
    }

    /**
     * @param string $TimestampCreate
     */
    public function setTimestampCreate($TimestampCreate)
    {
        $this->TimestampCreate = $TimestampCreate;
    }

    /**
     * @param string $TimestampUpdate
     */
    public function setTimestampUpdate($TimestampUpdate)
    {
        $this->TimestampUpdate = $TimestampUpdate;
    }

    /**
     * @param string $TrialCompRecentRenewalDate
     */
    public function setTrialCompRecentRenewalDate($TrialCompRecentRenewalDate)
    {
        $this->TrialCompRecentRenewalDate = $TrialCompRecentRenewalDate;
    }

    /**
     * @param string $TrialCompNextRenewalDate
     */
    public function setTrialCompNextRenewalDate($TrialCompNextRenewalDate)
    {
        $this->TrialCompNextRenewalDate = $TrialCompNextRenewalDate;
    }

    /**
     * @param string $TrialCompApprovalDate
     */
    public function setTrialCompApprovalDate($TrialCompApprovalDate)
    {
        $this->TrialCompApprovalDate = $TrialCompApprovalDate;
    }

    /**
     * @param string $TrialCompCloseDate
     */
    public function setTrialCompCloseDate($TrialCompCloseDate)
    {
        $this->TrialCompCloseDate = $TrialCompCloseDate;
    }

    /**
     * @param string $TrialCompClose
     */
    public function setTrialCompClose($TrialCompClose)
    {
        $this->TrialCompClose = $TrialCompClose;
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->TrialCompKeyID)) ? $this->TrialCompKeyID : null);  //vld TrialCompJoin.TrialCompKeyID
            if ($validationMessage = validateField($val,'TrialCompKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyID)) ? $this->TrialKeyID : null);  //vld: TrialCompJoin.TrialKeyID
            if ($validationMessage = validateField($val,'TrialKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->CompKeyID)) ? $this->CompKeyID : null);  //vld: TrialCompJoin.CompKeyID
            if ($validationMessage = validateField($val,'CompKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TimestampCreate)) ? $this->TimestampCreate : null);  //vld: TrialCompJoin.TimestampCreate
            if ($validationMessage = validateField($val,'TimestampCreate','nullok;emptyok;zerodatetimeeok;datetime',__CLASS__)) { break; }

            $val = ((isset($this->TimestampUpdate)) ? $this->TimestampUpdate : null);  //vld: TrialCompJoin.TimestampUpdate
            if ($validationMessage = validateField($val,'TimestampUpdate','nullok;emptyok;zerodatetimeeok;datetime',__CLASS__)) { break; }

            $val = ((isset($this->TrialCompRecentRenewalDate)) ? $this->TrialCompRecentRenewalDate : null);  //vld: TrialCompJoin.TrialCompRecentRenewalDate
            if ($validationMessage = validateField($val,'TrialCompRecentRenewalDate','nullok;emptyok;zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->TrialCompNextRenewalDate)) ? $this->TrialCompNextRenewalDate : null);  //vld: TrialCompJoin.TrialCompNextRenewalDate
            if ($validationMessage = validateField($val,'TrialCompNextRenewalDate','nullok;emptyok;zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->TrialCompApprovalDate)) ? $this->TrialCompApprovalDate : null);  //vld: TrialCompJoin.TrialCompApprovalDate
            if ($validationMessage = validateField($val,'TrialCompApprovalDate','nullok;zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->TrialCompCloseDate)) ? $this->TrialCompCloseDate : null);  //vld: TrialCompJoin.TrialCompCloseDate
            if ($validationMessage = validateField($val,'TrialCompCloseDate','nullok;emptyok;zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->TrialCompClose)) ? $this->TrialCompClose : null);  //vld: TrialCompJoin.TrialCompClose
            if ($validationMessage = validateField($val,'TrialCompClose','nullok;0or1',__CLASS__)) { break; }

        } while(false);

        return $validationMessage;
    }

}