<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');

class States extends BaseClass {

	public $StatesKeyID; //int(2)
	public $StateAbbreviation; //varchar(4)
	public $StateName; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_States($StateAbbreviation,$StateName){
		$this->StateAbbreviation = $StateAbbreviation;
		$this->StateName = $StateName;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM States WHERE StatesKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->StatesKeyID = $row["StatesKeyID"];
			$this->StateAbbreviation = $row["StateAbbreviation"];
			$this->StateName = $row["StateName"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int$key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'States', 'StatesKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE States SET\n".
                "StateAbbreviation = ".     $this->SqlQuote($this->StateAbbreviation).",\n".
                "StateName = ".             $this->SqlQuote($this->StateName)."\n".
                "WHERE StatesKeyID = ".     $this->SqlQuote($this->StatesKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'States','StatesKeyID', $this->StatesKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO States (StateAbbreviation, StateName) VALUES (\n".
                $this->SqlQuote($this->StateAbbreviation).",\n".
                $this->SqlQuote($this->StateName).')';
            $this->StatesKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'States','StatesKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT StatesKeyID FROM States ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["StatesKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="StatesKeyID" id="StatesKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT StatesKeyID, StateName FROM States ORDER BY StatesKeyID");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escStatesKeyID = htmlentities($row["StatesKeyID"]);
            $escStateName   = htmlentities($row["StateName"]);
            if ($row["StatesKeyID"] == $selected) {
                 $select .= "<option value=\"$escStatesKeyID\" selected>$escStateName</option>\n";
            } else {
                $select .= "<option value=\"$escStatesKeyID\">$escStateName</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $StatesKeyID
	 */
	public function getStatesKeyID()
    {
		return $this->StatesKeyID;
	}

	/**
	 * @return null|string $StateAbbreviation
	 */
	public function getStateAbbreviation()
    {
		return $this->StateAbbreviation;
	}

	/**
	 * @return null|string $StateName
	 */
	public function getStateName()
    {
		return $this->StateName;
	}

	/**
	 * @param int $StatesKeyID
	 */
	public function setStatesKeyID($StatesKeyID)
    {
		$this->StatesKeyID = $StatesKeyID;
	}

	/**
	 * @param string $StateAbbreviation
	 */
	public function setStateAbbreviation($StateAbbreviation)
    {
		$this->StateAbbreviation = $StateAbbreviation;
	}

	/**
	 * @param string $StateName
	 */
	public function setStateName($StateName)
    {
		$this->StateName = $StateName;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->StatesKeyID)) ? $this->StatesKeyID : null);  //vld: States.StatesKeyID
            if ($validationMessage = validateField($val,'StatesKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->StateAbbreviation)) ? $this->StateAbbreviation : null);  //vld: States.StateAbbreviation
            if ($validationMessage = validateField($val,'StateAbbreviation','nestring',__CLASS__)) { break; }

            $val = ((isset($this->StateName)) ? $this->StateName : null);  //vld: States.StateName
            if ($validationMessage = validateField($val,'StateName','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

