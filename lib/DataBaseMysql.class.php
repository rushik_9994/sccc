<?php

/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

global $transactionActive;

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));
require_once(ROOT_PATH . '/lib/commonErrorHandling.php');


class DataBaseMysql {

    public $conn;

    public function __construct() {
        global $cfg;
        if ((isset($cfg)) &&
            (is_array($cfg)) &&
            (isset($cfg['appEnv'])) &&
            ($cfg['appEnv'] == 'development')) {
            //DEVELOPMENT
            $this->conn = new mysqli("localhost", "root", "sccc", "root");
        } else {
            //PRODUCTION
            // $this->conn = new mysqli("localhost", "oris", "aqua163bask", "oris");
            $this->conn = new mysqli("localhost", "root", "1256", "sccc");
        }
        if($this->conn->connect_error){
            echo "Error connect to mysql";die;
        }
    }

    /**
     * Start a transaction
     *
     * @return int
     */
    public function BeginTransaction()
    {
        global $transactionActive;
        try {
            if ((!isset($transactionActive)) || (!$transactionActive)) {
                $ok = mysqli_begin_transaction($this->conn);
                if ($ok) {
                    $transactionActive = 1;
                } else {
                    $transactionActive = 0;
                }
            }
        } catch (\Exception $e) {
            logException($e);
            $ok = 0;
            $transactionActive = 0;
        }

        return ($ok) ? 1 : 0;
    }

    /**
     * Commit a transaction
     *
     * @return int
     */
    public function CommitTransaction()
    {
        global $transactionActive;
        try {
            if ((isset($transactionActive)) && ($transactionActive)) {
                $ok = mysqli_commit($this->conn);
            } else {
                $ok = 0;
            }
        } catch (\Exception $e) {
            logException($e);
            $ok = 0;
        }
        $transactionActive = 0;
        return ($ok) ? 1 : 0;
    }

    /**
     * Roll a transaction back
     *
     * @return int
     */
    public function RollbackTransaction()
    {
        global $transactionActive;
        try {
            if ((isset($transactionActive)) && ($transactionActive)) {
                $ok = mysqli_rollback($this->conn);
            } else {
                $ok = 0;
            }
        } catch (\Exception $e) {
            logException($e);
            $ok = 0;
        }

        $transactionActive = 0;
        return ($ok) ? 1 : 0;
    }


    /**
     * Check if a transaction is active (mostly to know if we can throw an Exception)
     * @return int $inTransaction
     */
    public function inTransaction()
    {
        global $transactionActive;
        if ((isset($transactionActive)) && ($transactionActive)) {
            return $transactionActive;
        } else {
            return 0;
        }
    }

    
    public function RunQuery($query_tag, $options=array())
    {
        $errorFlag = 0;
        $result = $this->conn->query($query_tag) or $errorFlag = 1;
        $wantException = ((isset($options['wantException'])) && $options['wantException']) ? 1 : 0;
        
        if ($errorFlag) {
            $error = mysqli_error($this->conn);
            if (($wantException) && (isTransactionActive())) {
                throw new \Exception("SQL-Error: $error");
            } else {
                addError("SQL-Error: $error");
            }

            if (is_callable('debug_backtrace')) {
                $arr   = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            } else {
                $arr   = array();
            }
            $file  = '';
            $line  = 0;
            if ((isset($arr[0])) && (isset($arr[0]['file']))) {
                $file = str_replace('.php', '', basename($arr[0]['file']));
            }
            if ((isset($arr[0])) && (isset($arr[0]['line']))) {
                $line = $arr[0]['line'];
            }
            $location = (($file) && ($line)) ?  "$file($line)" : '';

            $message =      "<table style=\"background-color:#EEEEEE;color:red;\" cellspacing=\"0\">\n".
                            "    <tr style=\"font-size:9pt;font-weight:bold;background-color:#DDDDDD;\">\n".
                            "        <td width=\"10%\">SQL Error:&nbsp;</td><td>$error</td>\n".
                            "    </tr>\n";
            if ($location) {
                $message .= "    <tr style=\"font-size:9pt;font-weight:bold;background-color:#DDDDDD;\">\n".
                            "        <td>Location:&nbsp;</td><td>$location</td>\n".
                            "    </tr>\n";
            }
            $message .=     "    <tr>\n".
                            "        <td colspan=\"2\">\n".
                            "            <pre style=\"font-size:8pt;font-weight:normal;\">$query_tag</pre>\n".
                            "         </td>\n".
                            "    </tr>\n".
                            "</table>\n";
            die($message);
        }
        return $result;
    }

    public function LastID()
    {
        $result = $this->conn->insert_id;
        return $result;
    }

    public function TotalOfRows($table_name, $options=array())
    {
        $result = $this->RunQuery("Select * from $table_name", $options);
        return $result->num_rows;
    }

    public function CloseMysql(){
        $this->conn->close();
    }


    /**
     * A smart quote function. If string then 'quotedstring'. If null then NULL.
     *
     * @param  string $string
     * @return string $sqlUsableString        Either 'quotedstring' or NULL
     */
    public function SqlQuote($string='') {
        if (!isset($string)) {
            return "''"; //----!!! This should be NULL not empty string
        } else {
            return "'".$this->Quote($string)."'";
        }
    }


    /**
     * A simple quote function. Does NOT surround the string with singlequotes!
     */
    public function Quote($string='') {
        return $this->conn->real_escape_string($string);
    }

}

