<?php
/**
 * Function to do a test import from ClinicalTrials.gov
 */

/**
 * Process all patients
 * @param int $ignoreAdminRights optional       defaults to 0
 * @param int $fromId optional                  defaults to 0
 * @param int $slice optional                   defaults to 200
 * @param int $oneLoopOnly optional             defaults to 0
 * @return array $retVal                        assoc array
 */
function adminProcessClinicalTrialsGovImport($ignoreAdminRights=0, $fromId=0, $slice=200, $oneLoopOnly=0) {
    global $cfg, $errors, $warnings, $success;

    exit; //readClinicalTrialsGovData('NCT02465060'); exit;

    $dryRunMode = ((isset($_POST['dryRunMode'])) && ($_POST['dryRunMode'])) ? 1 : 0;

    $weFailed = 0;
    $retVal = array('done' => 0, 'fromId' => 0, 'total' => 0, 'changed'=>0, 'failures'=>0);
    set_time_limit(0);
    try {
        $connection = startAdminFunction($ignoreAdminRights);

        $publicPdo = getOldPublicPdo($error);
        if ($error) {
            throw new \Exception($error);
        }

        $scorPdo = scorGetPdo($error);
        if ($error) {
            throw new \Exception($error);
        }

        if (($dryRunMode) || ($errCount)) {
            exit;
        }

    } catch (\Exception $e) {
        $errors[] = __FUNCTION__.': '. $e->getMessage();
        $success  = '';
        $weFailed = 1;
        $retVal['failures']++;
    }

    if (!$weFailed) {
        $success = __FUNCTION__.": Function succeeded and imported data entries from WPpublic";
    }

    return $retVal;
}

/**
 * https://clinicaltrials.gov/ct2/about-site/crawling
 * @param $id
 */
function readClinicalTrialsGovData($id) {
    $cont = file_get_contents('https://clinicaltrials.gov/ct2/show/'.urlencode($id).'?displayxml=true');
    if ($cont) {
        /*-----------
        $p = xml_parser_create();
        xml_parse_into_struct($p, $cont, $xmlData, $index);
        xml_parser_free($p);
        print "<pre>"; print_r($xmlData); exit;
        --------------*/

        /*------------
        $movies = new SimpleXMLElement($cont);
        print "<pre>"; print_r($movies); exit;
        --------------*/

        //------------
        $xml = simplexml_load_string($cont, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json,true);

        if (isset($_POST['flatArrayMode'])) {
            $array = flattenAssoc($array);
            print "<table width='100%' border='1'>";
            foreach($array as $key => $val) {
                print
                    "<tr>\n".
                    "<td style='background-color:#DDDDDD;' width='25%' valign='top'>".htmlentities($key)."</td>\n".
                    "<td style='background-color:#EEEEEE;' >".htmlentities($val)."</td></tr>";
            }
            print "</table>";
            exit;
        } else {
            print "<pre>";
            print_r($array);
            exit;
        }
        //------------
    }
    print $cont; exit; //----!!!
}

