<?php
/**
 * A program that can call itself repeatedly until done
 */

global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../../'));
require_once(ROOT_PATH . '/cfg/config.php');
require_once(ROOT_PATH . '/lib/commonBase.php');
require_once(ROOT_PATH . '/lib/DataBaseMysql.class.php');
require_once(ROOT_PATH . '/lib/admin/adminBase.php');
//require_once(ROOT_PATH . '/lib/admin/adminProcessPatientsUrbanRural.php');
list($cfg, $errors, $warnings, $success) = startRequest();
if (!isSysAdmin()) {
    exit;
}

if (!isset($_GET['data'])) {
    $method = 'adminProcessPatientsUrbanRural';
    $id     = 0;
} else {
    $data = json_decode($_GET['data'], true);
    $method = (isset($data['m'])) ? $data['m'] : '';
    $id = (isset($data['id'])) ? $data['id'] : 0;
}

//www.morpheusrising.com/sccc/lib/admin/updateUrbanRural.php?data=%7B%22m%22%3A%22adminProcessPatientsUrbanRural%22%2C%22id%22%3A0%7D
//$data   = array('m'=>'adminProcessPatientsUrbanRural', 'id'=>0); print "admin/updateUrbanRural.php?data=".urlencode(json_encode($data)); exit;

if ($method == 'adminProcessPatientsUrbanRural') {
    $ignoreAdminRights = 1;
    $fromId            = $id;
    $slice             = 500;
    $oneLoopOnly       = 1;
    $result = adminProcessPatientsUrbanRural($ignoreAdminRights, $fromId, $slice, $oneLoopOnly);
    print "<pre>"; print_r($result);
    if ($result['done']) {
        print "\nDone!";
        $protocoll = ((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == 'ok')) ? 'https://' : 'http://';
        $url = '../../trials.php';
        print "<br /><br /><a href='$url'>Patient update is done! Please CLICK HERE to go back to trials.php...</a>'";
        exit;
    } else {
        print "\nID = ".$result['fromId']; flush(); sleep(2);
        $data = array('id' => $result['fromId'], 'm' => $method);
        $protocoll = ((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == 'ok')) ? 'https://' : 'http://';
        $url = $protocoll . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?data=' . urlencode(json_encode($data));
        print "<br /><br /><a href='$url'>Click here for next 500 patients...</a>'";
        //header("Location: $url");
    }
}


