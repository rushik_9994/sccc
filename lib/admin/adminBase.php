<?php
/**
 * Centralized authentication checking
 */

global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../../'));
require_once(ROOT_PATH . '/cfg/config.php');
require_once(ROOT_PATH . '/lib/commonBase.php');
require_once(ROOT_PATH . '/lib/DataBaseMysql.class.php');

require_once(ROOT_PATH . '/lib/admin/adminProcessPatientsUrbanRural.php');
require_once(ROOT_PATH . '/lib/admin/adminProcessWpOneTimeDataImport.php');
require_once(ROOT_PATH . '/lib/admin/adminProcessClinicalTrialsGovImport.php');


/**
 * Starts an admin function (checks for sys admin access)
 *
 * @param int $ignoreAdminRights optional       defaults to 0
 * @return DataBaseMysql $connection
 * @throws Exception
 */
function startAdminFunction($ignoreAdminRights=0) {
    global $cfg, $errors, $warnings, $success;
    if (!isset($errors)) {
        $errors = array();
    }
    if (!isset($warnings)) {
        $warnings = array();
    }
    if (!isset($success)) {
        $success = '';
    }

    if ((!$ignoreAdminRights) && (!isSysAdmin())) {
        throw new \Exception('Have no SysAdmin rights!');
    }
    $connection = new DataBaseMysql();
    return $connection;
}

