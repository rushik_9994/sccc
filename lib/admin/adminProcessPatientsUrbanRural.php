<?php


/**
 * Process all patients
 * @param int $ignoreAdminRights optional       defaults to 0
 * @param int $fromId optional                  defaults to 0
 * @param int $slice optional                   defaults to 200
 * @param int $oneLoopOnly optional             defaults to 0
 * @return array $retVal                        assoc array
 */
function adminProcessPatientsUrbanRural($ignoreAdminRights=0, $fromId=0, $slice=200, $oneLoopOnly=0) {
    global $cfg, $errors, $warnings, $success;

    $retVal = array('done' => 0, 'fromId' => 0, 'total' => 0, 'changed'=>0, 'failures'=>0);
    set_time_limit(0);
    try {
        $connection = startAdminFunction($ignoreAdminRights);

        $oneLoopOnly = ($slice == 1) ? 1 : $oneLoopOnly;
        $quFromId = $connection->Quote($fromId);
        $quSlice  = $connection->Quote($slice);
        $count    = 0;
        $weFailed = 0;
        while (1) {
            //Fetch 200 rows (or maybe only 1 if we got a slice of 1)
            $sql =
                "SELECT Patient.PatientKeyID, Patient.PatientZipCode, Patient.PatientUrbanRural,\n".
                "       (SELECT ZipCodes.UrbanRural\n".
                "        FROM   ZipCodes\n".
                "        WHERE  ZipCodes.ZipCode=SUBSTRING(Patient.PatientZipCode, 1, 5)\n".
                "        AND    ZipCodes.ValidFrom <= Patient.PatientRegistrationDate\n".
                "        ORDER BY ZipCodes.ValidFrom DESC\n".
                "        LIMIT 1) as UrbanRural\n".
                "FROM  Patient\n".
                "WHERE Patient.PatientKeyID > $quFromId\n".
                "ORDER BY Patient.PatientKeyID ASC\n".
                "LIMIT $quSlice";
            $result = $connection->RunQuery($sql);
            $rows = array();
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $rows[] = $row;
            }
            if (count($rows) == 0) {
                $retVal['done'] = 1;
                break;
            }

            foreach($rows as $row) {
                $retVal['total']++;
                $id               = $row['PatientKeyID'];
                $fromId           = $id;
                $retVal['fromId'] = $id;
                $wantStatus = (isset($row['UrbanRural'])) ? strtolower($row['UrbanRural']) : 'unknown';
                $haveStatus = (isset($row['PatientUrbanRural'])) ? strtolower($row['PatientUrbanRural']) : '';
                if ($wantStatus != $haveStatus) {
                    $count++;
                    $sql =
                        "UPDATE Patient SET PatientUrbanRural=" . $connection->SqlQuote($wantStatus)."\n".
                        "WHERE  Patient.PatientKeyID = ".$connection->Quote($id);
                    $connection->RunQuery($sql);
                    $retVal['changed']++;
                }
            }
            if ($oneLoopOnly) {
                break;
            }
        }

    } catch (\Exception $e) {
        $errors[] = __FUNCTION__.': '. $e->getMessage();
        $success  = '';
        $weFailed = 1;
        $retVal['failures']++;
    }

    if (!$weFailed) {
        $success = __FUNCTION__.": Function succeeded and updated $count Patient records with their ZipCodeStatus entries";
    }

    return $retVal;
}


