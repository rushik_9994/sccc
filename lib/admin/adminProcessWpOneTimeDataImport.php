<?php
/**
 * Function to do a one-time-import from the public site
 */

/**
 * Process all patients
 * @param int $ignoreAdminRights optional       defaults to 0
 * @param int $fromId optional                  defaults to 0
 * @param int $slice optional                   defaults to 200
 * @param int $oneLoopOnly optional             defaults to 0
 * @return array $retVal                        assoc array
 */
function adminProcessWpOneTimeDataImport($ignoreAdminRights=0, $fromId=0, $slice=200, $oneLoopOnly=0) {
    global $cfg, $errors, $warnings, $success;

    $dryRunMode = ((isset($_POST['dryRunMode'])) && ($_POST['dryRunMode'])) ? 1 : 0;

    $weFailed = 0;
    $retVal = array('done' => 0, 'fromId' => 0, 'total' => 0, 'changed'=>0, 'failures'=>0);

    /*We scrapped the idea of a one-time-input from the WP site

    set_time_limit(0);
    try {
        $connection = startAdminFunction($ignoreAdminRights);

        $publicPdo = getOldPublicPdo($error);
        if ($error) {
            throw new \Exception($error);
        }

        $scorPdo = scorGetPdo($error);
        if ($error) {
            throw new \Exception($error);
        }

        //Read our CSV translation file in
        $translation = readTrialProtocolTranslationFile(ROOT_PATH.'/data/trial_numbers_comparison_from_scor.csv');

        //Verify and possibly change our scor DB schema
        changeScorSchemaForWpImportIfNeeded($scorPdo);

        //Fetch rows from public site
        $sql =
            "SELECT TrialProtocolNumber, TrialStudyDescription, TrialEligibility, TrialTreatment, TrialPurpose\n".
            "FROM   Trial";
        $publicRows = $publicPdo->select($sql);
        $err        = $scorPdo->getError();

        $successCount = 0;
        $errCount     = 0;
        print
            "\n<table border='1' width='50%'>\n".
            "    <tr>\n".
            "    <td style='background-color:#DDDDDD' width='10%'>Status</td>\n".
            "    <td style='background-color:#DDDDDD' width='20%'>Public</td>\n".
            "    <td style='background-color:#DDDDDD' width='20%'>ORIS</td>\n".
            //"    <td style='background-color:#DDDDDD'>&nbsp;</td>\n".
            "</tr>\n";

        foreach($publicRows as $publicRow) {
            $publicTrialProtocolNumber   = assocVal($publicRow, 'TrialProtocolNumber', '');
            $publicTrialStudyDescription = assocVal($publicRow, 'TrialStudyDescription', '');
            $publicTrialEligibility      = assocVal($publicRow, 'TrialEligibility', '');
            $publicTrialPurpose          = assocVal($publicRow, 'TrialPurpose', '');
            if ($publicTrialProtocolNumber) {
                $privateTrialProtocolNumber = findPrivateTrialProtocolNumber($scorPdo, $publicRow, $testTrialProtocolNumberUsed);

                if (!$privateTrialProtocolNumber) {
                    print
                        "    <tr style='background-color:red;color:white;'>\n".
                        "        <td>NOT FOUND</td>\n".
                        "        <td>\"".htmlentities($publicTrialProtocolNumber)."\"</td>\n".
                        "        <td>&nbsp;</td>\n".
                      //"        <td>&nbsp;</td>\n".
                        "    </tr>\n";
                    $errCount++;
                } else {
                    print
                        "    <tr>\n".
                        "        <td>OK</td>\n".
                        "        <td>\"".htmlentities($publicTrialProtocolNumber)."\"</td>\n".
                        "        <td>\"".htmlentities($privateTrialProtocolNumber)."\"</td>\n".
                      //"        <td>&nbsp;</td>\n".
                        "    </tr>\n";
                    if (!$dryRunMode) {
                        $sql =
                            "UPDATE TrialORIS SET\n" .
                            "TrialStudyDescription=" . $scorPdo->quote($publicTrialStudyDescription).",\n".
                            "TrialEligibility=".$scorPdo->quote($publicTrialEligibility).",\n".
                            "TrialPurpose=".$scorPdo->quote($publicTrialPurpose).",\n".
                            "WHERE TrialProtocolNumber=".$scorPdo->quote($privateTrialProtocolNumber)."\n".
                            "LIMIT 1";
                        $affected = $scorPdo->update($sql);
                        $err      = $scorPdo->getError();
                        if ((!$affected) || ($err)) {

                        } else {
                            $successCount++;
                        }
                    }//!dryRunMode
                }
            }//if protocol
        }//foreach

        print "</table>\n";

        if (($dryRunMode) || ($errCount)) {
            exit;
        }

    } catch (\Exception $e) {
        $errors[] = __FUNCTION__.': '. $e->getMessage();
        $success  = '';
        $weFailed = 1;
        $retVal['failures']++;
    }

    if (!$weFailed) {
        $success = __FUNCTION__.": Function succeeded and imported data entries from WPpublic";
    }
    */

    return $retVal;
}


/**
 * @param $csvFile
 * @return array
 */
/*
function readTrialProtocolTranslationFile($csvFile) {
    $translation = array();

    if (!file_exists($csvFile)) {
        throw new \Exception("CSV file '$csvFile' does not exist!");
    }
    if (!is_callable('fgetcsv')) {
        throw new \Exception('PHP problem - function fgetcsv is not callable');
    }
    $fh = fopen($csvFile,'r');
    if (!$fh) {
        throw new \Exception("Failed to open the CSV file '$csvFile'");
    }
    while(! feof($fh)) {
        $lineArray = fgetcsv($fh);
        $translation[] = $lineArray;
    }
    fclose($fh);

    return $translation;
}
*/

/**
 * Translate the public protocolNumber into the private one
 * @param  object $scorPdo
 * @param  array $publicRow
 * @param  string ref $testTrialProtocolNumberUsed
 * @return string $privateTrialProtocolNumber
 */
/*
function findPrivateTrialProtocolNumber($scorPdo, $publicRow, &$testTrialProtocolNumberUsed) {
    $privateTrialProtocolNumber = '';

    $publicTrialProtocolNumber  = assocVal($publicRow, 'TrialProtocolNumber', '');

    $testTrialProtocolNumber   = trim($publicTrialProtocolNumber);

    $testTrialProtocolNumber = str_replace('/', ' ', $testTrialProtocolNumber);
    $testTrialProtocolNumber = str_replace('-', ' ', $testTrialProtocolNumber);
    $parts = explode(' ', $testTrialProtocolNumber);
    $f1 = strpos($testTrialProtocolNumber, ' ');
    $secondary = '/-/';
    $tertiary  = '/-/';
    $quarty    = '/-/';
    $quinty    = '/-/';
    $sexti     = '/-/';
    $septi     = '/-/';

    if ($f1) {
        $testTrialProtocolNumber = trim(substr($testTrialProtocolNumber, $f1));
    }
    $secondary = $parts[0].'-'.$parts[1];
    $tertiary  = str_replace('/', '-', $secondary);
    $quarty = str_replace(' ', '-', $testTrialProtocolNumber);
    $quinty = $parts[count($parts) - 1];
    $sexti  = 'WF'.$parts[count($parts) - 1];
    $septi  = $testTrialProtocolNumber;
    if (substr($septi, 0, 1) == 'A') {
        $septi = 'A0'.substr($septi, 1);
    }
    $testTrialProtocolNumberUsed = $testTrialProtocolNumber;

    $sql =
        "SELECT *\n".
        "FROM   TrialORIS\n".
        "WHERE  TrialProtocolNumber=" . $scorPdo->quote($testTrialProtocolNumber)." -- 1\n".
        "OR     TrialProtocolNumber=" . $scorPdo->quote($secondary)." -- 2\n".
        "OR     TrialProtocolNumber=" . $scorPdo->quote($tertiary)." -- 3\n".
        "OR     TrialProtocolNumber=" . $scorPdo->quote($quarty)." -- 4\n".
        "OR     TrialProtocolNumber=" . $scorPdo->quote($quinty)." -- 5\n".
        "OR     TrialProtocolNumber=" . $scorPdo->quote($sexti)." -- 6\n".
        "OR     TrialProtocolNumber=" . $scorPdo->quote($septi)." -- 7\n";
        //"AND    Trial."  ---- just look at open trials
    $rows = $scorPdo->select($sql);
    //$error = $scorPdo->getError();
    $cnt = count($rows);
    if ($cnt) {
        $privateTrialProtocolNumber = $rows[0]['TrialProtocolNumber'];
    } else {
    }

    return $privateTrialProtocolNumber;
}
*/

/**
 * Make sure our SCOR Trial table contains the public fields we're importing
 * @param $scorPdo
 * @throws Exception
 */
/*
function changeScorSchemaForWpImportIfNeeded(EzPdo $scorPdo) {
    $rows = $scorPdo->select("SELECT * FROM TrialORIS LIMIT 1");
    if (count($rows) == 0) {
        throw new \Exception('Could not read SCOR.Trial table schema');
    }

    $foundFieldNumber = 0;
    $wantedFields = array('TrialStudyDescription' => 1, 'TrialEligibility' => 1, 'TrialPurpose' => 1);
    foreach($rows[0] as $row) {
        foreach($row as $key => $val) {
            if (isset($wantedFields[$key])) {
                $foundFieldNumber++;
            }
        }
    }
    if ($foundFieldNumber != count($wantedFields)) {
        $sql =
            "ALTER TABLE `morpheu6_SCCC`.`TrialORIS`\n".
            "ADD COLUMN `TrialStudyDescription` LONGTEXT NULL AFTER `TrialQoLC`,\n".
            "ADD COLUMN `TrialEligibility` LONGTEXT NULL AFTER `TrialStudyDescription`,\n".
            "ADD COLUMN `TrialPurpose` LONGBLOB NULL AFTER `TrialEligibility`";
        $scorPdo->anyquery($sql);

        $rows = $scorPdo->select("SELECT * FROM TrialORIS LIMIT 1");
        if (count($rows) == 0) {
            throw new \Exception('Could not change the SCOR.Trial table schema');
        }
    }
}
*/

