<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class UsersType extends BaseClass {

	private $UserTypeKeyID; //int(2)
	private $UserTypeName; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_UsersType($UserTypeName){
		$this->UserTypeName = $UserTypeName;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM UsersType WHERE UserTypeKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->UserTypeKeyID = $row["UserTypeKeyID"];
			$this->UserTypeName = $row["UserTypeName"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'UsersType','UserTypeKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE UsersType SET\n".
                "UserTypeName = ".           $this->SqlQuote($this->UserTypeName)."\n".
                "WHERE UserTypeKeyID = ".    $this->SqlQuote($this->UserTypeKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'UsersType','UserTypeKeyID', $this->UserTypeKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql = "INSERT INTO UsersType (UserTypeName) VALUES (". $this->SqlQuote($this->UserTypeName).')';
            $this->UserTypeKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'UsersType','UserTypeKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

		$i = 0;
		$result = $this->connection->RunQuery("SELECT UserTypeKeyID FROM UsersType ORDER BY $column $order");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["UserTypeKeyID"];
            $i++;
        }
    	return $keys;
	}

	/**
	 * @return null|int $UserTypeKeyID
	 */
	public function getUserTypeKeyID()
    {
		return $this->UserTypeKeyID;
	}

	/**
	 * @return null|string $UserTypeName
	 */
	public function getUserTypeName()
    {
		return $this->UserTypeName;
	}

	/**
	 * @param int $UserTypeKeyID
	 */
	public function setUserTypeKeyID($UserTypeKeyID)
    {
		$this->UserTypeKeyID = $UserTypeKeyID;
	}

	/**
	 * @param string $UserTypeName
	 */
	public function setUserTypeName($UserTypeName)
    {
		$this->UserTypeName = $UserTypeName;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        $validationMessage = '';

        //Validate
        do {
            $val = ((isset($this->UserTypeKeyID)) ? $this->UserTypeKeyID : null);  //vld: UsersType.UserTypeKeyID
            if ($validationMessage = validateField($val,'UserTypeKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->UserTypeName)) ? $this->UserTypeName : null);  //vld: UsersType.UserTypeName
            if ($validationMessage = validateField($val,'UserTypeName','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

