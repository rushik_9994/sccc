<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class Doctor extends BaseClass {

    public $DoctorKeyID; //int(10)
    public $TimestampCreate; //datetime
    public $TimestampUpdate; //timestamp
    public $DoctorFirstName; //varchar(250)
    public $DoctorLastName; //varchar(250)
    public $DoctorSpecialty; //varchar(250)
    public $DoctorAddress1; //varchar(250)
    public $DoctorAddress2; //varchar(250)
    public $DoctorCity; //varchar(250)
    public $DoctorState; //varchar(250)
    public $DoctorZip; //int(9)
    public $DoctorPhone; //int(10)
    public $DoctorEmail; //varchar(250)
    public $DoctorIsActive; //tinyint(1)

    //Pagination variables
    public $showPagination     = 0;
    public $showFirstButton    = 0;
    public $showPreviousButton = 0;
    public $showNextButton     = 0;
    public $legendText         = '';
    public $totalRowCount      = 0;


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_Doctor($TimestampCreate,$TimestampUpdate,$DoctorFirstName,$DoctorLastName,$DoctorSpecialty,$DoctorAddress1,$DoctorAddress2,$DoctorCity,$DoctorState,$DoctorZip,$DoctorPhone,$DoctorEmail,$DoctorIsActive=1)
    {
        $this->TimestampCreate = $TimestampCreate;
        $this->TimestampUpdate = $TimestampUpdate;
        $this->DoctorFirstName = $DoctorFirstName;
        $this->DoctorLastName = $DoctorLastName;
        $this->DoctorSpecialty = $DoctorSpecialty;
        $this->DoctorAddress1 = $DoctorAddress1;
        $this->DoctorAddress2 = $DoctorAddress2;
        $this->DoctorCity = $DoctorCity;
        $this->DoctorState = $DoctorState;
        $this->DoctorZip = $DoctorZip;
        $this->DoctorPhone = $DoctorPhone;
        $this->DoctorEmail = $DoctorEmail;
        $this->DoctorIsActive = ((isset($DoctorIsActive)) && ($DoctorIsActive)) ? 1 : 0;
    }*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $result = $this->connection->RunQuery("SELECT *\n".
                                              "FROM   `Doctor`\n".
                                              "WHERE  `DoctorKeyID`=". $this->SqlQuote($key_row)
                                             );

        $found = 0;
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $found = 1;
            $this->DoctorKeyID     = $row["DoctorKeyID"];
            $this->TimestampCreate = $row["TimestampCreate"];
            $this->TimestampUpdate = $row["TimestampUpdate"];
            $this->DoctorFirstName = $row["DoctorFirstName"];
            $this->DoctorLastName  = $row["DoctorLastName"];
            $this->DoctorSpecialty = $row["DoctorSpecialty"];
            $this->DoctorAddress1  = $row["DoctorAddress1"];
            $this->DoctorAddress2  = $row["DoctorAddress2"];
            $this->DoctorCity      = $row["DoctorCity"];
            $this->DoctorState     = $row["DoctorState"];
            $this->DoctorZip       = $row["DoctorZip"];
            $this->DoctorPhone     = $row["DoctorPhone"];
            $this->DoctorEmail     = $row["DoctorEmail"];
            $this->DoctorIsActive  = ((isset($row['DoctorIsActive'])) && ($row['DoctorIsActive'])) ? 1 : 0;
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Doctor', 'DoctorKeyID', $key_row);
    }

    /**
     * Update the active row table on table
     * @throws \Exception
     */
    public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $this->DoctorIsActive = ((isset($this->DoctorIsActive)) && ($this->DoctorIsActive)) ? 1 : 0;

            TrackChanges::trackBeforeUpdate($this->connection, 'Doctor', 'DoctorKeyID', $this->DoctorKeyID);
            $sql =
                "UPDATE `Doctor` SET\n".
                "    `DoctorFirstName`=". $this->SqlQuote($this->DoctorFirstName) .",\n".
                "    `DoctorLastName`=".  $this->SqlQuote($this->DoctorLastName) .",\n".
                "    `DoctorSpecialty`=". $this->SqlQuote($this->DoctorSpecialty) .",\n".
                "    `DoctorAddress1`=".  $this->SqlQuote($this->DoctorAddress1) .",\n".
                "    `DoctorAddress2`=".  $this->SqlQuote($this->DoctorAddress2) .",\n".
                "    `DoctorCity`=".      $this->SqlQuote($this->DoctorCity) .",\n".
                "    `DoctorState`=".     $this->SqlQuote($this->DoctorState) .",\n".
                "    `DoctorZip`=".       $this->SqlQuote($this->DoctorZip) .",\n".
                "    `DoctorPhone`=".     $this->SqlQuote($this->DoctorPhone) .",\n".
                "    `DoctorEmail`=".     $this->SqlQuote($this->DoctorEmail) .",\n".
                "    `DoctorIsActive`=".  $this->SqlQuote($this->DoctorIsActive)."\n".
                "WHERE `DoctorKeyID`=". $this->SqlQuote($this->DoctorKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Doctor','DoctorKeyID', $this->DoctorKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO `Doctor` (`DoctorFirstName`, `DoctorLastName`, `DoctorSpecialty`,\n".
                "                      `DoctorAddress1`, `DoctorAddress2`, `DoctorCity`, `DoctorState`,\n".
                "                      `DoctorZip`, `DoctorPhone`, `DoctorEmail`, `DoctorIsActive`) VALUES (\n".
                $this->SqlQuote($this->DoctorFirstName). ",\n".
                $this->SqlQuote($this->DoctorLastName). ",\n".
                $this->SqlQuote($this->DoctorSpecialty). ",\n".
                $this->SqlQuote($this->DoctorAddress1). ",\n".
                $this->SqlQuote($this->DoctorAddress2). ",\n".
                $this->SqlQuote($this->DoctorCity). ",\n".
                $this->SqlQuote($this->DoctorState). ",\n".
                $this->SqlQuote($this->DoctorZip). ",\n".
                $this->SqlQuote($this->DoctorPhone). ",\n".
                $this->SqlQuote($this->DoctorEmail). ",\n".
                $this->SqlQuote($this->DoctorIsActive).')';
            $this->DoctorKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Doctor','DoctorKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @param int    $from optional                  Default is 0   (for LIMIT $from,...)
     * @param int    $limit optional                 Default is 0   (for LIMIT ...,$limit)
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($column, $order='ASC', $from=0, $limit=0, $sClauses=array())
    {
        $keys = array(); $i = 0;

        if ((!isset($order)) || (!is_string($order))) { $order='ASC'; }
        $order = strtoupper($order);
        if (($order!='ASC') && ($order!='DESC')) { $order='ASC'; }

        $column = $this->connection->Quote($column);

        //Support SearchFilter
        //Get additional joins and where clauses as supplied by the searchFilter class
        //print "<pre>".print_r($sClauses,true)."</pre>";
        if ((!isset($sClauses)) || (!is_array($sClauses))) { $sClauses=array(); }
        if ((!isset($sClauses['joins'])) || (!is_array($sClauses['joins'])))   { $sClauses['joins']=array(); }
        if ((!isset($sClauses['where'])) || (!is_array($sClauses['where'])))   { $sClauses['where']=array(); }
        if ((!isset($sClauses['debug'])) || (!is_numeric($sClauses['debug']))) { $sClauses['debug']=0; }
        $joins    = '';
        $whereAnd = '';
        $orderIndex          = (isset($sClauses['order_index']))       ? $sClauses['order_index'] : '_0';
        $needDoctorSpecialty = ((isset($sClauses['order_field'.$orderIndex])) && ($sClauses['order_field'.$orderIndex]=='order_specialty'.$orderIndex)) ? 1 : 0;
        $needDoctorSites     = ((isset($sClauses['order_field'.$orderIndex])) && ($sClauses['order_field'.$orderIndex]=='order_sites'.$orderIndex))     ? 1 : 0;
        foreach($sClauses['joins'] as $join=>$ignore) {
            if (($needDoctorSpecialty) && (strpos($join, 'DoctorSpecialty')!==false)) { $needDoctorSpecialty=0; }
            $joins .= $join;
        }
        if ($needDoctorSpecialty) {
            $joins .= "\nLEFT JOIN `DoctorSpecialty` ON `Doctor`.`DoctorSpecialty`=`DoctorSpecialty`.`DoctorSpecialtyKeyID`\n";
        }
        reset($sClauses['joins']);
        if ($joins) { $joins = trim($joins)."\n"; }


        foreach($sClauses['where'] as $ignore=>$whr) {
            $whereAnd .= $whr;
        }
        reset($sClauses['where']);
        if ($whereAnd) { $whereAnd = trim($whereAnd)."\n"; }

        //Support showing of inactive doctors also
        if (isset($sClauses['show_inactive_also'])) {
            $showInactiveAlso = $sClauses['show_inactive_also'];
            if (!$showInactiveAlso) {
                //This is the default: Only show doctors that are active
                $whereAnd .= "AND    `Doctor`.`DoctorIsActive` = 1\n";
            //} else {
                //This needs no clause: Show both active and inactive doctors
            }
        }

        //COUNT ROWS:  Build the query to just count our results
        $sql = "SELECT count(*) as cnt\n".
               "FROM   `Doctor`\n".
               $joins.
               "WHERE  `Doctor`.`DoctorKeyID` > 0\n".
               $whereAnd;
        $result = $this->connection->RunQuery($sql);
        $this->totalRowCount = 0;
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (isset($row['cnt'])) {
                $this->totalRowCount = $row['cnt'];
                break;
            }
        }

        //GET KEYS:  Build the query to actually get our Keys
        $sql  = "SELECT Doctor.`DoctorKeyID`\n";

        if ($needDoctorSites) {
            $sql .= ", (SELECT Site.`SiteName`\n".
                    "   FROM   DoctorSiteJoin\n".
                    "   LEFT JOIN Site ON DoctorSiteJoin.SiteKeyID=Site.`SiteKeyID`\n".
                    "   WHERE  DoctorSiteJoin.`DoctorKeyID`=Doctor.DoctorKeyID\n".
                    "   ORDER BY DoctorSiteJoin.`DoctorSiteKeyID`\n".
                    "   LIMIT 1) as DoctorSiteList\n";
        }
        
        $sql .= "FROM   `Doctor`\n".
                $joins.
                "WHERE  `Doctor`.`DoctorKeyID` > 0\n".
                $whereAnd;

        //Support table column sorting 'order_field_0' (thead)...
        $orderBy = "ORDER BY `".$this->connection->Quote($column)."` $order";
        $orderIndex      = (isset($sClauses['order_index']))                 ? $sClauses['order_index'] : '_0';
        $orderField      = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection  = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }
        if ($orderField) {
            if ($orderField == 'order_doctors'.$orderIndex) {
                $orderBy = "ORDER BY CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) $orderDirection";
            } else if ($orderField == 'order_specialty'.$orderIndex) {
                $orderBy = "ORDER BY DoctorSpecialty.DoctorSpecialtyText $orderDirection, CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) ASC";
            } else if ($orderField == 'order_email'.$orderIndex) {
                $orderBy = "ORDER BY Doctor.DoctorEmail $orderDirection, CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) ASC";
            } else if ($orderField == 'order_sites'.$orderIndex) {
                $orderBy = "ORDER BY DoctorSiteList $orderDirection, CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) ASC";
            } else if ($orderField == 'order_date_created'.$orderIndex) {
                $orderBy = "ORDER BY Doctor.TimeStampCreate $orderDirection, CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) ASC";
            }
        } 
        $sql .= $orderBy;

        //Calculate our final SELECT query with limit clause (if $limit>0)
        if ((!isset($from))  || (!is_numeric($from))  || ($from  < 0)) { $from  = 0; }
        if ((!isset($limit)) || (!is_numeric($limit)) || ($limit < 0)) { $limit = 0; }
        if (($from > 0 ) && ($limit==0)) {
            //get pagination limit
            $pg = new Pagination();
            $limit = $pg->getPaginationSize('doctors.pagination.size');
        }
        $this->showPagination     = (($from) || ($limit)) ? 1 : 0;
        $this->showFirstButton    = ($from > 0)  ?  1  : 0;
        $this->showPreviousButton = ($from > 0)  ?  1  : 0;
        $this->showNextButton     = 0;
        $this->legendText         = ($limit > 0) ? "Displaying Records ".($from+1)." to ".($from+$limit) : '';
        $this->limitFrom          = $from;
        $this->limitSlice         = $limit;
        $sql .= (($from) || ($limit))  ?  "\nLIMIT $from,".($limit+1) : '';

        //DebugDisplay for SearchFilter
        if ((0) || ($sClauses['debug'])) {
            print "<br /><pre style='font-size:8pt;color:black;'>$sql</pre>";
            print "<pre style='font-size:8pt;color:blue;'>".print_r($sClauses,true)."</pre>";
            if ($sClauses['debug'] > 1) { exit; }
        }

        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (($limit) && ($i >= $limit)) {
                $this->showNextButton = 1;
                break;
            }
            $keys[$i] = $row["DoctorKeyID"];
            $i++;
            if ($limit > 0) { $this->legendText = "Displaying Records ".($from+1)." to ".($from+$i); }
        }

        if (count($keys)==0) { $this->showPagination=0; }

        return $keys;
    }

    /**
     * Returns a select field and assigns selected
     *
     * @param string $selected
     * @param string $where
     * @return string $select
     * @throws \Exception
     */
    public function CreateSelect($selected, $where)
    {
        $select = '<select name="DoctorKeyID" id="DoctorKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
        $result = $this->connection->RunQuery("SELECT `DoctorKeyID`, `DoctorLastName`, `DoctorFirstName`\n".
                                              "FROM   `Doctor`\n".
                                              "$where\n".
                                              "ORDER BY `DoctorKeyID`"
                                             );
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escDoctorKeyID     = htmlentities($row["DoctorKeyID"]);
            $escDoctorLastName  = htmlentities($row["DoctorLastName"]);
            $escDoctorFirstName = htmlentities($row["DoctorFirstName"]);

            if ($row["DoctorKeyID"] == $selected) {
                 $select .= "<option value=\"$escDoctorKeyID\" selected>$escDoctorLastName, $escDoctorFirstName</option>\n";
            } else {
                $select .= "<option value=\"$escDoctorKeyID\">$escDoctorLastName, $escDoctorFirstName</option>\n";
            }
        }
        $select .= '</select>'."\n";
        return $select;
    }

    /**
     * @return null|int $DoctorKeyID
     */
    public function getDoctorKeyID()
    {
        return $this->DoctorKeyID;
    }

    /**
     * @return null|string $TimestampCreate
     */
    public function getTimestampCreate()
    {
        return $this->TimestampCreate;
    }

    /**
     * @return null|string $TimestampUpdate
     */
    public function getTimestampUpdate()
    {
        return $this->TimestampUpdate;
    }

    /**
     * @return null|string $DoctorFirstName
     */
    public function getDoctorFirstName()
    {
        return $this->DoctorFirstName;
    }

    /**
     * @return null|string $DoctorLastName
     */
    public function getDoctorLastName()
    {
        return $this->DoctorLastName;
    }

    /**
     * @return null|string $DoctorSpecialty
     */
    public function getDoctorSpecialty()
    {
        return $this->DoctorSpecialty;
    }

    /**
     * @return null|string $DoctorAddress1
     */
    public function getDoctorAddress1()
    {
        return $this->DoctorAddress1;
    }

    /**
     * @return null|string $DoctorAddress2
     */
    public function getDoctorAddress2()
    {
        return $this->DoctorAddress2;
    }

    /**
     * @return null|string $DoctorCity
     */
    public function getDoctorCity()
    {
        return $this->DoctorCity;
    }

    /**
     * @return null|string $DoctorState
     */
    public function getDoctorState()
    {
        return $this->DoctorState;
    }

    /**
     * @return null|string $DoctorZip
     */
    public function getDoctorZip()
    {
        return $this->DoctorZip;
    }

    /**
     * @return null|string $DoctorPhone
     */
    public function getDoctorPhone()
    {
        return $this->DoctorPhone;
    }

    /**
     * @return null|string $DoctorEmail
     */
    public function getDoctorEmail()
    {
        return $this->DoctorEmail;
    }

    /**
     * @return null|int $DoctorIsActive
     */
    public function getDoctorIsActive()
    {
        return ((isset($this->DoctorIsActive)) && ($this->DoctorIsActive)) ? 1 : 0;
    }


    /**
     * @return string $isActive     depending on DoctorIsActive either 'Active' or '[bold,red]Inactive[/bold,/red]'
     */
    public function getDoctorIsActiveDisplay() {
        $isActive = $this->getDoctorIsActive();
        return (($isActive) ? 'Active' : "<div style='display:inline;font-weight:bold;background-color:red;color:white;'>Doctor Is Inactive</div>");
    }

    /**
     * @param int $DoctorKeyID
     */
    public function setDoctorKeyID($DoctorKeyID)
    {
        $this->DoctorKeyID = $DoctorKeyID;
    }

    /**
     * @param string $TimestampCreate
     */
    public function setTimestampCreate($TimestampCreate)
    {
        $this->TimestampCreate = $TimestampCreate;
    }

    /**
     * @param string $TimestampUpdate
     */
    public function setTimestampUpdate($TimestampUpdate)
    {
        $this->TimestampUpdate = $TimestampUpdate;
    }

    /**
     * @param string $DoctorFirstName
     */
    public function setDoctorFirstName($DoctorFirstName)
    {
        $this->DoctorFirstName = $DoctorFirstName;
    }

    /**
     * @param string $DoctorLastName
     */
    public function setDoctorLastName($DoctorLastName)
    {
        $this->DoctorLastName = $DoctorLastName;
    }

    /**
     * @param string $DoctorSpecialty
     */
    public function setDoctorSpecialty($DoctorSpecialty)
    {
        $this->DoctorSpecialty = $DoctorSpecialty;
    }

    /**
     * @param string $DoctorAddress1
     */
    public function setDoctorAddress1($DoctorAddress1)
    {
        $this->DoctorAddress1 = $DoctorAddress1;
    }

    /**
     * @param string $DoctorAddress2
     */
    public function setDoctorAddress2($DoctorAddress2)
    {
        $this->DoctorAddress2 = $DoctorAddress2;
    }

    /**
     * @param string $DoctorCity
     */
    public function setDoctorCity($DoctorCity)
    {
        $this->DoctorCity = $DoctorCity;
    }

    /**
     * @param string $DoctorState
     */
    public function setDoctorState($DoctorState)
    {
        $this->DoctorState = $DoctorState;
    }

    /**
     * @param string $DoctorZip
     */
    public function setDoctorZip($DoctorZip)
    {
        $this->DoctorZip = $DoctorZip;
    }

    /**
     * @param string $DoctorPhone
     */
    public function setDoctorPhone($DoctorPhone)
    {
        $this->DoctorPhone = $DoctorPhone;
    }

    /**
     * @param string $DoctorEmail
     */
    public function setDoctorEmail($DoctorEmail)
    {
        $this->DoctorEmail = $DoctorEmail;
    }

    /**
     * @param int $DoctorIsActive
     */
    public function setDoctorIsActive($DoctorIsActive=1)
    {
        $this->DoctorIsActive = ((isset($DoctorIsActive)) && ($DoctorIsActive)) ? 1 : 0;
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Fix
        $this->DoctorIsActive = ((isset($this->DoctorIsActive)) && ($this->DoctorIsActive)) ? 1 : 0;

        //Validate
        do {

            $val = ((isset($this->DoctorKeyID)) ? $this->DoctorKeyID : null);  //vld: Doctor.DoctorKeyID
            if ($validationMessage = validateField($val,'DoctorKeyID','nullok;positiveint',__CLASS__)) { break; }

            //public $TimestampCreate; //datetime   vld: Doctor.TimestampCreate
            //public $TimestampUpdate; //timestamp  vld: Doctor.TimestampUpdate

            $val = ((isset($this->DoctorFirstName)) ? $this->DoctorFirstName : null);  //vld: Doctor.DoctorFirstName
            if ($validationMessage = validateField($val,'DoctorFirstName','nestring',__CLASS__)) { break; }

            $val = ((isset($this->DoctorLastName)) ? $this->DoctorLastName : null);  //vld: Doctor.DoctorLastName
            if ($validationMessage = validateField($val,'DoctorLastName','nestring',__CLASS__)) { break; }

            $val = ((isset($this->DoctorSpecialty)) ? $this->DoctorSpecialty : null);  //vld: Doctor.DoctorSpecialty
            if ($validationMessage = validateField($val,'DoctorSpecialty','zeroorpositiveint',__CLASS__)) { break; }  //----!!! zero?

            //public $DoctorAddress1; //varchar(250)  //vld: Doctor.DoctorAddress1
            //public $DoctorAddress2; //varchar(250)  //vld: Doctor.DoctorAddress2
            //public $DoctorCity; //varchar(250)      //vld: Doctor.DoctorCity
            //public $DoctorState; //varchar(250)     //vld: Doctor.DoctorState
            //public $DoctorZip; //int(9)             //vld: Doctor.DoctorZip
            //public $DoctorPhone; //int(10)          //vld: Doctor.DoctorPhone

            $val = ((isset($this->DoctorEmail)) ? $this->DoctorEmail : null);  //vld: Doctor.DoctorEmail
            if ($validationMessage = validateField($val,'DoctorEmail', 'nullok;emptyok;email',__CLASS__)) { break; }

            $val = ((isset($this->DoctorIsActive)) ? $this->DoctorIsActive : null);  //vld: Doctor.DoctorIsActive
            if ($validationMessage = validateField($val,'DoctorIsActive', 'nullok;0or1',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

