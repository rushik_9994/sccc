<?php
/**
 * Common includes in the <head> section of every page
 */

defined('SEARCH_COMMENT') || define('SEARCH_COMMENT', ' ---- ');

?>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="js/common.js<?php print '?t='.time(); ?>"></script>
        <script type="text/javascript" src="js/chosen.jquery.js"></script>
        
	    <link rel="stylesheet" href="js/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css/colorbox.css" media="screen" />
        <link rel="stylesheet" href="css/chosen.css">
        <link rel="stylesheet" href="css/style.css?t=<?php print time();?>" />

        <script type="text/javascript">
            //Calculate our myUrlBase
            var myUrlBase  = window.location.protocol + "//" +  window.location.host;
            var myUrlParts = window.location.pathname.split( '/' );
            for (i = 0; i < myUrlParts.length; i++) {
                if (myUrlParts[i].indexOf('.php') == -1) {
                    myUrlBase += myUrlParts[i];
                    myUrlBase += "/";
                } else {
                    break;
                }
            }

            $(document).ready(function(){
                //$(".ui-tabs").tabs();
                //$('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', autoSize: true, changeYear: true, yearRange: '1916:2017'});
                $('.chosen-select').chosen();


                var searchFilterAutocompleteUrl = getBaseUrl()+'ajax_SearchFilterAutocomplete.php';

                $('#search_text_0').autocomplete({
                    source: function(request, response) {                        
                        $.ajax({
                            type: "POST",
                            url: searchFilterAutocompleteUrl,
                            data: {term:request.term, search_tag_0:$('#search_tag_0').val(), search_start_date:$('#search_start_date').val(), search_end_date:$('#search_end_date').val() ,search_page:window.location.href},
                            success: response,
                            dataType: 'json',
                            minLength: 2,
                            delay: 100
                        })
                    }
                    //source: searchFilterAutocompleteUrl //,  OK
                });

                $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', autoSize: true, changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
            });

            window.setInterval(intervalFunction, 300);
            
            function intervalFunction() {
                var searchText = $('#search_text_0').val();
                if (searchText) {
                    var i = searchText.indexOf('<?php print SEARCH_COMMENT; ?>');
                    if (i > 1) {
                        searchText = searchText.substr(0, i);
                        $('#search_text_0').val(searchText);
                    }
                }
            }

            function changeSortOrder(fieldName) {
                var orderIndex            = fieldName.slice(-2);
                var orderFieldId          = "#"+"order_field"+orderIndex;
                var orderDirectionId      = "#"+"order_direction"+orderIndex;

                var orderField     = $(orderFieldId).val();
                var orderDirection = $(orderDirectionId).val();

               $("#order_last_clicked").val(orderIndex);

                if (orderField == fieldName) {
                    if (orderDirection == "desc") {
                        $(orderDirectionId).val("asc");
                    } else {
                        $(orderDirectionId).val("desc");
                    }
                } else {
                    $(orderFieldId).val(fieldName);
                    $(orderDirectionId).val('asc');
                }

                //Add anchor tag of the table to scroll back to the table (useful for secondary pages)
                var formUrl = $("#main_form").attr("action");
                if ( undefined != formUrl) {
                    $( "#main_form" ).attr("action", formUrl+"#tb"+orderIndex);
                } else {
                    $( "#main_form" ).attr("action", "#tb"+orderIndex);
                }

                $( "#main_form" ).submit();
            }

        </script>
