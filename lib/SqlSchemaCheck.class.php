<?php
/*
 * Author: Reinhard Hopperger rhopperger@gmail.com
 * 
 * Create Date: 11-19-2016
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/DataBaseMysql.class.php');
require_once(ROOT_PATH.'/cfg/config.php');

//Note:  At the bottom of this file we actually instantiate this class
//       so we automatically trigger a schema check every time this class is included


class SqlSchemaCheck
{

    /**
     * @var Records the successful SQL schema change actions
     */
    protected $changeActions       = array();


    /**
     * Constructor
     *
     * @access public
     */
    public function __construct() {
        $this->connection = new DataBaseMysql();
        $this->checkSqlSchema();
    }


    /**
     * Function to check and update the SQL Schema
     *
     * Note: This function will print an error message and die if a major problem is found! (Unless $noDie is set)
     *
     * @access public
     * @param  int $noDie optional     Set to 0 (script will die)
     * @return int $ok                 (Will only return if $noDie is set to 1!)
     */
    public function checkSqlSchema($noDie=0)
    {
        global $cfg, $errors, $warnings, $success;
        $ok = 1;
        $errors   = array();
        $warnings = array();
        $success  = '';

        try {
            $this->changeActions = array();

            //$this->checkDoctorTable($noDie);
            //$this->checkTrialCategoryTable($noDie);

            //$this->createSystemStatusTable($noDie);                   // Create the SystemStatus table

            //$this->changeTablesToInnoDb($noDie);                      // Convert all MyIsam tables to InnoDB

            /*ZipCodes
            $zipRowsAdded      = $this->createAndImportZipCodeFile($noDie);
            $zipCodeFieldExist = $this->checkPatientTableForZipCodeField($noDie);
            if (($zipCodeFieldExist) && ($zipRowsAdded)) {
                require_once(ROOT_PATH . '/lib/admin/adminBase.php');
                $data = array('id' => 0, 'm' => 'adminProcessPatientsUrbanRural');
                $protocoll = ((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == 'ok')) ? 'https://' : 'http://';
                $url = 'lib/admin/updateUrbanRural.php?data=' . urlencode(json_encode($data));
                $style = "style=\"color:white;font-weight:bold;\"";
                $msg = "<br /><br /><a $style href='$url'>Now please CLICK HERE to update the Urban/Rural fields in patient records also...</a><br /><br />";
                print $msg;
                $this->changeActions[] = 'Please go to lib/admin/updateUrbanRural.php to update the PatientUrbanRural fields also';
                //if (is_callable('adminProcessPatientsUrbanRural')) {
                    //$ignoreAdminRights = 1;
                    //adminProcessPatientsUrbanRural($ignoreAdminRights);
                //}
            }*/

            //$this->checkTrialTableForNctField($noDie);
            //$this->checkTrialTableForSummaryFields($noDie);

            //TrackChanges
            //$this->createTrackChangesTable($noDie);                 // UNUSED: Create the TrackChanges table
            $this->createTrackInfoTable($noDie);                      // Create the TrackInfo table
            //$this->createTrackLocationsTable($noDie);               // UNUSED: Create the TrackLocations table

            //Display the successfully performed actions
            if (count( $this->changeActions) > 0) {
                print "<table width='100%' style='background-color:white;color:green;font-weight:bold;font-size:11pt;'><tr><td>\n".
                      "<big><b>Schema changed successfully:</b></big><br />";
                foreach($this->changeActions as $message) {
                    print '&nbsp;&nbsp;&nbsp;<big>*</big>&nbsp;&nbsp;'.htmlentities($message)."<br />";
                }
                print "<br />Don't forget to turn config['sql.checkSchemaActive'] off again so as not to slow page loads down!<br /><br />";
                print "</td></tr></table>\n";
            }
        } catch(\Exception $e) {
            $ok = 0;
            $error = $e->getMessage();
            if (!$noDie) {
                $this->reportSchemaCheckProblem("Cannot check schema! ($error)");
                exit;
            }
        }

        return $ok;
    }

    /**
     * Function to check and update the Trial table for missing Trial.TrialNct field
     *
     * @param  int $noDie optional
     * @access protected
     * @throws Exception
     */
    protected function checkTrialTableForNctField($noDie=0)
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $trialRow = $this->getOneTrialTableRow();

        //CHECK 1: Trial.TrialNct
        if (!isset($trialRow['TrialNct'])) {
            $sql =
                "ALTER TABLE `Trial`\n".
                "ADD COLUMN `TrialNct` VARCHAR(100) NOT NULL AFTER `TrialQoLC`,\n".
                "ADD INDEX `idxTrialNct` (`TrialNct`)";
            $this->connection->RunQuery($sql);

            //CHECK 2: Trial.TrialNct
            $trialRow = $this->getOneTrialTableRow();
            if (!isset($trialRow['TrialNct'])) {
                throw new \Exception('Failed to insert the Trial.TrialNct field!');
            } else {
                $this->changeActions[] = 'Added Trial.TrialNct column';
            }
        }
    }


    /**
     * Function to check and update the Trial table for missing TrialSummaryEn, TrialSummaryEs fields
     *
     * @param  int $noDie optional
     * @access protected
     * @throws Exception
     */
    protected function checkTrialTableForSummaryFields($noDie=0)
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $trialRow = $this->getOneTrialTableRow();

        //CHECK 1: Trial.TrialSummaryEn, Trial.TrialSummaryEs
        if (!isset($trialRow['TrialSummaryEn'])) {
            $sql =
                "ALTER TABLE `Trial`\n".
                "ADD COLUMN `TrialSummaryEn` LONGTEXT NOT NULL AFTER `TrialNct`,\n".
                "ADD COLUMN `TrialSummaryEs` LONGTEXT NOT NULL AFTER `TrialSummaryEn`,\n".
                "ADD COLUMN `TrialSummaryLast` LONGTEXT NOT NULL AFTER `TrialSummaryEs`";
            $this->connection->RunQuery($sql);

            //CHECK 2: Trial.TrialSummaryEn, Trial.TrialSummaryEs, Trial.TrialSummaryLast
            $trialRow = $this->getOneTrialTableRow();
            if (!isset($trialRow['TrialSummaryEn'])) {
                throw new \Exception('Failed to insert the Trial.TrialNct field!');
            } else {
                $this->changeActions[] = 'Added TrialSummaryEn, TrialSummaryEs, TrialSummaryLast columns';
            }
        }
    }

    /**
     * Get the very first Trial row
     * @return array|mixed
     * @throws Exception
     */
    protected function getOneTrialTableRow()
    {
        $trialRow = array();

        $readSql = "SELECT * FROM `Trial` ORDER BY TrialKeyID DESC LIMIT 1";
        $result = $this->connection->RunQuery($readSql);
        $trialRow = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $trialRow = $row;
        }
        if ((!isset($trialRow['TrialKeyID']))) {
            throw new \Exception("Cannot check schema on 'Trial' table. No trial row found!");
        }
        return $trialRow;
    }


    /**
     * Change all MyIsam tables to InnoDB
     *
     * @param $noDie
     * @throws Exception
     */
    /*
    protected function changeTablesToInnoDb($noDie)
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $myIsamTables = $this->findMyIsamTables();
        if (count($myIsamTables) > 0) {
            foreach($myIsamTables as $tableName) {
                print "<br /><div style='color:white;'>Changing table $tableName to InnoDB engine. Please wait... <div>";
                $sql = "ALTER TABLE $tableName ENGINE = InnoDB";
                $this->connection->RunQuery($sql);
                $this->changeActions[] = "Changed $tableName' to InnoDb engine";
            }
        }

        $myIsamTables = $this->findMyIsamTables();
        if (count($myIsamTables) > 0) {
            print "<br />";
            foreach ($myIsamTables as $tableName) {
                print "<br /><div style='color:red;'>Failed to convert table $tableName to InnoDB</div>";
            }
            throw new \Exception('Failed to change all tables to InnoDB');
        }
        print "<br /><br />";
    }*/


    /**
     * Function to check and update the Patient table
     *
     * @access protected
     * @return int $zipCodeFieldExist
     * @throws Exception
     */
    /*
    protected function checkPatientTableForZipCodeField()
    {
        $zipCodeFieldExist = 0;
        $patientRow = $this->fetchPatientRow();

        //CHECK 1: Doctor.DoctorIsActive
        if (!isset($patientRow['PatientUrbanRural'])) {
            $sql =
                "ALTER TABLE `Patient`\n".
                "ADD COLUMN `PatientUrbanRural` VARCHAR(50) DEFAULT 'unknown' NOT NULL AFTER `PatientZipCode`";
            $this->connection->RunQuery($sql);

            //CHECK 2: Patient.PatientUrbanRural
            $patientRow = $this->fetchPatientRow();
            if (!isset($patientRow['PatientUrbanRural'])) {
                throw new \Exception('Failed to insert the Patient.PatientUrbanRural column!');
            } else {
                $this->changeActions[] = 'Added Patient.PatientUrbanRural column';
                $zipCodeFieldExist = 1;
            }
        } else {
            $zipCodeFieldExist = 1;
        }
        return $zipCodeFieldExist;
    }*/


    /**
     * Import a zip-code file if we have one
     *
     * @param $noDie
     * @return int $zipRowsAdded
     * @throws Exception
     */
    /*
    protected function createAndImportZipCodeFile($noDie)
    {
        global $transactionActive;
        $zipRowsAdded = 0;

        $haveZipRows = $this->checkZipRows();
        if ($haveZipRows == 0) {
            $fileName = ROOT_PATH.'/../UrbanRuralZips.txt';
            if (!file_exists($fileName)) {
                die("ZipCode input file '$fileName' not found");
            }
            $fh = fopen($fileName, 'r');
            if (!$fh) {
                die("Failed to open ZipCode input file '$fileName'");
            }

            $sql =
                "CREATE TABLE IF NOT EXISTS ZipCodes (\n".
                "`ZipCodeID` int(10) NOT NULL AUTO_INCREMENT,\n".
                "`ZipCode` varchar(20) DEFAULT NULL,\n".
                "`UrbanRural` varchar(10) DEFAULT NULL,\n".
                "`ValidFrom` datetime DEFAULT '0000-00-00 00:00:00',\n".
                "PRIMARY KEY (`ZipCodeID`),\n".
                "UNIQUE KEY `idxZipCodeValidFrom` (`ZipCode`,`ValidFrom`),\n".
                "KEY `idxZipCode` (`ZipCode`),\n".
                "KEY `idxValidFrom` (`ValidFrom`)\n".
                ") ENGINE=InnoDB DEFAULT CHARSET=latin1\n";
            $this->connection->RunQuery($sql);

            $this->connection->BeginTransaction();

            $success  = 0;
            $failures = 0;
            $insertOptions = array('wantException' => 1);
            while (($data = fgetcsv($fh, 1000, "\t")) !== false) {

                if ((isset($data[1])) && ($data[1]) && ($data[0] != 'Status')) {
                    $zipCode       = trim($data[0]);
                    $designation   = strtolower(trim($data[1]));
                    $quDesignation = $this->connection->Quote($designation);
                    $quZipCode     = $this->connection->Quote($zipCode);

                    //Omit a few that would be duplicates
                    if ($quDesignation == 'Rural') {
                        //Suppress certain rural rows that would lead to duplicates
                        if (strpos(',10464,', ",$quZipCode,") !== false) {
                            continue;
                        }
                    }

                    try {
                        $sql =
                            "INSERT INTO ZipCodes (ZipCode, UrbanRural, ValidFrom) VALUES (\n" .
                            "'$quZipCode', '$quDesignation', '0000-00-00 00:00:00')";
                        $this->connection->RunQuery($sql, $insertOptions);
                        $insertId = $this->connection->LastID();
                    } catch (\Exception $e) {
                        $insertId = 0;
                    }
                    if (!$insertId) {
                        $failures++;
                        print "<br />Duplicate ZipCode Entry: Failed to insert '$quZipCode' as '$quDesignation'";
                    } else {
                        $success++;
                    }
                }
            }//while
            fclose($fh);
            $this->connection->CommitTransaction();
            print "<br />Successfully inserted $success ZipCode rows";
            print "<br />Failed to insert $failures ZipCode rows<br />";

            if ($failures > 0) {
                $id = logSystemStatus('Failed to enter ' . $failures . " ZipCode rows", 1, 'warning', "Source=$fileName, success=$success failures=$failures");
            }
            $this->changeActions[] = "Imported $success ZipCode rows ($failures failures)";
            if ($success > 0) {
                $zipRowsAdded = 1;
            }
        }

        if ($failures) {
            exit;
        }
        return $zipRowsAdded;
    }*/


    /**
     * Create the SystemStatus table if it is missing
     * 
     * @param $noDie
     * @throws Exception
     */
    /*
    protected function createSystemStatusTable($noDie)
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $quTableName = 'SystemStatus';
        if ($this->checkTableStatus($quTableName) == 'missing') {
            $sql =
                "CREATE TABLE `$quTableName`(\n".
                "`systemstatusid` INT(14) UNSIGNED NOT NULL AUTO_INCREMENT,\n".
                "`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),\n".
                "`severity` int(5) NOT NULL,\n".
                "`rowtype` VARCHAR(50) NOT NULL,\n".
                "`message` VARCHAR(255) NOT NULL,\n".
                "`details` LONGTEXT,\n".
                "PRIMARY KEY (`systemstatusid`),\n".
                "KEY `idx_created` (`created`),\n".
                "KEY `idx_severity` (`severity`),\n".
                "KEY `idx_rowtype` (`rowtype`)\n".
                ") ENGINE=INNODB CHARSET=latin1 COLLATE=latin1_swedish_ci\n";
            $this->connection->RunQuery($sql);

            $id = logSystemStatus('First test', 0, '', 'test');
            if (!$id) {
                die("Failed test-insert into create the '$quTableName' table");
            }
            $this->changeActions[] = "Added $quTableName' table";
        }

        if ($this->checkTableStatus($quTableName) != 'exists') {
            die("Failed to create the '$quTableName' table");
        }
    }*/


    /**
     * Return a list of all MyIsam tables
     *
     * @return array $myIsamTables
     * @throws Exception
     */
    /*
    protected function findMyIsamTables()
    {
        $myIsamTables = array();

        $ourTables =
            ',Comments,Comp,Doctor,DoctorSiteJoin,DoctorSpecialty,Patient,PatientEthnicity'.
            ',PatientIdentifier,PatientRace,PatientSex,Permissions,Site,SiteTrialsOpenJoin'.
            ',States,SystemStatus,Trial,TrialCategory,TrialCompJoin,TrialCompanion'.
            ',TrialDiseaseType,TrialGroup,TrialPhase,TrialTrialDiseaseTypeJoin'.
            ',TrialType,Users,UsersType,ZipCodes,';

        $sql = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE engine = 'MyISAM'";
        $result = $this->connection->RunQuery($sql);
        $rows = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $tableName = $row['table_name'];
            if (strpos($ourTables, ",$tableName,") !==false) {
                $myIsamTables[] = $tableName;
            }
        }
        return $myIsamTables;
    }*/

    /**
     * Check if the ZipCodes table exists and has any rows in it
     *
     * @return int
     * @throws Exception
     */
    /*
    protected function checkZipRows()
    {
        $haveZipRows = 0;
        $tableHash = $this->getTableHash();
        if (isset($tableHash['ZipCodes'])) {
            $sql = "SELECT * FROM ZipCodes limit 10";
            $result = $this->connection->RunQuery($sql);
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                if (isset($row['ZipCode'])) {
                    $haveZipRows = 1;
                }
            }
        }
        return $haveZipRows;
    }*/


    /**
     * Function to check and update the Doctor table
     *
     * @access protected
     * @throws Exception
     */
    /*
    protected function checkDoctorTable()
    {
        $doctorRow = $this->fetchDoctorRow();

        //CHECK 1: Doctor.DoctorIsActive
        if (!isset($doctorRow['DoctorIsActive'])) {
            $sql = "ALTER TABLE `Doctor`\n".
                   "ADD COLUMN `DoctorIsActive` TINYINT(1) UNSIGNED DEFAULT 1 NOT NULL AFTER `DoctorEmail`,\n".
                   "ADD INDEX `idxDoctorIsActive` (`DoctorIsActive`)";
            $this->connection->RunQuery($sql);

            //CHECK 2: Doctor.DoctorIsActive    
            $doctorRow = $this->fetchDoctorRow();
            if (!isset($doctorRow['DoctorIsActive'])) {
                throw new \Exception('Failed to insert the Doctor.DoctorIsActive field!');
            } else {
                $this->changeActions[] = 'Added Doctor.DoctorIsActive column';
            }
        }
    }*/


    /**
     * Fetches one doctor row (the one with the highest DoctorKeyID)
     *
     * @access protected
     * @throws Exception
     */
    /*
    protected function fetchDoctorRow()
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $sql    = "SELECT *\n".
                  "FROM   `Doctor`\n".
                  "ORDER BY DoctorKeyID DESC\n".
                  "LIMIT 1";

        $result = $this->connection->RunQuery($sql);
        $doctorRow = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $doctorRow = $row;
        }
        if ((!isset($doctorRow['DoctorKeyID']))) {
            throw new \Exception("Cannot check schema on 'Doctor' table. No doctor row found!");
        }

        return $doctorRow;
    }*/


    /**
     * Fetches one patient row (the one with the highest PatientKeyID)
     *
     * @access protected
     * @throws Exception
     */
    /*
    protected function fetchPatientRow()
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $sql    =
            "SELECT *\n".
            "FROM   `Patient`\n".
            "ORDER BY PatientKeyID DESC\n".
            "LIMIT 1";

        $result = $this->connection->RunQuery($sql);
        $patientRow = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $patientRow = $row;
        }
        if ((!isset($patientRow['PatientKeyID']))) {
            throw new \Exception("Cannot check schema on 'Patient' table. No patient row found!");
        }

        return $patientRow;
    }*/


    /**
     * Function to check and update the Doctor table
     *
     * @access protected
     * @throws Exception
     */
    /*
    protected function checkTrialCategoryTable()
    {
        $tableExists = $this->doesTrialCategoryTableExist();

        //CHECK 1: Create TrialCategory table
        if (!$tableExists) {
            $sql = "ALTER TABLE `Trial` ADD COLUMN `TrialCategoryKeyID` INT(10) DEFAULT 1 NULL AFTER `TimestampUpdate`, ADD INDEX `idxTrialCategoryKeyID` (`TrialCategoryKeyID`)"; 
            $this->connection->RunQuery($sql);

            $sql = "CREATE TABLE `TrialCategory` (\n".
                   "             `TrialCategoryKeyID` int(10) NOT NULL AUTO_INCREMENT,\n".
                   "             `TrialCategoryText` varchar(250) NOT NULL,\n".
                   "PRIMARY KEY (`TrialCategoryKeyID`)\n".
                   ") ENGINE=MyISAM DEFAULT CHARSET=latin1";
            $this->connection->RunQuery($sql);

            $sql = "INSERT INTO `TrialCategory`(`TrialCategoryKeyID`,`TrialCategoryText`) VALUES (1,'None')";
            $this->connection->RunQuery($sql);

            $sql = "INSERT INTO `TrialCategory`(`TrialCategoryKeyID`,`TrialCategoryText`) VALUES (2,'Base Intervention')";
            $this->connection->RunQuery($sql);

            $sql = "INSERT INTO `TrialCategory`(`TrialCategoryKeyID`,`TrialCategoryText`) VALUES (3,'Specimen')";
            $this->connection->RunQuery($sql);

            $sql = "INSERT INTO `TrialCategory`(`TrialCategoryKeyID`,`TrialCategoryText`) VALUES (4,'Screening')";
            $this->connection->RunQuery($sql);

            $sql = "INSERT INTO `TrialCategory`(`TrialCategoryKeyID`,`TrialCategoryText`) VALUES (5,'DCP-001')";
            $this->connection->RunQuery($sql);

            //CHECK 2: Post-create check
            $tableExists = $this->doesTrialCategoryTableExist();
            if (!$tableExists) {
                throw new \Exception('Failed to create the TrialCategory table!');
            } else {
                $this->changeActions[] = 'Created the TrialCategory table';
                $this->changeActions[] = 'Added Trial.TrialCategoryKeyID colum to Trial table';
            }
        }
    }*/

    /**
     * Count the TrialCategory rows
     *
     * @access protected
     * @return int $tableExists             0/1
     * @throws Exception
     */
    /*
    protected function doesTrialCategoryTableExist()
    {
        $tableExists = 0;
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        try {
            $sql    = "SELECT count(*) as cnt FROM `TrialCategory`";
            $result = $this->connection->RunQuery($sql, array('wantException' => 1));
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $tableExists = 1;
        } catch(\Exception $e) {
            $tableExists = 0;
        }

        return $tableExists;
    }*/

    /**
     * Create the TrackChanges table if it is missing
     *
     * @param $noDie
     * @throws Exception
     */
    /*
    protected function createTrackChangesTable($noDie)
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $quTableName = 'TrackChanges';
        if ($this->checkTableStatus($quTableName) == 'missing') {
            $sql = <<< __END__
CREATE TABLE `TrackChanges` (
  `TrackChangesID` bigint(14) unsigned NOT NULL AUTO_INCREMENT,
  `EventID` varchar(100) NOT NULL,
  `EventMain` varchar(30) NOT NULL,
  `EventSub` varchar(10) DEFAULT NULL,
  `TableName` varchar(100) NOT NULL,
  `FieldName` varchar(100) NOT NULL,
  `BeforeShort` varchar(255) DEFAULT NULL,
  `BeforeLong` longtext,
  `AfterLong` longtext,
  `AfterShort` varchar(255) DEFAULT NULL,
  `TrackLocationId` bigint(14) unsigned DEFAULT NULL,
  `SessionUser` varchar(100) NOT NULL,
  `EventTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`TrackChangesID`),
  UNIQUE KEY `idx_eventid` (`EventID`),
  KEY `idx_tablename` (`TableName`),
  KEY `idx_fieldname` (`FieldName`),
  KEY `idx_sessionuser` (`SessionUser`),
  KEY `idx_eventtime` (`EventTime`),
  KEY `idx_tracklocationid` (`TrackLocationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

__END__;
            $this->connection->RunQuery($sql);

            $id = logSystemStatus('First test', 0, '', 'test');
            if (!$id) {
                die("Failed test-insert into create the '$quTableName' table");
            }
            $this->changeActions[] = "Added $quTableName' table";
        }

        if ($this->checkTableStatus($quTableName) != 'exists') {
            die("Failed to create the '$quTableName' table");
        }
    }*/

    /**
     * Create the TrackInfo table if it is missing
     *
     * @param $noDie
     * @throws Exception
     */
    protected function createTrackInfoTable($noDie)
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $quTableName = 'TrackInfo';
        if ($this->checkTableStatus($quTableName) == 'missing') {
            $sql = <<< __END__
CREATE TABLE `TrackInfo` (
  `TrackInfoID` bigint(14) unsigned NOT NULL AUTO_INCREMENT,
  `EventID` varchar(100) NOT NULL,
  `EventString` varchar(199) NOT NULL,
  `ParentTable` varchar(100) NOT NULL,
  `ParentID` int(14) NOT NULL,
  `ParentHint` varchar(50) NOT NULL,
  `TableName` varchar(100) NOT NULL,
  `NumChanges` int(14) NOT NULL,
  `ChangesMade` longtext NOT NULL,
  `SessionUser` varchar(100) NOT NULL,
  `EventTime` datetime DEFAULT NULL,
  PRIMARY KEY (`TrackInfoID`),
  KEY `idx_eventid` (`EventID`),
  KEY `idx_eventstring` (`EventString`),
  KEY `idx_parenttable` (`ParentTable`),
  KEY `idx_parentid` (`ParentID`),
  KEY `idx_parenttable_id` (`ParentTable`,`ParentID`),
  KEY `idx_tablename` (`TableName`),
  KEY `idx_sessionuser` (`SessionUser`),
  KEY `idx_eventtime` (`EventTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

__END__;
            $this->connection->RunQuery($sql);

            $id = logSystemStatus('First test', 0, '', 'test');
            if (!$id) {
                die("Failed test-insert into create the '$quTableName' table");
            }
            $this->changeActions[] = "Added $quTableName' table";
        }

        if ($this->checkTableStatus($quTableName) != 'exists') {
            die("Failed to create the '$quTableName' table");
        }
    }

    /**
     * Create the TrackLocations table if it is missing
     *
     * @param $noDie
     * @throws Exception
     */
    /*
    protected function createTrackLocationsTable($noDie)
    {
        if (!isset($this->connection)) {
            throw new \Exception('Cannot check schema. Have no database connection.');
        }

        $quTableName = 'TrackLocations';
        if ($this->checkTableStatus($quTableName) == 'missing') {
            $sql = <<< __END__
CREATE TABLE `TrackLocations` (
  `TrackLocationId` bigint(14) unsigned NOT NULL AUTO_INCREMENT,
  `TrackLocationFile` varchar(255) NOT NULL,
  `TrackLocationLine` int(14) unsigned DEFAULT NULL,
  PRIMARY KEY (`TrackLocationId`),
  KEY `idx_tracklocationfile` (`TrackLocationFile`),
  KEY `idx_tracklocationfile_line` (`TrackLocationFile`,`TrackLocationLine`),
  KEY `idx_tracklocationline` (`TrackLocationLine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

__END__;
            $this->connection->RunQuery($sql);

            $id = logSystemStatus('First test', 0, '', 'test');
            if (!$id) {
                die("Failed test-insert into create the '$quTableName' table");
            }
            $this->changeActions[] = "Added $quTableName' table";
        }

        if ($this->checkTableStatus($quTableName) != 'exists') {
            die("Failed to create the '$quTableName' table");
        }
    }*/


    /**
     * Get associative array of tables (BASE TABLE only!)
     *
     * @return array $tableHash
     * @throws Exception
     */
    protected function getTableHash()
    {
        $tableHash = array();

        $sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES";
        $result = $this->connection->RunQuery($sql);
        $rows = array();
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $rows[] = $row;
        }

        foreach($rows as $row) {
            $tableName = (isset($row['TABLE_NAME'])) ? $row['TABLE_NAME'] : '';
            $tableType = (isset($row['TABLE_TYPE'])) ? strtoupper($row['TABLE_TYPE']) : 'BASE TABLE';


            if (($tableName) && ($tableType == 'BASE TABLE')) {
                $tableHash[$tableName] = $row;
            }
        }

        return $tableHash;
    }


    /**
     * Check table status result: exist,missing,unknown,error
     *
     * @param $tableName
     * @return string $retVal
     */
    protected function checkTableStatus($tableName)
    {
        $retVal = 'unknown';
        try {
            $sql = "SHOW TABLES";
            $result = $this->connection->RunQuery($sql);
            $rows = array();
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $rows[] = $row;
            }
            if (count($rows)) {
                $allTablesString = strtoupper(json_encode($rows));
                if (strpos($allTablesString, strtoupper('"' . $tableName . '"')) !== false) {
                    $retVal = 'exists';
                } else {
                    $retVal = 'missing';
                }
            } else {
                $retVal = 'error';
            }
        } catch (\Exception $e) {
            $retVal = 'error';
        }

        return $retVal;
    }


    /**
     * Displays an error message
     *
     * @access protected
     * @param  string $error
     */
    protected function reportSchemaCheckProblem($error)
    {
        //for safety reasons we don't display the actual error message in production (can be temporarily disabled)
        $error = "DB-SCHEMA-CHECK ERROR: $error"; 

        print "<div class='errormessage' style='color:red'>".htmlentities($error)."</div><br /><br />";
    }

}


