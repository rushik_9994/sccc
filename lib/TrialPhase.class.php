<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialPhase extends BaseClass {

	public $TrialPhaseKeyID; //int(2)
	public $TrialPhaseText; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialPhase($TrialPhaseText){
		$this->TrialPhaseText = $TrialPhaseText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM TrialPhase WHERE TrialPhaseKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
		    $found = 1;
			$this->TrialPhaseKeyID = $row["TrialPhaseKeyID"];
			$this->TrialPhaseText = $row["TrialPhaseText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialPhase', 'TrialPhaseKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE TrialPhase SET\n".
                "TrialPhaseText = ".        $this->SqlQuote($this->TrialPhaseText)."\n".
                "WHERE TrialPhaseKeyID = ". $this->SqlQuote($this->TrialPhaseKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialPhase','TrialPhaseKeyID', $this->TrialPhaseKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO TrialPhase (TrialPhaseText) VALUES (\n".
                $this->SqlQuote($this->TrialPhaseText).')';
            $this->TrialPhaseKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialPhase','TrialPhaseKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $where
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysWhereOrderBy($where, $column, $order)
    {
		$keys = array();

		//$where not quoted! ++++++++
        $quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialPhaseKeyID FROM TrialPhase WHERE $where ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialPhaseKeyID"];
            $i++;
        }
    	return $keys;
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

		$quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialPhaseKeyID FROM TrialPhase ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialPhaseKeyID"];
            $i++;
        }
    	return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="trial[TrialPhaseKeyID]" id="TrialPhaseKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT TrialPhaseKeyID, TrialPhaseText from TrialPhase order by TrialPhaseKeyID");

        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialPhaseKeyID = htmlentities($row["TrialPhaseKeyID"]);
            $escTrialPhaseText  = htmlentities($row["TrialPhaseText"]);
            if ($row["TrialPhaseKeyID"] == $selected) {
                $select .= "<option value=\"$escTrialPhaseKeyID\" selected>$escTrialPhaseText</option>\n";
            } else {
                $select .= "<option value=\"$escTrialPhaseKeyID\">$escTrialPhaseText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $TrialPhaseKeyID
	 */
	public function getTrialPhaseKeyID()
    {
		return $this->TrialPhaseKeyID;
	}

	/**
	 * @return null|string $TrialPhaseText
	 */
	public function getTrialPhaseText()
    {
		return $this->TrialPhaseText;
	}

	/**
	 * @param int $TrialPhaseKeyID
	 */
	public function setTrialPhaseKeyID($TrialPhaseKeyID)
    {
		$this->TrialPhaseKeyID = $TrialPhaseKeyID;
	}

	/**
	 * @param string $TrialPhaseText
	 */
	public function setTrialPhaseText($TrialPhaseText)
    {
		$this->TrialPhaseText = $TrialPhaseText;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        $validationMessage = '';

        //Validate
        do {
            $val = ((isset($this->TrialPhaseKeyID)) ? $this->TrialPhaseKeyID : null);  //vld: TrialPhase.TrialPhaseKeyID
            if ($validationMessage = validateField($val,'TrialPhaseKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialPhaseText)) ? $this->TrialPhaseText : null);  //vld: TrialPhase.TrialPhaseText
            if ($validationMessage = validateField($val,'TrialPhaseText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}