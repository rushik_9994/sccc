<?php
/*
 * 
 * Create Date: 10-18-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/cfg/config.php');

/**
 * Patient class
 */
class Pagination {

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Calculates all pagination related variables
     *
     * @param  array $request                                Usually $_REQUEST
     * @param  string $configKey optional                    e.g. patients.pagination.size
     * @return array  $pgInfo
     */ 
    public function getPaginationVarsFromRequest($request=array(), $paginationSizeConfigKey='') {
        $pgInfo      = array();
        for ($i=0; $i < 3; $i++) {
            //FROM
            $fromId = 'from_'.$i;
            $pgInfo[$i]['from'] = ((isset($request['from_'.$i])) && (is_numeric($request['from_'.$i])) && ($request['from_'.$i]>0)) 
                ? $request['from_'.$i] : 0;
            if ((isset($request['search_filter_'.$i])) && ($request['search_filter_'.$i]=='Search')) {
                $pgInfo[$i]['from']       = 0;
            }

            //LIMIT
            $pgInfo[$i]['limit']          = $this->getPaginationSize($paginationSizeConfigKey);

            //ORDERFIELD
            $pgInfo[$i]['orderField']     = '';
            if (isset($request['order_field_'.$i])) {
                $pgInfo[$i]['orderField'] = $request['order_field_'.$i];
            }

            //ORDERDIRECTION
            $pgInfo[$i]['orderDirection'] = '';
            if (isset($request['order_direction_'.$i])) {
                $pgInfo[$i]['orderDirection'] = $request['order_direction_'.$i];
            }

            //ORDERLASTCLICKED
            $pgInfo['orderLastClicked'] = '';
            if (isset($request['order_last_clicked'])) {
                $pgInfo['orderLastClicked'] = trim($request['order_last_clicked'],'_');
            }

            //PAGINATIONEVENT
            $pgInfo[$i]['paginationEvent']     = '';
            if (isset($request['next_page_'.$i]))  {
                $pgInfo[$i]['from']      += $pgInfo[$i]['limit'];
                $pgInfo[$i]['paginationEent'] = 'next_page';
            }
            if (isset($request['prev_page_'.$i]))  {
                $pgInfo[$i]['from'] -= $pgInfo[$i]['limit'];
                if ($pgInfo[$i]['from'] < 0) { $pgInfo[$i]['from'] = 0; }
                $pgInfo[$i]['paginationEvent'] = 'prev_page';
            }
            if (isset($request['first_page_'.$i])) {

                $pgInfo[$i]['from']  = 0;
                $pgInfo[$i]['paginationEvent'] = 'first_page';
            }
        }

        return $pgInfo;
    }

    /**
     * Returns the number of rows per page (default 25)
     *
     * @param  string $configKey optional        e.g. patients.pagination.size
     * @return int    $limit
     */ 
    public function getPaginationSize($configKey='')
    {
        global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;

        //Set a hardcoded limit
        $limit = 25;

        //Use the default from the config
        if ((isset($cfg)) && (is_array($cfg)) && (isset($cfg['pagination.size']))) {
            $limit = $cfg['pagination.size'];
        }

        //Override the default with a specialized pagination size?
        if (($configKey) && (strpos($configKey,'pagination')!==false)) {
            if ((isset($cfg)) && (is_array($cfg)) && (isset($cfg[$configKey]))) {
                $limit = $cfg[$configKey];
            }
        }

        return $limit;
    }


    /**
     * Generates HTML code for pagination
     *
     * @param  array  $pgInfo                    pagination information
     * @param  int    $pgIndex optional          which pgInfo[pgIndex] to use...
     * @param  object $object optional           E.g. $patients
     * @return string $htmlCode
     */
    public function generatePaginationCode($pgInfo, $pgIndex=0, $object=null, $totalRowCount=null)
    {
        $code = '';

        if ((isset($object)) && 
            (isset($object->showPagination)) &&
            ($object->showPagination > 0) &&
            (isset($object->legendText)) &&
            (isset($object->showFirstButton)) &&
            (isset($object->showPreviousButton)) &&
            (isset($object->showNextButton))) {
            if ($pgInfo[$pgIndex]['limit'] > 0) {
                $totalRowCountText = ((isset($totalRowCount)) && (is_numeric($totalRowCount)))
                                         ?  " of $totalRowCount"  : '';
                $code .= "<br />\n".
                                  "<table border=\"0\" width=\"100%\">\n".
                                  "    <tr>\n".
                                  "        <td align=\"left\"  nowrap=\"nowrap\"><b>".$object->legendText.$totalRowCountText."</b></td>\n".
                                  "        <td align=\"right\" nowrap=\"nowrap\">\n";
                $pgIndexText = "_$pgIndex";
                $disabledAttrib = ($object->showFirstButton)  ?  ''  : 'disabled=disabled';
                $code          .=             "<input type=\"submit\" id=\"first_page$pgIndexText\" name=\"first_page$pgIndexText\" $disabledAttrib value=\"First\">&nbsp;&nbsp;&nbsp;";
                $disabledAttrib = ($object->showPreviousButton)  ?  ''  : 'disabled=disabled';
                $code          .=             "<input type=\"submit\" id=\"prev_page$pgIndexText\" name=\"prev_page$pgIndexText\" $disabledAttrib value=\"Previous\">&nbsp;&nbsp;&nbsp;";
                $disabledAttrib = ($object->showNextButton)      ?  ''  : 'disabled=disabled';
                $code          .=             "<input type=\"submit\" id=\"next_page$pgIndexText\" name=\"next_page$pgIndexText\" $disabledAttrib value=\"Next\">";
                $code          .= "        </td>\n".
                                  "    </tr>\n".
                                  "    </table>\n";
            }
        }

        return $code;
    }

    /**
     * Generates HTML code for pagination
     *
     * @param  array  $pgInfo
     * @return string $htmlCode
     */
    public function generateFormVariables($pgInfo) {
        $code = '';

        $fieldType             = 'hidden';
        $st                    = '';
        $debugMode             = 0;

        if (0) {
            $debugMode         = 1;  //Turn Debug Fields on (red)
            $fieldType         = 'text';
            $st                = ' style="background-color:red;color:white;"';
        }

        if ($debugMode) {
            $code .= "<table width='100%' style='background-color:white;padding:0px;border-spacing:0px;border-collapse:separate;' cellspacing=0 cellpadding=0>\n";
        }
        for ($i=0; $i < 3; $i++) {
            if ($debugMode) {
                $code .= "<tr>\n".
                         "    <td nowrap='nowrap'>from: <input type=\"$fieldType\" id=\"from_$i\"                name=\"from_$i\"            $st value=\"".$pgInfo[$i]['from']."\">&nbsp;&nbsp;</td>\n".
                         "    <td nowrap='nowrap'>limit: <input type=\"$fieldType\" id=\"limit_$i\"              name=\"limit_$i\"           $st value=\"".$pgInfo[$i]['limit']."\">&nbsp;&nbsp;</td>\n".
                         "    <td nowrap='nowrap'>orderFld: <input type=\"$fieldType\" id=\"order_field_$i\"     name=\"order_field_$i\"     $st value=\"".$pgInfo[$i]['orderField']."\">&nbsp;&nbsp;</td>\n".
                         "    <td nowrap='nowrap'>orderDir: <input type=\"$fieldType\" id=\"order_direction_$i\" name=\"order_direction_$i\" $st value=\"".$pgInfo[$i]['orderDirection']."\">&nbsp;&nbsp;</td>\n".
                         "</tr>\n";
            } else {
                $code .= 
                         "    <input type=\"$fieldType\" id=\"from_$i\"             name=\"from_$i\"            $st value=\"".$pgInfo[$i]['from']."\">\n".
                         "    <input type=\"$fieldType\" id=\"limit_$i\"            name=\"limit_$i\"           $st value=\"".$pgInfo[$i]['limit']."\">\n".
                         "    <input type=\"$fieldType\" id=\"order_field_$i\"      name=\"order_field_$i\"     $st value=\"".$pgInfo[$i]['orderField']."\">\n".
                         "    <input type=\"$fieldType\" id=\"order_direction_$i\"  name=\"order_direction_$i\" $st value=\"".$pgInfo[$i]['orderDirection']."\">\n";
            }
        }
        if ($debugMode) {
            $code .= "</table>\n";
        }

        if ($debugMode) {
            $code .= "    <br />Last clicked: <input type=\"$fieldType\" id=\"order_last_clicked\"  name=\"order_last_clicked\" $st value=\"".$pgInfo['orderLastClicked']."\">\n";
        } else {
            $code .= "    <input type=\"$fieldType\" id=\"order_last_clicked\"  name=\"order_last_clicked\" $st value=\"".$pgInfo['orderLastClicked']."\">\n";
        }

        return $code;
	}

    /**
     * Generate one table-header line (respecting sort selection)
     *
     * return string $htmlCode
     */
    public function generateTheadLine($id, $displayText, $options=array()) {
        $orderIndex      = substr($id,-2);
        $orderField     = (isset($_REQUEST['order_field'.$orderIndex]))     ? $_REQUEST['order_field'.$orderIndex] : '';
        $orderDirection = (isset($_REQUEST['order_direction'.$orderIndex])) ? $_REQUEST['order_direction'.$orderIndex] : '';

        $extra     = (isset($options['extra']))     ? ' '.$options['extra'] : ' ';
        $leftRight = (isset($options['right']))     ? 'right' : 'left';
        $noSort    = (isset($options['nosort']))    ? 1 : 0;
        $isDefault = (isset($options['isdefault'])) ? 1 : 0;

        if ((!isset($_REQUEST['order_field'.$orderIndex])) && ($isDefault)) {
            $orderField                          = $id;
            $_REQUEST['order_field'.$orderIndex] = $orderField;
        }
        if ((!isset($_REQUEST['order_direction'.$orderIndex])) && ($isDefault)) {
            $orderDirection                          = 'asc';
            $_REQUEST['order_direction'.$orderIndex] = $orderDirection;
        }

        if (!$noSort) {
            //Note: The actual sorting happens in the model classes. e.g. look for 'order_field_0' in Trial.class.php
            if ($orderField == $id) {
                if ($orderDirection=='desc') {
                    //$displayText .= '&nbsp;<b>&uarr;</b>';
                    $displayText   = "<div class='thead_selected' style='display:inline;' >$displayText".
                                     '<img src="css/images/arrow_sort_desc.gif" />'.
                                     '</div>';
                } else {
                    //$displayText .= '&nbsp;<b>&darr;</b>';
                    $displayText   = "<div class='thead_selected' style='display:inline;' >$displayText".
                                     '<img src="css/images/arrow_sort_asc.gif" />'.
                                     '</div>';
                }
            } else {
                //$displayText .= '&nbsp;&nbsp;';
                $displayText   = "<div class='thead_unselected' style='display:inline;' >$displayText".
                                 '<img src="css/images/arrow_sort_none.gif" />'.
                                 '</div>';
            }
        }
        $displayText .= '&nbsp;';
        $onClickText  = ($noSort) ? '' : " onclick='changeSortOrder(\"$id\")'";

        return "        <th name=\"$id\", id=\"$id\",$extra class=\"$leftRight\"$onClickText nowrap=\"nowrap\" valign='top'>$displayText</th>\n";
    }


}


