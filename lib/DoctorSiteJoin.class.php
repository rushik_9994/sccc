<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class DoctorSiteJoin extends BaseClass {

    public $DoctorSiteKeyID; //int(10)
    public $DoctorKeyID; //int(10)
    public $SiteKeyID; //int(10)
    public $DoctorSiteOpenDate; //datetime
    public $DoctorSiteClose; //tinyint(1)
    public $DoctorSiteCloseDate; //datetime


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_DoctorSiteJoin($DoctorKeyID,$SiteKeyID,$DoctorSiteOpenDate,$DoctorSiteCloseDate){
        $this->DoctorKeyID = $DoctorKeyID;
        $this->SiteKeyID = $SiteKeyID;
        $this->DoctorSiteOpenDate = $DoctorSiteOpenDate;
        $this->DoctorSiteClose = $DoctorSiteClose;
        $this->DoctorSiteCloseDate = $DoctorSiteCloseDate;
    }*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $result = $this->connection->RunQuery("SELECT * FROM DoctorSiteJoin WHERE DoctorSiteKeyID = ".$this->SqlQuote($key_row));

        $found =0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->DoctorSiteKeyID = $row["DoctorSiteKeyID"];
            $this->DoctorKeyID = $row["DoctorKeyID"];
            $this->SiteKeyID = $row["SiteKeyID"];
            $this->DoctorSiteOpenDate = $row["DoctorSiteOpenDate"];
            $this->DoctorSiteClose = $row["DoctorSiteClose"];
            $this->DoctorSiteCloseDate = $row["DoctorSiteCloseDate"];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param string $key
     * @param int $key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key, $key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'DoctorSiteJoin', $key, $key_row);
    }

    /**
     * Update the active row table on table
     * @throws \Exception
     */
    public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql  = "UPDATE DoctorSiteJoin SET\n".
                    "DoctorKeyID = ".          $this->SqlQuote($this->DoctorKeyID).",\n".
                    "SiteKeyID = ".            $this->SqlQuote($this->SiteKeyID).",\n".
                    "DoctorSiteOpenDate = ".   $this->SqlQuote($this->DoctorSiteOpenDate).",\n".
                    "DoctorSiteClose = ".      $this->SqlQuote($this->DoctorSiteClose).",\n".
                    "DoctorSiteCloseDate = ".  $this->SqlQuote($this->DoctorSiteCloseDate)."\n".
                    "WHERE DoctorSiteKeyID = ".$this->SqlQuote($this->DoctorSiteKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'DoctorSiteJoin','DoctorSiteKeyID', $this->DoctorSiteKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO DoctorSiteJoin (DoctorKeyID, SiteKeyID, DoctorSiteOpenDate, DoctorSiteClose, DoctorSiteCloseDate) VALUES (\n".
                $this->SqlQuote($this->DoctorKeyID).",\n".
                $this->SqlQuote($this->SiteKeyID).",\n".
                $this->SqlQuote($this->DoctorSiteOpenDate).",\n".
                $this->SqlQuote($this->DoctorSiteClose).",\n".
                $this->SqlQuote($this->DoctorSiteCloseDate).')';
            $this->DoctorSiteKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'DoctorSiteJoin','DoctorSiteKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Returns array of keys with where key = $key order by $column -> name of column $order -> desc or acs
     *
     * @param string $key
     * @param string $whereTarget
     * @param string|int $targetVal
     * @param string $column
     * @param string $order
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysWhereDoctorKeyIdOrderBy($key, $whereTarget, $targetVal, $column, $order, $sClauses=array())
    {

        return $this->GetKeysWhereOrderBy($key, $quWhereTarget, $quTargetVal, $column, $order, $sClauses=array());
    }

    /**
     * Returns array of keys with where key = $key order by $column -> name of column $order -> desc or acs
     *
     * @param string $key
     * @param string $where
     * @param string $column
     * @param string $order
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysWhereOrderBy($key, $where, $column, $order, $sClauses=array())
    {
        $keys = array(); $i = 0;

        if (count($sClauses) == 0) {
            $sql = "SELECT $key\n".
                   "FROM   DoctorSiteJoin\n";
        } else {
            $sql = "SELECT DoctorSiteJoin.$key\n".
                   "FROM   DoctorSiteJoin\n";
        }

        //Support table column sorting 'order_field_0' (thead)...
        $joins          = '';
        $orderBy        = "ORDER BY $column $order";
        $orderIndex     = (isset($sClauses['order_index']))                 ? $sClauses['order_index'] : '_0';
        if ((isset($sClauses['order_last_clicked'])) && ($sClauses['order_last_clicked'])) {
            $orderIndex = $sClauses['order_last_clicked'];
        }
        $orderField     = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }

        if ($orderField) {
            if ($orderField == 'order_doctors'.$orderIndex) {
                $joins   = "LEFT JOIN Doctor ON DoctorSiteJoin.DoctorKeyID=Doctor.DoctorKeyID\n";
                $orderBy = "ORDER BY CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) $orderDirection";
            } else if ($orderField == 'order_specialty'.$orderIndex) {
                $joins   = "LEFT JOIN Doctor ON DoctorSiteJoin.DoctorKeyID=Doctor.DoctorKeyID\n".
                           "LEFT JOIN DoctorSpecialty ON DoctorSpecialty.DoctorSpecialtyKeyID=Doctor.DoctorSpecialty\n";
                $orderBy = "ORDER BY DoctorSpecialty.DoctorSpecialtyText $orderDirection, CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) ASC";
            } else if ($orderField == 'order_email'.$orderIndex) {
                $joins   = "LEFT JOIN Doctor ON DoctorSiteJoin.DoctorKeyID=Doctor.DoctorKeyID\n";
                $orderBy = "ORDER BY Doctor.DoctorEmail $orderDirection, CONCAT(Doctor.DoctorLastName, Doctor.DoctorFirstName) ASC";

            //From doctorViewEdit.php
            } else if ($orderField == 'order_sites'.$orderIndex) {
                $joins   = "LEFT JOIN Site ON DoctorSiteJoin.SiteKeyID=Site.SiteKeyID\n";
                $orderBy = "ORDER BY Site.SiteName $orderDirection";
            } else if ($orderField == 'order_site_code'.$orderIndex) {
                $joins   = "LEFT JOIN Site ON DoctorSiteJoin.SiteKeyID=Site.SiteKeyID\n";
                $orderBy = "ORDER BY Site.SiteCode $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_site_phone'.$orderIndex) {
                $joins   = "LEFT JOIN Site ON DoctorSiteJoin.SiteKeyID=Site.SiteKeyID\n";
                $orderBy = "ORDER BY Site.SitePhone $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_site_join_date'.$orderIndex) {
                $joins   = "LEFT JOIN Site ON DoctorSiteJoin.SiteKeyID=Site.SiteKeyID\n";
                $orderBy = "ORDER BY DoctorSiteJoin.DoctorSiteOpenDate $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_site_close_date'.$orderIndex) {
                $joins   = "LEFT JOIN Site ON DoctorSiteJoin.SiteKeyID=Site.SiteKeyID\n";
                $orderBy = "ORDER BY DoctorSiteJoin.DoctorSiteCloseDate $orderDirection, Site.SiteName ASC";
            }
        }
        $sql .= $joins.
                "WHERE  DoctorSiteJoin.$where\n".
                $orderBy;

        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["$key"];
            $i++;
        }

        return $keys;
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($column, $order)
    {
        $keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

        $i = 0;
        $result = $this->connection->RunQuery("SELECT DoctorSiteKeyID FROM DoctorSiteJoin ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["DoctorSiteKeyID"];
            $i++;
        }
        return $keys;
    }

    /**
     * @return null|int $DoctorSiteKeyID
     */
    public function getDoctorSiteKeyID()
    {
        return $this->DoctorSiteKeyID;
    }

    /**
     * @return null|int $DoctorKeyID
     */
    public function getDoctorKeyID()
    {
        return $this->DoctorKeyID;
    }

    /**
     * @return null|int $SiteKeyID
     */
    public function getSiteKeyID()
    {
        return $this->SiteKeyID;
    }

    /**
     * @return null|string $DoctorSiteOpenDate
     */
    public function getDoctorSiteOpenDate()
    {
        return $this->DoctorSiteOpenDate;
    }

    /**
     * @return null|string $DoctorSiteCloseDate
     */
    public function getDoctorSiteCloseDate()
    {
        return $this->DoctorSiteCloseDate;
    }

    /**
     * @param int $DoctorSiteKeyID
     */
    public function setDoctorSiteKeyID($DoctorSiteKeyID)
    {
        $this->DoctorSiteKeyID = $DoctorSiteKeyID;
    }

    /**
     * @param int $DoctorKeyID
     */
    public function setDoctorKeyID($DoctorKeyID)
    {
        $this->DoctorKeyID = $DoctorKeyID;
    }

    /**
     * @param int $SiteKeyID
     */
    public function setSiteKeyID($SiteKeyID)
    {
        $this->SiteKeyID = $SiteKeyID;
    }

    /**
     * @param string $DoctorSiteOpenDate
     */
    public function setDoctorSiteOpenDate($DoctorSiteOpenDate)
    {
        $this->DoctorSiteOpenDate = $DoctorSiteOpenDate;
    }

    /**
     * @param string $DoctorSiteCloseDate
     */
    public function setDoctorSiteCloseDate($DoctorSiteCloseDate)
    {
        $this->DoctorSiteCloseDate = $DoctorSiteCloseDate;
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->DoctorSiteKeyID)) ? $this->DoctorSiteKeyID : null);  //vld: DoctorSiteJoin.DoctorSiteKeyID
            if ($validationMessage = validateField($val,'DoctorSiteKeyID','nullok;positiveint',__CLASS__)) { break; } //----!!! zero?

            $val = ((isset($this->DoctorKeyID)) ? $this->DoctorKeyID : null);  //vld: DoctorSiteJoin.DoctorKeyID
            if ($validationMessage = validateField($val,'DoctorKeyID','zeroorpositiveint',__CLASS__)) { break; } //----!!! zero?

            $val = ((isset($this->SiteKeyID)) ? $this->SiteKeyID : null);  //vld: DoctorSiteJoin.SiteKeyID
            if ($validationMessage = validateField($val,'SiteKeyID','zeroorpositiveint',__CLASS__)) { break; } //----!!! zero?

            $val = ((isset($this->DoctorSiteOpenDate)) ? $this->DoctorSiteOpenDate : null);  //vld: DoctorSiteJoin.DoctorSiteOpenDate
            if ($validationMessage = validateField($val,'DoctorSiteOpenDate','date',__CLASS__)) { break; } //----!!! zero?

            $val = ((isset($this->DoctorSiteClose)) ? $this->DoctorSiteClose : null);  //vld: DoctorSiteJoin.DoctorSiteClose
            if ($validationMessage = validateField($val,'DoctorSiteClose', 'nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->DoctorSiteCloseDate)) ? $this->DoctorSiteCloseDate : null);  //vld: DoctorSiteJoin.DoctorSiteCloseDate
            if ($validationMessage = validateField($val,'DoctorSiteCloseDate','nullok;zerodate;date',__CLASS__)) { break; } //----!!! zero?

        } while(false);

        return $validationMessage;
    }

}

