<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));


class SiteTrialsOpenJoin extends BaseClass {

    public $SiteTrialOpenKeyID; //int(10)
    public $SiteKeyID; //int(10)
    public $TrialKeyID; //int(10)
    public $SiteTrialOpenApprovalDate; //datetime
    public $SiteTrialOpenCloseDate; //datetime
    public $SiteTrialOpenClose; //varchar(250)
    public $SiteTrialOpenTerminationDate; //datetime
    public $SiteTrialOpenTermination; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_SiteTrialsOpenJoin($SiteKeyID,$TrialKeyID,$SiteTrialOpenApprovalDate,$SiteTrialOpenCloseDate,$SiteTrialOpenClose,$SiteTrialOpenTerminationDate,$SiteTrialOpenTermination){
        $this->SiteKeyID = $SiteKeyID;
        $this->TrialKeyID = $TrialKeyID;
        $this->SiteTrialOpenApprovalDate = $SiteTrialOpenApprovalDate;
        $this->SiteTrialOpenCloseDate = $SiteTrialOpenCloseDate;
        $this->SiteTrialOpenClose = $SiteTrialOpenClose;
        $this->SiteTrialOpenTerminationDate = $SiteTrialOpenTerminationDate;
        $this->SiteTrialOpenTermination = $SiteTrialOpenTermination;
    }*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $result = $this->connection->RunQuery("SELECT * FROM SiteTrialsOpenJoin WHERE SiteTrialOpenKeyID = ". $this->SqlQuote($key_row));

        $found = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->SiteTrialOpenKeyID = $row["SiteTrialOpenKeyID"];
            $this->SiteKeyID = $row["SiteKeyID"];
            $this->TrialKeyID = $row["TrialKeyID"];
            $this->SiteTrialOpenApprovalDate = $row["SiteTrialOpenApprovalDate"];
            $this->SiteTrialOpenCloseDate = $row["SiteTrialOpenCloseDate"];
            $this->SiteTrialOpenClose = $row["SiteTrialOpenClose"];
            $this->SiteTrialOpenTerminationDate = $row["SiteTrialOpenTerminationDate"];
            $this->SiteTrialOpenTermination = $row["SiteTrialOpenTermination"];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
    }

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $trial_key
     * @param int $site_key
     * @throws \Exception
     */
    public function Load_from_trial_and_site_id($trial_key, $site_key)
    {
        $quTrialKey = trim($this->connection->SqlQuote($trial_key), "'");
        $quSiteKey  = trim($this->connection->SqlQuote($site_key), "'");

        $result = $this->connection->RunQuery("SELECT * FROM SiteTrialsOpenJoin WHERE TrialKeyID = \"$quTrialKey\" AND SiteKeyID = \"$quSiteKey\"");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $this->SiteTrialOpenKeyID = $row["SiteTrialOpenKeyID"];
            $this->SiteKeyID = $row["SiteKeyID"];
            $this->TrialKeyID = $row["TrialKeyID"];
            $this->SiteTrialOpenApprovalDate = $row["SiteTrialOpenApprovalDate"];
            $this->SiteTrialOpenCloseDate = $row["SiteTrialOpenCloseDate"];
            $this->SiteTrialOpenClose = $row["SiteTrialOpenClose"];
            $this->SiteTrialOpenTerminationDate = $row["SiteTrialOpenTerminationDate"];
            $this->SiteTrialOpenTermination = $row["SiteTrialOpenTermination"];
        }
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param string $key
     * @param int $key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key, $key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'SiteTrialsOpenJoin', $key, $key_row);
    }

    /**
     * Update the active row table on table
     * @throws \Exception
     */
    public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE SiteTrialsOpenJoin SET\n".
                "SiteKeyID = ".                      $this->SqlQuote($this->SiteKeyID).",\n".
                "TrialKeyID = ".                     $this->SqlQuote($this->TrialKeyID).",\n".
                "SiteTrialOpenApprovalDate = ".      $this->SqlQuote($this->SiteTrialOpenApprovalDate).",\n".
                "SiteTrialOpenCloseDate = ".         $this->SqlQuote($this->SiteTrialOpenCloseDate).",\n".
                "SiteTrialOpenClose = ".             $this->SqlQuote($this->SiteTrialOpenClose).",\n".
                "SiteTrialOpenTerminationDate = ".   $this->SqlQuote($this->SiteTrialOpenTerminationDate).",\n".
                "SiteTrialOpenTermination = ".       $this->SqlQuote($this->SiteTrialOpenTermination)."\n".
                "WHERE SiteTrialOpenKeyID = ".   $this->SqlQuote($this->SiteTrialOpenKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'SiteTrialsOpenJoin','SiteTrialOpenKeyID', $this->SiteTrialOpenKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO SiteTrialsOpenJoin (SiteKeyID, TrialKeyID, SiteTrialOpenApprovalDate, SiteTrialOpenCloseDate, SiteTrialOpenClose, SiteTrialOpenTerminationDate, SiteTrialOpenTermination) VALUES (\n".
                $this->SqlQuote($this->SiteKeyID).",\n".
                $this->SqlQuote($this->TrialKeyID).",\n".
                $this->SqlQuote($this->SiteTrialOpenApprovalDate).",\n".
                $this->SqlQuote($this->SiteTrialOpenCloseDate).",\n".
                $this->SqlQuote($this->SiteTrialOpenClose).",\n".
                $this->SqlQuote($this->SiteTrialOpenTerminationDate).",\n".
                $this->SqlQuote($this->SiteTrialOpenTermination).')';
            $this->SiteTrialOpenKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'SiteTrialsOpenJoin','SiteTrialOpenKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($column, $order)
    {
        $keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

        $i = 0;
        $result = $this->connection->RunQuery("SELECT SiteTrialOpenKeyID FROM SiteTrialsOpenJoin ORDER BY $quColumn $quOrder");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["SiteTrialOpenKeyID"];
            $i++;
        }
        return $keys;
    }


    /**
     * Returns array of keys with where key = $key order by $column -> name of column $order -> desc or acs
     *
     * @param string $key
     * @param string $where
     * @param string $column
     * @param string $order
     * @param array $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysWhereOrderBy($key, $where, $column, $order, $sClauses=array())
    {
        $keys = array(); $i = 0;

        if (count($sClauses)) {
            //The call from trialViewEdit (with sClauses)
            $sql  = "SELECT SiteTrialsOpenJoin.$key\n".
                    "FROM   SiteTrialsOpenJoin\n";
        } else {
            //No change to not affect any other code other than the trialViewEdit call (without sClauses)
            $sql  = "SELECT $key\n".
                    "FROM   SiteTrialsOpenJoin\n";
        }

        //Support table column sorting 'order_field_0' (thead)...
        $joins          = '';
        $orderBy        = "ORDER BY $column $order";
        $orderIndex     = (isset($sClauses['order_index']))       ? $sClauses['order_index'] : '_0';
        if ((isset($sClauses['order_last_clicked'])) && ($sClauses['order_last_clicked'])) {
            $orderIndex = $sClauses['order_last_clicked'];
        }
        $orderField     = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }

        if ($orderField) {
            if ($orderField == 'order_site_name'.$orderIndex) {
                $joins   = "LEFT JOIN Site ON SiteTrialsOpenJoin.SiteKeyID=Site.SiteKeyID\n";
                $orderBy = "ORDER BY Site.SiteName $orderDirection";
            } else if ($orderField == 'order_approval_date'.$orderIndex) {
                $orderBy = "ORDER BY SiteTrialsOpenJoin.SiteTrialOpenApprovalDate $orderDirection";
            } else if ($orderField == 'order_close_date'.$orderIndex) {
                $orderBy = "ORDER BY SiteTrialsOpenJoin.SiteTrialOpenCloseDate $orderDirection";
            } else if ($orderField == 'order_termination_date'.$orderIndex) {
                $orderBy = "ORDER BY SiteTrialsOpenJoin.SiteTrialOpenTerminationDate $orderDirection";
            //from siteViewEdit.php
            } else if ($orderField == 'order_trial_protocol'.$orderIndex) {
                $joins   = "LEFT JOIN Trial ON SiteTrialsOpenJoin.TrialKeyID=Trial.TrialKeyID\n";
                $orderBy = "ORDER BY Trial.TrialProtocolNumber $orderDirection";
            } else if ($orderField == 'order_recent_renewal_date'.$orderIndex) {
                $orderBy = "ORDER BY SiteTrialsOpenJoin.SiteTrialOpenApprovalDate $orderDirection";
            } else if ($orderField == 'order_close_date'.$orderIndex) {
                $orderBy = "ORDER BY SiteTrialsOpenJoin.SiteTrialOpenCloseDate $orderDirection";
            } else if ($orderField == 'order_term_date'.$orderIndex) {
                $orderBy = "ORDER BY SiteTrialsOpenJoin.SiteTrialOpenTerminationDate $orderDirection";
            }
        }
        $sql .= $joins.
                "WHERE  $where\n".
                $orderBy;

        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row[$key];
            $i++;
        }
        return $keys;
    }

    // /**
    //  * @return getSiteTrialOpenKeyByTrialAndSite - int(10)
    //  */
    // public function getSiteTrialOpenKeyByTrialAndSite($trial_key, $site_key){
    //  $keys = array(); $i = 0;
    //  $result = $this->connection->RunQuery("SELECT SiteTrialOpenKeyID from SiteTrialsOpenJoin WHERE TrialKeyID = $trial_key AND SiteKeyID = $site_key");
    //      while($row = $result->fetch_array(MYSQLI_ASSOC)){
    //          $keys[$i] = $row[$key];
    //          $i++;
    //      }
    // return $keys;
    // }

    /**
     * @return null|int $SiteTrialOpenKeyID
     */
    public function getSiteTrialOpenKeyID()
    {
        return $this->SiteTrialOpenKeyID;
    }

    /**
     * @return null|int $SiteKeyID
     */
    public function getSiteKeyID()
    {
        return $this->SiteKeyID;
    }

    /**
     * @return null|int $TrialKeyID
     */
    public function getTrialKeyID()
    {
        return $this->TrialKeyID;
    }

    /**
     * @return null|string $SiteTrialOpenApprovalDate
     */
    public function getSiteTrialOpenApprovalDate()
    {
        return $this->SiteTrialOpenApprovalDate;
    }

    /**
     * @return null|string $SiteTrialOpenCloseDate
     */
    public function getSiteTrialOpenCloseDate()
    {
        return $this->SiteTrialOpenCloseDate;
    }

    /**
     * @return null|string $SiteTrialOpenClose
     */
    public function getSiteTrialOpenClose()
    {
        return $this->SiteTrialOpenClose;
    }

    /**
     * @return null|string $SiteTrialOpenTerminationDate
     */
    public function getSiteTrialOpenTerminationDate()
    {
        return $this->SiteTrialOpenTerminationDate;
    }

    /**
     * @return null|string $SiteTrialOpenTermination
     */
    public function getSiteTrialOpenTermination()
    {
        return $this->SiteTrialOpenTermination;
    }

    /**
     * @param int $SiteTrialOpenKeyID
     */
    public function setSiteTrialOpenKeyID($SiteTrialOpenKeyID)
    {
        $this->SiteTrialOpenKeyID = $SiteTrialOpenKeyID;
    }

    /**
     * @param int $SiteKeyID
     */
    public function setSiteKeyID($SiteKeyID)
    {
        $this->SiteKeyID = $SiteKeyID;
    }

    /**
     * @param int $TrialKeyID
     */
    public function setTrialKeyID($TrialKeyID)
    {
        $this->TrialKeyID = $TrialKeyID;
    }

    /**
     * @param string $SiteTrialOpenApprovalDate
     */
    public function setSiteTrialOpenApprovalDate($SiteTrialOpenApprovalDate)
    {
        $this->SiteTrialOpenApprovalDate = $SiteTrialOpenApprovalDate;
    }

    /**
     * @param string $SiteTrialOpenCloseDate
     */
    public function setSiteTrialOpenCloseDate($SiteTrialOpenCloseDate)
    {
        $this->SiteTrialOpenCloseDate = $SiteTrialOpenCloseDate;
    }

    /**
     * @param string $SiteTrialOpenClose
     */
    public function setSiteTrialOpenClose($SiteTrialOpenClose)
    {
        $this->SiteTrialOpenClose = $SiteTrialOpenClose;
    }

    /**
     * @param string $SiteTrialOpenTerminationDate
     */
    public function setSiteTrialOpenTerminationDate($SiteTrialOpenTerminationDate)
    {
        $this->SiteTrialOpenTerminationDate = $SiteTrialOpenTerminationDate;
    }

    /**
     * @param string $SiteTrialOpenTermination
     */
    public function setSiteTrialOpenTermination($SiteTrialOpenTermination)
    {
        $this->SiteTrialOpenTermination = $SiteTrialOpenTermination;
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->SiteTrialOpenKeyID)) ? $this->SiteTrialOpenKeyID : null);  //vld: SiteTrialsOpenJoin.SiteTrialOpenKeyID
            if ($validationMessage = validateField($val,'SiteTrialOpenKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->SiteKeyID)) ? $this->SiteKeyID : null);  //vld: SiteTrialsOpenJoin.SiteKeyID
            if ($validationMessage = validateField($val,'SiteKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyID)) ? $this->TrialKeyID : null);  //vld: SiteTrialsOpenJoin.TrialKeyID
            if ($validationMessage = validateField($val,'TrialKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->SiteTrialOpenApprovalDate)) ? $this->SiteTrialOpenApprovalDate : null);  //vld: SiteTrialsOpenJoin.SiteTrialOpenApprovalDate
            if ($validationMessage = validateField($val,'SiteTrialOpenApprovalDate','zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->SiteTrialOpenClose)) ? $this->SiteTrialOpenClose : null);  //vld: SiteTrialsOpenJoin.SiteTrialOpenClose
            if ($validationMessage = validateField($val,'SiteTrialOpenClose','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->SiteTrialOpenTerminationDate)) ? $this->SiteTrialOpenTerminationDate : null);  //vld: SiteTrialsOpenJoin.SiteTrialOpenTerminationDate
            if ($validationMessage = validateField($val,'SiteTrialOpenTerminationDate','nullok;zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->SiteTrialOpenTermination)) ? $this->SiteTrialOpenTermination : null);  //vld: SiteTrialsOpenJoin.SiteTrialOpenTermination
            if ($validationMessage = validateField($val,'SiteTrialOpenTermination','nullok;0or1',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

