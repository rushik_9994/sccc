<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class DoctorSpecialty extends BaseClass {

	public $DoctorSpecialtyKeyID; //int(2)
	public $DoctorSpecialtyText; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_DoctorSpecialty($DoctorSpecialtyText){
		$this->DoctorSpecialtyText = $DoctorSpecialtyText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM DoctorSpecialty WHERE DoctorSpecialtyKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->DoctorSpecialtyKeyID = $row["DoctorSpecialtyKeyID"];
			$this->DoctorSpecialtyText = $row["DoctorSpecialtyText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'DoctorSpecialty', 'DoctorSpecialtyKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE DoctorSpecialty SET\n".
                "DoctorSpecialtyText = ".        $this->SqlQuote($this->DoctorSpecialtyText)."\n".
                "WHERE DoctorSpecialtyKeyID = ". $this->SqlQuote($this->DoctorSpecialtyKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'DoctorSpecialty','DoctorSpecialtyKeyID', $this->DoctorSpecialtyKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO DoctorSpecialty (DoctorSpecialtyText) VALUES (\n".
                $this->SqlQuote($this->DoctorSpecialtyText).')';
            $this->DoctorSpecialtyKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'DoctorSpecialty','DoctorSpecialtyKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT DoctorSpecialtyKeyID FROM DoctorSpecialty ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["DoctorSpecialtyKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="doctor[DoctorSpecialty]" id="DoctorSpecialtyKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT DoctorSpecialtyKeyID, DoctorSpecialtyText FROM DoctorSpecialty ORDER BY DoctorSpecialtyKeyID");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escDoctorSpecialtyKeyID = htmlentities($row["DoctorSpecialtyKeyID"]);
            $escDoctorSpecialtyText  = htmlentities($row["DoctorSpecialtyText"]);
            if ($row["DoctorSpecialtyKeyID"] == $selected) {
                 $select .= "<option value=\"$escDoctorSpecialtyKeyID\" selected>$escDoctorSpecialtyText</option>\n";
            } else {
                $select .= "<option value=\"$escDoctorSpecialtyKeyID\">$escDoctorSpecialtyText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $DoctorSpecialtyKeyID
	 */
	public function getDoctorSpecialtyKeyID()
    {
		return $this->DoctorSpecialtyKeyID;
	}

	/**
	 * @return null|string $DoctorSpecialtyText
	 */
	public function getDoctorSpecialtyText()
    {
		return $this->DoctorSpecialtyText;
	}

	/**
	 * @param int $DoctorSpecialtyKeyID
	 */
	public function setDoctorSpecialtyKeyID($DoctorSpecialtyKeyID)
    {
		$this->DoctorSpecialtyKeyID = $DoctorSpecialtyKeyID;
	}

	/**
	 * @param string $DoctorSpecialtyText
	 */
	public function setDoctorSpecialtyText($DoctorSpecialtyText)
    {
		$this->DoctorSpecialtyText = $DoctorSpecialtyText;
	}


    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->DoctorSpecialtyKeyID)) ? $this->DoctorSpecialtyKeyID : null);  //vld: DoctorSpecialty.DoctorSpecialtyKeyID
            if ($validationMessage = validateField($val,'DoctorSpecialtyKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->DoctorSpecialtyText)) ? $this->DoctorSpecialtyText : null);  //vld: DoctorSpecialty.DoctorSpecialtyText
            if ($validationMessage = validateField($val,'DoctorSpecialtyText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

