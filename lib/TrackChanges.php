<?php
/**
 * A class to track changes
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/DataBaseMysql.class.php');
require_once(ROOT_PATH.'/cfg/config.php');



/**
 * Class TrackChanges
 */
class TrackChanges
{

    /**
     * @var string
     */
    static protected $errorBefore = '';

    /**
     * @var string
     */
    static protected $errorAfter = '';

    /**
     * @var array
     */
    static protected $rowBefore = array();

    /**
     * @var array
     */
    static protected $rowAfter = array();

    /**
     * This supports a more big-picture overview
     * @var array
     */
    static protected $eventInfo = array();

    /**
     * This is not actually used
     * @var array
     */
    static protected $eventData = array();


    /**
     * Set the context for the following changes (if any)
     * @param string $fileName
     * @param int $lineNumber
     * @param string $eventString
     * @param string $action
     * @param array $post optional
     */
    static public function startUserAction($fileName, $lineNumber, $eventString, $action, $post=array())
    {
        $connection = new DataBaseMysql();

        //Calculate the eventString
        $eventString = str_replace('.php', '', basename($fileName)).'('.$lineNumber.')_'.$action;

        //VARIATION 1:  One row per changed field
        self::$eventData = array();
        self::$eventData['#event']['eventMain']   = '#'.$eventString;
        self::$eventData['#event']['eventSub']    = '#';
        self::$eventData['#event']['eventId']     = self::createQuEventId();
        self::$eventData['#event']['user']        = $_SESSION['UserName'];
        self::$eventData['#event']['tableName']   = '';
        self::$eventData['#event']['before']      = '';
        self::$eventData['#event']['after']       = '';
        self::$eventData['#event']['parentTable'] = '';
        self::$eventData['#event']['parentId']    = '';
        self::$eventData['#event']['file']        = $fileName;
        self::$eventData['#event']['line']        = $lineNumber;

        //VARIATION 2: One row per event with field-changes in a sub-data tree
        self::$eventInfo = array();
        self::$eventInfo['event'] = array(
            'user'                   => $_SESSION['UserName'],
            'eventString'            => $eventString,
            'action'                 => $action,
            'eventId'                => self::createQuEventId(),
            'file'                   => $fileName,
            'line'                   => $lineNumber,
        );
        self::$eventInfo['changes'] = array();
    }

    static public function endUserAction()
    {
        $connection = new DataBaseMysql();

        $parentHint = (isset(self::$eventInfo['event']['parentHint'])) ? self::$eventInfo['event']['parentHint'] : '';

        //------- variation 2
        //Write TrackInfo records
        if ((isset(self::$eventInfo['changes'])) && (count(self::$eventInfo['changes']))) {
            foreach (self::$eventInfo['changes'] as $parentTable => $parentDetails) {
                foreach($parentDetails as $parentId => $details) {
                    foreach($details as $tableName => $changesMade) {
                        if (defined('JSON_PRETTY_PRINT')) {
                            $jsonChangesMade = json_encode($changesMade, JSON_PRETTY_PRINT);
                        } else {
                            $jsonChangesMade = json_encode($changesMade);
                        }
                        $countChangesMade = count($changesMade);
                        $jsonChangesMade  = join("\r\n", $changesMade);

                        //print "<br />parentTable=$parentTable parentId=$parentId tableName=$tableName changes=".json_encode($changesMade);
                        $sql =
                            "INSERT INTO TrackInfo (EventID, EventString, ParentTable, ParentID, ParentHint, TableName, NumChanges, ChangesMade, SessionUser, EventTime) VALUES \n".
                            "(".
                            $connection->SqlQuote(self::$eventInfo['event']['eventId']).",\n".
                            $connection->SqlQuote(self::$eventInfo['event']['eventString']).",\n".
                            $connection->SqlQuote($parentTable).",\n".
                            $connection->SqlQuote($parentId).",\n".
                            $connection->SqlQuote($parentHint).",\n".
                            $connection->SqlQuote($tableName).",\n".
                            "$countChangesMade,\n".
                            $connection->SqlQuote($jsonChangesMade).",\n".
                            $connection->SqlQuote(self::$eventInfo['event']['user']).",\n".
                            "NOW())";
                        try {
                            $connection->conn->query($sql);
                        } catch (\Exception $e) {
                        }//try
                    }//foreach $details
                }//foreach $parentDetails
            }//foreach changes
        }
        //print "<pre>"; print_r(self::$eventInfo); exit;
        //print "<pre>"; print_r(self::$eventData); exit;
        //------- variation 2
    }


    /**
     * Called before a delete
     *
     * @param DataBaseMysql $connection
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $primeKeyValue
     */
    static public function trackBeforeDelete($connection, $tableName, $fieldName, $primeKeyValue)
    {
        list(self::$rowBefore, self::$errorBefore) = self::loadByKey($connection, $tableName, $fieldName, $primeKeyValue);
    }

    /**
     * Called after a delete
     *
     * @param DataBaseMysql $connection
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $primeKeyValue optional
     */
    static public function trackAfterDelete($connection, $tableName, $fieldName, $primeKeyValue=0)
    {
        list(self::$rowAfter, self::$errorAfter) = self::loadByKey($connection, $tableName, $fieldName, $primeKeyValue);
        self::trackDeltas('deleteRow', $connection, $tableName);
    }


    /**
     * Load row before Update
     *
     * @param DataBaseMysql $connection
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $primeKeyValue
     */
    static public function trackBeforeUpdate($connection, $tableName, $fieldName, $primeKeyValue)
    {
        list(self::$rowBefore, self::$errorBefore) = self::loadByKey($connection, $tableName, $fieldName, $primeKeyValue);
    }

    /**
     * Track changes after update
     *
     * @param DataBaseMysql $connection
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $primeKeyValue
     */
    static public function trackAfterUpdate($connection, $tableName, $fieldName, $primeKeyValue)
    {
        list(self::$rowAfter, self::$errorAfter) = self::loadByKey($connection, $tableName, $fieldName, $primeKeyValue);
        self::trackDeltas('updateRow', $connection, $tableName);
    }


    /**
     * Load row before insert
     *
     * @param DataBaseMysql $connection
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $primeKeyValue optional
     */
    static public function trackBeforeInsert($connection, $tableName, $fieldName, $primeKeyValue=0)
    {
        self::$rowBefore   = array();
        self::$errorBefore = '';
    }

    /**
     * Track changes after update
     *
     * @param DataBaseMysql $connection
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $primeKeyValue
     */
    static public function trackAfterInsert($connection, $tableName, $fieldName, $primeKeyValue)
    {
        list(self::$rowAfter, self::$errorAfter) = self::loadByKey($connection, $tableName, $fieldName, $primeKeyValue);
        self::trackDeltas('insertRow', $connection, $tableName);
    }

    /**
     * @return string
     */
    static public function createQuEventId()
    {
        $pid = getmypid();
        list($usec, $sec) = explode(' ', microtime());
        $usec = trim($usec, '.0');
        $quEventId = sprintf('%s_%s_%s', dechex($sec), dechex($usec), dechex($pid));
        return $quEventId;
    }

    /**
     * Load a row by key into self::$rowBefore
     *
     * @param DataBaseMysql $connection
     * @param string $tableName
     * @param string $fieldName
     * @param mixed $primeKeyValue
     * @return array array($normalizedRow, $error)
     */
    static protected function loadByKey($connection, $tableName, $fieldName, $primeKeyValue)
    {
        $normalizedRow = array();
        $error         = '';

        $quTableName     = trim($connection->SqlQuote(trim($tableName, "'")), "'");
        $quFieldName     = trim($connection->SqlQuote(trim($fieldName, "'")), "'");
        $quPrimeKeyValue = trim($connection->SqlQuote(trim($primeKeyValue, "'")), "'");

        try {
            $sql    = "SELECT * FROM `$quTableName` WHERE `$quFieldName`='$quPrimeKeyValue' LIMIT 1";
            $result = $connection->RunQuery($sql, array('wantException' => 1));
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $normalizedRow = self::normalizeRow($tableName, $row);
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        return array($normalizedRow, $error);
    }

    /**
     * Eliminate the TimestampUpdate field if it exists and turn all null-values into ''
     * @param  string $tableName
     * @param  array $row
     * @return array $row
     */
    static protected function normalizeRow($tableName, $row)
    {
        if (isset($row['TimestampUpdate'])) {
            unset($row['TimestampUpdate']);
        }
        foreach ($row as $key => $val) {
            if ((!isset($val)) || (null == $val)) {
                $row[$key] = '';
            }
        }
        return $row;
    }


    /**
     * TrackChanges constructor.
     * @param string $eventName
     * @param DataBaseMysql $connection
     * @param string $tableName
     */
    static protected function trackDeltas($eventName, DataBaseMysql $connection, $tableName)
    {
        $needTracking     = 0;
        $haveDifferences  = 0;

        if ((isset(self::$eventInfo['event']['eventId'])) && (self::$eventInfo['event']['eventId'])) {
            $eventId = self::$eventInfo['event']['eventId'];
        } else {
            $eventId = self::createQuEventId();
        }
        //$quTableName      = $connection->SqlQuote(trim($tableName, "'"));
        //$quEventName      = $connection->SqlQuote(trim($eventName, "'"));
        $userName       = $connection->SqlQuote($_SESSION['UserName']);

        //Decide tracking or not
        if ((self::$errorBefore) || (self::$errorAfter)) {
            $needTracking = 1;
        }
        $jsonBefore = json_encode(self::$rowBefore);
        $jsonAfter  = json_encode(self::$rowAfter);
        if ($jsonBefore != $jsonAfter) {
            $needTracking    = 1;
            $haveDifferences = 1;
        }
        if (!$needTracking) {
            return;
        }
        if (!$haveDifferences) {
            return;
        }

        //Get the data ready
        if ((!isset(self::$rowBefore)) || (!is_array(self::$rowBefore))) {
            self::$rowBefore = array();
        }
        if ((!isset(self::$rowAfter)) || (!is_array(self::$rowAfter))) {
            self::$rowAfter = array();
        }

        if (count(self::$rowAfter)) {
            list($parentTable, $parentId, $parentHint) = self::getParentInfoFromRow($connection, $tableName, self::$rowAfter);
        } else {
            list($parentTable, $parentId, $parentHint) = self::getParentInfoFromRow($connection, $tableName, self::$rowBefore);
        }
        if (!isset(self::$eventInfo['changes'][$parentTable][$parentId])) {
            self::$eventInfo['changes'][$parentTable][$parentId] = array();
        }
        if (!isset(self::$eventInfo['changes'][$parentTable][$parentId][$tableName])) {
            self::$eventInfo['changes'][$parentTable][$parentId][$tableName] = array();
        }

        if (($parentHint) && (!isset(self::$eventInfo['event']['parentHint']))) {
            self::$eventInfo['event']['parentHint'] = $parentHint;
        }

        if (self::$errorBefore) {
            $key = 'errorBefore';
            //------- variation 1
            self::$eventData[$key]                = array();
            self::$eventData[$key]['eventMain']   = $eventName;
            self::$eventData[$key]['eventSub']    = 'errorBefore';
            self::$eventData[$key]['eventId']     = $eventId;
            self::$eventData[$key]['user']        = $userName;
            self::$eventData[$key]['tableName']   = $tableName;
            self::$eventData[$key]['before']      = '';
            self::$eventData[$key]['after']       = self::$errorBefore;
            self::$eventData[$key]['parentTable'] = $parentTable;
            self::$eventData[$key]['parentId']    = $parentId;
            self::$eventData[$key]['parentHint']  = $parentHint;
            //------- variation 1
            //------- variation 2
            self::$eventInfo['changes'][$parentTable][$parentId][$tableName][] =
                self::TrackEncode(array('erb', '#'.$key, '', self::$errorBefore));
            //------- variation 2
        }

        if (self::$errorAfter) {
            $key = 'errorAfter';
            //------- variation 1
            self::$eventData[$key]                = array();
            self::$eventData[$key]['eventMain']   = $eventName;
            self::$eventData[$key]['eventSub']    = 'errorAfter';
            self::$eventData[$key]['eventId']     = $eventId;
            self::$eventData[$key]['user']        = $userName;
            self::$eventData[$key]['tableName']   = $tableName;
            self::$eventData[$key]['before']      = '';
            self::$eventData[$key]['after']       = self::$errorAfter;
            self::$eventData[$key]['parentTable'] = $parentTable;
            self::$eventData[$key]['parentId']    = $parentId;
            self::$eventData[$key]['parentHint']  = $parentHint;
            //------- variation 1
            //------- variation 2
            self::$eventInfo["changes"][$parentTable][$parentId][$tableName][] =
                self::TrackEncode(array('era', "#$key", '', self::$errorBefore));
            //------- variation 2
        }

        foreach (self::$rowBefore as $key => $valBefore) {
            if (isset(self::$rowAfter[$key])) {
                $valAfter = self::$rowAfter[$key];
                if ($valBefore != $valAfter) {
                    //------- variation 1
                    self::$eventData[$key]                = array();
                    self::$eventData[$key]['eventMain']   = $eventName;
                    self::$eventData[$key]['eventSub']    = 'changed';
                    self::$eventData[$key]['eventId']     = $eventId;
                    self::$eventData[$key]['user']        = $userName;
                    self::$eventData[$key]['tableName']   = $tableName;
                    self::$eventData[$key]['before']      = $valBefore;
                    self::$eventData[$key]['after']       = $valAfter;
                    self::$eventData[$key]['parentTable'] = $parentTable;
                    self::$eventData[$key]['parentId']    = $parentId;
                    self::$eventData[$key]['parentHint']  = $parentHint;
                    //------- variation 1
                    //------- variation 2
                    self::$eventInfo['changes'][$parentTable][$parentId][$tableName][] =
                        self::TrackEncode(array('chg', $key, $valBefore, $valAfter));
                    //------- variation 2
                }
            } else {
                //------- variation 1
                self::$eventData[$key]                = array();
                self::$eventData[$key]['eventMain']   = $eventName;
                self::$eventData[$key]['eventSub']    = 'removed';
                self::$eventData[$key]['eventId']     = $eventId;
                self::$eventData[$key]['user']        = $userName;
                self::$eventData[$key]['tableName']   = $tableName;
                self::$eventData[$key]['before']      = $valBefore;
                self::$eventData[$key]['after']       = '';
                self::$eventData[$key]['parentTable'] = $parentTable;
                self::$eventData[$key]['parentId']    = $parentId;
                self::$eventData[$key]['parentHint']  = $parentHint;
                //------- variation 1
                //------- variation 2
                self::$eventInfo['changes'][$parentTable][$parentId][$tableName][] =
                    self::TrackEncode(array('del', $key, $valBefore, ''));
                //------- variation 2
            }
        }//foreach

        foreach (self::$rowAfter as $key => $valAfter) {
            if (!isset(self::$eventData[$key])) {
                if (isset(self::$rowBefore[$key])) {
                    $valBefore = self::$rowBefore[$key];
                    if ($valBefore != $valAfter) {
                        //Should never arrive here
                        //------- variation 1
                        self::$eventData[$key]                = array();
                        self::$eventData[$key]['eventMain']   = $eventName;
                        self::$eventData[$key]['eventSub']    = 'changed';
                        self::$eventData[$key]['eventId']     = $eventId;
                        self::$eventData[$key]['user']        = $userName;
                        self::$eventData[$key]['tableName']   = $tableName;
                        self::$eventData[$key]['before']      = $valBefore;
                        self::$eventData[$key]['after']       = $valAfter;
                        self::$eventData[$key]['parentTable'] = $parentTable;
                        self::$eventData[$key]['parentId']    = $parentId;
                        self::$eventData[$key]['parentHint']  = $parentHint;
                        //------- variation 1
                        //------- variation 2
                        self::$eventInfo['changes'][$parentTable][$parentId][$tableName][] =
                            self::TrackEncode(array('cha', $key, $valBefore, $valAfter));
                        //------- variation 2
                    }
                } else {
                    //------- variation 1
                    self::$eventData[$key]                = array();
                    self::$eventData[$key]['eventMain']   = $eventName;
                    self::$eventData[$key]['eventSub']    = 'added';
                    self::$eventData[$key]['eventId']     = $eventId;
                    self::$eventData[$key]['user']        = $userName;
                    self::$eventData[$key]['tableName']   = $tableName;
                    self::$eventData[$key]['before']      = '';
                    self::$eventData[$key]['before']      = $valAfter;
                    self::$eventData[$key]['parentTable'] = $parentTable;
                    self::$eventData[$key]['parentId']    = $parentId;
                    self::$eventData[$key]['parentHint']  = $parentHint;
                    //------- variation 1
                    //------- variation 2
                    self::$eventInfo['changes'][$parentTable][$parentId][$tableName][] =
                        self::TrackEncode(array('add', $key, '', $valAfter));
                    //------- variation 2
                }
            }
        }//foreach

    }

    /**
     * Encode an array for tracking
     * @param $arr
     * @return false|string
     */
    static protected function TrackEncode($arr)
    {
        $string = '';
        for ($i=0,$maxi=count($arr); $i<$maxi; $i++) {
            $arr[$i] = str_replace("\n", '[LF]', $arr[$i]);
            $arr[$i] = str_replace("\r", '[CR]', $arr[$i]);
            $arr[$i] = str_replace("\t", '[TAB]', $arr[$i]);
            $arr[$i] = str_replace("|", '[PP]', $arr[$i]);
            if ($i > 0) {
                if ($string) {
                    $string .= ' | ';
                }
                $string .= $arr[$i];
            }
        }
        $string = $string . ' | ' . $arr[0] . ' =###= ';
        return $string;
    }

    /**
     * Get quoted parentTableName and parentId from a given tableName/row
     * @param DataBaseMysql $connection
     * @param $tableName
     * @param $row
     * @return array array($parentTable, $parentId, $parentHint)
     */
    static protected function getParentInfoFromRow(DataBaseMysql $connection, $tableName, $row)
    {
        $parentTable = '';
        $parentId    = '';
        $parentHint  = '';

        //Get the parentTable and ID
        if ($tableName == 'Comments') {
            if ((isset($row['TrialKeyID'])) && ($row['TrialKeyID'])) {
                $parentTable = 'Trial';
                $parentId    = $row['TrialKeyID'];
            } else if ((isset($row['SiteKeyID'])) && ($row['SiteKeyID'])) {
                $parentTable = 'Site';
                $parentId    = $row['SiteKeyID'];
            } else if ((isset($row['CompKeyID'])) && ($row['CompKeyID'])) {
                $parentTable = 'Comp';
                $parentId    = $row['SiteKeyID'];
            }
        } else if ($tableName == 'DoctorSiteJoin') {
            if ((isset($row['DoctorKeyID'])) && ($row['DoctorKeyID'])) {
                $parentTable = 'Doctor';
                $parentId    = $row['DoctorKeyID'];
            } else if ((isset($row['SiteKeyID'])) && ($row['SiteKeyID'])) {
                $parentTable = 'Site';
                $parentId    = $row['SiteKeyID'];
            }
        } else if ($tableName == 'SiteTrialsOpenJoin') {
            if ((isset($row['SiteKeyID'])) && ($row['SiteKeyID'])) {
                $parentTable = 'Site';
                $parentId    = $row['SiteKeyID'];
            } else if ((isset($row['TrialKeyID'])) && ($row['TrialKeyID'])) {
                $parentTable = 'Trial';
                $parentId    = $row['TrialKeyID'];
            }
        } else if ($tableName == 'TrialCompJoin') {
            if ((isset($row['TrialKeyID'])) && ($row['TrialKeyID'])) {
                $parentTable = 'Trial';
                $parentId    = $row['TrialKeyID'];
            } else if ((isset($row['CompKeyID'])) && ($row['CompKeyID'])) {
                $parentTable = 'Comp';
                $parentId    = $row['CompKeyID'];
            }
        } else if ($tableName == 'TrialGroup') {
            if ((isset($row['TrialKeyID'])) && ($row['TrialKeyID'])) {
                $parentTable = 'Trial';
                $parentId    = $row['TrialKeyID'];
            }
        } else if ($tableName == 'TrialTrialDiseaseTypeJoin') {
            if ((isset($row['TrialKeyID'])) && ($row['TrialKeyID'])) {
                $parentTable = 'Trial';
                $parentId    = $row['TrialKeyID'];
            } else if ((isset($row['TrialDiseaseTypeKeyID'])) && ($row['TrialDiseaseTypeKeyID'])) {
                $parentTable = 'TrialDiseaseType';
                $parentId    = $row['TrialDiseaseTypeKeyID'];
            }
        } else {
            if ((isset($row[$tableName . 'KeyID'])) && ($row[$tableName . 'KeyID'])) {
                $parentTable = $tableName;
                $parentId    = $row[$tableName . 'KeyID'];
            }
        }

        //Get the human readable ParentHint for a few tables
        if ($tableName == 'Comp') {
            $parentHint = $row['CompName'];
        } else if ($tableName == 'Doctor') {
                $parentHint = 'Dr. '. $row['DoctorFirstName'].' '.$row['DoctorLastName'];
        } else if ($tableName == 'Permissions') {
            $parentHint = $row['Action'];
        } else if ($tableName == 'Site') {
            $parentHint = $row['SiteCode'];
        } else if ($tableName == 'Trial') {
            $parentHint = $row['TrialProtocolNumber'];
        }
        if (strlen($parentHint) > 47) {
            $parentHint = substr($parentHint, 0, 47).'...';
        }

        return array($parentTable, $parentId, $parentHint);
    }

}

