<?php
/**
 * Centralized shared functions
 */


/**
 * Page start function
 *
 * @param int isAjax optional
 * @return array $cfg
 */
function startRequest($isAjax = 0) {
    global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;

    //Initialize $errors and $success
    list($errors, $warnings, $success) = initSuccessAndErrors();

    //session start
    if (!defined('SKIP_BASE_AUTH_CHECK')) {
        session_start();
    }

    //Get our callingEvent
    $httpHost        = ((isset($_SERVER)) && (isset($_SERVER['HTTP_HOST'])))   ? trim($_SERVER['HTTP_HOST'], '/')   : 'a.com';
    $requestUri      = ((isset($_SERVER)) && (isset($_SERVER['REQUEST_URI']))) ? $_SERVER['REQUEST_URI'] : '';
    $urlParts        = parse_url("http://$httpHost/$requestUri");
    $callingEvent    = str_replace('.php', '', basename($urlParts['path']));
    $callingSubEvent = 'pageload';

    //db schema check
    if (!$isAjax) {
        if ((isset($cfg['sql.checkSchemaActive'])) && ($cfg['sql.checkSchemaActive']) && (isSysAdmin())) {
            print "<br /><div style='background-color:#ff471a;color:white;'>Notice: config['sql.checkSchemaActive'] is still active!</div>";
            $sqlSchemaCheck = new SqlSchemaCheck();
        }
    }

    //access control
    if (!defined('SKIP_BASE_AUTH_CHECK')) {
        if ((!isset($_SESSION)) || (empty($_SESSION['UserKeyID']))) {
            header('location: login.php');
            exit;
        }
    }

    //Change our cfg to give admins more rights
    if (isSysAdmin()) {
        global $cfgOverridesForAdmins;
        if ((isset($cfgOverridesForAdmins)) && (is_array($cfgOverridesForAdmins))) {
            $cfg = array_merge($cfg, $cfgOverridesForAdmins);
        }
    }

    return array($cfg, $errors, $warnings, $success);
}

/**
 * Are we in a transaction?
 * @return int $weAreInATransaction
 */
function isTransactionActive() {
    global $transactionActive;
    return ( ((!isset($transactionActive)) || (!$transactionActive)) ? 0 : 1 );
}

/**
 * Create pageURL
 * @return string $pageUrl
 */
function getPageUrl() {
    $pageURL = ((isset($_SERVER['HTTPS'])) && ($_SERVER['HTTPS'] == "on")) ? "https://" : "http://";
    if ($_SERVER['SERVER_PORT'] != '80') {
        $pageURL .= $_SERVER['SERVER_NAME'] . ":" . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
    } else {
        $pageURL .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    }

    return $pageURL;
}

/**
 * Checks if $_SESSION[UserName] == $cfg[sysAdminUserName] and cfg[showAdminTab]
 *
 * @return int $showSysAdminTab
 */
function showSysAdminTab() {
    global $cfg;
    return ((isSysAdmin()) && (isset($cfg['showAdminTab'])) && ($cfg['showAdminTab'])) ? 1 : 0;
}

/**
 * Checks if $_SESSION[UserName] == $cfg[sysAdminUserName]
 *
 * @return int $isSysAdmin
 */
function isSysAdmin() {
    global $cfg;
    $isSysAdmin = 0;

    $sysAdminUserName = ((isset($cfg)) && (is_array($cfg)) && (isset($cfg['sysAdminUserName']))) ? $cfg['sysAdminUserName'] : '';
    if ($sysAdminUserName) {
        if (((isset($_SESSION))) && (isset($_SESSION['UserName']))) {
            $sessionUserName = $_SESSION['UserName'];
            if ($sessionUserName == $sysAdminUserName) {
                $isSysAdmin = 1;
            }
        }
    }
    return $isSysAdmin;
}

/**
 * Returns the SESSION[UserTypeKeyID] if any. If it doesn't exist, returns 3
 *
 * @return int $loggedInUserTypeKeyID
 */
function getUserPrivilegeType() {
    return ((isset($_SESSION['UserTypeKeyID'])) && ($_SESSION['UserTypeKeyID'])) ? $_SESSION['UserTypeKeyID'] : 3;
}


function validateField($val, $fieldName, $ruleString, $className) {
    global $cfg, $callingEvent, $callingSubEvent;
    $validationMessage = '';
    $eventString  = $callingEvent . '.' . $callingSubEvent . '.' . $fieldName . '.';
    $ruleIsIgnored = '';

    $rules      = explode(';', $ruleString);
    $failedRule = '';
    foreach($rules as $rule) {
        $f1 = strpos($rule, '=');
        if ($f1 !== false) {
            $param = trim(substr($rule, ($f1+1)));
            $rule  = trim(substr($rule, 0, $f1));
        } else {
            $param = '';
            $rule  = trim($rule);
        }

        //nullcheck
        if ((!isset($val)) || (null == $val)) {
            //Exit the loop. No other rules are processed
            if ($rule == 'nullok') {
                break;
            } else {
                $validationMessage = "$fieldName cannot be NULL";
                $failedRule = 'nullcheck';
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        }

        if (($rule == 'emptyok') && (trim($val) == '')) {
            //Exit the loop. No other rules are processed
            break;
        }
        if (($rule == 'zerodateok') && ('0000-00-00' === $val)) {
            //Exit the loop. No other rules are processed
            break;
        }
        if (($rule == 'zerodatetimeeok') && ('0000-00-00 00:00:00' === $val)) {
            //Exit the loop. No other rules are processed
            break;
        }

        //Check if this is a valid clinicaltrials.gov page
        if ($rule == 'nctcheck') {
            if ((isset($val)) && (trim($val) != '')) {
                $cont = @file_get_contents('https://clinicaltrials.gov/ct2/show/' . urlencode($val));
                if (($cont === false) || (strpos($cont, '<h1 class="solo_record">') === false)) {
                    $validationMessage = "$fieldName cannot be found at clinicaltrials.gov/ct2/show/" . urlencode($val);
                    $failedRule = $rule;
                    $eventString .= $failedRule;
                    //Ignore the event as per cfg setting?
                    if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                        $validationMessage = $failedRule = $ruleIsIgnored;
                    } else {
                        break;
                    }
                }
            }
        }

        if ($rule == 'nestring') {
            if (!isNeString($val)) {
                $validationMessage = "$fieldName cannot be empty";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'minlen') {
            $param = $param+0;
            if (strlen($val) < $param) {
                $validationMessage = "$fieldName must be at least $param characters long";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == '0or1') {
            if (!isZeroOrOne($val)) {
                $validationMessage = "$fieldName must be 0 or 1";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'email') {
            if (!isEmail($val)) {
                $validationMessage = "$fieldName must be an email address";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'date') {
            if (!isDate($val)) {
                $validationMessage = "$fieldName must be a date like YYYY-MM-DD";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'datetime') {
            if (!isDateTime($val)) {
                $validationMessage = "$fieldName must be a datetime like YYYY-MM-DD HH:MM:SS";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'zeroorpositiveint') {
            if (!isZeroOrPositiveInt($val)) {
                $validationMessage = "$fieldName must be zero or a positive integer";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'positiveint') {
            if (!isKeyId($val)) {
                $validationMessage = "$fieldName must be a positive integer";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'zeroorpositivenumber') {
            if ((!is_numeric($val)) || ($val < 0)) {
                $validationMessage = "$fieldName must be zero or a positive number";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'numeric') {
            if (!is_numeric($val)) {
                $validationMessage = "$fieldName must be numeric";
                $failedRule = $rule;
                $eventString .= $failedRule;
                //Ignore the event as per cfg setting?
                if ((isset($cfg["ignore.$eventString"])) && ($cfg["ignore.$eventString"])) {
                    $validationMessage = $failedRule = $ruleIsIgnored;
                } else {
                    break;
                }
            }
        } else if ($rule == 'checktags') {
            $allowedTags = '<strong> <b> <i> <em> <u> <br> <p>';
            $before = $val;
            $clean = strip_tags($val, $allowedTags);
            if ($before != $clean) {
                $failedRule = $rule;
                $validationMessage = "$fieldName contains invalid html tags (allowed: strong,b,i,em,u,br,p)";
            }
        }

    }

    if ($validationMessage) {
        $callingEventString = (isset($eventString)) ? "eventString='$eventString' " : '';
        if ((!isset($val)) || (null === $val)) {
            $validationMessage .= " ({$callingEventString}failedRule='$failedRule'  val=NULL)";
        } else {
            if ($failedRule == 'checktags') {
                $dispVal = '[VALUE_SUPPRESSED]';
            } else {
                $dispVal = strip_tags($val);
            }
            $validationMessage .= " ({$callingEventString}failedRule='$failedRule'  val='$dispVal')";
        }
    }

    return $validationMessage;
}


/**
 * Check for positive integer
 * @param  mixed|null $val
 * @return int $isPositiveInt
 */
function isKeyId($val=null) {
    if ((!isset($val)) ||
        (!is_numeric(trim($val))) ||
        (filter_var($val, FILTER_VALIDATE_INT) === false) ||
        (($val+0) < 1)) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * Check for 0 or positive integer
 * @param  mixed|null $val
 * @return int $isZeroOrPositiveInt
 */
function isZeroOrPositiveInt($val=null, $options=array()) {
    if (!isset($val)){
        return 0;
    }

    $stripLeadingZeroes = isset($options['stripLeadingZeroes']) ? $options['stripLeadingZeroes'] : 0;
    if (($stripLeadingZeroes) && (substr($val, 0, 1) == '0')) {
        $val = ltrim($val, '0');
        if ($val == '') {
            $val = 0;
        }
    }
    if ((!is_numeric(trim($val))) ||
        (filter_var($val, FILTER_VALIDATE_INT) === false) ||
        (($val+0) < 0)) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * Check for YYYY-MM-DD (or optionally also for 0000-00-00)
 * @param  mixed|null $val
 * @param  $zeroDateAllowed optional     1 means 0000-00-00 is allowed
 * @return int $isDate
 */
function isDate($val=null, $zeroDateAllowed=0) {
    if (!isset($val)) {
        return 0;
    }
    if (($zeroDateAllowed) && ($val == '0000-00-00')) {
        return 1;
    }
    if ((!$val) || (strlen($val) != 10)) {
        return 0;
    }
    $parts = explode('-', $val);
    for ($i = 0; $i <= 2; $i++) {
        if ((!isset($parts[$i])) || (!isZeroOrPositiveInt($parts[$i], array('stripLeadingZeroes' => 1)))) {
            return 0;
        } else {
            $parts[$i] = ltrim($parts[$i], '0');  //stripLeadingZeroes
            if ($parts[$i] == '') {
                $parts[$i] = 0;
            }
        }
    }
    if ($parts[0] < 1880) {
        return 0;
    }
    if (!checkdate( $parts[1], $parts[2], $parts[0])) {
        return 0;
    }
    return 1;
}


/**
 * Check for YYYY-MM-DD HH:MM:DD(or optionally also for 0000-00-00 00:00:00)
 * @param  mixed|null $val
 * @param  $zeroDateTimeAllowed optional     1 means 0000-00-00 00:00:00 is allowed
 * @return int $isValidDateTime
 */
function isDateTime($val=null, $zeroDateTimeAllowed=0) {
    $violation = '';
    $isValid   = 1;

    do {
        if (!isset($val)) {
            $violation = 'not set';
            $isValid   = 0;
            break;
        }
        if (($zeroDateTimeAllowed) && ($val == '0000-00-00 00:00:00')) {
            $isValid   = 1;
            break;
        }
        if ((!$val) || (strlen($val) != 19)) {
            $violation = 'string length not 19';
            $isValid   = 0;
            break;
        }
        list($date, $time) = explode(' ', $val);
        if (!isset($date)) {
            $violation = 'explode yielded no date';
            $isValid   = 0;
            break;
        }
        if ((!isset($time)) || (strlen($time) != 8) || (strpos($time, ':') === false)) {
            $violation = 'time not 8 chars';
            $isValid   = 0;
            break;
        }
        list($hour, $min, $sec) = explode(':', $time);
        $hour = intval($hour);
        $min  = intval($min);
        $sec  = intval($sec );

        if ((!isset($hour)) ||
            (!isZeroOrPositiveInt($hour, array('stripLeadingZeroes' => 1))) ||
            ($hour < 0) ||
            ($hour > 23)) {
            $violation = 'invalid hour';
            $isValid   = 0;
            break;
        }
        if ((!isset($min)) ||
            (!isZeroOrPositiveInt($min, array('stripLeadingZeroes' => 1))) ||
            ($min < 0) ||
            ($min > 59)) {
            $violation = 'invalid minutes';
            $isValid   = 0;
            break;
        }
        if ((!isset($sec)) ||
            (!isZeroOrPositiveInt($sec, array('stripLeadingZeroes' => 1))) ||
            ($sec < 0) ||
            ($sec > 59)) {
            $violation = 'invalid seconds';
            $isValid   = 0;
            break;
        }

        $parts = explode('-', $date);
        for ($i = 0; $i <= 2; $i++) {
            if ((!isset($parts[$i])) || (!isZeroOrPositiveInt($parts[$i], array('stripLeadingZeroes' => 1)))) {
                $violation = "date part $i not numeric";
                $isValid   = 0;
                break;
            } else {
                $parts[$i] = ltrim($parts[$i], '0');  //stripLeadingZeroes
                if ($parts[$i] == '') {
                    $parts[$i] = 0;
                }
            }
        }
        if ($parts[0] < 1880) {
            $violation = 'year older than 1880';
            $isValid   = 0;
            break;
        }
        if (!checkdate($parts[1], $parts[2], $parts[0])) {
            $violation = 'date not valid';
            $isValid   = 0;
            break;
        }
    } while (false);

    //if (!$isValid) {
    //    print "<br />DateTime-Violation: $violation<br />"; exit;
    //}

    return $isValid;
}

/**
 * Check for positive integer
 * @param  mixed|null $val
 * @return int $isPositiveInt
 */
function isZeroOrOne($val=null) {
    if ((!isset($val)) ||
        (!is_numeric(trim($val))) ||
        (filter_var($val, FILTER_VALIDATE_INT) === false) ||
        (($val+0) < 0) ||
        (($val+0) > 1)) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * Check for non empty string
 * @param  mixed|null $val
 * @return int $isNeString
 */
function isNeString($val=null) {
    if ((!isset($val)) ||
        (!is_string(trim($val))) ||
        (trim($val) == '')) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * Check for non empty string
 * @param  mixed|null $val
 * @return int $isNeString
 */
function isEmail($val=null) {
    if ((!isset($val)) ||
        (!is_string($val)) ||
        (trim($val) == '') ||
        (filter_var($val, FILTER_VALIDATE_EMAIL) === false)) {
        return 0;
    } else {
        return 1;
    }
}

/**
 * Get a string containing an (appendable) json representation
 * @param null $val
 * @param int $forAppend
 * @return string $jsonRepresentation
 */
function getJsonVal($val=null, $forAppend=1) {
    $json = json_encode($val);
    if ($forAppend) {
        $json = " (VAL: ".$json.')';
    }
    return $json;
}


/**
 * Get a PDO connection to the NEW WP PUBLIC database (the one with clinicaltrials.gov support)
 *
 * IP of testserver: 184.154.224.110  ns1.siteground214.com
 *
 * @param  string reference $error      Will receive '' or an error message
 * @param  int $wantNewPdo optional
 * @return EzPdo $newPublicPdo
 */
function getNewPublicPdo(&$error, $wantNewPdo=0) {
    global $gNewPublicPdo, $cfg;
    $error = '';

    $mainKey = 'newPublicDb';
    if ((!isset($gNewPublicPdo)) || ($wantNewPdo)) {
        $dsn = sprintf('mysql:host=%s;dbname=%s', $cfg[$mainKey]['host'], $cfg[$mainKey]['dbname']);
        try {
            $gNewPublicPdo = new EzPdo($dsn, $cfg[$mainKey]['user'], $cfg[$mainKey]['pass'], array());
        } catch (\Exception $e) {
            $error = 'Failed to get a database connection to new WPpublic2 ('.$e->getMessage().')';
        }
    }
    return $gNewPublicPdo;
}


/**
 * Get a PDO connection to the OLD WP PUBLIC database
 *
 * IP of testserver: 184.154.224.110  ns1.siteground214.com
 *
 * @param  string reference $error      Will receive '' or an error message
 * @param  int $wantNewPdo optional
 * @return EzPdo $oldPublicPdo
 */
function getOldPublicPdo(&$error, $wantNewPdo=0) {
    global $gOldPublicPdo, $cfg;
    $error = '';

    $mainKey = 'oldPublicDb';
    if ((!isset($gOldPublicPdo)) || ($wantNewPdo)) {
        $dsn = sprintf('mysql:host=%s;dbname=%s', $cfg[$mainKey]['host'], $cfg[$mainKey]['dbname']);
        try {
            $gOldPublicPdo = new EzPdo($dsn, $cfg[$mainKey]['user'], $cfg[$mainKey]['pass'], array());
        } catch (\Exception $e) {
            $error = 'Failed to get a database connection to old WPpublic1 ('.$e->getMessage().')';
        }
    }
    return $gOldPublicPdo;
}


/**
 * Get a PDO connection to the private SCOR database (same as our main SQL class)
 *
 * @param  string reference $error      Will receive '' or an error message
 * @param  int $wantNewPdo optional
 * @return EzPdo $scorPdo
 */
function scorGetPdo(&$error, $wantNewPdo=0) {
    global $gScorPdo, $cfg;
    $error = '';

    if ((!isset($gScorPdo)) || ($wantNewPdo)) {
        $dsn = sprintf('mysql:host=%s;dbname=%s', $cfg['scordb']['host'], $cfg['scordb']['dbname']);
        try {
            $gScorPdo = new EzPdo($dsn, $cfg['scordb']['user'], $cfg['scordb']['pass'], array());
        } catch (\Exception $e) {
            //++++++++++++++++++++REMOVE
            if (php_sapi_name() == 'cli') {
                print
                    "\nDEBUG-------------Failed PRIVATE-SCOR connection\n".
                    print_r($cfg['scordb'], true) .
                    "\nDSN = $dsn   USER=".$cfg['scordb']['user']." PASS=".$cfg['scordb']['pass'].
                    "\nDEBUG-------------Failed PRIVATE-SCOR connection\n";
            }
            //++++++++++++++++++++REMOVE
            $error = 'Failed to get a database connection to SCORprivate['.$cfg['appEnv'].'] ('.$e->getMessage().')';
        }
    }
    return $gScorPdo;
}

/**
 * Returns $assoc[$key] if it exists, otherwise returns $default
 * @param array $assoc
 * @param string $key
 * @param null $default
 * @return mixed|null
 */
function assocVal($assoc=array(), $key='', $default=null) {
    if ((!isset($assoc)) || (!is_array($assoc))) {
        $assoc = array();
    }
    if ((!$key) || (!array_key_exists($key, $assoc))) {
        return $default;
    } else {
        return $assoc[$key];
    }
}


function validateHtml($html, $options=array('addHtmlAndBodyTags'=>1)) {
    $error = '';
    $addHtmlAndBodyTags = assocVal($options, 'addHtmlAndBodyTags', 0);

    try {
        $dom = new DOMDocument;
        if ($addHtmlAndBodyTags) {
            $html = "<html>\n<body>$html</body></html>";
        }
        @$dom->loadHTML($html);
        //$rootNode = $dom->getElementsByTagName('html') ;
        showDOMNode($dom);

    } catch (\Exception $e) {
        $error = $e->getMessage();
    }

    return $error;
}


function showDOMNode(DOMNode $domNode) {
    foreach ($domNode->childNodes as $node) {
        //print "<br />TAG: ".$node->nodeName.':'.$node->nodeValue;
        if ($node->hasChildNodes()) {
            showDOMNode($node);
        }
    }
}

/**
 * High level static function to create a flat hash from a (nested) array
 *
 * @access public
 * @param  array $nestedHash
 * @return array $flatHash
 */
function flattenAssoc($nestedArray) {
    $dataHash = array();

    if ((!isset($nestedArray)) || (!is_array($nestedArray))) {
        $dataHash['val'] = $nestedArray;
    } else {
        $dataHash = flattenAssocRaw($dataHash, $nestedArray);
    }

    return $dataHash;
}

/**
 * Low level static function to flatten the nestedArray into the dataHash reference
 *
 * @access protected
 * @param  array $dataHash                  The target. Will receive new keys.
 * @param  array $nestedArray               The source (will be recursively walked through)
 * @param  string $keyPrefix optional
 * @return array $dataHash
 */
function flattenAssocRaw($dataHash=array(), $nestedArray=array(), $keyPrefix='') {
    foreach($nestedArray as $key => $val) {
        $thisKey  = ($keyPrefix) ? "$keyPrefix." : '';
        $thisKey .= str_replace('.','{dot}',$key);
        if (is_array($val)) {
            $dataHash = flattenAssocRaw($dataHash, $val, $thisKey);
        } else {
            if (isset($dataHash[$thisKey])) {
                $found = 0;
                for ($i=2; $i<=500; $i++) {
                    if (!isset($dataHash[$thisKey.$i])) {
                        $dataHash[$thisKey.$i] = $val;
                        $found = 1;
                        break;
                    }
                    if (!$found) {
                        $dataHash[$thisKey] = $val;
                    }
                }
            } else {
                $dataHash[$thisKey] = $val;
            }
        }
    }

    return $dataHash;
}


/**
 * This script will DIE (exit the process) if we are called as a WEB call (and are not allowed)
 * @param int $allowWebFromDev
 */
function ensureCliExecutionOnly($allowWebFromDev = 1) {
    if ($allowWebFromDev) {
        if (isDev()) {
            //DEV: Allowed no matter what
            return;
        }
    }

    if (isWebCall()) {
        die ('This script cannot be called as a WEB call');
    }
}

/**
 * Checks if we're running in development
 * @return int
 */
function isDev() {
    global $cfg;

    if ((!isset($cfg)) || (!is_array($cfg))) {
        require_once(ROOT_PATH . '/cfg/config.php');
    }
    if ((!isset($cfg)) || (!is_array($cfg)) || (!isset($cfg['appEnv']))) {
        return 0;
    } else {
        if ($cfg['appEnv'] == 'development') {
            return 1;
        }
    }

    return 1;
}


/**
 * Checks if we're running a WEB call or CLI
 * @return int
 */
function isWebCall() {
    if ((isset($_SERVER)) && (isset($_SERVER['SERVER_PROTOCOL']))) {
        $protocol = strtoupper($_SERVER['SERVER_PROTOCOL']);
        if (substr($protocol, 0, 4) == 'HTTP') {
            return 1;
        }
    }
    return 0;
}

/**
 * Tries to fetch the data from clinicaltrials.gov/ct2/show/TRIALNCT?displayxml=true
 * @param string $trialNct
 * @return array($result, $error, $cont, $array)
 */
function fetchNctData($trialNct='') {
    $result = 0;
    $error  = '';
    $cont   = '';
    $array  = array();

    if ($trialNct) {
        $cont = file_get_contents('https://clinicaltrials.gov/ct2/show/' . urlencode($trialNct) . '?displayxml=true');
        if ($cont) {
            $xml    = simplexml_load_string($cont, "SimpleXMLElement", LIBXML_NOCDATA);
            $json   = json_encode($xml);
            $array  = json_decode($json,true);
            $result = ((isset($array)) && (is_array($array)) && (count($array))) ? true : false;
            if (!$result) {
                $array = array();
            }
        } else {
            $result = false;
            $error  = 'Could not load raw NTC data. Wrong trialnct?';
        }
    } else {
        $result = false;
        $error  = 'trialnct parameter missing';
    }

    if ($error) {
        $array = array('error' => $error);
        $cont  = "<error>".htmlentities($error)."</error>";
    }

    return array($result, $error, $cont, $array);
}


/**
 * Clean an nct string
 * @param string $string
 * @return string
 */
function cleanClinicalTrialsString($string='') {
    $string = trim($string);
    for ($i=20; $i > 1; $i--) {
        $string = str_replace(str_repeat(' ', $i), ' ', $string);
    }
    $string = str_replace("\n ", "\n", $string);
    return $string;
}


