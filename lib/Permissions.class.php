<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class Permissions extends BaseClass {

	public $PermissionsKeyID; //int(2)
	public $Action; //varchar(50)
	public $UserTypeIDs; //varchar(20)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_Permissions($Action,$UserTypeIDs){
		$this->Action = $Action;
		$this->UserTypeIDs = $UserTypeIDs;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_action($key_row)
    {
        $quKeyRow = trim($this->connection->SqlQuote($key_row), "'");

		$result = $this->connection->RunQuery("Select * from Permissions where Action = \"$quKeyRow\" ");
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$this->PermissionsKeyID = $row["PermissionsKeyID"];
			$this->Action = $row["Action"];
			// if (strlen($this->UserTypeIDs) >= 1) {
			// 	$this->UserTypeIDs .=  ',' . $row["UserTypeIDs"];
			// } else {
				$this->UserTypeIDs =  $row["UserTypeIDs"];
			// }
		}
	}

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("SELECT * FROM Permissions WHERE PermissionsKeyID = ". $this->SqlQuote($key_row));

        $found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->PermissionsKeyID = $row["PermissionsKeyID"];
			$this->Action = $row["Action"];
			$this->UserTypeIDs =  $row["UserTypeIDs"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Permissions', 'PermissionsKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE Permissions SET\n".
                "Action = ".                  $this->SqlQuote($this->Action).",\n".
                "UserTypeIDs = ".             $this->SqlQuote($this->UserTypeIDs)."\n".
                "WHERE PermissionsKeyID = ".  $this->SqlQuote($this->PermissionsKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Permissions','PermissionsKeyID', $this->PermissionsKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO Permissions (Action, UserTypeIDs) VALUES (\n".
                $this->SqlQuote($this->Action).",\n".
                $this->SqlQuote($this->UserTypeIDs).')';
            $this->PermissionsKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Permissions','PermissionsKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT PermissionsKeyID FROM Permissions ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["PermissionsKeyID"];
            $i++;
        }
	    return $keys;
	}

	/**
	 * @return null|int PermissionsKeyID
	 */
	public function getPermissionsKeyID()
    {
		return $this->PermissionsKeyID;
	}

	/**
	 * @return null|string $Action
	 */
	public function getAction()
    {
		return $this->Action;
	}

	/**
	 * @return string $UserTypeIDs
	 */
	public function getUserTypeIDs()
    {
		return $this->UserTypeIDs;
	}

	/**
	 * @param int $PermissionsKeyID
	 */
	public function setPermissionsKeyID($PermissionsKeyID)
    {
		$this->PermissionsKeyID = $PermissionsKeyID;
	}

	/**
	 * @param string $Action
	 */
	public function setAction($Action)
    {
		$this->Action = $Action;
	}

	/**
	 * @param string $UserTypeIDs
	 */
	public function setUserTypeIDs($UserTypeIDs)
    {
		$this->UserTypeIDs = $UserTypeIDs;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->PermissionsKeyID)) ? $this->PermissionsKeyID : null);  //vld: Permissions.PermissionsKeyID
            if ($validationMessage = validateField($val,'PermissionsKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->Action)) ? $this->Action : null);  //vld: Permissions.Action
            if ($validationMessage = validateField($val,'Action','nestring',__CLASS__)) { break; }

            $val = ((isset($this->UserTypeIDs)) ? $this->UserTypeIDs : null);  //vld: Permissions.UserTypeIDs
            if ($validationMessage = validateField($val,'UserTypeIDs','nestring',__CLASS__)) { break; }
            $ids = explode(',', $this->UserTypeIDs);
            foreach($ids as $id) {
                if (!isKeyId($id)) {
                    $validationMessage = 'UserTypIDs must contain all positive integers';
                    break;
                }
            }
        } while(false);

        return $validationMessage;
    }

}

