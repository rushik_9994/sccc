<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialType extends BaseClass {

	public $TrialTypeKeyID; //int(2)
	public $TrialTypeText; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialType($TrialTypeText){
		$this->TrialTypeText = $TrialTypeText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
        $quKeyRow = trim($this->connection->SqlQuote($key_row), "'");
		$result = $this->connection->RunQuery("SELECT * FROM TrialType WHERE TrialTypeKeyID = $quKeyRow");

		$found = 0;
		while($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->TrialTypeKeyID = $row["TrialTypeKeyID"];
			$this->TrialTypeText = $row["TrialTypeText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialType', 'TrialTypeKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE TrialType SET\n".
                "TrialTypeText = ".          $this->SqlQuote($this->TrialTypeText)."\n".
                "WHERE TrialTypeKeyID = ".   $this->SqlQuote($this->TrialTypeKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialType','TrialTypeKeyID', $this->TrialTypeKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO TrialType (TrialTypeText) VALUES (\n".
                $this->SqlQuote($this->TrialTypeText).')';
            $this->TrialTypeKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialType','TrialTypeKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialTypeKeyID from TrialType order by $column $order");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["TrialTypeKeyID"];
            $i++;
        }
    	return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param  int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="TrialTypeKeyID" id="TrialTypeKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT TrialTypeKeyID, TrialTypeText FROM TrialType ORDER BY TrialTypeKeyID");
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialTypeKeyID = htmlentities($row["TrialTypeKeyID"]);
            $escTrialTypeText  = htmlentities($row["TrialTypeText"]);
            if ($row["TrialTypeKeyID"] == $selected) {
                $select .= "<option value=\"$escTrialTypeKeyID\" selected>$escTrialTypeText</option>\n";
            } else {
                $select .= "<option value=\"$escTrialTypeKeyID\">$escTrialTypeText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $TrialTypeKeyID
	 */
	public function getTrialTypeKeyID()
    {
		return $this->TrialTypeKeyID;
	}

	/**
	 * @return null|string $TrialTypeText
	 */
	public function getTrialTypeText()
    {
		return $this->TrialTypeText;
	}

	/**
	 * @param int $TrialTypeKeyID
	 */
	public function setTrialTypeKeyID($TrialTypeKeyID)
    {
		$this->TrialTypeKeyID = $TrialTypeKeyID;
	}

	/**
	 * @param string $TrialTypeText
	 */
	public function setTrialTypeText($TrialTypeText)
    {
		$this->TrialTypeText = $TrialTypeText;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        $validationMessage = '';

        //Validate
        do {
            $val = ((isset($this->TrialTypeKeyID)) ? $this->TrialTypeKeyID : null);  //vld: TrialType.TrialTypeKeyID
            if ($validationMessage = validateField($val,'TrialTypeKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialTypeText)) ? $this->TrialTypeText : null);  //vld: TrialType.TrialTypeText
            if ($validationMessage = validateField($val,'TrialTypeText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}