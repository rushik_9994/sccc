<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class PatientIdentifier extends BaseClass {

	public $PatientIdentifierKeyID; //int(2)
	public $PatientIdentifierText; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_PatientIdentifier($PatientIdentifierText){
		$this->PatientIdentifierText = $PatientIdentifierText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM PatientIdentifier WHERE PatientIdentifierKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->PatientIdentifierKeyID = $row["PatientIdentifierKeyID"];
			$this->PatientIdentifierText = $row["PatientIdentifierText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int$key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'PatientIdentifier', 'PatientIdentifierKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE PatientIdentifier SET\n".
                "PatientIdentifierText = ".        $this->SqlQuote($this->PatientIdentifierText)."\n".
                "WHERE PatientIdentifierKeyID = ". $this->SqlQuote($this->PatientIdentifierKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'PatientIdentifier','PatientIdentifierKeyID', $this->PatientIdentifierKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO PatientIdentifier (PatientIdentifierText) VALUES (\n".
                $this->SqlQuote($this->PatientIdentifierText).')';
            $this->PatientIdentifierKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'PatientIdentifier','PatientIdentifierKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT PatientIdentifierKeyID FROM PatientIdentifier ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["PatientIdentifierKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="PatientIdentifierKeyID" id="PatientIdentifierKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT PatientIdentifierKeyID, PatientIdentifierText from PatientIdentifier order by PatientIdentifierKeyID");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escPatientIdentifierKeyID = htmlentities($row["PatientIdentifierKeyID"]);
            $escPatientIdentifierText  = htmlentities($row["PatientIdentifierText"]);
            if ($row["PatientIdentifierKeyID"] == $selected) {
                $select .= "<option value=\"$escPatientIdentifierKeyID\" selected>$escPatientIdentifierText</option>\n";
            } else {
                $select .= "<option value=\"$escPatientIdentifierKeyID\">$escPatientIdentifierText</option>\n";
            }
        }
    $select .= '</select>'."\n";
    	return $select;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $selected
     * @throws \Exception
     */
	public function CreateSelectCreditor($selected)
    {
        $select = '<select name="PatientCreditor" id="PatientCreditor" class="req">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT PatientIdentifierKeyID, PatientIdentifierText FROM PatientIdentifier order by PatientIdentifierKeyID");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escPatientIdentifierKeyID = htmlentities($row["PatientIdentifierKeyID"]);
            $escPatientIdentifierText  = htmlentities($row["PatientIdentifierText"]);

            if ($row["PatientIdentifierKeyID"] == $selected) {
                 $select .= "<option value=\"$escPatientIdentifierKeyID\" selected>$escPatientIdentifierText</option>\n";
            } else {
                $select .= "<option value=\"$escPatientIdentifierKeyID\">$escPatientIdentifierText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $PatientIdentifierKeyID
	 */
	public function getPatientIdentifierKeyID()
    {
		return $this->PatientIdentifierKeyID;
	}

	/**
	 * @return null|string $PatientIdentifierText
	 */
	public function getPatientIdentifierText()
    {
		return $this->PatientIdentifierText;
	}

	/**
	 * @param int $PatientIdentifierKeyID
	 */
	public function setPatientIdentifierKeyID($PatientIdentifierKeyID)
    {
		$this->PatientIdentifierKeyID = $PatientIdentifierKeyID;
	}

	/**
	 * @param string $PatientIdentifierText
	 */
	public function setPatientIdentifierText($PatientIdentifierText)
    {
		$this->PatientIdentifierText = $PatientIdentifierText;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->PatientIdentifierKeyID)) ? $this->PatientIdentifierKeyID : null);  //vld: PatientIdentifier.PatientIdentifierKeyID
            if ($validationMessage = validateField($val,'PatientIdentifierKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->PatientIdentifierText)) ? $this->PatientIdentifierText : null);  //vld: PatientIdentifier.PatientIdentifierText
            if ($validationMessage = validateField($val,'PatientIdentifierText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

