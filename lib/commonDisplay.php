<?php
/**
 * Display functions (sadly we don't have MVC in this application yet so this is a first start)
 */

/**
 * Displays our horizontal tabs
 *
 * @param  string $activeTab
 * @return string $cont
 */
function displayTabs($activeTab='trials') {
    $cont =  "    <ul class=\"ui-tabs-nav\">\n";

    $classString = ($activeTab == 'trials') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"trials.php\">Trials</a></li>\n";

    $classString = ($activeTab == 'components') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"components.php\">Components</a></li>\n";

    $classString = ($activeTab == 'sites') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"sites.php\">Sites</a></li>\n";

    $classString = ($activeTab == 'doctors') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"doctors.php\">Doctors</a></li>\n";

    $classString = ($activeTab == 'patients') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"patients.php\">Patients</a></li>\n";

    $classString = ($activeTab == 'reports') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"reports.php\">Reports</a></li>\n";

    $classString = ($activeTab == 'chart_reports') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"chart_report.php\">Chart Reports</a></li>\n";

    if (showSysAdminTab()) {
        $classString = ($activeTab == 'admin') ? ' class="ui-tabs-selected"' : '';
        $extraString = ($activeTab == 'admin') ? ' style="background-color:red;color:white;"' : '';
        $cont .= "        <li$classString><a href=\"admin.php\"$extraString>Admin</a></li>\n";
    }


    /*
    $classString = ($activeTab == 'charts') ? ' class="ui-tabs-selected"' : '';
    $cont .= "        <li$classString><a href=\"charts.php\">Charts</a></li>\n";
    */

    $cont .=  "    </ul>\n";

    return $cont;
}

/**
 * Generate html code for success and errors
 *
 * @param array $options optional
 * Uses global $errors and $success
 */
function displaySuccessAndErrors($options=array()) {
    global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;

    $displayErrorsText       = (isset($cfg['error.displayErrorsText']))          ? $cfg['error.displayErrorsText'] : 0;
    $displayWarningsText     = (isset($cfg['warning.displayWarningsText']))      ? $cfg['warning.displayWarningsText'] : 0;
    $displayWarningMinimally = (isset($cfg['warning.displayWarningsOnlyMinimally'])) ? $cfg['warning.displayWarningsOnlyMinimally'] : 0;
    $displayWarningFullInfo  = (isset($cfg['warning.displayDetailedInformation']))          ? $cfg['warning.displayDetailedInformation'] : 0;
    if ($displayWarningFullInfo) {
        $displayWarningsText     = 1;
        $displayWarningMinimally = 0;
    }

    //ERRORS
    if ((isset($errors)) && (count($errors) > 0) && ($displayErrorsText)) {
        print "\n<table width=\"100%\">";
        foreach($errors as $error) {
            print "\n<tr><td style=\"background-color:#d73532;color:white;font-size:10pt;font-weight:bold;\">";
            if (substr($error, 0 ,5) == 'HTML:') {
                print substr($error, 5);
            } else {
                $id = 0;
                if (isSysAdmin()) {
                    $f1 = strpos($error, 'SystemStatusId[');
                    if ($f1 !== false) {
                        $f1 += strlen('SystemStatusId[');
                        $f2 = strpos($error, ']', $f1);
                        if ($f2 !== false) {
                            $id = substr($error, $f1, ($f2-$f1));
                        }
                    }
                }
                if (!$id) {
                    print "ERROR: " . htmlentities($error);
                } else {
                    $linkStart = (isSysAdmin()) ? "<a href=errorDisplay.php?id=" . urlencode($id) . ' target="_blank">' : '';
                    $linkEnd = (isSysAdmin()) ? '</a>' : '';
                    print $linkStart."ERROR:".$linkEnd.' '.htmlentities($error);
                }
            }
            print "</td></tr>\n";
        }
        print "</table>\n";
    }

    //WARNINGS
    if ((isset($warnings)) && (count($warnings) > 0)) {
        if (($displayWarningsText) || ($displayWarningMinimally)) {
            print "\n<table width=\"100%\">";
            if ($displayWarningsText) {
                foreach ($warnings as $warning) {
                    print "\n<tr><td style=\"background-color:#ffc299;color:#555555;font-size:10pt;font-weight:normal;\">";
                    if (substr($warning, 0, 5) == 'HTML:') {
                        print substr($warning, 5);
                    } else {
                        $id = 0;
                        if (isSysAdmin()) {
                            $f1 = strpos($warning, 'SystemStatusId[');
                            if ($f1 !== false) {
                                $f1 += strlen('SystemStatusId[');
                                $f2 = strpos($warning, ']', $f1);
                                if ($f2 !== false) {
                                    $id = substr($warning, $f1, ($f2-$f1));
                                }
                            }
                        }
                        if (!$id) {
                            print "WARNING: " . htmlentities($warning);
                        } else {
                            $linkStart = (isSysAdmin()) ? "<a href=errorDisplay.php?id=" . urlencode($id) . ' target="_blank">' : '';
                            $linkEnd = (isSysAdmin()) ? '</a>' : '';
                            print $linkStart."WARNING:".$linkEnd.' '.htmlentities($warning);
                        }

                    }
                    print "</td></tr>\n";
                }
            } else if ($displayWarningMinimally) {
                print
                    "\n<tr><td width='80%'>&nbsp;</td>".
                    "<td style=\"color:#ff944d;font-size:8pt;font-weight:normal;\" align='right'>".
                    'Logged '. ((count($warnings) == 1) ? '1 warning' : count($warnings).' warnings') . '&nbsp;'.
                    "</td></tr>\n";
            }
            print "</table>\n";
        }
    }

    if (isset($success)) {
        $haveSuccessMessages = ((is_string($success)) && ($success)) ? 1 : 0;
        if ((is_array($success)) && (count($success))) {
            $haveSuccessMessages = 1;
        }
        if ($haveSuccessMessages) {
            print "\n<table width=\"100%\">\n";
            $prefix = "<tr><td style=\"background-color:#639b4e;color:white;font-size:11pt;font-weight:bold;\">\n";
            $postfix = "</td></tr>\n";
            if (is_string($success)) {
                print $prefix . "SUCCESS: " . htmlentities($success) . $postfix;
            } else if (is_array($success)) {
                foreach ($success as $successRow) {
                    print $prefix . "SUCCESS: " . htmlentities($successRow) . $postfix;
                }
            }
            print "</table>\n";
        }
    }
}



function sysAdminStyleBox() {
    return "display:inline;background-color:".sysAdminBg().";color:".sysAdminFg().";";
}
function sysAdminStyleTxt() {
    return "display:inline;color:".sysAdminBg();
}
function sysAdminBg() {
    return '#ff471a';
}
function sysAdminFg() {
    return 'white';
}

function displayTopRightInfo() {
    $styleString  = (isSysAdmin()) ? " style=\"color:".sysAdminBg().";\"" : '';
    $legendString = (isSysAdmin()) ? "<div style='".sysAdminStyleBox().";'>ADMIN USER</div>" : 'Active User';

    return
        "<div class=\"userinfo\"><br />".
        "<div style='display:inline;'>$legendString:</div> ".
        "<div style='display:inline;'>".$_SESSION['UserName']."</div>".
        "&emsp;<a href=\"logout.php\">logout</a>".
        "</div>\n";
}

/**
 * Jsonify a variable
 * @param null $var
 * @param int $surroundWithParantheses  optional
 * @return string
 */
function jsonVar($var = null, $surroundWithParantheses=0) {
    $json = json_encode($var);
    if ($surroundWithParantheses) {
        $json = "( $json )";
    }
    return $json;
}
