<?php
/*
 * Search filter class
 *
 * Create Date: 10-18-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */
 
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
defined('SEARCH_COMMENT')    || define('SEARCH_COMMENT',    ' ---- ');
defined('SEARCH_DEBUG_MODE') || define('SEARCH_DEBUG_MODE', 0); // Set to 1 to turn debug mode on for searches

defined('ARROW_UP_DOWN')  || define('ARROW_UP_DOWN', '&#8597;');
defined('ARROW_UP')       || define('ARROW_UP',      '&#8593;');
defined('ARROW_DOWN')     || define('ARROW_DOWN',    '&#8595;');


require_once(ROOT_PATH.'/cfg/config.php');
require_once(ROOT_PATH.'/lib/DataBaseMysql.class.php');


/**
 * SearchFilter class
 */
class SearchFilter 
{
    /**
     * _REQUEST if given
     */
    protected $request            = array();

    /**
     * Database object for quoting
     */    
    protected $connection         = null;

    /**
     * 'trials', 'components', etc.
     */    
    protected $_type              = '';

    /**
     * Various flags and settings
     */    
    protected $_info              = array();

    /**
     * Describes the available tags
     */    
    protected $_flags              = array();

    /**
     * Remember the current sort field
     */
    protected $_sortField          = '';

    /**
     * Remember the current sort direction
     */
    protected $_sortDirection     = '';


    /**
     * Constructor
     */
    public function __construct($type='', $request=array())
    {
        $this->connection = new DataBaseMysql();
        
        if ((isset($request)) && (is_array($request)) && (count($request))) {
            $this->request = $request;
        }

        $this->_type = $type;

        $searchDebug = SEARCH_DEBUG_MODE;  //Set to 1 to turn debug mode on for searches
        
        if ($this->_type=='trials') {
            $this->_info = array(
                                 'placeholder' => 'Enter search text here...' //Example:  name:abc*'
                                );
            $this->_tags = array(
                                 array('tag'       => 'all',
                                       'aliases'   => 'a,al,all',
                                       'dropdown'  => '',
                                       'legend'    => 'all:... searches for TrialProtocolNumber and TrialTitle',
                                       'joins'     => '',
                                       'where'     => "AND    CONCAT(`Trial`.`TrialProtocolNumber`,' ',`Trial`.`TrialTitle`) LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'Z<<',
                                       'debug'     => $searchDebug,
                                      ),
                                 array('tag'       => 'name',
                                       'aliases'   => 'n,na,nam,name,compname',
                                       'dropdown'  => 'Trial Protocol Number',
                                       'legend'    => 'name:... searches for TrialProtocolNumber and TrialTitle',
                                       'joins'     => '',
                                       'where'     => "AND    CONCAT(`Trial`.`TrialProtocolNumber`,' ',`Trial`.`TrialTitle`) LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'A',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'type',
                                       'aliases'   => 't,ty,typ,type',
                                       'dropdown'  => 'Trial Type',
                                       'legend'    => 'type:... searches for TrialType',
                                       'joins'     => "LEFT JOIN `TrialType` ON `TrialType`.`TrialTypeKeyID`=`Trial`.`TrialTypeKeyID`\n",
                                       'where'     => "AND    `TrialType`.`TrialTypeText` LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'B',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'cirb',
                                       'aliases'   => 'c,cr,cb,crb',
                                       'dropdown'  => 'CIRB',
                                       'legend'    => 'cirb:... searches for CIRB',
                                       'joins'     => '',
                                       'where'     => "AND    `Trial`.`TrialCIRB` IN ([*SEARCHBOOL*])\n",
                                       'dispsort'  => 'C',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'site',
                                       'aliases'   => 's,st,sit,site',
                                       'dropdown'  => '',
                                       'legend'    => 'site:... searches for CompName+SiteName+SiteCode',
                                       'joins'     => '',
                                       'where'     => "AND EXISTS      (SELECT `SiteTrialsOpenJoin`.`TrialKeyID`\n".
                                                      "                FROM   `SiteTrialsOpenJoin`\n".
                                                      "                LEFT JOIN `DoctorSiteJoin` ON `SiteTrialsOpenJoin`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "                LEFT JOIN `Site` ON `Site`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "                LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                                                      "                WHERE  `Trial`.`TrialKeyID`=`SiteTrialsOpenJoin`.`TrialKeyID`\n".
                                                      "                AND    CONCAT(`Comp`.`CompName`,' ',`Site`.`SiteName`,`Site`.`SiteCode`) LIKE [*SEARCHTEXT*]\n".
                                                      "               )\n",
                                       'dispsort'  => 'B',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'doc',
                                       'aliases'   => 'd,dc,doc,doctor',
                                       'dropdown'  => '',
                                       'legend'    => 'doc:... searches for Doctor First/Lastname, DoctorKey, DoctorCity',
                                       'joins'     => '',
                                       'where'     => "AND EXISTS      (SELECT `SiteTrialsOpenJoin`.`TrialKeyID`\n".
                                                      "                FROM   `SiteTrialsOpenJoin`\n".
                                                      "                LEFT JOIN `DoctorSiteJoin` ON `SiteTrialsOpenJoin`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "                LEFT JOIN `Doctor` ON `Doctor`.`DoctorKeyID`=`DoctorSiteJoin`.`DoctorKeyID`\n".
                                                      "                WHERE  `Trial`.`TrialKeyID`=`SiteTrialsOpenJoin`.`TrialKeyID`\n".
                                                      "                AND    CONCAT(`Doctor`.`DoctorFirstName`,' ',`Doctor`.`DoctorLastName`,' ',`Doctor`.`DoctorKeyID`, ' ', `Doctor`.`DoctorCity`) LIKE [*SEARCHTEXT*]\n".
                                                      "               )\n",
                                       'dispsort'  => 'C',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'phase',
                                       'aliases'   => 'p,ph,phase',
                                       'dropdown'  => '',
                                       'legend'    => 'phase:... searches for TrialPhase',
                                       'joins'     => "LEFT JOIN `TrialPhase` ON `Trial`.`TrialPhaseKeyID`=`TrialPhase`.`TrialPhaseKeyID`\n",
                                       'where'     => "AND    `TrialPhase`.`TrialPhaseText` LIKE [/SEARCHTEXT/]\n",
                                       'dispsort'  => 'D<',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'group',
                                       'aliases'   => 'g,gr,grp,group',
                                       'dropdown'  => '',
                                       'legend'    => 'group:... searches for TrialGroupTitle and TrialGroupDescription',
                                       'joins'     => '',
                                       'where'     => "AND    EXISTS (SELECT `TrialGroup`.`TrialKeyID`\n".
                                                      "               FROM   `TrialGroup`\n".
                                                      "               WHERE  `TrialGroup`.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                                                      "               AND    CONCAT(`TrialGroup`.`TrialGroupTitle`,' ',`TrialGroup`.`TrialGroupDescription`) LIKE [*SEARCHTEXT*])\n",
                                       'dispsort'  => 'E',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'dis',
                                       'aliases'   => 'd,dis,disease',
                                       'dropdown'  => 'Disease Type',
                                       'legend'    => 'dis:... searches for TrialDiseaseType',
                                       'joins'     => '',
                                       'where'     => "AND    EXISTS (SELECT `TrialTrialDiseaseTypeJoin`.`TrialKeyID`\n".
                                                      "               FROM   `TrialTrialDiseaseTypeJoin`\n".
                                                      "               LEFT JOIN `TrialDiseaseType` ON `TrialTrialDiseaseTypeJoin`.`TrialDiseaseTypeKeyID`=`TrialDiseaseType`.`TrialDiseaseTypeKeyID`\n".
                                                      "               WHERE  `Trial`.`TrialKeyID`=`TrialTrialDiseaseTypeJoin`.`TrialKeyID`\n".
                                                      "               AND    `TrialDiseaseType`.`TrialDiseaseTypeName` LIKE [*SEARCHTEXT*])\n",
                                       'dispsort'  => 'F',
                                       'debug'     => $searchDebug,
                                      ),
                                );

        } else if ($this->_type=='components') {
            $this->_info = array(
                                 'placeholder' => 'Enter search text here...'//Example:  name:abc*'
                                );
            $this->_tags = array(
                                 array('tag'       => 'all',
                                       'aliases'   => 'a,al,all',
                                       'dropdown'  => '',
                                       'legend'    => 'all:... searches for CompName+CompCRAName, CRAAddress and Phone',
                                       'joins'     => '',
                                       'where'     => "AND    CONCAT(`Comp`.`CompName`,' ', ".
                                                                    "`Comp`.`CompCRAName`, ' ', ".
                                                                    "`Comp`.`CompCRAAddress1`, ' ', ".
                                                                    "`Comp`.`CompCRAAddress2`, ' ', ".
                                                                    "`Comp`.`CompCRACity`, ' ', ".
                                                                    "`Comp`.`CompCRAState`, ' ', ".
                                                                    "`Comp`.`CompCRAZip`, ' ', ".
                                                                    "`Comp`.`CompCRAPhone`, ' ', ".
                                                                    "`Comp`.`CompCRAEmail`, ' '".
                                                                    ") LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'Z<<',
                                       'debug'     => $searchDebug,
                                      ),                                                                    

                                 array('tag'       => 'name',
                                       'aliases'   => 'n,na,nam,name,compname',
                                       'dropdown'  => 'Component Name',
                                       'legend'    => 'name:... searches for CompName+CompCraName',
                                       'joins'     => '',
                                       'where'     => "AND    CONCAT(`Comp`.`CompName`,' ',`Comp`.`CompCRAName`) LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'B',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'tpno',
                                       'aliases'   => 't,tr,trial,tp,tpno,tpn',
                                       'dropdown'  => 'Trial Protocol Number',
                                       'legend'    => 'tpno:... searches for TrialProtocolNumber',
                                       'joins'     => '',
                                       'where'     => "AND EXISTS (SELECT `Trial`.`TrialProtocolNumber`\n".
                                                      "            FROM   `Trial`\n".
                                                      "            LEFT JOIN `TrialCompJoin` ON `TrialCompJoin`.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                                                      "            WHERE  `TrialCompJoin`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                                                      "            AND    `Trial`.`TrialProtocolNumber` LIKE [*SEARCHTEXT*])\n",
                                       'dispsort'  => 'C',
                                       'debug'     => $searchDebug,
                                      ),


                                 array('tag'       => 'stat',
                                       'aliases'   => 'st,stat,status',
                                       'dropdown'  => '',
                                       'legend'    => 'stat:Active/Inactive searches for CompStatus="Active" or "Inactive"',
                                       'joins'     => '',
                                       'where'     => "AND    CONCAT( IF(`Comp`.`CompStatus`=1, 'Active', 'Inactive'), ' ') LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'E',
                                       'debug'     => $searchDebug,
                                      ),
                                );

        } else if ($this->_type=='sites') {
            $this->_info = array(
                                 'placeholder' => 'Enter search text here...'//Example:  name:abc*'
                                );
            $this->_tags = array(
                                 array('tag'       => 'all',
                                       'aliases'   => 'a,al,all',
                                       'dropdown'  => '',
                                       'legend'    => 'all:... searches for ComponentName+SiteName+SiteCode + Address and Phone',
                                       'joins'     => "LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n",
                                       'where'     => "AND    CONCAT(`Comp`.`CompName`,' ',`Site`.`SiteName`,`Site`.`SiteCode`,".
                                                                    "`Site`.`SiteAddress1`, ' ', `Site`.`SiteAddress2`, ' ', ".
                                                                    "`Site`.`SiteCity`, ' ', `Site`.`SiteState`, ' ', ".
                                                                    "`Site`.`SiteZip`, ' ', `Site`.`SitePhone`) LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'Z<<',
                                       'debug'     => $searchDebug,
                                      ),                                                                    

                                 array('tag'       => 'name',
                                       'aliases'   => 'n,na,nam,name,sitename',
                                       'dropdown'  => '',
                                       'legend'    => 'name:... searches for ComponentName+SiteName+SiteCode',
                                       'joins'     => "LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n",
                                       'where'     => "AND    CONCAT(`Comp`.`CompName`,' ',`Site`.`SiteName`,`Site`.`SiteCode`) LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'B',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'sn',
                                       'aliases'   => 's,sn,sitename',
                                       'dropdown'  => 'Site Name',
                                       'legend'    => 'sn:... searches for SiteName',
                                       'joins'     => '',
                                       'where'     => "AND    `Site`.`SiteName` LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'C',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'sc',
                                       'aliases'   => 'c,sc,cd,cod,code,sitecode',
                                       'dropdown'  => 'Site Code',
                                       'legend'    => 'sc:... searches for SiteCode',
                                       'joins'     => '',
                                       'where'     => "AND    `Site`.`SiteCode` LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'D',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'stat',
                                       'aliases'   => 'st,stat,status',
                                       'dropdown'  => '',
                                       'legend'    => 'stat:Active/Inactive searches for SiteStatus="Active" or "Inactive"',
                                       'joins'     => '',
                                       'where'     => "AND    CONCAT( IF(`Site`.`SiteStatus`=1, 'Active', 'Inactive'), ' ') LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'E',
                                       'debug'     => $searchDebug,
                                      ),
                                );

        } else if ($this->_type=='doctors') {
            $this->_info = array(
                                 'placeholder' => 'Enter search text here...'//Example:  na:Doe*John*'
                                );
            $this->_tags = array(
                                 array('tag'       => 'all',
                                       'aliases'   => 'all',
                                       'dropdown'  => '',
                                       'legend'    => 'all:... searches for DoctorKeyID, Firstname, Lastname and ProtocolName',
                                       'joins'     => "LEFT JOIN `DoctorSpecialty` ON `Doctor`.`DoctorSpecialty`=`DoctorSpecialty`.`DoctorSpecialtyKeyID`\n",
                                       'where'     => "AND    (\n".
                                                      "               (`Doctor`.`DoctorKeyID`=[NUMERICSEARCHTEXT])\n".
                                                      "           OR\n".
                                                      "               (\n".
                                                      "                      ( CONCAT(`Doctor`.`DoctorFirstName`,`Doctor`.`DoctorLastName`) LIKE [SEARCHTEXT] )\n".
                                                      "                   OR (`DoctorSpecialty`.`DoctorSpecialtyText` LIKE [SEARCHTEXT])\n".
                                                      "               )\n".
                                                      "           OR EXISTS\n".
                                                      "               (SELECT `Trial`.`TrialKeyID`\n".
                                                      "                FROM   `SiteTrialsOpenJoin`\n".
                                                      "                LEFT JOIN `Trial` ON `SiteTrialsOpenJoin`.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                                                      "                LEFT JOIN `DoctorSiteJoin` ON `SiteTrialsOpenJoin`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "                LEFT JOIN `Site` ON `Site`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "                LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                                                      "                WHERE  `DoctorSiteJoin`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`\n".
                                                      "                AND    CONCAT(`Trial`.`TrialProtocolNumber`) LIKE [*SEARCHTEXT*]\n".
                                                      "               )\n".
                                                      "       )\n",
                                       'dispsort'  => 'Z<<',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'na',
                                       'aliases'   => 'n,na,nam,name',
                                       'dropdown'  => 'Doctor Name',
                                       'legend'    => 'na:... searches for Doctor\'s Firstname and Lastname',
                                       'joins'     => '',
                                       'where'     => "AND    CONCAT(`Doctor`.`DoctorFirstName`, ', ', `Doctor`.`DoctorLastName`) LIKE [*SEARCHTEXT*]\n",
                                       'dispsort'  => 'A',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'sp',
                                       'aliases'   => 's,sp,spec,special,Specialty',
                                       'dropdown'  => 'Speciality',
                                       'legend'    => 'sp:... searches for Doctor\'s Specialty',
                                       'joins'     => "LEFT JOIN `DoctorSpecialty` ON `Doctor`.`DoctorSpecialty`=`DoctorSpecialty`.`DoctorSpecialtyKeyID`\n",
                                       'where'     => "AND    `DoctorSpecialty`.`DoctorSpecialtyText` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'B',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'st',
                                       'aliases'   => 'st,si,sit,site,sites',
                                       'dropdown'  => '',
                                       'legend'    => 'st:... searches for Doctor\'s Sites (CompName SiteName)',
                                       'joins'     => '',
                                       'where'     => "AND    EXISTS (SELECT `Site`.`SiteKeyID`\n".
                                                      "               FROM   `DoctorSiteJoin`\n".
                                                      "               LEFT JOIN `Site` ON `Site`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "               LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                                                      "               WHERE  `DoctorSiteJoin`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`\n".
                                                      "               AND    CONCAT(`Comp`.`CompName`,' ',`Site`.`SiteName`) LIKE [*SEARCHTEXT*]\n".
                                                      "               )\n",
                                       'dispsort'  => 'C',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'prot',
                                       'aliases'   => 'p,pr,pro,prot,proto,protoc,protocol,protocoll',
                                       'dropdown'  => '',
                                       'legend'    => 'prot:... searches for Doctor\'s Protocols (TrialProtocolNumber)',
                                       'joins'     => '',
                                       'where'     => "AND    EXISTS (SELECT `Trial`.`TrialKeyID`\n".
                                                      "               FROM   `SiteTrialsOpenJoin`\n".
                                                      "               LEFT JOIN `Trial` ON `SiteTrialsOpenJoin`.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                                                      "               LEFT JOIN `DoctorSiteJoin` ON `SiteTrialsOpenJoin`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "               LEFT JOIN `Site` ON `Site`.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`\n".
                                                      "               LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                                                      "               WHERE  `DoctorSiteJoin`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`\n".
                                                      "               AND    CONCAT(`Trial`.`TrialProtocolNumber`) LIKE [*SEARCHTEXT*]\n".
                                                      "               )\n",
                                       'dispsort'  => 'C',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'id',
                                       'aliases'   => 'id,ident,identifier',
                                       'dropdown'  => '',
                                       'legend'    => 'id:... searches for DoctorKeyID',
                                       'joins'     => '',
                                       'where'     => "AND    `Doctor`.`DoctorKeyID`=[NUMERICSEARCHTEXT]\n",
                                       'dispsort'  => 'M<',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'fn',
                                       'aliases'   => 'fn,first,firstname',
                                       'dropdown'  => '',
                                       'legend'    => 'fn:... searches for Doctor\'s Firstname',
                                       'joins'     => '',
                                       'where'     => "AND    `Doctor`.`DoctorFirstName` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'N',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'ln',
                                       'aliases'   => 'ln,last,lastname',
                                       'dropdown'  => '',
                                       'legend'    => 'ln:... searches for Doctor\'s Lastname',
                                       'joins'     => '',
                                       'where'     => "AND    `Doctor`.`DoctorLastName` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'N',
                                       'debug'     => $searchDebug,
                                      ),

                                );
                                
        } else if ($this->_type=='patients') {
            $this->_info = array(
                                 'placeholder' => 'Enter search text here...'//Example:  id:abc*'
                                );
            $this->_tags = array(
                                 array('tag'       => 'all',
                                       'aliases'   => 'all',
                                       'dropdown'  => '',
                                       'legend'    => 'all:... searches for PatientInitials, PatientIdentifier, Addendum, ZipCode and Protocol',
                                       'joins'     => "LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                                                      "LEFT JOIN `PatientIdentifier` ON `Patient`.`PatientIdentifier`=`PatientIdentifier`.`PatientIdentifierKeyID`\n",
                                       'where'     => "AND    (\n".
                                                      "           (CONCAT(`PatientIdentifier`.`PatientIdentifierText`,`Patient`.`PatientIdentifierAddendum`) LIKE [SEARCHTEXT])\n".
                                                      "        OR (CONCAT(`PatientFirstInit`, `PatientMiddleInit`, `PatientLastInit`) LIKE [SEARCHTEXT])\n".
                                                      "        OR (`Trial`.`TrialProtocolNumber` LIKE [SEARCHTEXT])\n".
                                                      "        OR (`PatientZipCode` LIKE [SEARCHTEXT])\n".
                                                      "       )\n",
                                       'dispsort'  => 'Z<<',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'id',
                                       'aliases'   => 'i,id,ident,identifier',
                                       'dropdown'  => 'Patient Identifier',
                                       'legend'    => 'id:... searches for PatientIdentifier and PatientIdentifierAddendum',
                                       'joins'     => "LEFT JOIN `PatientIdentifier` ON `Patient`.`PatientIdentifier`=`PatientIdentifier`.`PatientIdentifierKeyID`\n",
                                       'where'     => "AND    CONCAT(`PatientIdentifier`.`PatientIdentifierText`,`Patient`.`PatientIdentifierAddendum`) LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'B',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'idonly',
                                       'aliases'   => 'idonly',
                                       'dropdown'  => '',
                                       'legend'    => 'idonly:... searches for PatientIdentifier (but not in PatientIdentifierAddendum)',
                                       'joins'     => '',
                                       'where'     => "AND    `PatientIdentifier`.`PatientIdentifierText` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'X<<',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'add',
                                       'aliases'   => 'a,ad,add,addendum',
                                       'dropdown'  => '',
                                       'legend'    => 'add:... searches for PatientIdentifierAddendum',
                                       'joins'     => '',
                                       'where'     => "AND    `Patient`.`PatientIdentifierAddendum` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'Y',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'bd',
                                       'aliases'   => 'b,bd,birth,birthdate',
                                       'dropdown'  => '',
                                       'legend'    => 'bd:... searches for Birthdate',
                                       'joins'     => '',
                                       'where'     => "AND    `Patient`.`PatientBirthDate` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'B',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'init',
                                       'aliases'   => 'init,initial,initials',
                                       'dropdown'  => '',
                                       'legend'    => 'init:... searches for Patient Initials (first,middle,last)',
                                       'joins'     => "",
                                       'where'     => "AND    CONCAT(`PatientFirstInit`, `PatientMiddleInit`, `PatientLastInit`) LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'Y',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'zip',
                                       'aliases'   => 'z,zi,zip,zipcode',
                                       'dropdown'  => 'Zip Code',
                                       'legend'    => 'zip:... searches for Patient Zip Code',
                                       'joins'     => "",
                                       'where'     => "AND    `PatientZipCode` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'E>',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'prot',
                                       'aliases'   => 'p,pr,prot,protocol',
                                       'dropdown'  => 'Trial Protocol Number',
                                       'legend'    => 'prot:... searches for Protocol',
                                       'joins'     => "LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`\n",
                                       'where'     => "AND    `Trial`.`TrialProtocolNumber` LIKE [SEARCHTEXT]\n",
                                       'dispsort'  => 'D',
                                       'debug'     => $searchDebug,
                                      ),

                                 array('tag'       => 'site',
                                       'aliases'   => 's,si,sit,site,sites',
                                       'dropdown'  => '',
                                       'legend'    => 'site:... searches for Sites (CompName SiteName)',
                                       'joins'     => '',
                                       'where'     => "AND    EXISTS (SELECT `Site`.`SiteKeyID`\n".
                                                      "               FROM   `DoctorSiteJoin`\n".
                                                      "               LEFT JOIN `Doctor` ON `DoctorSiteJoin`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`\n".
                                                      "               LEFT JOIN `Site`   ON `DoctorSiteJoin`.`SiteKeyID`=`Site`.`SiteKeyID`\n".
                                                      "               LEFT JOIN `Comp`   ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                                                      "               WHERE  `DoctorSiteJoin`.`DoctorKeyID`=`Patient`.`DoctorKeyID`\n".
                                                      "               AND    CONCAT(`Comp`.`CompName`,' ',`Site`.`SiteName`) LIKE [*SEARCHTEXT*]\n".
                                                      "              )\n",
                                       'dispsort'  => 'D',
                                       'debug'     => $searchDebug,
                                      ),
                                );
        }

    }


    /**
     * Returns the defined tags
     *
     * @access public
     * @return array  $tags           e.g. $this->_tags['trials']
     */
    public function getDefinedTags()
    {
        return $this->_tags;
    }

    /**
     * Get SearchFilter code
     *
     * @param int $index               which index to use for fieldIDs like search_text_0, ...
     * @param array $options optional  Supports: 'alsoShowInactive'
     * @return string $htmlCode
     */ 
    public function generateSearchFilterCode($index=0, $options=array())
    {
        global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;

        $code = '';
        $searchTextId          = "search_text_$index";
        $searchTagId           = "search_tag_$index";
        $searchFilterId        = "search_filter_$index";

        $alsoShowInactiveField = (isset($options['alsoShowInactive'])) ? 1 : 0;
        $alsoShowInactiveValue = ($alsoShowInactiveField) ? $options['alsoShowInactive'] : 0;
        $alsoShowInactiveText  = (isset($options['alsoShowInactiveText'])) ? $options['alsoShowInactiveText'] : 'Also show Inactive';

        if (count($this->_tags) > 0) {
            $request = $this->getRequestArray();
            $searchText  = (isset($request[$searchTextId])) ? $request[$searchTextId] : '';
            if ((!$searchText) && (isset($request['stx']))) {
                $searchText = $request['stx'];
            }

            $searchTag   = (isset($request[$searchTagId]))  ? $request[$searchTagId]  : '';
            if ((!$searchTag) && (isset($request['stg']))) {
                $searchTag = $request['stg'];
            }
            $placeholder = htmlentities($this->_info['placeholder']);
        
            $code .= "<table border=\"0\" width=\"100%\">\n".
                     "    <tr>\n".
                     "        <td nowrap=\"nowrap\" width=\"10%\">\n";

            if (count($this->_tags)) {
                $code .= "        Search &nbsp;&nbsp;\n".
                         "<select id=\"$searchTagId\" name=\"$searchTagId\" class=\"chosen-select button\" onChange=\"onSearchTagChange()\">\n";
                foreach($this->_tags as $tagEntry) {
                    $escTagId        = htmlentities($tagEntry['tag']);
                    $escTagLegend    = htmlentities($tagEntry['legend']);
                    $escDropDownText = (isset($tagEntry['dropdown'])) ? htmlentities($tagEntry['dropdown']) : '';
                    //$f1 = strpos($escTagLegend, ':');
                    //if ($f1 !== false) {
                    if ($escDropDownText) {
                        $escText = trim(substr($escTagLegend,($f1+1)),'. ');
                        if (strpos($escText, 'searches for ')===0) {
                            $escText = trim(substr($escText,13));
                        }
                        $escTagLegend = htmlentities(strtoupper($escTagId)).":&nbsp;&nbsp;&nbsp;$escText";
                    }
                    $selected = ($escTagId == $searchTag) ? " selected=\"selected\"" : '';
                    if ($escDropDownText) {
                        $code .= "<option value=\"$escTagId\"$selected>$escDropDownText</option>\n";
                    }
                }
                $code .= "</select>&nbsp;&nbsp;";
            } else {
                $code .= "        Search Filter: &nbsp;&nbsp;\n";
            }

            if ($alsoShowInactiveField) {
                $checked = ($alsoShowInactiveValue) ? ' checked' : '';
                $code .= "&nbsp;&nbsp;<div style='display:inline;font-size:9pt;'><input type='checkbox' name='searchShowInactiveAlso' id='searchShowInactiveAlso' $checked>&nbsp;".htmlentities($alsoShowInactiveText)."</div>&nbsp;&nbsp;".
                         "<input type='hidden' name='searchShowInactiveIsShown' id='searchShowInactiveIsShown value='yes'>\n";
            } else {
                $code .= "<input type='hidden' name='searchShowInactiveIsShown' id='searchShowInactiveIsShown value='no'>\n";
            }

            $code .= "        </td>\n".
                     "        <td nowrap=\"nowrap\" width=\"40%\">".
                     "            <input class=\"autocomplete\" id=\"$searchTextId\" name=\"$searchTextId\" size=\"40\" placeholder=\"$placeholder\" value=\"".htmlentities($searchText)."\">&nbsp;&nbsp;".
                     "            <input type=\"submit\" id=\"$searchFilterId\" name=\"$searchFilterId\" value=\"Search\">\n".
                     "        </td>\n";
    
            $code .= "    </tr>\n";
            
            if ($this->_type=='patients') {
                $searchStartDate   = $cfg['fiscalyear.startdate'];   //Beginning of this fiscal year
                //$searchStartDate = date('Y-01-01');                //(Beginning of this year)
                $searchEndDate     = date('Y-m-d');                  //today
                $request           = $this->getRequestArray();
                if (isset($request['search_start_date'])) {
                    $searchStartDate = $request['search_start_date'];
                }
                if (isset($request['search_end_date'])) {
                    $searchEndDate   = $request['search_end_date'];
                }
                $code .= "    <tr>\n".
                         "        <td nowrap=\"nowrap\">Start Date:&nbsp;</td>\n".
                         "        <td class=\"left\">\n".
                         "            <input type=\"text\" name=\"search_start_date\" id=\"search_start_date\" size=\"32\" class=\"dpick req\" value=\"$searchStartDate\" />".
                         "            &nbsp;&nbsp;End Date:&nbsp;\n".
                         "            <input type=\"text\" name=\"search_end_date\" id=\"search_end_date\" size=\"32\" class=\"dpick req\" value=\"$searchEndDate\" />".
                         "        </td>\n".
                         "    </tr>\n";
            }

            if ($code) {
                $code .= "</table>\n".
                         $this->addJsFunctionCode('onSearchTagChange', $index).
                         "<hr>\n";
            }
        }//count tags>0

        return $code;
    }

    public function analyzeSearchFilter(&$request, $index=0, $cfg=array())
    {
        $sClauses = array('joins'             => array(),
                          'where'             => array(),
                          'debug'             => 0);

        $searchTextId   = "search_text_$index";
        $searchTagId    = "search_tag_$index";
        $searchFilterId = "search_filter_$index";
        
        if(!$ndex) {
            $index = '_0';
        }
        $sClauses['search_index']       = $index;
        $sClauses['order_index']        = $index;
        $sClauses['from_index']         = $index;
        $sClauses['order_last_clicked'] = (isset($request['order_last_clicked'])) ? $request['order_last_clicked'] : 0;
        
        if (isset($request['searchShowInactiveIsShown'])) {
            $sClauses['show_inactive_also'] = ((isset($request['searchShowInactiveAlso'])) && ($request['searchShowInactiveAlso'])) ? 1 : 0;
        }

        if ((isset($request['search_filter_'.$index])) && ($request['search_filter_'.$index]=='Search')) {
            $request['from_'.$index] = 0;
        }

        //Add sort order and sort direction (asc,desc) (order_field_0, order_direction_0, etc.)
        for ($i=0; $i < 3; $i++) {
            $index = '_'.chr(48+$i);
            $sClauses['order_field'.$index] = '';
            $sClauses['order_field'.$index] = '';
            if ((isset($request['order_field'.$index])) && ($request['order_field'.$index])) {
                $sClauses['order_field'.$index] = $request['order_field'.$index];
            }
            if ((isset($request['order_direction'.$index])) && ($request['order_direction'.$index])) {
                $sClauses['order_direction'.$index] = $request['order_direction'.$index];
            }
        }


        $text      = (isset($request[$searchTextId])) ? trim($request[$searchTextId]) : '';
        $searchTag = (isset($request[$searchTagId])) ? trim($request[$searchTagId])   : '';
        if ($searchTag == 'all') { $searchTag = ''; }

        if ($searchTag) {
            $text = $request[$searchTagId].":*$text*";
        }

        //------------------------- parse text string
        $searchtags = array();
        $taghash    = array();
        if ($text) {
            $text = str_replace('|', '', $text);

            foreach($this->_tags as $t) {
                $tags = explode(',', $t['tag'].','.$t['aliases']);
                foreach($tags as $tag) {
                    //CLEANUP 
                    if (!isset($this->_tags[$key]['aliases'])) {
                        $this->_tags[$key]['aliases'] = $this->_tags[$key]['tag'];
                    }
                    if (!isset($this->_tags[$key]['debug'])) {
                        $this->_tags[$key]['debug'] = 0;
                    }
                    if (!isset($this->_tags[$key]['dispsort'])) {
                        //Average priority (M) plus alphabetical by tag name
                        $this->_tags[$key]['dispsort'] = 'M'.$this->_tags[$key]['tag'];
                    }

                    //ANALYSIS
                    $tag = trim($tag);
                    if ($tag) {
                        $taghash[$tag] = $t;
                        $text = str_replace(" $tag:", "|$tag:", ' '.$text);
                    }
                }
            }
        }

        $tmp = explode('|', $text);
        foreach($tmp as $ignore=>$tm) {
            $tm = trim($tm);
            if ($tm) {
                $f1 = strpos($tm, ':');
                if ($f1!==false) {
                    $key = substr($tm, 0, $f1);
                    $key = $taghash[$key]['tag'];
                    $val = trim(substr($tm, ($f1+1)));
                } else {
                    $key = 'all';
                    $val = trim($tm);
                }
                if (($key) && ($val)) {
                    $searchtags[$key] = $val;
                }
            }
        }
        //------------------------- parse text string


        //------------------------- create clauses
        foreach($searchtags as $tag=>$val) {

            //How many val-entries do we have?
            $strings = $this->extractSearchPatternsFromString($val);
            $t       = $taghash[$tag];

            if ((isset($t['debug'])) && ($t['debug'])) {
                if ($sClauses['debug'] < $t['debug']) {
                    $sClauses['debug'] = $t['debug'];
                }
            }

            //print "<br />TAG=$tag VALUE=$val t=".serialize($t);
            $joins = trim($t['joins'])."\n";

            //Substitutions
            $sClauses['where'][] = $this->buildOneWhereClauseFromStrings($t['where'], $strings);

            if ($joins) {
                if (!isset($sClauses['joins'][$joins])) {
                    $sClauses['joins'][$joins]  = count($sClauses['where']);
                } else {
                    $sClauses['joins'][$joins] .= ','.count($sClauses['where']);
                }
            } 
        }
        reset($searchtags);
        //------------------------- create clauses

        //------------------------- process additional search fields
        if (isset($request['search_start_date'])) {
            $sClauses['where'][] = "AND    `Patient`.`PatientRegistrationDate` >= ".
                                  $this->connection->SqlQuote($request['search_start_date'])."\n";
        }
        if (isset($request['search_end_date'])) {
            $sClauses['where'][] = "AND    `Patient`.`PatientRegistrationDate` <= ".
                                  $this->connection->SqlQuote($request['search_end_date'])."\n";
        }
        //------------------------- process additional search fields

        if ((isset($request['debug'])) && ($request['debug']='100catsIStooMANY')) {
            $sClauses['debug'] = 1;
        }
        if ((isset($request['debug'])) && ($request['debug']='200catsIStooMANY')) {
            $sClauses['debug'] = 2;
        }
        if ((isset($cfg)) && (is_array($cfg)) && (isset($cfg['debug.searchfilter']))) {
            if ($sClauses['debug'] < $cfg['debug.searchfilter']) {
                $sClauses['debug'] = $cfg['debug.searchfilter'];
            }
        }

        //$sClauses['debug'] = 2;
        //print "<pre style='color:black;font-size:9pt;'>".print_r($sClauses,true)."</pre>";

        return $sClauses;
    }


    protected function buildOneWhereClauseFromStrings($rawWhere, $strings=array()) {
        $finalWhere = $rawWhere;

        //remove the 'AND...' from the beginning of the where-clause

        if (count($strings) == 1) {
            $finalWhere = $this->rawSearchTextReplacement($rawWhere, $strings[0]);
        } else {
            $where      = substr($rawWhere,3);
            $finalWhere = "AND (\n";
            for ($i=0,$maxi=count($strings); $i < $maxi; $i++) {
                $string = $strings[$i];
                if ($i == 0) {
                    $finalWhere .= "   (\n".
                                   $this->rawSearchTextReplacement($where, $strings[0]).
                                   ")\n";
                } else {
                    $finalWhere .= "OR (\n".
                                   $this->rawSearchTextReplacement($where, $strings[$i]).
                                   ")\n";
                }
            }
            $finalWhere .= "    )\n";
        }

        return $finalWhere;
    }

    protected function rawSearchTextReplacement($where, $string) {
        $rawVal     = trim($this->connection->SqlQuote($string), " '");
        $finalWhere = $where;

        $boolRaw = str_replace('%','',$rawVal);
        if (!$boolRaw) {
            $boolRawVal = "0,1";
        } else {
            if (strpos(',f,fl,fa,fl,fls,fal,fals,false,', ','.trim(strtolower($boolRaw)).',') !== false) {
                $boolRawVal = 0;
            } else {
                $boolRawVal = 1;
            }
        }
        $finalWhere = str_replace("[*SEARCHBOOL*]", "$boolRawVal", $finalWhere);

        $finalWhere = str_replace("[/SEARCHTEXT/]", "'".str_replace('%','', $rawVal)."'",   $finalWhere);
        $finalWhere = str_replace("[/SEARCHTEXT]",  "'$rawVal'",   $finalWhere);
        $finalWhere = str_replace("[SEARCHTEXT/]",  "'$rawVal'",   $finalWhere);

        $finalWhere = str_replace("[*SEARCHTEXT]",  "'%$rawVal'",  $finalWhere);
        $finalWhere = str_replace("[SEARCHTEXT*]",  "'$rawVal%'",  $finalWhere);
        $finalWhere = str_replace("[*SEARCHTEXT*]", "'%$rawVal%'", $finalWhere);

        $finalWhere = str_replace("[SEARCHTEXT]", "'$rawVal'", $finalWhere);
        $finalWhere = str_replace("[NUMERICSEARCHTEXT]", "'".intval(str_replace('%','',trim($rawVal," '")))."'", $finalWhere);
        $finalWhere = str_replace('%%', '%', $finalWhere);
        return $finalWhere;
    }

    /**
     * Handle doublequoted-strings. e.g. >>>"John Muir" test<<< becomes array('John Muir', 'test')
     *
     * @access public
     * @param  string $string
     * @return array  $strings
     */
    public function extractSearchPatternsFromString($string='') {
        $strings  = array();

        //Cut off the 'comment'-section of the search string (after SEARCH_COMMENT)
        $f1 = strpos($string, SEARCH_COMMENT);
        if ($f1 !== false) {
            $string = trim(substr($string,0,$f1));
        }

        $tmp = str_replace('*', '%', $string);

        //Logic that uses ' OR ' (in all caps) to separate strings
        if (1) {
            if (strpos($tmp, ' OR ') !== false) {
                $tmpArray = explode(' OR ', $tmp);
                foreach($tmpArray as $t) {
                    $t = trim($t, ' %');
                    if ($t) {
                        $strings[] = "%$t%";
                    }
                }
            }
        }

        //Logic that treats the whole strings as one result
        if (count($strings) == 0) {
            while(strpos($tmp, '%%')!==false) {
                $tmp = str_replace('%%','%', $tmp);
            }
            if (trim($tmp, ' %') != '') {                
                $strings[] = $tmp;
            } else {
                $strings[] = $tmp;
            }
        }

        /*Logic supporting multiple strings like this "abc def" "xyz" suppressed
        $dQuoted  = 0;

        $string = str_replace('~', ' ', $string); //we use ~ as space-replacement (non-breaking-spaces)

        $splittableString  = '';
        for ($i=0,$maxi=strlen($string); $i < $maxi; $i++) {
            $x = substr($string,$i,1);
            if ((strpos(" \t\n\r",$x)!==false) && ($dQuoted)) {
                //Whitespaces inside double-quoted strings are ignored (replaced by spaces)
                $splittableString .= '~';
                continue;
            }

            //Am inside "..." (Double-quote-string)?
            if ($dQuoted) {
                if ($x == '"') {
                    $dQuoted = 0;
                    $x       = ' ';
                }
                $splittableString .= $x;
                continue;
            }

            //Not inside double-quoted string
            if ($x == '"') {
                $dQuoted = 1;
                $x       = ' ';
            }
            $splittableString .= $x;
        }

        $tmparray = explode(' ', $splittableString);
        foreach($tmparray as $tmp) {
            $tmp = str_replace('~', ' ', $tmp);
            $tmp = trim($tmp);
            if ($tmp) {
                $tmp = "%$tmp%";
                $tmp = str_replace('*', '%', $tmp);
                while(strpos($tmp, '%%')!==false) {
                    $tmp = str_replace('%%','%', $tmp);
                }
                if (trim($tmp, ' %') != '') {                
                    $strings[] = $tmp;
                }
            }
        }
        */

        if (count($strings) == 0) {
            $strings[] = '%';
        }

        return $strings;
    }

    /**
     * Get our REQUEST array
     *
     * @access protected
     * @return array $request
     */
    protected function getRequestArray() {
        $request = $this->request;
        if ((count($request) == 0) && (isset($_REQUEST)) && (is_array($_REQUEST))) {
            $request = $_REQUEST;
        }
        return $request;
    }


    protected function addJsFunctionCode($jsFunctionName, $index) {
        $code = '';

        if ($jsFunctionName == 'onSearchTagChange') {
            $code = <<<__END__

<script type='text/javascript'>
function onSearchTagChange() {
    $('#search_text_$index').val('');
    $('#search_filter_$index').click(); //Submit the form by simulating a click on 'Search'
}
</script>

__END__;
            
        }

        return $code;
    }

}


