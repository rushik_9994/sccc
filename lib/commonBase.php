<?php
/**
 * Centralized authentication checking
 */

global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));
require_once(ROOT_PATH . '/cfg/config.php');

require_once(ROOT_PATH . '/lib/SqlSchemaCheck.class.php');
require_once(ROOT_PATH . '/lib/EzPdo.php');
require_once(ROOT_PATH . '/lib/commonDisplay.php');
require_once(ROOT_PATH . '/lib/commonErrorHandling.php');
require_once(ROOT_PATH . '/lib/SecurityFunctions.php');

require_once(ROOT_PATH . '/lib/commonFunctions.php');
