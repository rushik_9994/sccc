<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialGroup extends BaseClass {

	public $TrialGroupKeyID; //int(10)
	public $TrialKeyID; //int(10)
	public $TrialGroupTitle; //varchar(250)
	public $TrialGroupDescription; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialGroup($TrialKeyID,$TrialGroupTitle,$TrialGroupDescription){
		$this->TrialKeyID = $TrialKeyID;
		$this->TrialGroupTitle = $TrialGroupTitle;
		$this->TrialGroupDescription = $TrialGroupDescription;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("SELECT * FROM TrialGroup WHERE TrialGroupKeyID = ".$this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->TrialGroupKeyID = $row["TrialGroupKeyID"];
			$this->TrialKeyID = $row["TrialKeyID"];
			$this->TrialGroupTitle = $row["TrialGroupTitle"];
			$this->TrialGroupDescription = $row["TrialGroupDescription"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param string $key
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key, $key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialGroup', $key, $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE TrialGroup SET\n".
                "TrialKeyID = ".            $this->SqlQuote($this->TrialKeyID).",\n".
                "TrialGroupTitle = ".       $this->SqlQuote($this->TrialGroupTitle).",\n".
                "TrialGroupDescription = ". $this->SqlQuote($this->TrialGroupDescription)."\n".
                "WHERE TrialGroupKeyID = ". $this->SqlQuote($this->TrialGroupKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialGroup','TrialGroupKeyID', $this->TrialGroupKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO TrialGroup (TrialKeyID, TrialGroupTitle, TrialGroupDescription) VALUES (\n".
                $this->SqlQuote($this->TrialKeyID).",\n".
                $this->SqlQuote($this->TrialGroupTitle).",\n".
                $this->SqlQuote($this->TrialGroupDescription).')';
            $this->TrialGroupKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialGroup','TrialGroupKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $where
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysWhereOrderBy($where, $column, $order)
    {
		$keys = array();

		//where not quoted ++++++++
        $quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialGroupKeyID FROM TrialGroup WHERE $where order by $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialGroupKeyID"];
            $i++;
        }
    	return $keys;
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

		$quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialGroupKeyID FROM TrialGroup ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialGroupKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param string $selected
     * @param string $where
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected, $where)
	{
	    global $gSqlDebugMode, $gSqlAddKeysToOptions, $gSqlSelectExtraText;

	    if (!isset($gSqlDebugMode))        { $gSqlDebugMode=0; }
	    if (!isset($gSqlAddKeysToOptions)) { $gSqlAddKeysToOptions=0; }
	    if (!isset($gSqlSelectExtraText))  { $gSqlSelectExtraText=''; }

        $select    = '<select name="TrialGroupKeyID" id="TrialGroupKeyID">'."\n";
        $extraText = ($gSqlSelectExtraText) ? ' ['.$gSqlSelectExtraText.']' : '';
        $select   .= '<option value="">Select...'.$extraText.'</option>'."\n";
        $sql       = "SELECT TrialGroupKeyID, TrialGroupTitle FROM TrialGroup $where ORDER BY TrialGroupTitle";
		$result    = $this->connection->RunQuery($sql);

        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $escExtraText=($gSqlAddKeysToOptions) ? htmlentities(' ['.$row['TrialGroupKeyID'].']') : '';
            $escTrialGroupKeyID = htmlentities($row["TrialGroupKeyID"]);
            $escTrialGroupTitle = htmlentities($row["TrialGroupTitle"]);
            if ($row["TrialGroupKeyID"] == $selected) {
                 $select .= "<option value=\"$escTrialGroupKeyID\" selected>$escTrialGroupTitle$escExtraText</option>\n";
            } else {
                $select .= "<option value=\"$escTrialGroupKeyID\">$escTrialGroupTitle$escExtraText</option>\n";
            }
        }
        $select .= '</select>'."\n";

        if ($gSqlDebugMode) {
            $select .= "<div style='font-size:8pt;color:blue;'>\n".
                       basename(__FILE__)." used SQL<br />".
                       "<pre>$sql</pre>\n".
                       "</div>";
        }
        
	    return $select;
	}

	/**
	 * @return null|int $TrialGroupKeyID
	 */
	public function getTrialGroupKeyID()
    {
		return $this->TrialGroupKeyID;
	}

	/**
	 * @return null|int $TrialKeyID
	 */
	public function getTrialKeyID()
    {
		return $this->TrialKeyID;
	}

	/**
	 * @return null|string $TrialGroupTitle
	 */
	public function getTrialGroupTitle()
    {
		return $this->TrialGroupTitle;
	}

	/**
	 * @return null|string $TrialGroupDescription
	 */
	public function getTrialGroupDescription()
    {
		return $this->TrialGroupDescription;
	}

	/**
	 * @param int $TrialGroupKeyID
	 */
	public function setTrialGroupKeyID($TrialGroupKeyID)
    {
		$this->TrialGroupKeyID = $TrialGroupKeyID;
	}

	/**
	 * @param int $TrialKeyID
	 */
	public function setTrialKeyID($TrialKeyID)
    {
		$this->TrialKeyID = $TrialKeyID;
	}

	/**
	 * @param string $TrialGroupTitle
	 */
	public function setTrialGroupTitle($TrialGroupTitle)
    {
		$this->TrialGroupTitle = $TrialGroupTitle;
	}

	/**
	 * @param string $TrialGroupDescription
	 */
	public function setTrialGroupDescription($TrialGroupDescription)
    {
		$this->TrialGroupDescription = $TrialGroupDescription;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //global $callingEvent;
        $validationMessage = '';

        //Validate
        do {
            $val = ((isset($this->TrialGroupKeyID)) ? $this->TrialGroupKeyID : null);  //vld: TrialGroup.TrialGroupKeyID
            if ($validationMessage = validateField($val,'TrialGroupKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyID)) ? $this->TrialKeyID : null);  //vld: TrialGroup.TrialKeyID
            if ($validationMessage = validateField($val,'TrialKeyID','positiveint',__CLASS__)) { break; }

            //public $TrialGroupDescription; //varchar(250)   vld: TrialGroup.TrialGroupDescription

        } while(false);

        return $validationMessage;
    }

}