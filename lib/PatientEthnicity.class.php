<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class PatientEthnicity extends BaseClass {

	public $PatientEthnicityKeyID; //int(2)
	public $PatientEthnicityText; //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don?t forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New(); 
     *
     */
    /* public function New_PatientEthnicity($PatientEthnicityText){
		$this->PatientEthnicityText = $PatientEthnicityText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("SELECT * FROM PatientEthnicity WHERE PatientEthnicityKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
		    $found = 1;
			$this->PatientEthnicityKeyID = $row["PatientEthnicityKeyID"];
			$this->PatientEthnicityText = $row["PatientEthnicityText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'PatientEthnicity', 'PatientEthnicityKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE PatientEthnicity SET\n".
                "PatientEthnicityText = ".       $this->SqlQuote($this->PatientEthnicityText)."\n".
                "WHERE PatientEthnicityKeyID = ".$this->SqlQuote($this->PatientEthnicityKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'PatientEthnicity','PatientEthnicityKeyID', $this->PatientEthnicityKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO PatientEthnicity (PatientEthnicityText) VALUES (\n".
                $this->SqlQuote($this->PatientEthnicityText).')';
            $this->PatientEthnicityKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'PatientEthnicity','PatientEthnicityKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn  = trim($this->connection->SqlQuote($column), "'");
        $quOrder   = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT PatientEthnicityKeyID FROM PatientEthnicity ORDER BY $quColumn $quOrder");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["PatientEthnicityKeyID"];
            $i++;
        }
    	return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected){
        $select = '<select name="patient[PatientEthnicity]" id="PatientEthnicityKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT PatientEthnicityKeyID, PatientEthnicityText FROM PatientEthnicity order by PatientEthnicityKeyID");

        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escPatientEthnicityKeyID = htmlentities($row["PatientEthnicityKeyID"]);
            $escPatientEthnicityText  = htmlentities($row["PatientEthnicityText"]);
            if ($row["PatientEthnicityKeyID"] == $selected) {
                 $select .= "<option value=\"$escPatientEthnicityKeyID\" selected>$escPatientEthnicityText</option>\n";
            }else{
                $select .= "<option value=\"$escPatientEthnicityKeyID\">$escPatientEthnicityText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return int $PatientEthnicityKeyID
	 */
	public function getPatientEthnicityKeyID()
    {
		return $this->PatientEthnicityKeyID;
	}

	/**
	 * @return string $PatientEthnicityText
	 */
	public function getPatientEthnicityText()
    {
		return $this->PatientEthnicityText;
	}

	/**
	 * @param int $PatientEthnicityKeyID
	 */
	public function setPatientEthnicityKeyID($PatientEthnicityKeyID)
    {
		$this->PatientEthnicityKeyID = $PatientEthnicityKeyID;
	}

	/**
	 * @param string $PatientEthnicityText
	 */
	public function setPatientEthnicityText($PatientEthnicityText)
    {
		$this->PatientEthnicityText = $PatientEthnicityText;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->PatientEthnicityKeyID)) ? $this->PatientEthnicityKeyID : null);  //vld: PatientEthnicity.PatientEthnicityKeyID
            if ($validationMessage = validateField($val,'PatientEthnicityKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->PatientEthnicityText)) ? $this->PatientEthnicityText : null);  //vld: PatientEthnicity.PatientEthnicityText
            if ($validationMessage = validateField($val,'PatientEthnicityText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

