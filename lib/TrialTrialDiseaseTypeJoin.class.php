<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialTrialDiseaseTypeJoin extends BaseClass {

	public $TrialTrialDiseaseTypeKeyID; //int(10)
	public $TrialKeyID; //int(10)
	public $TrialDiseaseTypeKeyID; //int(10)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialTrialDiseaseTypeJoin($TrialKeyID,$TrialDiseaseTypeKeyID){
		$this->TrialKeyID = $TrialKeyID;
		$this->TrialDiseaseTypeKeyID = $TrialDiseaseTypeKeyID;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("SELECT * FROM TrialTrialDiseaseTypeJoin WHERE TrialTrialDiseaseTypeKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
		    $found = 1;
			$this->TrialTrialDiseaseTypeKeyID = $row["TrialTrialDiseaseTypeKeyID"];
			$this->TrialKeyID = $row["TrialKeyID"];
			$this->TrialDiseaseTypeKeyID = $row["TrialDiseaseTypeKeyID"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param string $key
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key, $key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialTrialDiseaseTypeJoin', $key, $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE TrialTrialDiseaseTypeJoin SET\n".
                "TrialKeyID = ".                      $this->SqlQuote($this->TrialKeyID).",\n".
                "TrialDiseaseTypeKeyID = ".           $this->SqlQuote($this->TrialDiseaseTypeKeyID)."\n".
                "WHERE TrialTrialDiseaseTypeKeyID = ".$this->SqlQuote($this->TrialTrialDiseaseTypeKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialTrialDiseaseTypeJoin','TrialTrialDiseaseTypeKeyID', $this->TrialTrialDiseaseTypeKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO TrialTrialDiseaseTypeJoin (TrialKeyID, TrialDiseaseTypeKeyID) VALUES (\n".
                $this->SqlQuote($this->TrialKeyID).",\n".
                $this->SqlQuote($this->TrialDiseaseTypeKeyID).')';
            $this->TrialTrialDiseaseTypeKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialTrialDiseaseTypeJoin','TrialTrialDiseaseTypeKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys with where key = $key order by $column -> name of column $order -> desc or acs
     *
     * @param  string $where
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysWhereOrderBy($where, $column, $order)
    {
		$keys = array();

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialDiseaseTypeKeyID FROM TrialTrialDiseaseTypeJoin WHERE $where ORDER BY $column $order");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["TrialDiseaseTypeKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialDiseaseTypeKeyID FROM TrialTrialDiseaseTypeJoin ORDER BY $column $order");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["TrialTrialDiseaseTypeKeyID"];
            $i++;
        }
	    return $keys;
	}

	/**
	 * @return null|int $TrialTrialDiseaseTypeKeyID
	 */
	public function getTrialTrialDiseaseTypeKeyID()
    {
		return $this->TrialTrialDiseaseTypeKeyID;
	}

	/**
	 * @return null|int $TrialKeyID
	 */
	public function getTrialKeyID()
    {
		return $this->TrialKeyID;
	}

	/**
	 * @return null|int $TrialDiseaseTypeKeyID
	 */
	public function getTrialDiseaseTypeKeyID()
    {
		return $this->TrialDiseaseTypeKeyID;
	}

	/**
	 * @param int $TrialTrialDiseaseTypeKeyID
	 */
	public function setTrialTrialDiseaseTypeKeyID($TrialTrialDiseaseTypeKeyID)
    {
		$this->TrialTrialDiseaseTypeKeyID = $TrialTrialDiseaseTypeKeyID;
	}

	/**
	 * @param int $TrialKeyID
	 */
	public function setTrialKeyID($TrialKeyID)
    {
		$this->TrialKeyID = $TrialKeyID;
	}

	/**
	 * @param int $TrialDiseaseTypeKeyID
	 */
	public function setTrialDiseaseTypeKeyID($TrialDiseaseTypeKeyID)
    {
		$this->TrialDiseaseTypeKeyID = $TrialDiseaseTypeKeyID;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        global $callingEvent;
        $validationMessage = '';

        //Validate
        do {
            $val = ((isset($this->TrialTrialDiseaseTypeKeyID)) ? $this->TrialTrialDiseaseTypeKeyID : null);  //vld: TrialTrialDiseaseTypeJoin.TrialTrialDiseaseTypeKeyID
            if ($validationMessage = validateField($val,'TrialTrialDiseaseTypeKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyID)) ? $this->TrialKeyID : null);  //vld: TrialTrialDiseaseTypeJoin.TrialKeyID
            $rulePrefix = ((isset($callingEvent)) && ($callingEvent == 'trialAddEdit')) ? 'nullok;' : '';
            if ($validationMessage = validateField($val,'TrialKeyID',$rulePrefix.'positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialDiseaseTypeKeyID)) ? $this->TrialDiseaseTypeKeyID : null);  //vld: TrialTrialDiseaseTypeJoin.TrialDiseaseTypeKeyID
            if ($validationMessage = validateField($val,'TrialDiseaseTypeKeyID','positiveint',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}