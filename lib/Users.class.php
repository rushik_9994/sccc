<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 29-04-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class Users extends BaseClass {

	public $UserKeyID; //int(10)
	public $UserName; //varchar(250)
	public $UserPassword; //varchar(250)
	public $UserLogins; //varchar(250)
	public $UserTypeKeyID; //int(10)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_Users($UserName,$UserPassword,$UserLogins,$UserTypeKeyID){
		$this->UserName = $UserName;
		$this->UserPassword = $UserPassword;
		$this->UserLogins = $UserLogins;
		$this->UserTypeKeyID = $UserTypeKeyID;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param string $key_row
     * @throws \Exception
     */
	public function Load_from_username($key_row)
    {
        $quKeyRow = trim($this->connection->SqlQuote($key_row), "'");

		$result = $this->connection->RunQuery("Select * from Users where UserName = \"$quKeyRow\" ");
		while($row = $result->fetch_array(MYSQLI_ASSOC)) {
			$this->UserKeyID = $row["UserKeyID"];
			$this->UserName = $row["UserName"];
			$this->UserPassword = $row["UserPassword"];
			$this->UserLogins = $row["UserLogins"];
			$this->UserTypeKeyID = $row["UserTypeKeyID"];
		}
	}

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
		$result = $this->connection->RunQuery("SELECT * FROM Users WHERE UserKeyID = ".$this->SqlQuote($key_row));

		$found = 0;
		while($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->UserKeyID = $row["UserKeyID"];
			$this->UserName = $row["UserName"];
			$this->UserPassword = $row["UserPassword"];
			$this->UserLogins = $row["UserLogins"];
			$this->UserTypeKeyID = $row["UserTypeKeyID"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Users', 'UsersKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE Users SET\n".
                "UserName = ".          $this->SqlQuote($this->UserName).",\n".
                "UserPassword = ".      $this->SqlQuote($this->UserPassword).",\n".
                "UserLogins = ".        $this->SqlQuote($this->UserLogins).",\n".
                "UserTypeKeyID = ".     $this->SqlQuote($this->UserTypeKeyID)."\n".
                "WHERE UserKeyID = ".$this->SqlQuote($this->UserKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Users','UserKeyID', $this->UserKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO Users (UserName, UserPassword, UserLogins, UserTypeKeyID) VALUES (\n".
                $this->SqlQuote($this->UserName).",\n".
                $this->SqlQuote($this->UserPassword).",\n".
                $this->SqlQuote($this->UserLogins).",\n".
                $this->SqlQuote($this->UserTypeKeyID).')';
            $this->UserKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Users','UserKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

		$i = 0;
		$result = $this->connection->RunQuery("SELECT UserKeyID from Users order by $column $order");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["UserKeyID"];
            $i++;
        }
    	return $keys;
	}

	/**
	 * @return null|int $UserKeyID
	 */
	public function getUserKeyID()
    {
		return $this->UserKeyID;
	}

	/**
	 * @return null|string $UserName
	 */
	public function getUserName()
    {
		return $this->UserName;
	}

	/**
	 * @return null|string $UserPassword
	 */
	public function getUserPassword()
    {
		return $this->UserPassword;
	}

	/**
	 * @return null|string $UserLogins
	 */
	public function getUserLogins()
    {
		return $this->UserLogins;
	}

	/**
	 * @return null|int $UserTypeKeyID
	 */
	public function getUserTypeKeyID()
    {
		return $this->UserTypeKeyID;
	}

	/**
	 * @param int $UserKeyID
	 */
	public function setUserKeyID($UserKeyID)
    {
		$this->UserKeyID = $UserKeyID;
	}

	/**
	 * @param string $UserName
	 */
	public function setUserName($UserName)
    {
		$this->UserName = $UserName;
	}

	/**
	 * @param string $UserPassword
	 */
	public function setUserPassword($UserPassword)
    {
		$this->UserPassword = $UserPassword;
	}

	/**
	 * @param string $UserLogins
	 */
	public function setUserLogins($UserLogins)
    {
		$this->UserLogins = $UserLogins;
	}

	/**
	 * @param int $UserTypeKeyID
	 */
	public function setUserTypeKeyID($UserTypeKeyID)
    {
		$this->UserTypeKeyID = $UserTypeKeyID;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        $validationMessage = '';

        //Validate
        do {
            $val = ((isset($this->UserKeyID)) ? $this->UserKeyID : null);  //vld: Users.UserKeyID
            if ($validationMessage = validateField($val,'UserKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->UserName)) ? $this->UserName : null);  //vld: Users.UserName
            if ($validationMessage = validateField($val,'UserName','nestring',__CLASS__)) { break; }

            $val = ((isset($this->UserPassword)) ? $this->UserPassword : null); //vld: Users.UserPassword
            if ($validationMessage = validateField($val,'UserPassword','nestring',__CLASS__)) { break; }

            $val = ((isset($this->UserLogins)) ? $this->UserLogins : null);  //vld: Users.UserLogins
            if ($validationMessage = validateField($val,'UserLogins','zeroorpositiveint',__CLASS__)) { break; }

            $val = ((isset($this->UserTypeKeyID)) ? $this->UserTypeKeyID : null);  //vld: Users.UserTypeKeyID
            if ($validationMessage = validateField($val,'UserTypeKeyID','positiveint',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

