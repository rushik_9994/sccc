<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


/**
 * Class Comments functionally similar to a model class
 */
class Comments extends BaseClass {

	public $CommentKeyID; //int(10)
	public $TimestampCreate; //timestamp
	public $TrialKeyID; //int(10)
	public $SiteKeyID; //int(10)
	public $CompKeyID; //int(10)
	public $Comment; //longtext


    /**
     * Class constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /*	public function New_Comments($TimestampCreate,$TrialKeyID,$SiteKeyID,$CompKeyID,$Comment)
    {
		$this->TimestampCreate = $TimestampCreate;
		$this->TrialKeyID = $TrialKeyID;
		$this->SiteKeyID = $SiteKeyID;
		$this->CompKeyID = $CompKeyID;
		$this->Comment = $Comment;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
	    //+++++++++ Todo: $key_row is empty. Fix the problem further up.
        $sql = "SELECT * FROM Comments WHERE CommentKeyID = ".$this->SqlQuote($key_row);
        $result = $this->connection->RunQuery($sql);

        $found = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->CommentKeyID = $row["CommentKeyID"];
            $this->TimestampCreate = $row["TimestampCreate"];
            $this->TrialKeyID = $row["TrialKeyID"];
            $this->SiteKeyID = $row["SiteKeyID"];
            $this->CompKeyID = $row["CompKeyID"];
            $this->Comment = $row["Comment"];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param string $key
     * @param mixed $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key, $key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Comments', $key, $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            TrackChanges::trackBeforeUpdate($this->connection, 'Comments', 'CommentKeyID', $this->CommentKeyID);
            $sql =
                "UPDATE Comments SET\n".
                "TrialKeyID = ".        $this->SqlQuote($this->TrialKeyID)."\n".
                "SiteKeyID = ".         $this->SqlQuote($this->SiteKeyID).",\n".
                "CompKeyID = ".         $this->SqlQuote($this->CompKeyID).",\n".
                "Comment = ".           $this->SqlQuote($this->Comment)."\n".
                "WHERE CommentKeyID = ".$this->SqlQuote($this->CommentKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Comments','CommentKeyID', $this->CommentKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO Comments (TrialKeyID, SiteKeyID, CompKeyID, Comment) VALUES (\n".
                $this->SqlQuote($this->TrialKeyID).",\n".
                $this->SqlQuote($this->SiteKeyID).",\n".
                $this->SqlQuote($this->CompKeyID).",\n".
                $this->SqlQuote($this->Comment).')';
            $this->CommentKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Comments','CommentKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $where
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysWhereOrderBy($where, $column, $order)
    {
		$keys = array(); $i = 0;
		$result = $this->connection->RunQuery("SELECT CommentKeyID from Comments WHERE $where order by $column $order");
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $keys[$i] = $row["CommentKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array(); $i = 0;
		$result = $this->connection->RunQuery("SELECT CommentKeyID from Comments order by $column $order");
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["CommentKeyID"];
            $i++;
        }
    	return $keys;
	}

	/**
	 * @return int $CommentKeyID
	 */
	public function getCommentKeyID()
    {
		return $this->CommentKeyID;
	}

	/**
	 * @return null|string $TimestampCreate
	 */
	public function getTimestampCreate()
    {
		return $this->TimestampCreate;
	}

	/**
	 * @return int $TrialKeyID
	 */
	public function getTrialKeyID()
    {
		return $this->TrialKeyID;
	}

	/**
	 * @return int $SiteKeyID
	 */
	public function getSiteKeyID()
    {
		return $this->SiteKeyID;
	}

	/**
	 * @return int $CompKeyID
	 */
	public function getCompKeyID()
    {
		return $this->CompKeyID;
	}

	/**
	 * @return null|string $Comment
	 */
	public function getComment()
    {
		return $this->Comment;
	}

	/**
	 * @param int $CommentKeyID
	 */
	public function setCommentKeyID($CommentKeyID)
    {
		$this->CommentKeyID = $CommentKeyID;
	}

	/**
	 * @param string $TimestampCreate
	 */
	public function setTimestampCreate($TimestampCreate)
    {
		$this->TimestampCreate = $TimestampCreate;
	}

	/**
	 * @param int $TrialKeyID
	 */
	public function setTrialKeyID($TrialKeyID)
    {
		$this->TrialKeyID = $TrialKeyID;
	}

	/**
	 * @param int $SiteKeyID
	 */
	public function setSiteKeyID($SiteKeyID)
    {
		$this->SiteKeyID = $SiteKeyID;
	}

	/**
	 * @param int $CompKeyID
	 */
	public function setCompKeyID($CompKeyID)
    {
		$this->CompKeyID = $CompKeyID;
	}

	/**
	 * @param null|string $Comment
	 */
	public function setComment($Comment)
    {
		$this->Comment = $Comment;
	}


    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            //public $TimestampCreate; //timestamp vld: Comments. TimestampCreate

            $val = ((isset($this->CommentKeyID)) ? $this->CommentKeyID : null); //vld: Comments.CommentKeyID
            if ($validationMessage = validateField($val,'CommentKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialKeyID)) ? $this->TrialKeyID : null); //vld: Comments.TrialKeyID
            if ($validationMessage = validateField($val,'TrialKeyID','nullok;zeroorpositiveint',__CLASS__)) { break; }

            $val = ((isset($this->SiteKeyID)) ? $this->SiteKeyID : null); //vld: Comments.SiteKeyID
            if ($validationMessage = validateField($val,'SiteKeyID','nullok;zeroorpositiveint',__CLASS__)) { break; }

            $val = ((isset($this->CompKeyID)) ? $this->CompKeyID : null); //vld: Comments.CompKeyID
            if ($validationMessage = validateField($val,'CompKeyID','nullok;zeroorpositiveint',__CLASS__)) { break; }

            //public $Comment; //longtext  //vld: Comments.Comment

        } while(false);

        return $validationMessage;
    }

}

