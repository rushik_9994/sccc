<?php
/**
 * Thin wrapper around \PDO offering some nice additional functions
 *
 * USAGE EXAMPLE
 *
 *    // CREATION-DIRECT: either direct creation
 *    $dsn      = sprintf('mysql:host=%s;dbname=%s', $config['host'], $config['db']);
 *    $satEzPdo = new \EzPdo($dsn, $username, $password);
 *
 *    // CREATION-PIMPLE: or get it via Pimple (SAT only)
 *    $satEzPdo = \Container\Factory::getInstance()->satEzPdo();
 *
 *    // FETCH: fetch some rows
 *    $sql      = "SELECT * FROM defaultsvc WHERE did=:did AND spid=:friendlySpid";
 *    $data     = array('did' => $did, 'friendlySpid' => SPID_FRIENDLY_TR69_ENABLED);
 *    $options  = array('debug' => 0);   //set to 1 for debug mode or call $this->setDebugMode(1|2)
 *    $rows     = $satEzPdo->select($sql, $data, $options);
 *    $error    = (count($rows)==0) ? $satEzPdo->getError() : '';
 */

use \PDO;
use \PDOException;

//require_once(ROOT_PATH . '/lib/commonErrorHandling.php');


/**
 * EzPdo class (thin wrapper around \PDO)
 */
class EzPdo extends \PDO
{

    /**
     * @var array $connectionDetails
     * Remembers the connection details so we can reconnect if needed
     */
    protected $connectionDetails = array();

    /**
     * @var array $savedRows
     * Full result set of the last selectOne() call
     */
    protected $savedRows          = array();

    /**
     * @var string $lastError
     * Saves the last error message
     */
    protected $lastError          = '';

    /**
     * @var null|object $lastException
     * Saves the last exception
     */
    protected $lastException      = null;

    /**
     * @var array $lastAction
     * Remembers details about the last action
     */
    protected $lastAction         = array();

    /**
     * @var int $debugBufferActive
     * A flag, if set then all debug printing will be saved in a member variable. Use getDebugBuffer() and clearDebugBuffer().
     */
    protected $debugBufferActive = 0;

    /**
     * @var string $debugBuffer
     * String buffer for generated debug code (if $debugBufferActive is set)
     */
    protected $debugBuffer        = '';

    /**
     * @var array $options
     * Hold our current options
     */
    protected $options            = array('debugMode' => 0);


    //################################################################################
    //#########################  MAIN PUBLIC FUNCTIONS  ##############################
    //################################################################################

    /**
     * Constructor
     *
     * @access public
     * @param  string $dsn
     * @param  string $username
     * @param  string $password
     * @param  array  $options optional
     * @throws \Exception
     */
    public function __construct($dsn, $username, $password, $options=array())
    {
        $this->setError();
        try {
            $ourDefaultOptions = array(\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION);
            $options           = array_merge($ourDefaultOptions, $options);

            $this->connectionDetails = array('dsn' => $dsn, 'username'=>$username, 'password'=>$password, 'options'=>$options);

            parent::__construct($dsn, $username, $password, $options);
            if (!isset($options[\PDO::ATTR_ERRMODE])) {
                $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }

        } catch(PDOException $e) {
            //$this->handleException($e, 0);
            throw $e;
        }
    }


    /**
     * Get options
     *
     * @return mixed $currentOptions
     */
    public function getOptions()
    {
        return $this->options;
    }


    /**
     * Execute a select statement
     *
     * @access public
     * @param  string $sql                   should contain place holders like :userid, :treeid,...
     * @param  array  $data optional          should look like this array(':userid'=>reinhard)
     * @param  array  $sqlOptions optional
     * @return array  $rows                  empty on error, use getError to get error string
     */
    public function select($sql='', $data=array(), $sqlOptions=array())
    {
        try {
            list($sql, $data) = $this->startQuery(__FUNCTION__, $sql, $data);

            $sth = parent::prepare($sql);
            $sth->execute($data);
            $sth->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $sth->fetchAll();
            $sth->closeCursor();

        } catch (PDOException $e) {
            $rows = $this->handleException($e, array(), $sql);
        }

        return $this->endQuery($rows);
    }

    /**
     * Execute an insert statement
     *
     * @param  string $sql                    should contain place holders like :userid, :treeid,...
     * @param  array  $data optional          should look like this array(':userid'=>reinhard)
     * @return int $insertId                  0 if no insert was detected
     */
    public function insert($sql='', $data=array())
    {
        $insertedId = 0;
        try {
            list($sql, $data) = $this->startQuery(__FUNCTION__, $sql, $data);

            $sth    = parent::prepare($sql);
            $result = $sth->execute($data);
            if ($result !== false) {
                $insertedId = $this->lastInsertId();
            }

        } catch (\Exception $e) {
            $insertedId = $this->handleException($e, 0, $sql);
        }

        return $insertedId;
    }


    /**
     * Execute an update statement
     *
     * @param  string $sql                        should contain place holders like :userid, :treeid,...
     * @param  array  $data optional              should look like this array(':userid'=>reinhard)
     * @param  array  $sqlOptions optional
     * @return int    $affected                   num rows affected   (0 on error)
     */
    public function update($sql='', $data=array(), $sqlOptions=array())
    {
        try {
            list($sql, $data) = $this->startQuery(__FUNCTION__, $sql, $data);

            $sth = parent::prepare($sql);
            $sth->execute($data);
            $affected = $sth->rowCount();

        } catch (PDOException $e) {
            $affected = $this->handleException($e, 0, $sql);
        }

        return $this->endQuery($affected);
    }


    /**
     * Execute a delete statement
     *
     * @param string $sql                      should contain place holders like :userid, :treeid,...
     * @param array $data optional             should look like this array(':userid'=>reinhard)
     * @param array $sqlOptions optional
     * @return int $deletedRows                num rows deleted
     */
    public function delete($sql='', $data=array(), $sqlOptions=array())
    {
        try {
            list($sql, $data) = $this->startQuery(__FUNCTION__, $sql, $data);

            $sth = parent::prepare($sql);
            $sth->execute($data);
            $affected = $sth->rowCount();

        } catch (PDOException $e) {
            $affected = $this->handleException($e, 0, $sql);
        }

        return $this->endQuery($affected);
    }


    /**
     * Executes any statement
     *
     * @param  string $sql                    should contain place holders like :userid, :treeid,...
     * @param  array  $data optional          should look like this array(':userid'=>reinhard)
     * @param  array  sqlOptions optional
     * @return mixed  $mixed
     */
    public function anyquery($sql='', $data=array(), $sqlOptions=array())
    {
        try {
            list($sql, $data) = $this->startQuery(__FUNCTION__, $sql, $data);

            $sth = parent::prepare($sql);
            $result = $sth->execute($data);

        } catch (PDOException $e) {
            $result = $this->handleException($e, 0, $sql);
        }

        return $this->endQuery($result);
    }


    /**
     * Begin a transaction
     *
     * @param  array sqlOptions optional
     * @return int   $result                     0 = error
     */
    public function beginTransaction($sqlOptions=array())
    {
        try {
            $this->startQuery(__FUNCTION__, 'begin transaction', array());

            parent::beginTransaction();
            $retval = 1;

        } catch (\Exception $e) {
            $retval = $this->handleException($e, 0, 'begin transaction');
        }

        return $this->endQuery($retval);
    }


    /**
     * Commit a transaction
     *
     * @param  array sqlOptions optional
     * @return int   $result                     0 = error
     */
    public function commit($sqlOptions=array())
    {

        try {
            $this->startQuery(__FUNCTION__, 'commit', array());

            parent::commit();
            $retval = 1;

        } catch (\Exception $e) {
            $retval = $this->handleException($e, 0, 'commit');
        }

        return $this->endQuery($retval);
    }


    /**
     * Rollback a transaction
     *
     * @param  array sqlOptions optional
     * @return int   $result                     0 = error
     */
    public function rollBack($sqlOptions=array())
    {
        try {
            $this->startQuery(__FUNCTION__, 'roll back', array());

            parent::rollBack();
            $retval = 1;

        } catch (\Exception $e) {
            $retval = $this->handleException($e, 0, 'roll back');
        }

        return $this->endQuery($retval);
    }


    public function insertArrayIntoTable($table, $rows=array(), $aliases=array(), $sqlOptions=array())
    {
        $retval = 0;
        try {
            $this->startQuery(__FUNCTION__, '', array());

            $quTable = trim($this->quote($table), "'");

            foreach($rows as $row) {
                foreach ($row as $key => $val) {
                    $key = (isset($aliases[$key])) ? $aliases[$key] : $key;
                    if (!$key) {
                        continue;
                    }
                    $quFields[] = "`$key`";
                    $quValues[] = $this->quote($val);
                }

                $quFieldString = implode(",", $quFields);
                $quValueString = implode(",", $quValues);
                $sql = "INSERT INTO `$quTable` ($quFieldString) VALUES ($quValueString)";

                $insertedId = 0;
                $this->setError();
                $sth = parent::prepare($sql);
                $result = $sth->execute();
                if ($result !== false) {
                    $insertedId = $this->lastInsertId();
                }
            }

        } catch (\Exception $e) {
            $retval = $this->handleException($e, 0, '');
        }

        return $this->endQuery($retval);
    }


    //################################################################################
    //######################  AUXILLARY PUBLIC FUNCTIONS  ############################
    //################################################################################

    /**
     * Either returns    NULL   or 'quotedString'
     *
     * @param  string $val optional
     * @return string $quVal
     */
    public function quotedReady($val=null)
    {
        if (!isset($val)) {
            return 'NULL';
        } else {
            return $this->quote($val, 0);
        }
    }

    /**
     * Quotes a string
     *
     * @param  string $val
     * @param  int    $removeQuotes optional        defaults to 0
     * @return string $quVal
     */
    public function quote($val='', $removeQuotes=0)
    {
        $this->setError();

        try {
            //Quote the value
            $result = parent::quote($val);

            //Optionally remove the surrounding single quotes
            if ($removeQuotes) {
                if ((strlen($result) > 1) && (substr($result, 0, 1) == "'") && (substr($result, -1, 1) == "'")) {
                    $result = substr($result, 1);
                    $result = substr($result, 0, (strlen($result)-1));
                }
            }

        } catch(PDOException $e) {
            $result = addslashes($val);
            $this->lastException = $e;
            $this->lastError     = $e->getMessage();
        }

        return $result;
    }


    /**
     * Set options (which only affect the non-native PDO functions!)
     *
     * @param  array $options optional
     * @param  int   $clearAllOptions optional   (defaults to 0)
     * @return array $currentOptions
     */
    public function setOptions(array $options=array(), $clearAllOptions=0)
    {
        //Do we need to clear all our options?
        if ($clearAllOptions) {
            $this->options = array();
        }

        //Merge our new options in
        $this->options = array_merge($this->options, $options);

        //Handle debugMode-related options separately if we got them
        if (isset($options['debugMode'])) {
            if (isset($options['debugBufferActive'])) {
                $this->setDebugMode($options['debugMode'], $options['debugBufferActive']);
            } else {
                $this->setDebugMode($options['debugMode']);
            }
        }

        return $this->getOptions();
    }


    /**
     * Sets our debugMode
     *
     * @param int $level optional                 0=debugMode off (default)  1=debugMode on   2=debugMode high
     * @param int $debugBufferActive optional     0=echo to STDOUT           1=append to $this->debugBuffer
     * @return int $debugMode
     */
    public function setDebugMode($level=null, $debugBufferActive=null)
    {
        if ((isset($level)) && (is_numeric($level)) && ($level >= 0)) {
            $this->options['debugMode'] = $level;
        } else {
            $this->options['debugMode'] = 0;
        }

        if ((isset($debugBufferActive)) && (is_numeric($debugBufferActive)) && ($debugBufferActive >= 0)) {
            $this->debugBufferActive = $debugBufferActive;
            if ($this->debugBufferActive) {
                $this->clearDebugBuffer();
            }
        }

        return $this->getDebugMode();
    }


    /**
     * Gets our debugMode
     *
     * @return int $debugMode
     */
    public function getDebugMode()
    {
        return (isset($this->options['debugMode'])) ? $this->options['debugMode'] : 0;
    }


    /**
     * Clears the debugBuffer
     *
     * @param int $alsoClearDebugBuffer optional     Defaults to 0. If 1 then clearDebugBuffer() is called
     * @return string $debugBuffer
     */
    public function getDebugBuffer($alsoClearDebugBuffer=0)
    {
        $debugBuffer = $this->debugBuffer;

        if ((isset($alsoClearDebugBuffer)) && ($alsoClearDebugBuffer)) {
            $this->clearDebugBuffer();
        }

        return $debugBuffer;
    }


    /**
     * Clears the debugBuffer
     */
    public function clearDebugBuffer()
    {
        $this->debugBuffer = '';
    }


    /**
     * Return the row count of the last selectOne call
     *
     * @access public
     * @return int $count
     */
    public function getSavedCount()
    {
        return count($this->savedRows);
    }


    /**
     * Return the result rows of the last selectOne call
     *
     * @access public
     * @return mixed $rows
     */
    public function getSavedRows()
    {
        return $this->savedRows;
    }


    /**
     * Get the last error variable
     *
     * @access public
     * @param  string $errorOverride optional      If (given) AND ($this->lastError == '') then this is returned
     * @return string $lastError
     */
    public function getError($errorOverride='')
    {
        if (($errorOverride) && ($this->lastError == '')) {
            return $errorOverride;
        } else {
            return $this->lastError;
        }
    }


    /**
     * Get the last thrown Exception
     *
     * @access public
     * @return null|object $lastException
     */
    public function getLastException()
    {
        return $this->lastException;
    }

    /**
     * For debug purposes only! Get the SQL query with wildcards replaced
     *
     * @param  string $sql
     * @param  array  $data
     * @return string $fullSql
     */
    public function debugFullSql($sql, array $data=array())
    {
        if (empty($data)) {
            return $sql;
        } else {
            krsort($data);
            foreach($data as $key => $val) {
                if (!isset($val)) {
                    $sql = str_replace(":$key", 'NULL', $sql);
                } else {
                    $sql = str_replace(":$key", $this->quote($val), $sql);
                }
            }

            return $sql;
        }
    }

    //################################################################################
    //##########################  PROTECTED FUNCTIONS  ###############################
    //################################################################################

    /**
     * Called to start a PDO query
     *
     * @access protected
     * @param  string $functionName optional
     * @param  string $sql  optional
     * @param  array  $data optional
     * @param  array  $sqlOptions optional
     * @return array  array($sql, $data)
     */
    protected function startQuery($functionName='', $sql='', $data=array(), $sqlOptions=array())
    {
        $this->setError();
        $this->lastAction = array('func' => $functionName, 'sql' => $sql, 'data' => $data, 'options' => $sqlOptions);

        return array($sql, $data);
    }


    /**
     * Called to end a PDO query (also calls debugDisplay)
     *
     * @param  mixed $retval
     * @return mixed $retval
     */
    protected function endQuery($retval=0)
    {
        //Debug mode   permanentDebugMode plus a local override
        $debugMode = (isset($this->options['debugMode'])) ? $this->options['debugMode'] : 0;
        if (isset($this->lastAction['options']['debug'])) {
            $debugMode = $this->lastAction['options']['debug'];
        }

        if ($debugMode > 0) {
            $this->debugDisplay($retval);
        }

        return $retval;
    }


    /**
     * Debug display
     *
     * @param mixed $retval
     */
    protected function debugDisplay($retval)
    {
        $sql             = (isset($this->lastAction['sql']))     ? $this->lastAction['sql']     : '';
        $data            = (isset($this->lastAction['data']))    ? $this->lastAction['data']    : array();
        $options         = (isset($this->lastAction['options'])) ? $this->lastAction['options'] : array();
        $error           = $this->lastError;
        $escError        = htmlentities($error);
        $functionName    = $this->lastAction['func'];
        $escFunctionName = htmlentities($functionName);
        $escComment      = (isset($options['comment']))     ? htmlentities($options['comment']) : '';
        $escComment      = (isset($options['htmlComment'])) ? $options['htmlComment'] : $escComment;

        //Replace all :placeholder strings with their actual 'quoted\"value'
        $dispSql = "$sql ";
        $delimitors = array(' ', ')', "\n");
        foreach($data as $key => $val) {
            $quVal = $this->quote($val);
            foreach($delimitors as $delim) {
                $dispSql = str_replace(":$key$delim", "$quVal$delim", $dispSql);
            }
        }
        $dispSql    = trim($dispSql);
        $escDispSql = htmlentities($dispSql);

        $location   = '';
        $callStack  = debug_backtrace();
        $remove     = array('object', 'args', 'type');
        for ($i=0, $maxi=count($callStack); $i < $maxi; $i++) {
            foreach($remove as $key) {
                if (isset($callStack[$i][$key])) {
                    unset($callStack[$i][$key]);
                }
            }
            if ((isset($callStack[$i]['class'])) && ($callStack[$i]['class'] == __CLASS__)) {
                continue;
            }
            if ($location == '') {
                $location = ((isset($callStack[$i]['file']))     ? $callStack[$i]['file'] : '') .
                    ((isset($callStack[$i]['line']))     ? '['.$callStack[$i]['line'].']' : '') .
                    ((isset($callStack[$i]['function'])) ? ' --- '.$callStack[$i]['function'].'()' : '');
            }
        }
        $escLocation = htmlentities($location);
        $escRetval   = '';
        if (!isset($retval)) {
            $escRetval = '[NULL]';
        } else if (is_array($retval)) {
            if (count($retval) == 0) {
                $escRetval = 'an empty array';
            } else {
                if ($functionName == 'selectOne') {
                    $escRetval = '1 row (with '.count($retval).' fields)';
                } else {
                    $escRetval = count($retval).' rows';
                }
            }
        } else if (is_numeric($retval)) {
            $escRetval = htmlentities($retval).' (affected rows / status / etc.)';
        }


        if (php_sapi_name() == 'cli') {
            //Todo:  Add this later, if needed
            $code      = 'EzPdo debug display not supported in CLI mode';
        } else {
            $code      = "<br /><br /><span style='display:inline;align:center'>\n".
                "<table class='ezpdo-table' width='90%' border='1' style='text-align:left;'>\n".
                "    <tr><td class='ezpdo-title'>EzPdo $escFunctionName() <small>$escLocation</small></td></tr>\n";
            if ($escComment) {
                $code     .= "    <tr><td><pre><b>$escComment</b></pre></td></tr>\n";
            }
            if ($error) {
                $code .= "    <tr><td class='ezpdo-error'>ERROR: $escError</td></tr>\n";
            }
            $code     .= "    <tr><td><pre>$escDispSql</pre></td></tr>\n";
            if ($escRetval) {
                $code .= "    <tr><td><b>Returning:&nbsp;&nbsp;$escRetval</b></td></tr>\n";
            }
            $code     .= "</table></span><br />\n";
        }

        if ($this->debugBufferActive) {
            $this->debugBuffer .= $code;
        } else {
            print $code;
        }
    }


    /**
     * Function handling the outer exception
     *
     * @param  object $e
     * @param  mixed  $defaultValue optional
     * @param  string $sql optional
     * @return mixed        $defaultValue
     */
    protected function handleException($e, $defaultValue=0, $sql='')
    {
        $this->lastException = $e;
        $this->lastError     = $e->getMessage();

        //Are we supposed to rethrow the exception?
        //if ((isset($this->options['throwExceptions'])) && ($this->options['throwExceptions'])) {
        //throw $e;
        //}

        return $defaultValue;
    }


    /**
     * Reconnect with the same details that we used in our constructor
     *
     * @access protected
     * @return object $this
     * @throws PDOException
     */
    protected function autoReconnect() {
        $dsn      = $this->connectionDetails['dsn'];
        $username = $this->connectionDetails['username'];
        $password = $this->connectionDetails['password'];
        $options  = $this->connectionDetails['options'];


        $ourDefaultOptions = array(\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION);
        $options           = array_merge($ourDefaultOptions, $options);

        parent::__construct($dsn, $username, $password, $options);

        //$this->_connected = true;
        return $this;
    }



    /**
     * Set the last error variable
     *
     * @access protected
     * @param  string $error
     * @return string $error
     */
    protected function setError($error='')
    {
        $this->lastError = $error;
        if ($error=='') { $this->lastException = null; }
        return $error;
    }

}

