<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialCategory extends BaseClass {

	public $TrialCategoryKeyID; //int(10)
	public $TrialCategoryText;  //varchar(250)


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialCategory($TrialCategoryText){
		$this->TrialCategoryText = $TrialCategoryText;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row)
    {
        $quKeyRow = trim($this->connection->SqlQuote($key_row), "'");

		$result = $this->connection->RunQuery("SELECT * FROM TrialCategory WHERE TrialCategoryKeyID = $quKeyRow");

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->TrialCategoryKeyID = $row["TrialCategoryKeyID"];
			$this->TrialCategoryText  = $row["TrialCategoryText"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialCategory', 'TrialCategoryKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE TrialCategory SET\n".
                "TrialCategoryText = ".         $this->SqlQuote($this->TrialCategoryText)."\n".
                "WHERE TrialCategoryKeyID = ".  $this->SqlQuote($this->TrialCategoryKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialCategory','TrialCategoryKeyID', $this->TrialCategoryKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO TrialCategory (TrialCategoryText) VALUES (\n".
                $this->SqlQuote($this->TrialCategoryText).')';
            $this->TrialCategoryKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialCategory','TrialCategoryKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

        $quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialCategoryKeyID from TrialCategory order by $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialCategoryKeyID"];
            $i++;
        }
	    return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected){
        $select = '<select name="TrialCategoryKeyID" id="TrialCategoryKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT TrialCategoryKeyID, TrialCategoryText FROM TrialCategory ORDER BY TrialCategoryKeyID");

        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialCategoryKeyID = htmlentities($row["TrialCategoryKeyID"]);
            $escTrialCategoryText  = htmlentities($row["TrialCategoryText"]);
            if ($row["TrialCategoryKeyID"] == $selected) {
                 $select .= "<option value=\"$escTrialCategoryKeyID\" selected>$escTrialCategoryText</option>\n";
            } else {
                 $select .= "<option value=\"$escTrialCategoryKeyID\">$escTrialCategoryText</option>\n";
            }
        }

        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $TrialCategoryKeyID
	 */
	public function getTrialCategoryKeyID()
    {
		return $this->TrialCategoryKeyID;
	}

	/**
	 * @return null|string $TrialCategoryText
	 */
	public function getTrialCategoryText()
    {
		return $this->TrialCategoryText;
	}

	/**
	 * @param int $TrialCategoryKeyID
	 */
	public function setTrialCategoryKeyID($TrialCategoryKeyID)
    {
		$this->TrialCategoryKeyID = $TrialCategoryKeyID;
	}

	/**
	 * @param string $TrialCategoryText
	 */
	public function setTrialCategoryText($TrialCategoryText)
    {
		$this->TrialCategoryText = $TrialCategoryText;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->TrialCategoryKeyID)) ? $this->TrialCategoryKeyID : null);  //vld: TrialCategory.TrialCategoryKeyID
            if ($validationMessage = validateField($val,'TrialCategoryKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialCategoryText)) ? $this->TrialCategoryText : null);  //vld: TrialCategory.TrialCategoryText
            if ($validationMessage = validateField($val,'TrialCategoryText','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

