<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class TrialDiseaseType extends BaseClass {

	public $TrialDiseaseTypeKeyID; //int(10)
	public $TrialDiseaseTypeName; //varchar(250)
    public $TrialDiseaseTypeCheckbox; //int(1)

    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_TrialDiseaseType($TrialDiseaseTypeName){
		$this->TrialDiseaseTypeName = $TrialDiseaseTypeName;
	}*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Load_from_key($key_row){
		$result = $this->connection->RunQuery("SELECT * FROM TrialDiseaseType WHERE TrialDiseaseTypeKeyID = ". $this->SqlQuote($key_row));

		$found = 0;
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
		    $found = 1;
			$this->TrialDiseaseTypeKeyID = $row["TrialDiseaseTypeKeyID"];
			$this->TrialDiseaseTypeName = $row["TrialDiseaseTypeName"];
		}

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
	}

    /**
     * Load one row into var_class as checkbox input. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @param int $checked
     * @throws \Exception
     */
	public function CreateCheckbox($key_row, $checked)
    {
        $quKeyRow = trim($this->connection->SqlQuote($key_row), "'");

		$result = $this->connection->RunQuery("SELECT * FROM TrialDiseaseType WHERE TrialDiseaseTypeKeyID = \"$quKeyRow\" ");
        $escChecked = (!empty($checked)) ? 'checked="checked"' : '' ;

		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialDiseaseTypeKeyID = htmlentities($row["TrialDiseaseTypeKeyID"]);
            $escTrialDiseaseTypeName  = htmlentities($row["TrialDiseaseTypeName"]);
			$this->TrialDiseaseTypeCheckbox =
                "<span class=\"reqwrap\"><input type=\"checkbox\" class=\"TrialDiseaseType\" name=\"TrialDiseaseType[]\" value=\"$escTrialDiseaseTypeKeyID\" $escChecked /></span>&emsp;$escTrialDiseaseTypeName";
		}
	}

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
	public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'TrialDiseaseType', 'TrialDiseaseTypeKeyID', $key_row);
	}

    /**
     * Update the active row table on table
     * @throws \Exception
     */
	public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE TrialDiseaseType SET\n".
                "TrialDiseaseTypeName = ".         $this->SqlQuote($this->TrialDiseaseTypeName)."\n".
                "WHERE TrialDiseaseTypeKeyID = ".  $this->SqlQuote($this->TrialDiseaseTypeKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'TrialDiseaseType','TrialDiseaseTypeKeyID', $this->TrialDiseaseTypeKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
	public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO TrialDiseaseType (TrialDiseaseTypeName) VALUES (\n".
                $this->SqlQuote($this->TrialDiseaseTypeName).')';
            $this->TrialDiseaseTypeKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'TrialDiseaseType','TrialDiseaseTypeKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
	}

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param  string $column
     * @param  string $order
     * @return array $keys
     * @throws \Exception
     */
	public function GetKeysOrderBy($column, $order)
    {
		$keys = array();

		$quColumn = trim($this->connection->SqlQuote($column), "'");
        $quOrder  = trim($this->connection->SqlQuote($order), "'");

		$i = 0;
		$result = $this->connection->RunQuery("SELECT TrialDiseaseTypeKeyID FROM TrialDiseaseType ORDER BY $quColumn $quOrder");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $keys[$i] = $row["TrialDiseaseTypeKeyID"];
            $i++;
        }
    	return $keys;
	}

    /**
     * Returns a select field and assigns selected
     *
     * @param int $selected
     * @return string $select
     * @throws \Exception
     */
	public function CreateSelect($selected)
    {
        $select = '<select name="TrialDiseaseTypeKeyID" id="TrialDiseaseTypeKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
		$result = $this->connection->RunQuery("SELECT TrialDiseaseTypeKeyID, TrialDiseaseTypeText FROM TrialDiseaseType ORDER BY TrialDiseaseTypeKeyID");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escTrialDiseaseTypeKeyID = htmlentities($row["TrialDiseaseTypeKeyID"]);
            $escTrialDiseaseTypeText  = htmlentities($row["TrialDiseaseTypeText"]);
            if ($row["TrialDiseaseTypeKeyID"] == $selected) {
                 $select .= "<option value=\"$escTrialDiseaseTypeKeyID\" selected>$escTrialDiseaseTypeText</option>\n";
            } else {
                $select .= "<option value=\"$escTrialDiseaseTypeKeyID\">$escTrialDiseaseTypeText</option>\n";
            }
        }
        $select .= '</select>'."\n";
	    return $select;
	}

	/**
	 * @return null|int $TrialDiseaseTypeKeyID
	 */
	public function getTrialDiseaseTypeKeyID()
    {
		return $this->TrialDiseaseTypeKeyID;
	}

	/**
	 * @return string $TrialDiseaseTypeName
	 */
	public function getTrialDiseaseTypeName()
    {
		return $this->TrialDiseaseTypeName;
	}

	/**
	 * @param int $TrialDiseaseTypeKeyID
	 */
	public function setTrialDiseaseTypeKeyID($TrialDiseaseTypeKeyID)
    {
		$this->TrialDiseaseTypeKeyID = $TrialDiseaseTypeKeyID;
	}

	/**
	 * @param string $TrialDiseaseTypeName
	 */
	public function setTrialDiseaseTypeName($TrialDiseaseTypeName)
    {
		$this->TrialDiseaseTypeName = $TrialDiseaseTypeName;
	}

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        $validationMessage = '';

        //Validate
        do {
            $val = ((isset($this->TrialDiseaseTypeKeyID)) ? $this->TrialDiseaseTypeKeyID : null);  //vld: TrialDiseaseType.TrialDiseaseTypeKeyID
            if ($validationMessage = validateField($val,'TrialDiseaseTypeKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->TrialDiseaseTypeName)) ? $this->TrialDiseaseTypeName : null);  //vld: TrialDiseaseType.TrialDiseaseTypeName
            if ($validationMessage = validateField($val,'TrialDiseaseTypeName','nestring',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}