<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 * 
 * Create Date: 2-05-2014
 * 
 * Version of MYSQL_to_PHP: 1.1
 * 
 * License: LGPL 
 * 
 */

defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class Site extends BaseClass {

    public $SiteKeyID; //int(10)
    public $CompKeyID; //int(10)
    public $SiteName; //varchar(250)
    public $SiteCode; //int(10)
    public $SiteAddress1; //varchar(250)
    public $SiteAddress2; //varchar(250)
    public $SiteCity; //varchar(250)
    public $SiteState; //varchar(250)
    public $SiteZip; //int(9)
    public $SitePhone; //varchar(250)
    public $SiteOpenDate; //datetime
    public $SiteStatus; //tinyint(1)


    //Pagination variables
    public $showPagination     = 0;
    public $showFirstButton    = 0;
    public $showPreviousButton = 0;
    public $showNextButton     = 0;
    public $legendText         = '';
    public $totalRowCount      = 0;


    /**
     * Class constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_Site($CompKeyID,$SiteName,$SiteCode,$SiteAddress1,$SiteAddress2,$SiteCity,$SiteState,$SiteZip,$SitePhone,$SiteOpenDate,$SiteStatusDate,$SiteStatus)
    {
        $this->CompKeyID    = $CompKeyID;
        $this->SiteName     = $SiteName;
        $this->SiteCode     = $SiteCode;
        $this->SiteAddress1 = $SiteAddress1;
        $this->SiteAddress2 = $SiteAddress2;
        $this->SiteCity     = $SiteCity;
        $this->SiteState    = $SiteState;
        $this->SiteZip      = $SiteZip;
        $this->SitePhone    = $SitePhone;
        $this->SiteOpenDate = $SiteOpenDate;
        $this->SiteStatus   = $SiteStatus;
    }*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name; 
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $result = $this->connection->RunQuery("SELECT *\n".
                                              "FROM   `Site`\n".
                                              "WHERE  `SiteKeyID`=". $this->SqlQuote($key_row)
                                             );

        $found = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->SiteKeyID    = $row["SiteKeyID"];
            $this->CompKeyID    = $row["CompKeyID"];
            $this->SiteName     = $row["SiteName"];
            $this->SiteCode     = $row["SiteCode"];
            $this->SiteAddress1 = $row["SiteAddress1"];
            $this->SiteAddress2 = $row["SiteAddress2"];
            $this->SiteCity     = $row["SiteCity"];
            $this->SiteState    = $row["SiteState"];
            $this->SiteZip      = $row["SiteZip"];
            $this->SitePhone    = $row["SitePhone"];
            $this->SiteOpenDate = $row["SiteOpenDate"];
            $this->SiteStatus   = $row["SiteStatus"];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Site', 'SiteKeyID', $key_row);
    }

    /**
     * Update the active row table on table
     * @throws \Exception
     */
    public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "UPDATE `Site` SET\n".
                "    `CompKeyID`=".    $this->SqlQuote($this->CompKeyID) .",\n".
                "    `SiteName`=".     $this->SqlQuote($this->SiteName) .",\n".
                "    `SiteCode`=".     $this->SqlQuote($this->SiteCode) .",\n".
                "    `SiteAddress1`=". $this->SqlQuote($this->SiteAddress1) .",\n".
                "    `SiteAddress2`=". $this->SqlQuote($this->SiteAddress2) .",\n".
                "    `SiteCity`=".     $this->SqlQuote($this->SiteCity) .",\n".
                "    `SiteState`=".    $this->SqlQuote($this->SiteState) .",\n".
                "    `SiteZip`=".      $this->SqlQuote($this->SiteZip) .",\n".
                "    `SitePhone`=".    $this->SqlQuote($this->SitePhone) .",\n".
                "    `SiteOpenDate`=". $this->SqlQuote($this->SiteOpenDate) .",\n".
                "    `SiteStatus`=".   $this->SqlQuote($this->SiteStatus) ."\n".
                "WHERE `SiteKeyID`=". $this->SqlQuote($this->SiteKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Site','SiteKeyID', $this->SiteKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO `Site` (`CompKeyID`, `SiteName`, `SiteCode`, `SiteAddress1`,\n".
                "                    `SiteAddress2`, `SiteCity`, `SiteState`, `SiteZip`, `SitePhone`,\n".
                "                    `SiteOpenDate`, `SiteStatus`) VALUES (\n".
                $this->SqlQuote($this->CompKeyID). ",\n".
                $this->SqlQuote($this->SiteName). ",\n".
                $this->SqlQuote($this->SiteCode). ",\n".
                $this->SqlQuote($this->SiteAddress1). ",\n".
                $this->SqlQuote($this->SiteAddress2). ",\n".
                $this->SqlQuote($this->SiteCity). ",\n".
                $this->SqlQuote($this->SiteState). ",\n".
                $this->SqlQuote($this->SiteZip). ",\n".
                $this->SqlQuote($this->SitePhone). ",\n".
                $this->SqlQuote($this->SiteOpenDate). ",\n".
                $this->SqlQuote($this->SiteStatus).')';
            $this->SiteKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Site','SiteKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * @param string $column
     * @param string $order optional
     * @param int    $from optional                  Default is 0   (for LIMIT $from,...)
     * @param int    $limit optional                 Default is 0   (for LIMIT ...,$limit)
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($column, $order='ASC', $from=0, $limit=0, $sClauses=array())
    {
        $keys = array();

        $i = 0;
        if ((!isset($order)) || (!is_string($order))) { $order='ASC'; }
        $order = strtoupper($order);
        if (($order!='ASC') && ($order!='DESC')) { $order='ASC'; }

        $column = $this->connection->Quote($column);

        //Support SearchFilter
        //Get additional joins and where clauses as supplied by the searchFilter class
        //print "<pre>".print_r($sClauses,true)."</pre>";
        if ((!isset($sClauses)) || (!is_array($sClauses))) { $sClauses=array(); }
        if ((!isset($sClauses['joins'])) || (!is_array($sClauses['joins'])))   { $sClauses['joins']=array(); }
        if ((!isset($sClauses['where'])) || (!is_array($sClauses['where'])))   { $sClauses['where']=array(); }
        if ((!isset($sClauses['debug'])) || (!is_numeric($sClauses['debug']))) { $sClauses['debug']=0; }
        $joins    = '';
        $whereAnd = '';
        foreach($sClauses['joins'] as $join=>$ignore) {
            $joins .= $join;
        }
        reset($sClauses['joins']);
        if ($joins) { $joins = trim($joins)."\n"; }


        foreach($sClauses['where'] as $ignore=>$whr) {
            $whereAnd .= $whr;
        }
        reset($sClauses['where']);
        if ($whereAnd) { $whereAnd = trim($whereAnd)."\n"; }

        //COUNT ROWS:  Build the query to just count our results
        $sql = "SELECT count(*) as cnt\n".
               "FROM   `Site`\n".
               $joins.
               "WHERE  `Site`.`SiteKeyID` > 0\n".      //Todo: HACK!!! Fix this 'select-all-patients' nonsense!
               $whereAnd;
        $result = $this->connection->RunQuery($sql);
        $this->totalRowCount = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (isset($row['cnt'])) {
                $this->totalRowCount = $row['cnt'];
                break;
            }
        }

        //GET KEYS:  Build the query to actually get our Keys
        $sql = "SELECT `SiteKeyID`\n".
               "FROM   `Site`\n".
               $joins.
               "WHERE  `Site`.`SiteKeyID` > 0\n".      //Todo: HACK!!! Fix this 'select-all-patients' nonsense!
               $whereAnd;

        //Support table column sorting 'order_field_0' (thead)...
        $orderBy = "ORDER BY `".$this->connection->Quote($column)."` $order";
        $orderIndex      = (isset($sClauses['order_index']))                 ? $sClauses['order_index'] : '_0';
        $orderField      = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection  = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }
        if ($orderField) {
            //Primary page sites.php
            if ($orderField == 'order_sites'.$orderIndex) {  //for _0 from sites.php
                $orderBy = "ORDER BY Site.SiteName $orderDirection";
            } else if ($orderField == 'order_site_code'.$orderIndex) {
                $orderBy = "ORDER BY Site.SiteCode $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_phone_number'.$orderIndex) {
                $orderBy = "ORDER BY Site.SitePhone $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_open_date'.$orderIndex) {
                $orderBy = "ORDER BY Site.SiteOpenDate $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_active_inactive'.$orderIndex) {
                $orderBy = "ORDER BY Site.SiteStatus $orderDirection, Site.SiteName ASC";
            }
        } 
        $sql .= $orderBy;

        //Calculate our final SELECT query with limit clause (if $limit>0)
        if ((!isset($from))  || (!is_numeric($from))  || ($from  < 0)) { $from  = 0; }
        if ((!isset($limit)) || (!is_numeric($limit)) || ($limit < 0)) { $limit = 0; }
        if (($from > 0 ) && ($limit==0)) {
            //get pagination limit
            $pg = new Pagination();
            $limit = $pg->getPaginationSize('sites.pagination.size');
        }
        $this->showPagination     = (($from) || ($limit)) ? 1 : 0;
        $this->showFirstButton    = ($from > 0)  ?  1  : 0;
        $this->showPreviousButton = ($from > 0)  ?  1  : 0;
        $this->showNextButton     = 0;
        $this->legendText         = ($limit > 0) ? "Displaying Records ".($from+1)." to ".($from+$limit) : '';
        $this->limitFrom          = $from;
        $this->limitSlice         = $limit;
        $sql .= (($from) || ($limit))  ?  "\nLIMIT $from,".($limit+1) : '';

        //DebugDisplay for SearchFilter
        if ((0) || ($sClauses['debug'])) {
            print "<br /><pre style='font-size:8pt;color:black;'>$sql</pre>";
            print "<pre style='font-size:8pt;color:blue;'>".print_r($sClauses,true)."</pre>";
            if ($sClauses['debug'] > 1) { exit; }
        }

        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (($limit) && ($i >= $limit)) {
                $this->showNextButton = 1;
                break;
            }
            $keys[$i] = $row["SiteKeyID"];
            $i++;
            if ($limit > 0) { $this->legendText = "Displaying Records ".($from+1)." to ".($from+$i); }
        }

        if (count($keys)==0) { $this->showPagination=0; }

        return $keys;
    }

    /**
     * Returns array of keys with where key = $key order by $column -> name of column $order -> desc or acs
     *
     * @param string $where
     * @param string $column
     * @param string $order
     * @param int    $from optional                  Default is 0   (for LIMIT $from,...)
     * @param int    $limit optional                 Default is 0   (for LIMIT ...,$limit)
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysWhereOrderBy($where, $column, $order='ASC', $from=0, $limit=0, $sClauses=array())
    {
        $keys = array(); $i = 0;

        if ((!isset($order)) || (!is_string($order))) { $order='ASC'; }
        $order = strtoupper($order);
        if (($order!='ASC') && ($order!='DESC')) { $order='ASC'; }

        $x = strtoupper(trim($where));
        if (($x) && (substr($x,0,6) != 'WHERE ')) { $where = "WHERE $where"; }

        $column = $this->connection->Quote($column);

        $sql = "SELECT `SiteKeyID`\n".
               "FROM   `Site`\n".
               "$where\n";

        //Support table column sorting 'order_field_0' (thead)...
        $orderBy = "ORDER BY `".$this->connection->Quote($column)."` $order";;
        $orderIndex      = (isset($sClauses['order_index']))                 ? $sClauses['order_index'] : '_0';
        $orderField      = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection  = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }
        if ($orderField) {
            //Primary page sites.php
            if ($orderField == 'order_sites'.$orderIndex) {  //for _0 from compViewEdit.php
                $orderBy = "ORDER BY Site.SiteName $orderDirection";
            } else if ($orderField == 'order_site_code'.$orderIndex) {
                $orderBy = "ORDER BY Site.SiteCode $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_phone_number'.$orderIndex) {
                $orderBy = "ORDER BY Site.SitePhone $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_open_date'.$orderIndex) {
                $orderBy = "ORDER BY Site.SiteOpenDate $orderDirection, Site.SiteName ASC";
            } else if ($orderField == 'order_active_inactive'.$orderIndex) {
                $orderBy = "ORDER BY Site.SiteStatus $orderDirection, Site.SiteName ASC";
            }
        } 
        $sql .= $orderBy;

        //Calculate our final SELECT query with limit clause (if $limit>0)
        if ((!isset($from))  || (!is_numeric($from))  || ($from  < 0)) { $from  = 0; }
        if ((!isset($limit)) || (!is_numeric($limit)) || ($limit < 0)) { $limit = 0; }
        if (($from > 0 ) && ($limit==0)) {
            //get pagination limit
            $pg = new Pagination();
            $limit = $pg->getPaginationSize('sites.pagination.size');
        }
        $this->showPagination     = (($from) || ($limit)) ? 1 : 0;
        $this->showFirstButton    = ($from > 0)  ?  1  : 0;
        $this->showPreviousButton = ($from > 0)  ?  1  : 0;
        $this->showNextButton     = 0;
        $this->legendText         = ($limit > 0) ? "Displaying Records ".($from+1)." to ".($from+$limit) : '';
        $sql .= (($from) || ($limit))  ?  "\nLIMIT $from,".($limit+1) : '';

        $result = $this->connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (($limit) && ($i >= $limit)) {
                $this->showNextButton = 1;
                break;
            }
            $keys[$i] = $row["SiteKeyID"];
            $i++;
            if ($limit > 0) { $this->legendText = "Displaying Records ".($from+1)." to ".($from+$i); }
        }

        if (count($keys)==0) { $this->showPagination=0; }

        return $keys;
    }

    /**
     * Returns a select field and assigns selected
     *
     * @param  string $selected
     * @param  string $where
     * @return string $select
     * @throws \Exception
     */
    public function CreateSelect($selected, $where)
    {
        $select = '<select name="SiteKeyID" id="SiteKeyID" class="button">'."\n";
        $select .= '<option value="">Select...</option>'."\n";

        $x = strtoupper(trim($where));
        if (($x) && (substr($x,0,6) != 'WHERE ')) { $where = "WHERE $where"; }

        $result = $this->connection->RunQuery("SELECT `SiteKeyID`, `SiteCode`, `SiteName`\n".
                                              "FROM   `Site`\n".
                                              "$where\n".
                                              "ORDER BY `SiteName`"
                                             );
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escSiteKeyID = htmlentities($row["SiteKeyID"]);
            $escSiteCode  = htmlentities($row["SiteCode"]);
            $escSiteName  = htmlentities($row["SiteName"]);
            if ($row["SiteKeyID"] == $selected) {
                 $select .= "<option value=\"$escSiteKeyID\" selected>$escSiteCode - $escSiteName</option>\n";
            } else {
                $select .= "<option value=\"$escSiteKeyID\">$escSiteCode - $escSiteName</option>\n";
            }
        }
        $select .= '</select>'."\n";
        return $select;
    }

    /**
     * @return null|int $SiteKeyID
     */
    public function getSiteKeyID()
    {
        return $this->SiteKeyID;
    }

    /**
     * @return null|int $CompKeyID
     */
    public function getCompKeyID()
    {
        return $this->CompKeyID;
    }

    /**
     * @return null|string $SiteName
     */
    public function getSiteName()
    {
        return $this->SiteName;
    }

    /**
     * @return null|int $SiteCode
     */
    public function getSiteCode()
    {
        return $this->SiteCode;
    }

    /**
     * @return null|string $SiteAddress1
     */
    public function getSiteAddress1()
    {
        return $this->SiteAddress1;
    }

    /**
     * @return null|string $SiteAddress2
     */
    public function getSiteAddress2()
    {
        return $this->SiteAddress2;
    }

    /**
     * @return null|string $SiteCity
     */
    public function getSiteCity()
    {
        return $this->SiteCity;
    }

    /**
     * @return null|string $SiteState
     */
    public function getSiteState()
    {
        return $this->SiteState;
    }

    /**
     * @return null|string $SiteZip
     */
    public function getSiteZip()
    {
        return $this->SiteZip;
    }

    /**
     * @return null|string $SitePhone
     */
    public function getSitePhone()
    {
        return $this->SitePhone;
    }

    /**
     * @return null|string $SiteOpenDate
     */
    public function getSiteOpenDate()
    {
        return $this->SiteOpenDate;
    }

    /**
     * @return null|int $SiteStatus
     */
    public function getSiteStatus()
    {
        return $this->SiteStatus;
    }

    /**
     * @param int $SiteKeyID
     */
    public function setSiteKeyID($SiteKeyID)
    {
        $this->SiteKeyID = $SiteKeyID;
    }

    /**
     * @param int $CompKeyID
     */
    public function setCompKeyID($CompKeyID)
    {
        $this->CompKeyID = $CompKeyID;
    }

    /**
     * @param string $SiteName
     */
    public function setSiteName($SiteName)
    {
        $this->SiteName = $SiteName;
    }

    /**
     * @param int $SiteCode
     */
    public function setSiteCode($SiteCode)
    {
        $this->SiteCode = $SiteCode;
    }

    /**
     * @param string $SiteAddress1
     */
    public function setSiteAddress1($SiteAddress1)
    {
        $this->SiteAddress1 = $SiteAddress1;
    }

    /**
     * @param string $SiteAddress2
     */
    public function setSiteAddress2($SiteAddress2)
    {
        $this->SiteAddress2 = $SiteAddress2;
    }

    /**
     * @param string $SiteCity
     */
    public function setSiteCity($SiteCity)
    {
        $this->SiteCity = $SiteCity;
    }

    /**
     * @param string $SiteState
     */
    public function setSiteState($SiteState)
    {
        $this->SiteState = $SiteState;
    }

    /**
     * @param string $SiteZip
     */
    public function setSiteZip($SiteZip)
    {
        $this->SiteZip = $SiteZip;
    }

    /**
     * @param string $SitePhone
     */
    public function setSitePhone($SitePhone)
    {
        $this->SitePhone = $SitePhone;
    }

    /**
     * @param string $SiteOpenDate
     */
    public function setSiteOpenDate($SiteOpenDate)
    {
        $this->SiteOpenDate = $SiteOpenDate;
    }

    /**
     * @param int $SiteStatus
     */
    public function setSiteStatus($SiteStatus)
    {
        $this->SiteStatus = $SiteStatus;
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Validate
        do {
            $val = ((isset($this->SiteKeyID)) ? $this->SiteKeyID : null);  //vld: Site.SiteKeyID
            if ($validationMessage = validateField($val,'SiteKeyID','nullok;positiveint',__CLASS__)) { break; }

            $val = ((isset($this->CompKeyID)) ? $this->CompKeyID : null);  //vld: Site.CompKeyID
            if ($validationMessage = validateField($val,'CompKeyID','positiveint',__CLASS__)) { break; }

            $val = ((isset($this->SiteName)) ? $this->SiteName : null);  //vld: Site.SiteName
            if ($validationMessage = validateField($val,'SiteName','nestring',__CLASS__)) { break; }

            $val = ((isset($this->SiteCode)) ? $this->SiteCode : null);  //vld: Site.SiteCode
            if ($validationMessage = validateField($val,'SiteCode','nestring',__CLASS__)) { break; }

            //public $SiteAddress1; //varchar(250)    vld: Site.SiteAddress1
            //public $SiteAddress2; //varchar(250)    vld: Site.SiteAddress2
            //public $SiteCity; //varchar(250)        vld: Site.SiteCity
            //public $SiteState; //varchar(250)       vld: Site.SiteState
            //public $SiteZip; //int(9)               vld: Site.SiteZip
            //public $SitePhone; //varchar(250)       vld: Site.SitePhone

            $val = ((isset($this->SiteOpenDate)) ? $this->SiteOpenDate : null);  //vld: Site.SiteOpenDate
            if ($validationMessage = validateField($val,'SiteOpenDate','date',__CLASS__)) { break; }

            $val = ((isset($this->SiteStatus)) ? $this->SiteStatus : null);  //vld: Site.SiteStatus
            if ($validationMessage = validateField($val,'SiteStatus','nullok;0or1',__CLASS__)) { break; }
        } while(false);

        return $validationMessage;
    }

}

