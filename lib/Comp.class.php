<?php
/*
 * Author: Rafael Rocha - www.rafaelrocha.net - info@rafaelrocha.net
 *
 * Create Date: 2-05-2014
 *
 * Version of MYSQL_to_PHP: 1.1
 *
 * License: LGPL
 *
 */
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__).'/../'));
require_once(ROOT_PATH.'/lib/Base.class.php');


class Comp extends BaseClass {

    public $CompKeyID; //int(10)
    public $TimestampCreate; //datetime
    public $TimestampUpdate; //timestamp
    public $CompName; //varchar(250)
    public $CompCRAName; //varchar(250)
    public $CompCRAAddress1; //varchar(250)
    public $CompCRAAddress2; //varchar(250)
    public $CompCRACity; //varchar(250)
    public $CompCRAState; //varchar(250)
    public $CompCRAZip; //int(9)
    public $CompCRAPhone; //int(10)
    public $CompCRAEmail; //varchar(250)
    public $CompTerminationDate; //datetime
    public $CompTermination; //varchar(250)
    public $CompStatus; //tinyint(1)

    //Pagination variables
    public $showPagination     = 0;
    public $showFirstButton    = 0;
    public $showPreviousButton = 0;
    public $showNextButton     = 0;
    public $legendText         = '';
    public $totalRowCount      = 0;


    /**
     * Class constructor.
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * New object to the class. Don't forget to save this new object "as new" by using the function $class->Save_Active_Row_as_New();
     *
     */
    /* public function New_Comp($TimestampCreate,$TimestampUpdate,$CompName,$CompCRAName,$CompCRAAddress1,$CompCRAAddress2,$CompCRACity,$CompCRAState,$CompCRAZip,$CompCRAPhone,$CompCRAEmail,$CompTerminationDate,$CompTermination,$CompStatus)
    {
        $this->TimestampCreate     = $TimestampCreate;
        $this->TimestampUpdate     = $TimestampUpdate;
        $this->CompName            = $CompName;
        $this->CompCRAName         = $CompCRAName;
        $this->CompCRAAddress1     = $CompCRAAddress1;
        $this->CompCRAAddress2     = $CompCRAAddress2;
        $this->CompCRACity         = $CompCRACity;
        $this->CompCRAState        = $CompCRAState;
        $this->CompCRAZip          = $CompCRAZip;
        $this->CompCRAPhone        = $CompCRAPhone;
        $this->CompCRAEmail        = $CompCRAEmail;
        $this->CompTerminationDate = $CompTerminationDate;
        $this->CompTermination     = $CompTermination;
        $this->CompStatus          = $CompStatus;
    }*/

    /**
     * Load one row into var_class. To use the vars use for exemple echo $class->getVar_name;
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Load_from_key($key_row)
    {
        $result = $this->connection->RunQuery("SELECT `Comp`.*\n".
                                              "FROM   `Comp`\n".
                                              "WHERE  `CompKeyID`=". $this->SqlQuote($key_row)
                                             );
        $found = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $found = 1;
            $this->CompKeyID           = $row["CompKeyID"];
            $this->TimestampCreate     = $row["TimestampCreate"];
            $this->TimestampUpdate     = $row["TimestampUpdate"];
            $this->CompName            = $row["CompName"];
            $this->CompCRAName         = $row["CompCRAName"];
            $this->CompCRAAddress1     = $row["CompCRAAddress1"];
            $this->CompCRAAddress2     = $row["CompCRAAddress2"];
            $this->CompCRACity         = $row["CompCRACity"];
            $this->CompCRAState        = $row["CompCRAState"];
            $this->CompCRAZip          = $row["CompCRAZip"];
            $this->CompCRAPhone        = $row["CompCRAPhone"];
            $this->CompCRAEmail        = $row["CompCRAEmail"];
            $this->CompTerminationDate = $row["CompTerminationDate"];
            $this->CompTermination     = $row["CompTermination"];
            $this->CompStatus          = $row["CompStatus"];
        }

        if (!$found) {
            try {
                if (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('key' => $key_row))) {
                    throw new \Exception(__CLASS__ . " record with key '$key_row' not found");
                }
            } catch (\Exception $e) {
                handleException($e, __CLASS__, __FUNCTION__, array(), array('key' => $key_row));  // might rethrow!
            }
        }

        $this->ValidateAndCorrectData();
    }

    /**
     * Delete the row by using the key as arg
     *
     * @param int $key_row
     * @throws \Exception
     */
    public function Delete_row_from_key($key_row)
    {
        $this->executeDelete($this->connection,__CLASS__, __FUNCTION__, 'Comp', 'CompKeyID', $key_row);
    }

    /**
     * Update the active row table on table
     * @throws \Exception
     */
    public function Save_Active_Row()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            TrackChanges::trackBeforeUpdate($this->connection, 'Comp', 'CompKeyID', $this->CompKeyID);
            $sql =
                "UPDATE `Comp` SET\n".
                "    `CompName`=".            $this->SqlQuote($this->CompName) .",\n".
                "    `CompCRAName`=".         $this->SqlQuote($this->CompCRAName) .",\n".
                "    `CompCRAAddress1`=".     $this->SqlQuote($this->CompCRAAddress1) .",\n".
                "    `CompCRAAddress2`=".     $this->SqlQuote($this->CompCRAAddress2) .",\n".
                "    `CompCRACity`=".         $this->SqlQuote($this->CompCRACity) .",\n".
                "    `CompCRAState`=".        $this->SqlQuote($this->CompCRAState) .",\n".
                "    `CompCRAZip`=".          $this->SqlQuote($this->CompCRAZip) .",\n".
                "    `CompCRAPhone`=".        $this->SqlQuote($this->CompCRAPhone) .",\n".
                "    `CompCRAEmail`=".        $this->SqlQuote($this->CompCRAEmail) .",\n".
                "    `CompTerminationDate`=". $this->SqlQuote($this->CompTerminationDate) .",\n".
                "    `CompTermination`=".     $this->SqlQuote($this->CompTermination) .",\n".
                "    `CompStatus`=".          $this->SqlQuote($this->CompStatus) ."\n".
                "WHERE `CompKeyID`=". $this->SqlQuote($this->CompKeyID);
            $this->executeUpdate($sql,__CLASS__, __FUNCTION__, 'Comp','CompKeyID', $this->CompKeyID);

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Save the active var class as a new row on table
     * @throws Exception
     */
    public function Save_Active_Row_as_New()
    {
        $sql   = '';
        $error = '';
        try {
            //Validation
            $validationMessage = $error = $this->ValidateAndCorrectData();
            if (($validationMessage) && (shouldWeRaiseAnException(__CLASS__, __FUNCTION__, array('message' => $validationMessage)))) {
                throw new \Exception(__CLASS__ . " failed validation ($validationMessage)");
            }

            $sql =
                "INSERT INTO `Comp` (`CompName`, `CompCRAName`, `CompCRAAddress1`, `CompCRAAddress2`,\n".
                "                    `CompCRACity`, `CompCRAState`, `CompCRAZip`, `CompCRAPhone`,\n".
                "                    `CompCRAEmail`, `CompTerminationDate`, `CompTermination`,\n".
                "                    `CompStatus`) VALUES (\n".
                $this->SqlQuote($this->CompName). ",\n".
                $this->SqlQuote($this->CompCRAName). ",\n".
                $this->SqlQuote($this->CompCRAAddress1). ",\n".
                $this->SqlQuote($this->CompCRAAddress2). ",\n".
                $this->SqlQuote($this->CompCRACity). ",\n".
                $this->SqlQuote($this->CompCRAState). ",\n".
                $this->SqlQuote($this->CompCRAZip). ",\n".
                $this->SqlQuote($this->CompCRAPhone). ",\n".
                $this->SqlQuote($this->CompCRAEmail). ",\n".
                $this->SqlQuote($this->CompTerminationDate). ",\n".
                $this->SqlQuote($this->CompTermination). ",\n".
                $this->SqlQuote($this->CompStatus).')';
            $this->CompKeyID = $this->executeInsert($sql,__CLASS__, __FUNCTION__, 'Comp','CompKeyID');

        } catch (\Exception $e) {
            handleException($e, __CLASS__, __FUNCTION__, array(), array('sql' => $sql, 'error' => $error));  // might rethrow!
        }
    }

    /**
     * Returns array of keys order by $column -> name of column $order -> desc or acs
     *
     * IMPORTANT:  CALLER is responsible for quoting the $where clause!!!  Todo: fix caller!
     *
     * @param string $key
     * @param string $where
     * @param string $column
     * @param string $order optional                 Default is 'ASC'
     * @param int    $from optional                  Default is 0   (for LIMIT $from,...)
     * @param int    $limit optional                 Default is 0   (for LIMIT ...,$limit)
     * @param array  $sClauses optional
     * @return array $keys
     * @throws \Exception
     */
    public function GetKeysOrderBy($key, $where, $column, $order='ASC', $from=0, $limit=0, $sClauses=array())
    {
        $keys = array();

        $i = 0;
        if ((!isset($order)) || (!is_string($order))) { $order='ASC'; }
        $order = strtoupper($order);
        if (($order!='ASC') && ($order!='DESC')) { $order='ASC'; }

        $x = strtoupper(trim($where));
        if (($x) && (substr($x,0,6) != 'WHERE ')) { $where = "WHERE $where"; }
        if (!$x) { $where = "WHERE   `Comp`.`CompKeyID`>0"; }

        $key  = $this->connection->Quote($key);  //++++++ unused, why?
        $column = $this->connection->Quote($column);

        //Support SearchFilter
        //Get additional joins and where clauses as supplied by the searchFilter class
        //print "<pre>".print_r($sClauses,true)."</pre>";
        if ((!isset($sClauses)) || (!is_array($sClauses))) { $sClauses=array(); }
        if ((!isset($sClauses['joins'])) || (!is_array($sClauses['joins'])))   { $sClauses['joins']=array(); }
        if ((!isset($sClauses['where'])) || (!is_array($sClauses['where'])))   { $sClauses['where']=array(); }
        if ((!isset($sClauses['debug'])) || (!is_numeric($sClauses['debug']))) { $sClauses['debug']=0; }
        $joins    = '';
        $whereAnd = '';
        foreach($sClauses['joins'] as $join=>$ignore) {
            $joins .= $join;
        }
        reset($sClauses['joins']);
        if ($joins) { $joins = trim($joins)."\n"; }


        foreach($sClauses['where'] as $ignore=>$whr) {
            $whereAnd .= $whr;
        }
        reset($sClauses['where']);
        if ($whereAnd) { $whereAnd = trim($whereAnd)."\n"; }

        //COUNT ROWS:  Build the query to just count our results
        $sql = "SELECT COUNT(*) AS cnt\n".
               "FROM   `Comp`\n".
               $joins.
               "$where\n".
               $whereAnd;
        $result = $this->connection->RunQuery($sql);
        $this->totalRowCount = 0;
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (isset($row['cnt'])) {
                $this->totalRowCount = $row['cnt'];
                break;
            }
        }

        //GET KEYS:  Build the query to actually get our Keys
        $sql = "SELECT `Comp`.`CompKeyID`\n".
               "FROM   `Comp`\n".
               $joins.
               "$where\n".
               $whereAnd;

        //Support table column sorting 'order_field_0' (thead)...
        $orderBy = "ORDER BY `Comp`.`$column` $order";
        $orderIndex     = (isset($sClauses['order_index']))                 ? $sClauses['order_index'] : '_0';
        $orderField     = (isset($sClauses['order_field'.$orderIndex]))     ? $sClauses['order_field'.$orderIndex] : '';
        $orderDirection = (isset($sClauses['order_direction'.$orderIndex])) ? strtoupper($sClauses['order_direction'.$orderIndex]) : '';
        if ($orderDirection) {
            if (($orderDirection !== 'ASC') && ($orderDirection !== 'DESC')) {
                $orderDirection = 'ASC';
            }
        }
        if ($orderField) {
            if ($orderField == 'order_component_name'.$orderIndex) {
                $orderBy = "ORDER BY Comp.CompName $orderDirection";
            } else if ($orderField == 'order_cra_name'.$orderIndex) {
                $orderBy = "ORDER BY Comp.CompCRAName $orderDirection, Comp.CompName ASC";
            } else if ($orderField == 'order_cra_phone'.$orderIndex) {
                $orderBy = "ORDER BY Comp.CompCRAPhone $orderDirection, Comp.CompName ASC";
            } else if ($orderField == 'order_cra_email'.$orderIndex) {
                $orderBy = "ORDER BY Comp.CompCRAEmail $orderDirection, Comp.CompName ASC";
            }
        } 
        $sql .= $orderBy;

        //DebugDisplay for SearchFilter
        if ((0) || ($sClauses['debug'])) {
            print "<br /><pre style='font-size:8pt;color:black;'>$sql</pre>";
            print "<pre style='font-size:8pt;color:blue;'>".print_r($sClauses,true)."</pre>";
            if ($sClauses['debug'] > 1) { exit; }
        }

        //Calculate our final SELECT query with limit clause (if $limit>0)
        if ((!isset($from))  || (!is_numeric($from))  || ($from  < 0)) { $from  = 0; }
        if ((!isset($limit)) || (!is_numeric($limit)) || ($limit < 0)) { $limit = 0; }
        if (($from > 0 ) && ($limit==0)) {
            //get pagination limit
            $pg = new Pagination();
            $limit = $pg->getPaginationSize('components.pagination.size');
        }
        $this->showPagination     = (($from) || ($limit)) ? 1 : 0;
        $this->showFirstButton    = ($from > 0)  ?  1  : 0;
        $this->showPreviousButton = ($from > 0)  ?  1  : 0;
        $this->showNextButton     = 0;
        $this->legendText         = ($limit > 0) ? "Displaying Records ".($from+1)." to ".($from+$limit) : '';
        $this->limitFrom          = $from;
        $this->limitSlice         = $limit;
        $sql .= (($from) || ($limit))  ?  "\nLIMIT $from,".($limit+1) : '';


        $result = $this->connection->RunQuery($sql);
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            if (($limit) && ($i >= $limit)) {
                $this->showNextButton = 1;
                break;
            }
            $keys[$i] = $row["CompKeyID"];
            $i++;
            if ($limit > 0) { $this->legendText = "Displaying Records ".($from+1)." to ".($from+$i); }
        }

        if (count($keys)==0) { $this->showPagination=0; }

        return $keys;
    }

    /**
     * Returns a select field and assigns selected
     *
     * IMPORTANT:  CALLER is responsible for quoting the $where clause!!!  Todo: fix caller!
     *
     * @param int $selected
     * @param string $where
     * @return string $select
     * @throws \Exception
     */
    public function CreateSelect($selected, $where)
    {
        $select = '<select name="CompKeyID" id="CompKeyID">'."\n";
        $select .= '<option value="">Select...</option>'."\n";
        $result = $this->connection->RunQuery("SELECT `Comp`.`CompKeyID`, `Comp`.`CompName`\n".
                                              "FROM   `Comp`\n".
                                              "$where\n".
                                              "ORDER BY `Comp`.`CompName` ASC");
        while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $escCompKeyID = htmlentities($row["CompKeyID"]);
            $escCompName  = htmlentities($row["CompName"]);
            if ($row["CompKeyID"] == $selected) {
                 $select .= "<option value=\"$escCompKeyID\" selected>$escCompName</option>\n";
            } else {
                $select .= "<option value=\"$escCompKeyID\">$escCompName</option>\n";
            }
        }
        $select .= '</select>'."\n";
        return $select;
    }

    /**
     * @return int $CompKeyID
     */
    public function getCompKeyID()
    {
        return $this->CompKeyID;
    }

    /**
     * @return string $TimestampCreate
     */
    public function getTimestampCreate()
    {
        return $this->TimestampCreate;
    }

    /**
     * @return string $TimestampUpdate
     */
    public function getTimestampUpdate()
    {
        return $this->TimestampUpdate;
    }

    /**
     * @return string $CompName
     */
    public function getCompName()
    {
        return $this->CompName;
    }

    /**
     * @return string $CompCRAName
     */
    public function getCompCRAName()
    {
        return $this->CompCRAName;
    }

    /**
     * @return string $CompCRAAddress1
     */
    public function getCompCRAAddress1()
    {
        return $this->CompCRAAddress1;
    }

    /**
     * @return string $CompCRAAddress2
     */
    public function getCompCRAAddress2()
    {
        return $this->CompCRAAddress2;
    }

    /**
     * @return string $CompCRACity
     */
    public function getCompCRACity()
    {
        return $this->CompCRACity;
    }

    /**
     * @return string $CompCRAState
     */
    public function getCompCRAState()
    {
        return $this->CompCRAState;
    }

    /**
     * @return string $CompCRAZip
     */
    public function getCompCRAZip()
    {
        return $this->CompCRAZip;
    }

    /**
     * @return string $CompCRAPhone
     */
    public function getCompCRAPhone()
    {
        return $this->CompCRAPhone;
    }

    /**
     * @return string $CompCRAEmail
     */
    public function getCompCRAEmail()
    {
        return $this->CompCRAEmail;
    }

    /**
     * @return string $CompTerminationDate
     */
    public function getCompTerminationDate()
    {
        return $this->CompTerminationDate;
    }

    /**
     * @return string $CompTermination
     */
    public function getCompTermination()
    {
        return $this->CompTermination;
    }

    /**
     * @return int $CompStatus
     */
    public function getCompStatus()
    {
        return $this->CompStatus;
    }

    /**
     * @param int $CompKeyID
     */
    public function setCompKeyID($CompKeyID)
    {
        $this->CompKeyID = $CompKeyID;
    }

    /**
     * @param string $TimestampCreate
     */
    public function setTimestampCreate($TimestampCreate)
    {
        $this->TimestampCreate = $TimestampCreate;
    }

    /**
     * @param string $TimestampUpdate
     */
    public function setTimestampUpdate($TimestampUpdate)
    {
        $this->TimestampUpdate = $TimestampUpdate;
    }

    /**
     * @param string $CompName
     */
    public function setCompName($CompName)
    {
        $this->CompName = $CompName;
    }

    /**
     * @param string $CompCRAName
     */
    public function setCompCRAName($CompCRAName)
    {
        $this->CompCRAName = $CompCRAName;
    }

    /**
     * @param string $CompCRAAddress1
     */
    public function setCompCRAAddress1($CompCRAAddress1)
    {
        $this->CompCRAAddress1 = $CompCRAAddress1;
    }

    /**
     * @param string $CompCRAAddress2
     */
    public function setCompCRAAddress2($CompCRAAddress2)
    {
        $this->CompCRAAddress2 = $CompCRAAddress2;
    }

    /**
     * @param string $CompCRACity
     */
    public function setCompCRACity($CompCRACity)
    {
        $this->CompCRACity = $CompCRACity;
    }

    /**
     * @param string $CompCRAState
     */
    public function setCompCRAState($CompCRAState)
    {
        $this->CompCRAState = $CompCRAState;
    }

    /**
     * @param string $CompCRAZip
     */
    public function setCompCRAZip($CompCRAZip)
    {
        $this->CompCRAZip = $CompCRAZip;
    }

    /**
     * @param string $CompCRAPhone
     */
    public function setCompCRAPhone($CompCRAPhone)
    {
        $this->CompCRAPhone = $CompCRAPhone;
    }

    /**
     * @param string $CompCRAEmail
     */
    public function setCompCRAEmail($CompCRAEmail)
    {
        $this->CompCRAEmail = $CompCRAEmail;
    }

    /**
     * @param string $CompTerminationDate
     */
    public function setCompTerminationDate($CompTerminationDate)
    {
        $this->CompTerminationDate = $CompTerminationDate;
    }

    /**
     * @param string $CompTermination
     */
    public function setCompTermination($CompTermination)
    {
        $this->CompTermination = $CompTermination;
    }

    /**
     * @param int $CompStatus
     */
    public function setCompStatus($CompStatus)
    {
        $this->CompStatus = $CompStatus;
    }

    /**
     * Validate and correct some data fields
     *
     * @return string $validationMessage
     */
    public function ValidateAndCorrectData()
    {
        //Fix
        if (!isset($this->CompTerminationDate)) {
            $this->CompTerminationDate = '0000-00-00';
        }
        if ((!isset($this->CompTermination)) || (!is_numeric($this->CompTermination)) || (!$this->CompTermination)) {
            $this->CompTermination = 0;
        }
        if ($this->CompTermination) {
            $this->CompTermination = 1;
        }

        if ((!isset($this->CompStatus)) || (!is_numeric($this->CompStatus))) {
            $this->CompStatus = 1;
        }

        //Validate
        do {

            $val = ((isset($this->CompKeyID)) ? $this->CompKeyID : null); //vld: Comp.CompKeyID
            if ($validationMessage = validateField($val,'CompKeyID','nullok;positiveint',__CLASS__)) { break; }

            //public $TimestampCreate; //datetime   vld: Comp.TimestampCreate
            //public $TimestampUpdate; //timestamp  vld: Comp.TimestampUpdate

            $val = ((isset($this->CompName)) ? $this->CompName : null);  //vld: Comp.CompName
            if ($validationMessage = validateField($val,'CompName','nestring;minlen=2',__CLASS__)) { break; }

            $val = ((isset($this->CompCRAName)) ? $this->CompCRAName : null);  //vld: Comp.CompCRAName
            if ($validationMessage = validateField($val,'CompCRAName','nestring',__CLASS__)) { break; }

            //public $CompCRAAddress1; //varchar(250)   //vld: Comp.CompCRAAddress1
            //public $CompCRAAddress2; //varchar(250)   //vld: Comp.CompCRAAddress2
            //public $CompCRACity; //varchar(250)       //vld: Comp.CompCRACity
            //public $CompCRAState; //varchar(250)      //vld: Comp.CompCRAState
            //public $CompCRAZip; //int(9)              //vld: Comp.CompCRAZip
            //public $CompCRAPhone; //int(10)           //vld: Comp.CompCRAPhone

            $val = ((isset($this->CompCRAEmail)) ? $this->CompCRAEmail : null);  //vld: Comp.TrialKeyID
            if ($validationMessage = validateField($val,'CompCRAEmail', 'nullok;emptyok;email',__CLASS__)) { break; }

            $val = ((isset($this->CompTerminationDate)) ? $this->CompTerminationDate : null);  //vld: Comp.CompTerminationDate
            if ($validationMessage = validateField($val,'CompTerminationDate','nullok;zerodateok;date',__CLASS__)) { break; }

            $val = ((isset($this->CompTermination)) ? $this->CompTermination : null);  //vld: Comp.CompTermination
            if ($validationMessage = validateField($val,'CompTerminationDate','nullok;0or1',__CLASS__)) { break; }

            $val = ((isset($this->CompStatus)) ? $this->CompStatus : null);  //vld: Comp.CompStatus
            if ($validationMessage = validateField($val,'CompStatus', 'nullok;0or1',__CLASS__)) { break; }

        } while(false);

        return $validationMessage;
    }

}

