<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

switch ($_GET['action']) {
    case 'insert_trial':
        $prefix = "Add";
        break;
    case 'update_trial':
        $prefix = "Update";
        break;
    default:
        $prefix = "";
}

//load required classes
require_once ('lib/Site.class.php');
require_once ('lib/Trial.class.php');
require_once ('lib/Comp.class.php');
require_once ('lib/SiteTrialsOpenJoin.class.php');
require_once ('lib/TrialCompJoin.class.php');

//create required objects
$site  = new Site;
$trial = new Trial;
$comp  = new Comp;
$stoj  = new SiteTrialsOpenjoin;
$tcj   = new TrialCompJoin;

$site->Load_from_key($_GET['id']);
$comp->Load_from_key($_GET['compid']);
if (!empty($_GET['stojid'])) {
    $stoj->Load_from_key($_GET['stojid']);
}
if (!empty($_GET['tid'])) {
    $trial->Load_from_key($_GET['tid']);
    $selectTrials = $tcj->CreateSelectWhere('WHERE TrialCompJoin.TrialKeyID = ' . $_GET['tid'], $_GET['tid']);
} else {
    //omit trials terminated at trials
    $used = $stoj->GetKeysWhereOrderBy('TrialKeyID','SiteKeyID = '.$_GET['id'], 'TrialKeyID', 'ASC');
    $omit = $trial->GetKeysWhereOrderBy('TrialKeyID', 'WHERE ((TrialTermination=1) OR (TrialClose=1))', 'TrialKeyID', 'ASC');
    $omit = array_merge($omit, $used);
    if (count($omit) > 0) {
        $omitTrialIDs = join(',', $omit);
        $selectTrials = $tcj->CreateSelectWhere('WHERE CompKeyID=' . $_GET['compid'] . ' AND TrialCompJoin.TrialKeyID NOT IN (' . $omitTrialIDs . ')', $_GET['tid']);
    } else {
        $selectTrials = $tcj->CreateSelectWhere('WHERE CompKeyID=' . $_GET['compid'], $_GET['tid']);
    }
}


?>
<html>
	<head>
    <script>
    $(document).ready(function(){
       $('#TrialKeyIDOUT').addClass('req');
       $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', autoSize: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
       $('#TrialKeyIDOUT').attr('name', 'stoj[TrialKeyID]');
    <?php if ($comp->CompTermination == 1)
{ ?>
                $('#siteTrial input, #siteTrial select').attr('disabled', true);
                $('.msg').show();
                <?php } ?>
    });
    </script>
	</head>
	<body>
        <form name="siteTrial" id="siteTrial" method="post" action="<?php echo $_GET['url']; ?>">
            <input type="hidden" name="stoj[SiteKeyID]" value="<?php echo $_GET['id']; ?>" />
            <?php if (!empty($_GET['stojid']))
{ ?>
            <input type="hidden" name="stoj[SiteTrialOpenKeyID]" value="<?php echo $_GET['stojid']; ?>" />
            <?php } ?>
            <?php if (!empty($_GET['tid']))
{ ?>
            <input type="hidden" name="stoj[TrialKeyID]" value="<?php echo $_GET['tid']; ?>" />
            <?php } ?>
            <input type="hidden" id="siteTrial_action" name="action" value="<?php echo $_GET['action']; ?>" />
            <div class="extra-padding">
                <div class="extra-padding">
    <?php if ($comp->CompTermination == 1)
{ ?>
                    <div class="msg"><?php echo $comp->CompName; ?> is terminated and it's related trials are inaccessible.</div>
                <?php } ?>
                    <table class="centered fullwidth" style="width: 680px;">
                        <tr>
                            <td class="left"><strong>Trial:</strong><?php echo $selectTrials; ?></td>
                            <td class="left"><strong>Approval Date:</strong> </td>
                            <td class="left"><input type="text" name="stoj[SiteTrialOpenApprovalDate]" id="SiteTrialOpenApprovalDate" size="32" class="dpick req" value="<?php echo (!empty($stoj->SiteTrialOpenApprovalDate)) ? $stoj->SiteTrialOpenApprovalDate : ''; ?>" /></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="left"></td>
                            <td class="left"><strong>Close Date:</strong> </td>
                            <td class="left"><?php echo ((!empty($stoj->SiteTrialOpenCloseDate)) && ($stoj->SiteTrialOpenCloseDate != '0000-00-00')) ? $stoj->SiteTrialOpenCloseDate : 'N/A'; ?></td>
                            <td><?php if (!empty($stoj->SiteTrialOpenKeyID))
{ ?><a href="javascript:void(0);" class="confirm-close" onmouseup="$('div.extra-padding').hide();$('#confirm-close').show();window.parent.$.colorbox.resize();"  ><input type="checkbox" id="TrialClose" <?php echo ($stoj->SiteTrialOpenTermination == 1) ? 'disabled="disabled"' : ''; ?> /></a>&ensp;<?php echo ($stoj->SiteTrialOpenClose == 0) ? 'Close' : 'Un-Close'; ?> Trial<?php } ?></td>
                        </tr>
                        <tr>
                            <td class="left"></td>
                            <td class="left"><strong>Term Date:</strong> </td>
                            <td class="left"><?php echo ((!empty($stoj->SiteTrialOpenTerminationDate)) && ($stoj->SiteTrialOpenTerminationDate != '0000-00-00')) ? $stoj->SiteTrialOpenTerminationDate : 'N/A'; ?></td>
                            <td><?php if (!empty($stoj->SiteTrialOpenKeyID))
{ ?><a href="javascript:void(0);" class="confirm-terminate" onmouseup="$('div.extra-padding').hide();$('#confirm-terminate').show();window.parent.$.colorbox.resize();" ><input type="checkbox" id="TrialTermination" <?php echo ($stoj->SiteTrialOpenClose == 0) ? 'disabled="disabled"' : ''; ?> /></a>&ensp;<?php echo ($stoj->SiteTrialOpenTermination == 0) ? 'Terminate' : 'Un-Terminate'; ?> Trial<?php } ?></td>
                        </tr>
    <?php if ($comp->CompTermination != 1)
{ ?>
                        <tr>
                            <td class="right extra-padding" colspan="4"><a class="button floatnone" href="javascript:void(0);" onmouseup="return validate('siteTrial');$('#siteTrial').submit();">&emsp;Add/Update&emsp;</a>
                            <?php if (!empty($_GET['stojid']))
    { ?>
                            <a class="button floatnone" href="javascript:void(0);" onmouseup="$('div.extra-padding').hide();$('#confirm_delete').show();window.parent.$.colorbox.resize();" >&emsp;Delete&emsp;</a></td>
                            <?php } ?>
                        </tr>
                <?php } ?>
                    </table>
                </div>
            </div>
           <!-- confirm delete -->
            <div id="confirm_delete" class="colorboxes">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete <?php echo $trial->TrialProtocolNumber; ?> for this Site?<br />Doing so will remove all associations with this Site and it's Patients and Doctors.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$('#siteTrial_action').val('delete_trial');$('#siteTrial').submit();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            <!-- confirm close -->
            <div id="confirm-close" class="colorboxes">
                <table class="centered">
                    <tr>
                    <?php if ($stoj->SiteTrialOpenClose == 0)
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Close <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will prevent updating of this Trial. You must also enter a Close Date for the Trial before updating.</td>
                    <?php } else
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Un-Close <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will enable updating of this Trial.</td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="window.parent.$.colorbox({href:'confirm_site.php?url=<?php echo $_GET['url']; ?>&id=<?php echo $site->SiteKeyID; ?>&tid=<?php echo $trial->TrialKeyID; ?>&stojid=<?php echo $stoj->SiteTrialOpenKeyID; ?>&action=close&status=<?php echo $stoj->SiteTrialOpenClose; ?>&stojid=<?php echo $stoj->SiteTrialOpenKeyID; ?>'});">&emsp;Yes&emsp;</button></td>
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm close -->
            <!-- confirm terminate -->
            <div id="confirm-terminate" class="colorboxes">
                <table class="centered">
                    <tr>
                    <?php if ($stoj->SiteTrialOpenTermination == 0)
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Terminate <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will prevent updating of this Trial for this Site. You must also enter a Terminate Date for the Trial before updating.</td>
                    <?php } else
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Un-Terminate <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will enable updating of this Trial.</td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="window.parent.$.colorbox({href:'confirm_site.php?url=<?php echo $_GET['url']; ?>&id=<?php echo $site->SiteKeyID; ?>&tid=<?php echo $trial->TrialKeyID; ?>&stojid=<?php echo $stoj->SiteTrialOpenKeyID; ?>&action=terminate&status=<?php echo $stoj->SiteTrialOpenTermination; ?>&stojid=<?php echo $stoj->SiteTrialOpenKeyID; ?>'});">&emsp;Yes&emsp;</button></td>
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm terminate -->
            
         </form>
	</body>
</html>
<?php


