<?php
error_reporting(0);
//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Permissions.class.php');
require_once ('lib/Trial.class.php');
require_once ('lib/Comments.class.php');
require_once ('lib/TrialDiseaseType.class.php');
require_once ('lib/TrialTrialDiseaseTypeJoin.class.php');
require_once ('lib/TrialPhase.class.php');
require_once ('lib/TrialCategory.class.php');
require_once ('lib/TrialType.class.php');
require_once ('lib/TrialGroup.class.php');
require_once ('lib/TrialCompanion.class.php');
require_once ('lib/TrialCompJoin.class.php');
require_once ('lib/Patient.class.php');
require_once ('lib/SiteTrialsOpenJoin.class.php');
$pageURL = getPageUrl();

//create required objects
$tdt                 = new TrialDiseaseType();
$ttdtj               = new TrialTrialDiseaseTypeJoin();
$permissions         = new Permissions();
$trial               = new Trial();
$comment             = new Comments();
$trialPhase          = new TrialPhase();
$trialCategory       = new TrialCategory();
$trialType           = new TrialType();
$trialGroup          = new TrialGroup();
$trialCompanion      = new TrialCompanion();
$trialCompanionTrial = new Trial();
$tcj                 = new TrialCompJoin();
$patient             = new Patient();
$stoj                = new SiteTrialsOpenjoin();

try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        if ($action == 'close') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'close trial', $action, $_POST);
            $trial->Load_from_key($_POST['TrialKeyID']);
            $trial->TrialClose = $_POST['trial']['TrialClose'];
            $trial->TrialCloseDate = (!empty($_POST['trial']['TrialCloseDate'])) ? $_POST['trial']['TrialCloseDate'] : '';
            $trial->Save_Active_Row();
            $comment->Comment = $_POST['comment'];
            $comment->TrialKeyID = $_POST['TrialKeyID'];
            $comment->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }

        if ($action == 'terminate') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'terminate trial', $action, $_POST);
            $trial->Load_from_key($_POST['TrialKeyID']);
            $trial->TrialTermination = $_POST['trial']['TrialTermination'];
            $trial->TrialTerminationDate = (!empty($_POST['trial']['TrialTerminationDate'])) ? $_POST['trial']['TrialTerminationDate'] : '';
            $trial->Save_Active_Row();
            $comment->Comment = $_POST['comment'];
            $comment->TrialKeyID = $_POST['TrialKeyID'];
            $comment->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }

        if ($action == 'suspend') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'suspend trial', $action, $_POST);
            $trial->Load_from_key($_POST['TrialKeyID']);
            $trial->TrialSuspend = $_POST['trial']['TrialSuspend'];
            $trial->Save_Active_Row();
            TrackChanges::endUserAction();
        }

        if (($action == 'update') || ($action == 'insert')) {
            if (!empty($_GET['id'])) {
                $trial->Load_from_key($_GET['id']);
            }
            //Copy a few dropdowns into the $_POST['trial'] array
            foreach(array('TrialTypeKeyID', 'TrialPhaseKeyID', 'TrialCategoryKeyID') as $key) {
                if ((isset($_POST[$key])) && (!isset($_POST['trial'][$key]))) {
                    //Copy some dropdowns into the 'trial' array so they are updated in the foreach 4 lines below
                    $_POST['trial'][$key] = $_POST[$key];
                }
            }
            //Update the trial object with our POST variables
            foreach ($_POST['trial'] as $key => $val) {
                $trial->$key = $val;
            }
            //Did our spanish summary change? If so then push our TrialSummaryEn into the TrialSummaryLast column
            //This means that we remember the english version in the LAST column at the point of the last spanish change
            if ((isset($_POST['TrialSummaryEsBefore'])) &&
                (isset($_POST['trial']['TrialSummaryEs'])) &&
                (isset($_POST['trial']['TrialSummaryEn'])) &&
                ($_POST['TrialSummaryEsBefore'] != $_POST['trial']['TrialSummaryEs'])) {
                $trial->TrialSummaryLast = $_POST['trial']['TrialSummaryEn'];
            }

            if (!empty($_POST['trial']['TrialQoL'])) {
                $trial->TrialQoL = 1;
            } else {
                $trial->TrialQoL = 0;
            }

            if ($action == 'update') {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'update trial', $action, $_POST);
                $ttdtj->Delete_row_from_key('TrialKeyID', $_GET['id']);
                if (empty($_POST['trial']['TrialReportExclude'])) {
                    $trial->TrialReportExclude = 0;
                }
                $trial->Save_Active_Row();
            } else {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'add trial', $action, $_POST);
                $trial_key_id = $trial->Save_Active_Row_as_New();
            }
            foreach ($_POST['TrialDiseaseType'] as $val) {
                $ttdtj->TrialKeyID = $trial->TrialKeyID;
                $ttdtj->TrialDiseaseTypeKeyID = $val;
                $ttdtj->Save_Active_Row_as_New();
            }
            TrackChanges::endUserAction();

            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: trialViewEdit.php?id=" . $trial->TrialKeyID);
            }
        }

        if ($action == 'delete') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete trial', $action, $_POST);
            $ttdtj->Delete_row_from_key('TrialKeyID', $_GET['id']);
            $trial->Delete_row_from_key($_GET['id']);
            $trialCompanion->Delete_row_from_key('TrialKeyIDIN', $_GET['id']);
            $trialCompanion->Delete_row_from_key('TrialKeyIDOUT', $_GET['id']);
            $trialGroup->Delete_row_from_key('TrialKeyID', $_GET['id']);
            $tcj->Delete_row_from_key('TrialKeyID', $_GET['id']);
            //$comment->Delete_row_from_key('TrialKeyID', $_GET['id']);
            TrackChanges::endUserAction();
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: trials.php");
            }
        }

        if (($action == 'group_update') || ($action == 'group_insert')) {
            if ($action == 'group_update') {
                $trialGroup->Load_from_key($_POST['TrialGroupKeyID']);
            }
            $trialGroup->TrialGroupTitle = $_POST['TrialGroupTitle'];
            $trialGroup->TrialGroupDescription = $_POST['TrialGroupDescription'];
            $trialGroup->TrialKeyID = $_GET['id'];
            if ($action == 'group_update') {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'update trial-group', $action, $_POST);
                $trialGroup->Save_Active_Row();
                TrackChanges::endUserAction();
            } else {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'add trial-group', $action, $_POST);
                $trialGroup->Save_Active_Row_as_New();
                TrackChanges::endUserAction();
            }
        }

        if ($action == 'group_delete') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete trial-group', $action, $_POST);
            $trialGroup->Delete_row_from_key('TrialGroupKeyID', $_POST['TrialGroupKeyID']);
            TrackChanges::endUserAction();
        }

        if ($action == 'insert_companion') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'add companion', $action, $_POST);
            $trialCompanion->TrialKeyIDIN = $_POST['TrialKeyIDIN'];
            $trialCompanion->TrialKeyIDOUT = $_POST['TrialKeyIDOUT'];
            $trialCompanion->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }

        if ($action == 'delete_companion') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete companion', $action, $_POST);
            $trialCompanion->Load_from_key($_POST['TrialCompanionKeyID']);
            $trialCompanion->Delete_row_from_key('TrialCompanionKeyID', $_POST['TrialCompanionKeyID']);
            TrackChanges::endUserAction();
        }
        //header("location: $pageURL");

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }//!empty($_POST['action']

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


//define required variables
$msg = '';

//Enforce user permissions or redirect to login.php
list($UserTypeIDs) = enforceUserPermissions($permissions);


if (!empty($_GET['id'])) { //editing trial
    //first get URL

    $trial->Load_from_key($_GET['id']);
    $ttdtj_keys = $ttdtj->GetKeysWhereOrderBy('TrialKeyID = \'' . $_GET['id'] . '\'', 'TrialDiseaseTypeKeyID', 'ASC');
    $tg_keys = $trialGroup->GetKeysWhereOrderBy('TrialKeyID = \'' . $_GET['id'] . '\'', 'TrialGroupKeyID', 'ASC');
    if (count($tg_keys) > 0) {
        $i = 0;
        foreach ($tg_keys as $TrialGroupKeyID) {
            $trialGroup->Load_from_key($TrialGroupKeyID);
            $trialGroups[$i] = '<tr><td class="left">' . $trialGroup->TrialGroupTitle . '</td><td class="left">' . $trialGroup->TrialGroupDescription . '</td><td class="right"><a class="colorbox" href="trialGroup.php?id=' . $trial->TrialKeyID . '&gid=' . $trialGroup->TrialGroupKeyID . '&url=' . $pageURL . '">Edit</a></td></tr>';
            $i++;
        }
    }
    $tc_keys = $trialCompanion->GetKeysWhereOrderBy('TrialKeyIDOUT', 'TrialKeyIDIN = \'' . $_GET['id'] . '\'', 'TrialKeyIDOUT', 'ASC');
    if (count($tc_keys) > 0) {
        $i = 0;
        foreach ($tc_keys as $TrialCompanionKeyID) {
            $trialCompanionTrial->Load_from_key($TrialCompanionKeyID);
            $trialCompanionIDforDelete = $trialCompanion->GetKeysWhereOrderBy('TrialCompanionKeyID', 'TrialKeyIDIN = \'' . $_GET['id'] . '\' AND TrialKeyIDOUT = \'' . $TrialCompanionKeyID . '\'', 'TrialCompanionKeyID', 'ASC');
            $trialCompanionTrials[$i] = '<tr><td class="left">' . $trialCompanionTrial->TrialProtocolNumber . '</td><td class="right"><a class="colorbox" href="deleteTrialCompanion.php?id=' . $trial->TrialKeyID . '&cid=' . $trialCompanionIDforDelete[0] . '&url=' . $pageURL . '">Delete</a></td></tr>';
            $i++;
        }
    }
    //$comment_keys = $comment->GetKeysWhereOrderBy('TrialKeyID = \'' . $_GET['id'] . '\'', 'CommentKeyID', 'ASC');
    $comment_keys = $comment->GetKeysByFieldConditionAndValue('Comments', 'CommentKeyID', 'TrialKeyID', '=', $_GET['id'], 'CommentKeyID', 'ASC');

    $comment->Load_from_key($comment_keys[0]);
} else {
    $ttdtj_keys = '';
}

$trialPhaseSelect = $trialPhase->CreateSelect($trial->TrialPhaseKeyID);
$trialTypeSelect = $trialType->CreateSelect($trial->TrialTypeKeyID);
$trialCategorySelect = $trialCategory->CreateSelect($trial->TrialCategoryKeyID);
$tdt_keys = $tdt->GetKeysOrderBy('TrialDiseaseTypeName', 'ASC');
$checkboxes = array();
$i = 0;
foreach ($tdt_keys as $tdt_key) {
    $checked = (in_array($tdt_key, $ttdtj_keys)) ? 'true' : '';
    $tdt->CreateCheckbox($tdt_key, $checked);
    $checkboxes[$i] = $tdt->TrialDiseaseTypeCheckbox;
    $i++;
}

foreach ($checkboxes as $checkbox) {
    $diseaseTypeCheckboxes .= $checkbox . '<br>';
}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<meta name="author" content="Cliff Garrett" />
		<title>
			Trial
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
		<script type="text/javascript">
        	$(document).ready(function(){
        	   $("a.confirm-delete").colorbox({inline:true});
        	   $("a.confirm-close").colorbox({inline:true, width:'650px'});
        	   $("a.confirm-terminate").colorbox({inline:true, width:'650px'});
        	   $("a.confirm-suspend").colorbox({inline:true, width:'650px'});
        	   $(".colorbox").colorbox();
               $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
               <?php if (!empty($_GET['id']))
{ ?>
               $('#action').val('update');
               <?php } else
{ ?>
               $('#action').val('insert');
               <?php } ?>
               
               $('#TrialTypeKeyID').addClass('req');
               /*$('#TrialCategoryKeyID').addClass('req');*/
               $('#TrialPhaseKeyID').addClass('req');
               $('.req').each(function(){
                    $(this).attr('title', 'Required Field');
               });
               

               <?php if ($trial->TrialTermination == 1)
{ ?>
                        $('input, select, textarea, button, .button').attr('disabled', true);
                        $('#TrialTermination').attr('disabled', false);
                        $()
                        <?php } elseif ($trial->TrialClose == 1)
{ ?>
                        $('input, select, textarea, button, .button').attr('disabled', true);
                        $('#TrialTermination').attr('disabled', false);
                        $('#TrialClose').attr('disabled', false);
                        <?php } elseif ($trial->TrialSuspend == 1)
{ ?>
                        $('input, select, textarea, button, .button').attr('disabled', true);
                        $('#TrialTermination').attr('disabled', true);
                        $('#TrialSuspend').attr('disabled', false);
                        $('#TrialClose').attr('disabled', false);
                        <?php } ?>
                $('button.floatnone, input:hidden').attr('disabled', false);
                    $('input:disabled').parent('.reqwrap').css('background', 'silver');

        	});
            
            function protocolCompare(protocol){
                $.get('ajaxTrialProtocolCheck.php?id='+protocol, function(data){
                    if (data != 'pass') {
                        alert('Protocol already exists!');
                        $('#TrialProtocolNumber').val('');
                        $('#TrialProtocolNumber').focus();
                    }
                });
            }
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                    <?php
                    print displayTopRightInfo();
                    print displayTabs('trials');
                    ?>
    				<div id="tabs-1" class="ui-tabs-panel">
    					<form method="post" action="" name="trialform" id="trialform">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="continue" id="continue" />
                        <?php if ($_GET['id'])
{ ?>
                        <input type="hidden" name="trial[TrialKeyID]" id="TrialKeyID" value="<?php echo $_GET['id']; ?>" />
                        <?php } ?>
                    <div class="subhead">
    					Trial Add/Edit
                            <a href="<?php echo (!empty($_GET['id'])) ? 'trialViewEdit.php?id=' . $_GET['id'] : 'trials.php'; ?>" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                        <table border="0" class="fullwidth">
                          <tr>
                            <td><strong>Protocol Number:</strong> </td>
                            <td><input class="req" type="text" name="trial[TrialProtocolNumber]" id="TrialProtocolNumber" value="<?php echo (!empty($trial->TrialProtocolNumber)) ? $trial->TrialProtocolNumber : ''; ?>" onblur="protocolCompare(this.value)" /></td>
                            <td><strong>Trial Type:</strong> </td>
                            <td><?php echo $trialTypeSelect; ?></td>
                            <td><strong>Open Date:</strong> </td>
                            <td><input class="req dpick" type="text" name="trial[TrialOpenDate]" id="TrialOpenDate" size="32" value="<?php echo (!empty($trial->TrialOpenDate)) ? $trial->TrialOpenDate : ''; ?>" /></td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="2">
                                <strong>CIRB:&emsp;</strong>
                                Yes <input type="radio" name="trial[TrialCIRB]" value="1" <?php echo ($trial->TrialCIRB == '1') ? 'checked' : ''; ?> />&nbsp;
                                No <input type="radio" name="trial[TrialCIRB]" value="0" <?php echo (($trial->TrialCIRB == '0')||(empty($_GET['id']))) ? 'checked' : ''; ?> />
                            </td>
                            <td><strong>Trial Phase:</strong> </td>
                            <td><?php echo $trialPhaseSelect; ?></td>
                            <td><?php if (!empty($trial->TrialKeyID))
{ ?><strong>Close Date:</strong> <?php } ?></td>
                            <td><?php echo ((!empty($trial->TrialCloseDate)) && ($trial->TrialCloseDate != '0000-00-00')) ? $trial->TrialCloseDate : ''; ?></td>
                            <td><?php if (!empty($trial->TrialKeyID))
{ ?><a href="#confirm-close" class="confirm-close" ><input type="checkbox" id="TrialClose" <?php echo ($trial->TrialClose == 0) ? '' : 'checked'; ?> /></a>&ensp;Close Trial<?php } ?></td>
                          </tr>
                          <tr>
                            <td><strong>Trial Credit:</strong> </td>
                            <td><input class="req" type="text" name="trial[TrialCredit]" id="TrialCredit" value="<?php echo (!empty($trial->TrialCredit)) ? $trial->TrialCredit : ''; ?>" /></td>
                            <td><strong>Trial Category:</strong> </td>
                            <td><?php echo $trialCategorySelect; ?></td>
                            <td><?php if (!empty($trial->TrialKeyID))
{ ?><strong>Term Date:</strong> <?php } ?></td>
                            <td><?php echo ((!empty($trial->TrialTerminationDate)) && ($trial->TrialTerminationDate != '0000-00-00')) ? $trial->TrialTerminationDate : ''; ?></td>
                            <td><?php if (!empty($trial->TrialKeyID))
{ ?><a href="#confirm-terminate" class="confirm-terminate"><input type="checkbox" id="TrialTermination" class="ays-ignore" <?php echo ($trial->TrialTermination == 0) ? '' : 'checked'; ?> /></a>&ensp;Terminate Trial<?php } ?></td>
                          </tr>
                          <tr>
                            <td colspan="2"><strong>Trial QoL: <input type="checkbox" name="trial[TrialQoL]" id="TrialQoL" <?php echo ($trial->TrialQoL == 0) ? '' : 'checked'; ?> />&emsp;Trial QoLC:&nbsp;</strong><input type="text" name="trial[TrialQoLC]" id="TrialQoLC" maxlength="13" value="<?php echo $trial->TrialQoLC; ?>" /><!-- size="4" --></td>
                            <td><strong>NCT:</strong> </td>
                            <td colspan="3">
                                <input type="text" name="trial[TrialNct]" id="TrialNct" value="<?php echo (!empty($trial->TrialNct)) ? $trial->TrialNct : ''; ?>" />

                                &nbsp;&nbsp;&nbsp;<div name="TrialNctLinks" id="TrialNctLinks" style="display:inline;">&nbsp;</div>
                                <script type="text/javascript">
                                    //https://www.morpheusrising.com/sccc/ajax_ClinicalTrialsGovCheck.php?trialnct=NCT00000102
                                    var lastCheckedTrialNct = '';

                                    function checkTrialNct() {
                                        var trialnct       = $("#TrialNct").val();
                                        var linktext       = "";
                                        var summaryEnField = $("#TrialSummaryEn");
                                        var summaryEnValue = "";
                                        var title          = '';

                                        if (trialnct) {
                                            //HAVE TRIALNC-STRING
                                            if (lastCheckedTrialNct != trialnct) {
                                                lastCheckedTrialNct = trialnct;
                                                $.getJSON("ajax_ClinicalTrialsGovCheck.php?trialnct=" + trialnct, function (data) {
                                                    //console.debug(data);
                                                    if (data.ok) {
                                                        var link = 'https://clinicaltrials.gov/ct2/show/' + encodeURI(trialnct);
                                                        var linktext1 = "<a href='" + link + "' target='top' style='font-size:8pt;'>[NCT]</a>";
                                                        var linktext2 = "<a href='trialNctData.php?format=xml&trialnct=" + encodeURI(trialnct) + "' target='top' style='font-size:8pt;'>[xml]</a>";
                                                        var linktext3 = "<a href='trialNctData.php?format=flat&trialnct=" + encodeURI(trialnct) + "' target='top' style='font-size:8pt;'>[flat]</a>";
                                                        linktext = linktext1 + '&nbsp;&nbsp;' + linktext2 + '&nbsp;&nbsp;' + linktext3;
                                                        if (data.havesummary) {
                                                            summaryEnValue = String(data.summary);
                                                        }
                                                        title = data.title;
                                                    } else {
                                                        linktext       = "<div style='display:inline;background-color:red;color:white;'>ERROR: NCT page not found!</div";
                                                        summaryEnValue = "";
                                                    }//if data.ok

                                                    $("#TrialNctLinks").html(linktext);
                                                    $("#TrialSummaryEn").html(String(summaryEnValue));
                                                    $("#ClinicalTrialsTitle").html('&nbsp;&nbsp;' + title);
                                                });
                                            }//if trialnc != last...
                                        } else {
                                            //EMPTY TRIALNC-STRING
                                            $("#TrialNctLinks").html('');
                                            $("#TrialSummaryEn").html('');
                                            $("#ClinicalTrialsTitle").html('');
                                        }

                                        setTimeout(checkTrialNct, 500);
                                    }

                                    setTimeout(checkTrialNct, 500);
                                </script>
                            </td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><strong>Target Accrual:</strong> </td>
                            <td><input class="req" type="text" name="trial[TrialTargetAccrual]" id="TrialTargetAccrual" value="<?php echo (!empty($trial->TrialTargetAccrual)) ? $trial->TrialTargetAccrual : ''; ?>" /></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><?php if (!empty($trial->TrialKeyID))
{ ?><a href="#confirm-suspend" class="confirm-suspend"><input type="checkbox" id="TrialSuspend" class="ays-ignore" <?php echo ($trial->TrialSuspend == 0) ? '' : 'checked'; ?> /></a>&ensp;Suspend Trial<?php } ?></td>
                          </tr>
                          <tr>
                            <td colspan="7"><input name="trial[TrialReportExclude]" value="1" type="checkbox" <?php echo ($trial->TrialReportExclude == 1) ? 'checked' : ''; ?> /> Exclude Trial from Reports</td>
                          </tr>
                        </table>
                        <hr />
                            <?php if (!empty($trial->TrialKeyID))
{ ?>
                        <table border="0" class="halfwidth floatright">
                          <tr>
                            <td>
                                <div class="formdiv">
                                    <a href="trialGroup.php" class="colorbox" id="trial-group-btn"><button type="button" class="button">Add Group</button></a>
                                    <div class="clear"></div>
                                    <table class="fullwidth zebra">
                                        <thead>
                                            <tr><th class="left"><div class="vertical-padding-light">Trial Groups</div></th><th></th><th></th></tr>
                                        </thead>
                                        <tbody>
                                            <?php     foreach ($trialGroups as $trialGroupRow)
    {
        echo $trialGroupRow;
    } ?>
                                      </tbody>
                                    </table>
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td>
                                <div class="formdiv">
                                    <a href="trialCompanion.php?id=<?php echo $trial->TrialKeyID; ?>" class="colorbox" id="trial-companion-btn"><button type="button" class="button">Add Companion Trial</button></a>
                                    <div class="clear"></div>
                                    <table class="fullwidth zebra">
                                        <thead>
                                            <tr><th class="left"><div class="vertical-padding-light">Companion Trials</div></th><th></th></tr>
                                        </thead>
                                        <tbody>
                                            <?php     foreach ($trialCompanionTrials as $trialCompanionTrialRow)
    {
        echo $trialCompanionTrialRow;
    } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                          </tr>
                        </table>
                                <?php } ?>
                        <table border="0" class="halfwidth">
                          <tr>
                            <td><div class="vertical-padding-light"><strong>Disease Types:</strong></div><div class="checkboxes"><?php echo $diseaseTypeCheckboxes; ?></div></td>
                          </tr>
                          <tr>
                            <td><div class="vertical-padding-light"><strong>Comment:</strong></div><textarea name="trial[TrialComment]" id="Comment" class="fullwidth" rows="4"><?php echo (!empty($trial->TrialComment)) ? $trial->TrialComment : ''; ?></textarea></td>
                          </tr>
                        </table>

                        <br />
                        <table border="1" class="fullwidth">
                            <tr>
                                <td>
                                    <table border="0" class="fullwidth">
                                        <tr class="thead_unselected">
                                            <td align="center" style="color:white;" colspan="4">
                                                Trial Information <div name='ClinicalTrialsTitle' id='ClinicalTrialsTitle' style="display:inline;"></div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="1%">&nbsp;</td>
                                            <td width="15%" valign="top">
                                                <div class="vertical-padding-light"><strong>Treatment:&nbsp;</strong></div>
                                            </td>
                                            <td>
                                                <textarea name="trial[TrialTreatment]" id="TrialTreatment" class="fullwidth" rows="4"><?php echo (!empty($trial->TrialTreatment)) ? $trial->TrialTreatment : ''; ?></textarea>
                                                <br />
                                            </td>
                                            <td width="1%">&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td width="1%">&nbsp;</td>
                                            <td width="15%" valign="top">
                                                <div class="vertical-padding-light"><strong>Summary:&nbsp;</strong></div>
                                            </td>
                                            <td>
                                                <textarea readonly="readonly" style="border:0;background-color:#DDDDDD" name="trial[TrialSummaryEn]" id="TrialSummaryEn" class="fullwidth" rows="4"><?php echo (!empty($trial->TrialSummaryEn)) ? $trial->TrialSummaryEn : ''; ?></textarea>
                                                <script type="text/javascript">
                                                    var summaryEnField = $("#TrialSummaryEn");
                                                    var summaryEnBefore = summaryEnField.html();
                                                </script>
                                                <br />
                                            </td>
                                            <td width="1%">&nbsp;</td>
                                        </tr>

                                        <tr>
                                            <td width="1%">&nbsp;</td>
                                            <td width="15%" valign="top">
                                                <div class="vertical-padding-light"><strong>Summary(ES):&nbsp;</strong></div>
                                            </td>
                                            <td>
                                                <input type="hidden" name="TrialSummaryEsBefore" id="TrialSummaryEsBefore" value="<?php echo (!empty($trial->TrialSummaryEs)) ? htmlentities($trial->TrialSummaryEs) : ''; ?>">

                                                <textarea name="trial[TrialSummaryEs]" id="TrialSummaryEs" class="fullwidth" rows="4"><?php echo (!empty($trial->TrialSummaryEs)) ? htmlentities($trial->TrialSummaryEs) : ''; ?></textarea>
                                                <br />
                                            </td>
                                            <td width="1%">&nbsp;</td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>

                        <div class="clear"></div>
                        <div class="right formdiv noborder">
                            <button type="button" class="inline" onmouseup="return validate('trialform'); $('#trialform').submit();">Add/Update</button>&emsp;
                            <?php if ((!empty($_GET['id'])) && ($_SESSION['UserTypeKeyID'] == 1))
{
    $uses1 = $patient->GetKeysWhere('PatientKeyID', 'WHERE TrialKeyID = ' . $_GET['id']);
    $uses2 = $stoj->GetKeysWhereOrderBy('SiteTrialOpenKeyID', 'TrialKeyID = ' . $_GET['id'], 'SiteTrialOpenKeyID');
    $uses3 = $tcj->GetKeysWhereOrderBy('TrialKeyID', 'TrialKeyID = ' . $_GET['id'], 'TrialKeyID');
    $uses = array_merge($uses1, $uses2, $uses3);
    if (count($uses) == 0)
    { ?>
                                        <a href="#confirm-delete" class="confirm-delete"><button type="button" class="inline">Delete</button></a>
                            <?php }
} ?>
                        </div>
    				</form>
    			</div>
    		</div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Delete <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will remove all associations with Communities, Sites, Patients and Doctors.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            
            <!-- confirm close -->
            <div id="confirm-close">
                <table class="centered fullwidth initial">
                    <tr>
                    <?php if ($trial->TrialClose == 0)
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Close <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will prevent updating of this Trial. You must also enter a Close Date and Comment for the Trial before updating.</td>
                    <?php } else
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Un-Close <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will enable updating of this Trial. You must also enter a Comment for the Trial before updating.</td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><button class="floatnone" onclick="$.colorbox({href:'confirm_trial.php?url=<?php echo $pageURL; ?>&id=<?php echo $trial->TrialKeyID; ?>&action=close&status=<?php echo $trial->TrialClose; ?>'});">&emsp;Yes&emsp;</button></td>
                        <td class="center extra-padding"><button class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm close
            
            <!-- confirm terminate -->
            <div id="confirm-terminate">
                <table class="centered fullwidth initial">
                    <tr>
                    <?php if ($trial->TrialTermination == 0)
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Terminate <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will prevent updating of this Trial. You must also enter a Terminate Date and Comment for the Trial before updating.</td>
                    <?php } else
{ ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Un-Terminate <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will enable updating of this Trial. You must also enter a Comment for the Trial before updating</td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><button class="floatnone" onclick="$.colorbox({href:'confirm_trial.php?url=<?php echo $pageURL; ?>&id=<?php echo $trial->TrialKeyID; ?>&action=terminate&status=<?php echo $trial->TrialTermination; ?>'});">&emsp;Yes&emsp;</button></td>
                        <td class="center extra-padding"><button class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm terminate
            
            <!-- confirm suspend -->
            <div id="confirm-suspend">
                <form id="suspend" action="" method="post">
                    <?php echo ($trial->TrialSuspend == 1) ? '<input type="hidden" name="trial[TrialSuspend]" value="0" />' : '<input type="hidden" name="trial[TrialSuspend]" value="1" />'; ?>
                    <input type="hidden" name="TrialKeyID" value="<?php echo $trial->TrialKeyID; ?>" />
                    <input type="hidden" name="action" value="suspend" />
                    <table class="centered fullwidth initial">
                        <tr>
                            <td class="center extra-padding" colspan="2">Are you sure you want to <?php echo ($trial->TrialSuspend == 1) ? 'Un-' : ''; ?>Suspend <?php echo $trial->TrialProtocolNumber; ?>?<br />Doing so will prevent updating of this Trial.</td>
                        </tr>
                        <tr>
                            <td class="center extra-padding"><button type="button" class="floatnone" onclick="$('#suspend').submit();">&emsp;Yes&emsp;</button></td>
                            <td class="center extra-padding"><button type="button" class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                        </tr>
                    </table>
                </form>
            </div>
            <!-- end confirm suspend -->
            
        </div>
	</body>
</html>
<?php


/**
 * Enforce user permissions or redirect to login.php
 *
 * @param $permissions
 * @return array array($UserTypeIDs)
 */
function enforceUserPermissions($permissions) {
    $permissions->Load_from_action("AddEditTrials");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }
    $permissions->Load_from_action("DeleteTrials");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }
    return array($UserTypeIDs);
}


