<?php


function queryLibraryGetSql($format, $connection, &$reportInfo) {
    $sql = 'SELECT * FROM `Patient` WHERE 1=0';  //No output is the default

    if      ($reportInfo['report']=='cred')   { 
        $sql = queryLibraryGetSqlForPrintCreditReport($format, $connection, $reportInfo);
    } else if ($reportInfo['report']=='prr')    { 
        $sql = queryLibraryGetSqlForPatientRegistrationReport($format, $connection, $reportInfo);
    } else if ($reportInfo['report']=='dac')    { 
        $sql = queryLibraryGetSqlForDoctorAccrualsReport($format, $connection, $reportInfo);
    } else if ($reportInfo['report']=='dacbs')  { 
        $sql = queryLibraryGetSqlForDoctorAccrualsBySiteReport($format, $connection, $reportInfo);
    } else if ($reportInfo['report']=='master') { 
        $sql = queryLibraryGetSqlForMasterReport($format, $connection, $reportInfo);
    } else if ($reportInfo['report']=='tracking') {
        $sql = queryLibraryGetSqlForTrackingReport($format, $connection, $reportInfo);
    }

    $reportInfo['sql'] = $sql;

    return $sql;
}


/**
 * Generates the SQL query for the 'Credit Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo           Reference! Array will be modified (sql added).
 */
function queryLibraryGetSqlForPrintCreditReport($format, $connection, &$reportInfo) {
    $sql = '';
    
    try {
        $post = (isset($reportInfo['post']))  ?  $reportInfo['post'] : array();

        $quReportStartDate = (isset($post['ReportStartDate'])) ?  $connection->Quote($post['ReportStartDate'])  : '2000-01-01';
        $quReportEndDate   = (isset($post['ReportEndDate']))   ?  $connection->Quote($post['ReportEndDate'])    : '2099-12-31';

        $sql      = "SELECT `Trial`.`TrialProtocolNumber` AS Protocol,\n".
                    "       CONCAT( '; proto=',  `Trial`.`TrialProtocolNumber`\n".
                    "              ,'; credit=', `Patient`.`PatientTrialCredit`\n";
        if (isset($post['cred_sex'])) {
            $sql .= "              ,'; sex=', PatientSex.PatientSexText\n";
        }
        if (isset($post['cred_race'])) {
            $sql .= "              ,'; race=', (IF (Patient.PatientMultiRacial, 'Multi-Racial', PatientRace.PatientRaceText))\n";
        }
        if (isset($post['cred_ethn'])) {
            $sql .= "              ,'; ethn=', PatientEthnicity.PatientEthnicityText\n";
        }

        $sql     .= "             ) AS x_groupkey,\n";
       
        if (isset($post['cred_sex'])) {
            $sql  .= "      `PatientSex`.`PatientSexText` as Sex,\n";
        }
        if (isset($post['cred_race'])) {
            $sql  .= "      IF (`Patient`.`PatientMultiRacial`, 'Multi-Racial', `PatientRace`.`PatientRaceText`) as Race,\n";
        }
        if (isset($post['cred_ethn'])) {
            $sql  .= "      PatientEthnicity.PatientEthnicityText as Ethnicity,\n";
        }
       
        $sql     .= "       COUNT(*) AS Accruals,\n".
                    "       `Patient`.`PatientTrialCredit` AS Credits,\n".
                    "       (`Patient`.`PatientTrialCredit` * COUNT(*)) AS Total_Credits\n".
                    "-- (MORE_SELECTS)\n".
                    "FROM `Trial`\n".
                    "LEFT JOIN `Patient`     ON `Trial`.`TrialKeyID` = `Patient`.`TrialKeyID`\n".
                    "LEFT JOIN `PatientSex`  ON `Patient`.`PatientSex` = `PatientSex`.`PatientSexKeyID`\n".
                    "LEFT JOIN `PatientRace` ON `Patient`.`PatientRace` = `PatientRace`.`PatientRaceKeyID`\n".
                    "LEFT JOIN `PatientEthnicity` ON `Patient`.`PatientEthnicity` = `PatientEthnicity`.`PatientEthnicityKeyID`\n".
                    "-- (MORE_JOINS)\n".
                    "WHERE   `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'\n".
                    "AND     `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'\n".
                    "AND     `Trial`.`TrialReportExclude`<>1\n".
                    "-- (MORE_WHERE_CLAUSES)\n".
                    "GROUP BY x_groupkey\n".
                    "ORDER BY x_groupkey ASC, Credits DESC\n".
                    "-- (LIMIT_CLAUSE)\n";

        //print "<pre>"; print $sql; print "</pre>"; exit;
    } catch(Exception $e) {
        $sql = '';
    }

    if (!$sql) { $sql = 'SELECT * FROM `Patient` WHERE 1=0'; }   //No output is the default
    return $sql;
}


/**
 * Generates the SQL query for the 'Patient Registration Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo           Reference! Array will be modified (sql added).
 */
function queryLibraryGetSqlForPatientRegistrationReport($format, $connection, &$reportInfo) {
    $sql = '';

    try {
        $post = (isset($reportInfo['post']))  ?  $reportInfo['post'] : array();

        $quReportStartDate     = (isset($post['ReportStartDate']))  ?  $connection->Quote($post['ReportStartDate'])    : '2000-01-01';
        $quReportEndDate       = (isset($post['ReportEndDate']))    ?  $connection->Quote($post['ReportEndDate'])      : '2099-12-31';

        $quSecondarySortField  = (isset($post['prr_OrderByField'])) ?  $connection->Quote($post['prr_OrderByField'])   : 'Date_of_Registration';
        $quSecondarySortUpDn   = (isset($post['prr_OrderByUpDn']))  ?  $connection->Quote($post['prr_OrderByUpDn'])    : 'ASC';
        $quSecondarySort       = ", $quSecondarySortField $quSecondarySortUpDn";

        //Spreadsheet doesn't have site-indicators so we need to add the Site
        $irbColum = '';
        if (isset($post['prr_submit_excel'])) {
            $irbColumn = "CONCAT(`Comp`.`CompName`, ' ', `Site`.`SiteName`) AS IRB,\n". //, ', ', `States`.`StateAbbreviation`) AS IRB,\n".
                         "       `Site`.`SiteName` AS Site,\n";                         
        }

        //PatientAge and RegistrationAge columns
        $quPatAgeClause = '';
        $quRegAgeClause = '';
        if ((isset($post['prr_add_age_columns'])) && ($post['prr_add_age_columns'])) {
            $quPatAgeClause    = "IFNULL(DATE_FORMAT(FROM_DAYS(DATEDIFF(DATE(NOW()), Patient.`PatientBirthDate`)) , '%Y')+0, 0) AS PatientAge,\n";
            $quRegAgeClause    = "IFNULL(DATE_FORMAT(FROM_DAYS(DATEDIFF(DATE(`Patient`.`PatientRegistrationDate`), Patient.`PatientBirthDate`)) , '%Y')+0, 0) AS RegistrationAge,\n";
            //$quPatAgeClause .= "Patient.`PatientBirthDate` as birthday,\n";
            //$quPatAgeClause .= "`Patient`.`PatientRegistrationDate` as regdate,\n";
            //$quPatAgeClause .= "Patient.PatientKeyID as kId,\n";
        }

        //Notice: This is a hack. The | separators below will be translated into <br />in the actual report
        //removed from the end....   , '| ', `States`.`StateAbbreviation`) AS x_IRB,
        $sql = <<< __END__
SELECT CONCAT(`Comp`.`CompName`, '|', `Site`.`SiteName`, '|') AS x_IRB,
       `Trial`.`TrialProtocolNumber` AS Protocol,
       $irbColumn
       `Doctor`.`DoctorLastName` AS MD_Name,
       `Comp`.`CompName` AS Component_Name,
       CONCAT(`PatientIdentifier`.`PatientIdentifierText`, '__', `Patient`.`PatientIdentifierAddendum`) AS Patient_Identifier,
       `Patient`.`PatientLastInit` AS Last_Name,
       `Patient`.`PatientFirstInit` AS First_Name,
       `Patient`.`PatientRegistrationDate` AS Date_of_Registration,
       $quPatAgeClause
       $quRegAgeClause
       `Patient`.`PatientTrialCredit` AS Credits,
       `TrialPhase`.`TrialPhaseText` AS PHASE
FROM   `Patient`
LEFT JOIN `PatientIdentifier` ON `Patient`.`PatientIdentifier`=`PatientIdentifier`.`PatientIdentifierKeyID`
LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
LEFT JOIN `Doctor` ON `Patient`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`
LEFT JOIN `TrialPhase` ON `Trial`.`TrialPhaseKeyID`=`TrialPhase`.`TrialPhaseKeyID`
LEFT JOIN `Site` ON `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`
LEFT JOIN `States` ON `Site`.`SiteState`=`States`.`StatesKeyID`
-- (MORE_JOINS)
WHERE  `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
AND    `Trial`.`TrialReportExclude`<>1

__END__;

if ((isset($post['prr_CompKeyID'])) && ($post['prr_CompKeyID'])) {
    $sql .= "AND    `Comp`.`CompKeyID` = ".$connection->Quote($post['prr_CompKeyID'])."\n";
}

if ((isset($post['prr_PatientIdentifier'])) && ($post['prr_PatientIdentifier'])) {
    $sql .= "AND    `Patient`.`PatientIdentifier` = ".$connection->Quote($post['prr_PatientIdentifier'])."\n";
}

$sql .= "-- (MORE_WHERE_CLAUSES)\n";

$sql .= <<< __END__
ORDER BY x_IRB ASC $quSecondarySort
-- (LIMIT_CLAUSE)
__END__;

        //print "<pre>"; print $sql; print "</pre>"; exit;
    } catch(Exception $e) {
        $sql = '';
    }

    if (!$sql) { $sql = 'SELECT * FROM `Patient` WHERE 1=0'; }   //No output is the default
 
    return $sql;
}



/**
 * Generates the SQL query for the 'Doctor Accruals Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo           Reference! Array will be modified (sql added).
 */
function queryLibraryGetSqlForDoctorAccrualsReport($format, $connection, &$reportInfo) {
    $sql = '';
    
    try {
        $post = (isset($reportInfo['post']))  ?  $reportInfo['post'] : array();

        $quReportStartDate = (isset($post['ReportStartDate'])) ?  $connection->Quote($post['ReportStartDate'])  : '2000-01-01';
        $quReportEndDate   = (isset($post['ReportEndDate']))   ?  $connection->Quote($post['ReportEndDate'])    : '2099-12-31';

        $sql = <<< __END__
SELECT Site.SiteCode AS IRB_Code,
       CONCAT(`Comp`.`CompName`, ' ', `Site`.`SiteName`, ', ', `States`.`StateAbbreviation`) AS IRB,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `TrialType`.`TrialTypeText` = 'Treatment'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS TX_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `TrialType`.`TrialTypeText` = 'Control'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS CC_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `TrialType`.`TrialTypeText` = 'Prevention'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS PX_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `TrialType`.`TrialTypeText` = 'CCDR'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS CCDR_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `TrialType`.`TrialTypeText` IN ('CCDR', 'Prevention', 'Control', 'Treatment')
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS Total_Accruals
FROM   `Site`
LEFT JOIN `Comp` ON Site.`CompKeyID`=Comp.`CompKeyID`
LEFT JOIN `States` ON `Site`.`SiteState`=`States`.`StatesKeyID`
-- (MORE_JOINS)
-- (MORE_WHERE_CLAUSES)
ORDER BY IRB ASC
-- (LIMIT_CLAUSE)
__END__;

        //print "<pre>"; print $sql; print "</pre>"; exit;
    } catch(Exception $e) {
        $sql = '';
    }

    if (!$sql) { $sql = 'SELECT * FROM `Patient` WHERE 1=0'; }   //No output is the default
    return $sql;
}


/**
 * Generates the SQL query for the 'Doctor Accruals By Site Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo           Reference! Array will be modified (sql added).
 */
function queryLibraryGetSqlForDoctorAccrualsBySiteReport($format, $connection, &$reportInfo) {
    $sql = '';

    try {
        $post = (isset($reportInfo['post']))  ?  $reportInfo['post'] : array();

        $quReportStartDate = (isset($post['ReportStartDate'])) ?  $connection->Quote($post['ReportStartDate'])  : '2000-01-01';
        $quReportEndDate   = (isset($post['ReportEndDate']))   ?  $connection->Quote($post['ReportEndDate'])    : '2099-12-31';


        //Removed   , `States`.`StateAbbreviation`    from IRB
        $sql = <<< __END__
SELECT -- `Site`.`SiteKeyID` as x_SiteKeyID,
       -- `Site`.`SiteCode` AS IRB_Code,      
       CONCAT(`Comp`.`CompName`, ' | ', `Site`.`SiteName`, ' | ') AS x_IRB,
       `Doctor`.`DoctorKeyID` as MD_No,
       CONCAT(`Doctor`.`DoctorFirstName`, ', ', `Doctor`.`DoctorLastName`) as MD_Name,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `Patient`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`
        AND    `TrialType`.`TrialTypeText` = 'Treatment'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS TX_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `Patient`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`
        AND    `TrialType`.`TrialTypeText` = 'Control'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS CC_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `Patient`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`
        AND    `TrialType`.`TrialTypeText` = 'CCDR'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS CCDR_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `Patient`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`
        AND    `TrialType`.`TrialTypeText` = 'Prevention'
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS PX_Accruals,
       (SELECT COUNT(*)
        FROM   `Patient`
        LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`
        LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`
        WHERE  `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`
        AND    `Patient`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`
        AND    `TrialType`.`TrialTypeText` IN ('CCDR', 'Prevention', 'Control', 'Treatment')
        AND    `Patient`.`PatientRegistrationDate` >= '$quReportStartDate'
        AND    `Patient`.`PatientRegistrationDate` <= '$quReportEndDate'
        AND    `Trial`.`TrialReportExclude`<>1) AS Total_Accruals
FROM   `DoctorSiteJoin`
LEFT JOIN `Doctor` ON `Doctor`.`DoctorKeyID`=`DoctorSiteJoin`.`DoctorKeyID`
LEFT JOIN `Site` ON Site.`SiteKeyID`=`DoctorSiteJoin`.`SiteKeyID`
LEFT JOIN `Comp` ON Site.`CompKeyID`=Comp.`CompKeyID`
LEFT JOIN `States` ON `Site`.`SiteState`=`States`.`StatesKeyID`
-- (MORE_JOINS)

WHERE   `Comp`.`CompKeyID` IS NOT NULL

__END__;

if ((isset($post['dacbs_CompKeyID'])) && ($post['dacbs_CompKeyID'])) {
    $sql .= "AND  `Comp`.`CompKeyID` = ".$connection->Quote($post['dacbs_CompKeyID'])."\n".
            "-- (MORE_WHERE_CLAUSES)\n";
} else {
    $sql .= "-- (WHERE_CLAUSES)\n";
}

$sql .= <<< __END__
ORDER BY x_IRB ASC
-- (LIMIT_CLAUSE)
__END__;

        //print "<pre>"; print $sql; print "</pre>"; exit;
    } catch(Exception $e) {
        $sql = '';
    }


    if (!$sql) { $sql = 'SELECT * FROM `Patient` WHERE 1=0'; }   //No output is the default
    return $sql;    
}




/**
 * Generates the SQL query for the 'MasterReport'
 *
 * @param object    $connection
 * @param array ref $reportInfo           Reference! Array will be modified (sql added).
 */
function queryLibraryGetSqlForMasterReport($format, $connection, &$reportInfo) {
    $sql = '';
    
    $debugAndStop = 0;  //Set to 1 to print the query and then exit

    try {
        $post = (isset($reportInfo['post']))  ?  $reportInfo['post'] : array();

        $quReportStartDate = (isset($post['ReportStartDate'])) ?  $connection->Quote($post['ReportStartDate'])  : '2000-01-01';
        $quReportEndDate   = (isset($post['ReportEndDate']))   ?  $connection->Quote($post['ReportEndDate'])    : '2099-12-31';

        $sql      = "SELECT\n";

        //GROUP-FIELD AND ORDER BY...
        $masterReportType = (isset($post['master_report_type'])) ? $post['master_report_type'] : 'none';
        $ord      = '';
        $reportInfo['reportLegend']  = '';
        $reportInfo['sortorderText'] = '';
        
        //SECONDARY SORT
        //$secondarySort = "         `PatientIdentifier`.`PatientIdentifierText` ASC,\n".
        //                 "         CONCAT(`Patient`.`PatientFirstInit`, `Patient`.`PatientMiddleInit`, `Patient`.`PatientLastInit`) ASC,\n".
        //                 "         `Patient`.`PatientIdentifierAddendum` ASC\n";
        $secondarySort   = "         `Patient`.`PatientRegistrationDate` ASC\n";

        //GROUPING BY TRIALS
        if ($masterReportType=='trials') {
            $sql .= "       CONCAT('Trial: ', `Trial`.`TrialProtocolNumber`) as x_GROUPKEY,\n".
                    "       `Trial`.`TrialProtocolNumber` as TrialProtocol,\n";

            $ord  = "ORDER BY `Trial`.`TrialProtocolNumber` ASC,\n".$secondarySort;
            $reportInfo['sortorderText'] = 'TrialProtocolNumber + PatientId + Initials + IdAddendum';
            $reportInfo['reportLegend']  = "Master report grouped by Trial";

        //GROUPING BY COMPONENTS
        } elseif ($masterReportType=='components') {
            $sql .= "       CONCAT('Component: ', `Comp`.`CompName`) as x_GROUPKEY,\n".
                    "       `Comp`.`CompName` as CompName,\n";

            $ord  = "ORDER BY `Comp`.`CompName` ASC,\n".$secondarySort;
            $reportInfo['sortorderText'] = 'CompName + PatientId + Initials + IdAddendum';
            $reportInfo['reportLegend']  = "Master report grouped by Components";

        //GROUPING BY SITES
        } elseif ($masterReportType=='sites') {

            $sql .= "       CONCAT('Site: ', `Site`.`SiteName`) as x_GROUPKEY,\n".
                    "       `Site`.`SiteName` as SiteName,\n";

            $ord  = "ORDER BY `Site`.`SiteName` ASC,\n".$secondarySort;
            $reportInfo['sortorderText'] = 'SiteName + PatientId + Initials + IdAddendum';
            $reportInfo['reportLegend']  = "Master report grouped by Sites";

        //GROUPING BY DOCTORS
        } elseif ($masterReportType=='doctors') {

            $sql .= "       CONCAT('Doctor: ', `Doctor`.`DoctorFirstName`,' ', `Doctor`.`DoctorLastName`, ' ', `Doctor`.`DoctorKeyId`) AS x_GROUPKEY,\n".
                    "       CONCAT(`Doctor`.`DoctorFirstName`,' ', `Doctor`.`DoctorLastName`) as Doctor,\n";

            $ord  = "ORDER BY CONCAT(`Doctor`.`DoctorFirstName`,' ', `Doctor`.`DoctorLastName`, ' ', `Doctor`.`DoctorKeyId`) ASC,\n".
                              $secondarySort;
            $reportInfo['sortorderText'] = 'DoctorName + PatientId + Initials + IdAddendum';
            $reportInfo['reportLegend']  = "Master report grouped by Doctors";

        //GROUPING BY RESEARCHBASES (PatientIdentifier)
        } elseif ($masterReportType=='researchbases') {
            $sql .= "       CONCAT('PatIdentifier: ', `PatientIdentifier`.`PatientIdentifierText`) AS x_GROUPKEY,\n".
                    "       `PatientIdentifier`.`PatientIdentifierText` as PatIdentifier,\n";

            $ord  = "ORDER BY `PatientIdentifier`.`PatientIdentifierText` ASC,\n".$secondarySort;
            $reportInfo['sortorderText'] = 'PatientIdentifier + Initials + IdAddendum';
            $reportInfo['reportLegend']  = "Master report grouped by PatientIdentifier";

        //NO GROUPING
        } else {
            $sql .= "       'Nogrouping' as x_GROUPKEY,\n";

            $ord  = "ORDER BY $secondarySort";
            $reportInfo['sortorderText'] = 'PatientId + Initials + IdAddendum';
            $reportInfo['reportLegend']  = "Master report not grouped (flat list)";
        }

        //--------- AGE CALCULATION
        $dropDown       = (isset($post['master_PatientAgeGroup']))      ?  $post['master_PatientAgeGroup']     : null;
        $high           = (isset($post['master_PatientAgeGroupFrom']))  ?  $post['master_PatientAgeGroupFrom'] : null;
        $low            = (isset($post['master_PatientAgeGroupTo']))    ?  $post['master_PatientAgeGroupTo']   : null;
        $patAgeVars     = calculateAgeFilterYears($dropDown, $high, $low);
        //print "<pre>"; print_r($patAgeVars); exit;
        $quAgeString    = "IFNULL(DATE_FORMAT(FROM_DAYS(DATEDIFF(DATE(NOW()), Patient.`PatientBirthDate`)) , '%Y')+0, 0)";
        //---------- AGE CALCULATION

        //--------- REGAGE CALCULATION
        $dropDown       = (isset($post['master_PatientRegAgeGroup']))      ?  $post['master_PatientRegAgeGroup']     : null;
        $high           = (isset($post['master_PatientRegAgeGroupFrom']))  ?  $post['master_PatientRegAgeGroupFrom'] : null;
        $low            = (isset($post['master_PatientRegAgeGroupTo']))    ?  $post['master_PatientRegAgeGroupTo']   : null;
        $regAgeVars     = calculateAgeFilterYears($dropDown, $high, $low);
        $quRegAgeString = "IFNULL(DATE_FORMAT(FROM_DAYS(DATEDIFF(DATE(`Patient`.`PatientRegistrationDate`), Patient.`PatientBirthDate`)) , '%Y')+0, 0)";
        //---------- REGAGE CALCULATION


        //SELECT CLAUSES
                  //"       `Comp`.`CompName` as CompName,\n".
        // if (isset($post['cred_TX_Accruals'])) {
        $sql     .=


                    "       `Site`.`SiteName` as Site, `Site`.`SiteCode` as Site__Code,\n";
        // }

        // if (isset($post['cred_TX_Accruals'])) {
                    $sql     .=


                    "       CONCAT(`Doctor`.`DoctorFirstName`,' ', `Doctor`.`DoctorLastName`) as Doctor,\n".
                    "       `Doctor`.`DoctorKeyId` as Doctor__ID,\n";
                // }
                    $sql     .=


                    "       `Trial`.`TrialProtocolNumber` as Trial__Remove,\n".
                    "       `TrialType`.`TrialTypeText` as TrialType__Remove,\n".
                    "       `TrialCategory`.`TrialCategoryText` as TrialCategory__Remove,\n".
                    "       `TrialGroup`.`TrialGroupTitle` as Group__Remove,\n".
                    "       `TrialPhase`.`TrialPhaseText` as Phase,\n".

                    "        CONCAT(`Patient`.`PatientFirstInit`, `Patient`.`PatientMiddleInit`, `Patient`.`PatientLastInit`) as Patient,\n".
                    "        CONCAT(`PatientIdentifier`.`PatientIdentifierText`, '__', `Patient`.`PatientIdentifierAddendum`) AS Patient__Identifier,\n".

                    "       `Patient`.`PatientBirthDate` as Birthdate,\n".

                    "        $quAgeString AS PatientAge,\n".
                    "        $quRegAgeString AS RegistrationAge,\n".
                    
                    "       `PatientSex`.`PatientSexText` as Sex,\n".
                    "       `PatientRace`.`PatientRaceText` as Race,\n".
                    "       `Patient`.`PatientMultiRacial` as MultiRacial,\n".
                    "       `PatientEthnicity`.`PatientEthnicityText` as Ethnicity,\n".
                    "       `Patient`.`PatientZipCode` as Zip__Code,\n".
                    "       `Patient`.`PatientUrbanRural` as UrbanRural,\n".
                    "       `Patient`.`PatientRegistrationDate` as Registration__Date,\n".

                    "       `Patient`.`TimestampCreate` as Creation__Date,\n".
                    "       `Patient`.`TimestampUpdate` as Update__Date,\n".
                    "       `Patient`.`PatientTrialCredit` as Trial__Credit,\n".
                    "       `Patient`.`PatientTrialCredit` as Trial__Value,\n".
                    "       `Patient`.`PatientQoL` as QoL,\n".
                    "       `Patient`.`PatientQoLC` as QoLC,\n".
                    "       (SELECT `PatientIdentifierText`\n".
                    "        FROM   `PatientIdentifier` as PatId\n".
                    "        WHERE  `PatientIdentifierKeyID`=`Patient`.`PatientCreditor`) as Creditor,\n";

        //ADD ACCRUAL-COLUMNS

        // if (isset($post['cred_TX_Accruals']) && $post['cred_TX_Accruals'] == 'on') {
            $sql     .= "      (SELECT COUNT(*)\n".
                        "       FROM   `Patient` pat\n".
                        "       LEFT JOIN `Trial` ON pat.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                        "       LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`\n".
                        "       WHERE  pat.`PatientKeyID` = `Patient`.`PatientKeyID`\n".
                        "       AND    `TrialType`.`TrialTypeText` = 'Treatment'\n".
                        "       AND    `Trial`.`TrialReportExclude`<>1) AS TX_Accruals,\n";
        // }

        $sql     .= "      (SELECT COUNT(*)\n".
                    "       FROM   `Patient` pat\n".
                    "       LEFT JOIN `Trial` ON pat.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                    "       LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`\n".
                    "       WHERE  pat.`PatientKeyID` = `Patient`.`PatientKeyID`\n".
                    "       AND    `TrialType`.`TrialTypeText` = 'Control'\n".
                    "       AND    `Trial`.`TrialReportExclude`<>1) AS CC_Accruals,\n";

        $sql     .= "      (SELECT COUNT(*)\n".
                    "       FROM   `Patient` pat\n".
                    "       LEFT JOIN `Trial` ON pat.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                    "       LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`\n".
                    "       WHERE  pat.`PatientKeyID` = `Patient`.`PatientKeyID`\n".
                    "       AND    `TrialType`.`TrialTypeText` = 'Prevention'\n".
                    "       AND    `Trial`.`TrialReportExclude`<>1) AS PX_Accruals,\n";

        $sql     .= "      (SELECT COUNT(*)\n".
                    "       FROM   `Patient` pat\n".
                    "       LEFT JOIN `Trial` ON pat.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                    "       LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`\n".
                    "       WHERE  pat.`PatientKeyID` = `Patient`.`PatientKeyID`\n".
                    "       AND    `TrialType`.`TrialTypeText` = 'CCDR'\n".
                    "       AND    `Trial`.`TrialReportExclude`<>1) AS CCDR_Accruals,\n";

        $sql     .= "      (SELECT COUNT(*)\n".
                    "       FROM   `Patient` pat\n".
                    "       LEFT JOIN `Trial` ON pat.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                    "       LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`\n".
                    "       WHERE  pat.`PatientKeyID` = `Patient`.`PatientKeyID`\n".
                    "       AND    `TrialType`.`TrialTypeText` IN ('CCDR', 'Prevention', 'Control', 'Treatment')\n".
                    "       AND    `Trial`.`TrialReportExclude`<>1) AS Total_Accruals\n";


        $sql     .= "FROM   `Patient`\n".

                    "LEFT JOIN `Site` ON `Patient`.`SiteKeyID`=`Site`.`SiteKeyID`\n".
                    "LEFT JOIN `Comp` ON `Site`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                    "LEFT JOIN `Doctor` ON `Patient`.`DoctorKeyID`=`Doctor`.`DoctorKeyID`\n".
                    "LEFT JOIN `Trial` ON `Patient`.`TrialKeyID`=`Trial`.`TrialKeyID`\n".
                    "LEFT JOIN `TrialType` ON `Trial`.`TrialTypeKeyID`=`TrialType`.`TrialTypeKeyID`\n".
                    "LEFT JOIN `TrialCategory` ON `Trial`.`TrialCategoryKeyID`=`TrialCategory`.`TrialCategoryKeyID`\n".
                    "LEFT JOIN `TrialPhase` ON `Trial`.`TrialPhaseKeyID`=`TrialPhase`.`TrialPhaseKeyID`\n".
                    "LEFT JOIN `TrialGroup` ON `Patient`.`TrialGroupKeyID`=`TrialGroup`.`TrialGroupKeyID`\n".
                    "LEFT JOIN `PatientSex` ON `Patient`.`PatientSex`=`PatientSex`.`PatientSexKeyID`\n".
                    "LEFT JOIN `PatientRace` ON `Patient`.`PatientRace`=`PatientRace`.`PatientRaceKeyID`\n".
                    "LEFT JOIN `PatientEthnicity` ON `Patient`.`PatientEthnicity`=`PatientEthnicity`.`PatientEthnicityKeyID`\n".
                    "LEFT JOIN `PatientIdentifier` ON `Patient`.`PatientIdentifier`=`PatientIdentifier`.`PatientIdentifierKeyID`\n".

                    "WHERE   CAST(`Patient`.`PatientRegistrationDate` AS DATE) >= '$quReportStartDate'\n".
                    "AND     CAST(`Patient`.`PatientRegistrationDate` AS DATE) <= '$quReportEndDate'\n";

        //Trial Filter
        if ((isset($post['master_TrialKeyID'])) && ($post['master_TrialKeyID'])) {
            $sql .= "AND     `Patient`.`TrialKeyID`=".$connection->Quote($post['master_TrialKeyID'])."\n";
        }

        //Trial Type Filter
        if ((isset($post['master_TrialTypeKeyID'])) && ($post['master_TrialTypeKeyID'])) {
            $sql .= "AND     `TrialType`.`TrialTypeKeyID`=".$connection->Quote($post['master_TrialTypeKeyID'])."\n";
        }

        //Trial Category Filter
        if ((isset($post['master_TrialCategoryKeyID'])) && ($post['master_TrialCategoryKeyID'])) {
            $sql .= "AND     `TrialCategory`.`TrialCategoryKeyID`=".$connection->Quote($post['master_TrialCategoryKeyID'])."\n";
        }

        //Component Filter
        if ((isset($post['master_CompKeyID'])) && ($post['master_CompKeyID'])) {
            $sql .= "AND     EXISTS (SELECT `TrialCompJoin`.`TrialCompKeyID`\n".
                    "                FROM   `TrialCompJoin`\n".
                    "                WHERE  `TrialCompJoin`.`TrialKeyID`=`Patient`.`TrialKeyID`\n".
                    "                AND    `TrialCompJoin`.`CompKeyID`=`Comp`.`CompKeyID`\n".
                    "                AND    `TrialCompJoin`.`CompKeyID`=".$connection->Quote(trim($post['master_CompKeyID']))."\n".
                    "                LIMIT 1)\n";
        }

        //Site Filter
        if ((isset($post['master_SiteKeyID'])) && ($post['master_SiteKeyID'])) {
            $sql .= "AND     `Patient`.`SiteKeyID`=".$connection->Quote($post['master_SiteKeyID'])."\n";
        }

        //Doctor Filter
        if ((isset($post['master_DoctorKeyID'])) && ($post['master_DoctorKeyID'])) {
            $sql .= "AND     `Patient`.`DoctorKeyID`=".$connection->Quote($post['master_DoctorKeyID'])."\n";
        }

        //Patient Identifier Filter
        if ((isset($post['master_PatientIdentifierKeyID'])) && ($post['master_PatientIdentifierKeyID'])) {
            $sql .= "AND     `Patient`.`PatientIdentifier`=".$connection->Quote($post['master_PatientIdentifierKeyID'])."\n";
        }
        //Patient Identifier Addendum Filter
        if ((isset($post['master_PatientIdentifierAddendum'])) && ($post['master_PatientIdentifierAddendum'])) {
            $addendum = $post['master_PatientIdentifierAddendum'];
            $addendum = str_replace('*', '%', $addendum);
            $sql .= "AND     `Patient`.`PatientIdentifierAddendum` LIKE \"".$connection->Quote($addendum)."\"\n";
        }

        //Patient Birthday Filter
        if ($patAgeVars['active']) {
            //MISSING BIRTHDAYS
            if ($patAgeVars['findInvalid']) {
                $sql .= "AND     (\n".
                        "            (`Patient`.`PatientBirthDate` IN ('0000-00-00', '')) OR\n".
                        "            (`Patient`.`PatientBirthDate` > DATE(NOW()))\n".
                        "        )\n";
            }
            if (($patAgeVars['lowAge']) || ($patAgeVars['highAge'])) {
                //FILTERED BIRTHDAY AGE
                $sql .= "AND     $quAgeString >= ".$patAgeVars['highAge']."\n".
                        "AND     $quAgeString <= ".$patAgeVars['lowAge']."\n";
            }
        }

        //Registration Age Filter
        if ($regAgeVars['active']) {
            //MISSING REGISTRATION AGE
            if ($regAgeVars['findInvalid']) {
                $sql .= "AND     (\n".
                        "            (`Patient`.`PatientRegistrationDate` IN ('0000-00-00', '')) OR\n".
                        "            (`Patient`.`PatientRegistrationDate` > DATE(NOW()))\n".
                        "        )\n";
            }
            if (($regAgeVars['lowAge']) || ($regAgeVars['highAge'])) {
                //FILTERED REGISTRATION AGE
                $sql .= "AND     $quRegAgeString >= ".$regAgeVars['highAge']."\n".
                        "AND     $quRegAgeString <= ".$regAgeVars['lowAge']."\n";
            }
        }

        //Patient ZipCode Filter
        if ((isset($post['master_PatientZipCode'])) && ($post['master_PatientZipCode'])) {
            $zipcode = $post['master_PatientZipCode'];
            $zipcode = str_replace('*', '%', $zipcode);
            $sql .= "AND     `Patient`.`PatientZipCode` LIKE \"".$connection->Quote($zipcode)."\"\n";
        }

        //Patient UrbanRural Filter
        if ((isset($post['master_PatientUrbanRural'])) && ($post['master_PatientUrbanRural'])) {
            $urbanRural = $post['master_PatientUrbanRural'];
            $urbanRural = str_replace('*', '%', $urbanRural);
            $sql .= "AND     `Patient`.`PatientUrbanRural` LIKE \"".$connection->Quote($urbanRural)."\"\n";
        }

        //Custom field filters
        for ($i=1; $i <= 3; $i++) {
            $field  = (isset($post["custom_field_$i"])) ?  $post["custom_field_$i"] : '';
            $cond   = (isset($post["custom_cond_$i"]))  ?  $post["custom_cond_$i"]  : '';
            $val    = (isset($post["custom_value_$i"])) ?  $post["custom_value_$i"] : '';

            $source = '';
            if ($field=='site_code') {
                $source = "`Site`.`SiteCode`";
            } else if ($field=='doctor') {
                $source = "CONCAT(`Doctor`.`DoctorFirstName`,' ', `Doctor`.`DoctorLastName`)";
            } else if ($field=='doctor_id') {
                $source = "`Doctor`.`DoctorKeyId`";
            } else if ($field=='trial') {
                $source = "`Trial`.`TrialProtocolNumber`";
            } else if ($field=='grp') {
                $source = "`TrialGroup`.`TrialGroupTitle`";
            } else if ($field=='phase') {
                $source = "`TrialPhase`.`TrialPhaseText`";
            } else if ($field=='pat') {
                $source = "CONCAT(`Patient`.`PatientFirstInit`, `Patient`.`PatientMiddleInit`, `Patient`.`PatientLastInit`)";
            } else if ($field=='pat_id') {
                $source = "CONCAT(`PatientIdentifier`.`PatientIdentifierText`, '__', `Patient`.`PatientIdentifierAddendum`)";
            } else if ($field=='birthdate') {
                $source = "`Patient`.`PatientBirthDate`";
            } else if ($field=='sex') {
                $source = "`PatientSex`.`PatientSexText`";
            } else if ($field=='race') {
                $source = "`PatientRace`.`PatientRaceText`";
            } else if ($field=='multirace') {
                $source = "`Patient`.`PatientMultiRacial`";
            } else if ($field=='ethnic') {
                $source = "`PatientEthnicity`.`PatientEthnicityText`";
            } else if ($field=='zip') {
                $source = "`Patient`.`PatientZipCode`";
            } else if ($field=='urbanrural') {
                $source = "`Patient`.`PatientUrbanRural`";
            } else if ($field=='regdate') {
                $source = "`Patient`.`PatientRegistrationDate`";
            } else if ($field=='creadate') {
                $source = "`Patient`.`TimestampCreate`";
            } else if ($field=='upddate') {
                $source = "`Patient`.`TimestampUpdate`";
            } else if ($field=='trialcred') {
                $source = "`Patient`.`PatientTrialCredit`";
            } else if ($field=='qol') {
                $source = "`Patient`.`PatientQoL`";
            } else if ($field=='qolc') {
                $source = "`Patient`.`PatientQoLC`";
            }

            $clause = '';
            if ($cond=='contains') {
                $val    = str_replace('*', '%', $val);
                $clause = "$source LIKE \"%".$connection->Quote($val)."%\"\n";
            } else if ($cond=='not_contains') {
                $val    = str_replace('*', '%', $val);
                $clause = "$source NOT LIKE \"%".$connection->Quote($val)."%\"\n";
            } else if ($cond=='is_like') {
                $val    = str_replace('*', '%', $val);
                $clause = "$source LIKE \"".$connection->Quote($val)."\"\n";
            } else if ($cond=='is_not_like') {
                $val    = str_replace('*', '%', $val);
                $clause = "$source NOT LIKE \"".$connection->Quote($val)."\"\n";
          //} else if ($cond=='is') {
          //    $clause = "$source=\"".$connection->Quote($val)."\"\n";
          //} else if ($cond=='is_not') {
          //    $clause = "$source<>\"".$connection->Quote($val)."\"\n";
            } else if ($cond=='is_larger') {
                $clause = '>';
                $clause = "$source > \"".$connection->Quote($val)."\"\n";
            } else if ($cond=='is_smaller') {
                $clause = "$source < \"".$connection->Quote($val)."\"\n";
            } else if ($cond=='is_null') {
                $clause = "$source IS NULL\n";
            } else if ($cond=='is_not_null') {
                $clause = "$source IS NOT NULL\n";
            }
            if (($source) && ($clause)) {
                $sql .= "AND     $clause";
            }
        }


        //ORDER BY        
        $sql .= $ord;

    } catch(Exception $e) {
        $sql = '';
    }
    
    if (!$sql) { $sql = 'SELECT * FROM `Patient` WHERE 1=0'; }   //No output is the default

    if ((0) || ($debugAndStop)) {
        print "<pre>$sql</pre>"; exit;
    }

    return $sql;
}


/**
 * Generates the SQL query for the 'ChangeTrackingReport'
 *
 * @param  string    $format
 * @param  object    $connection
 * @param  array ref $reportInfo           Reference! Array will be modified (sql added).
 * @return string $sql
 */
function queryLibraryGetSqlForTrackingReport($format, $connection, $reportInfo) {
    $sql = '';

    $startDate = '2019-03-01 00:00:00'; //++++++++
    $endDate   = '2019-03-04 23:59:59'; //++++++++

    $trackingStartDate   = (isset($reportInfo['post']['TrackingStartDate']))   ? $reportInfo['post']['TrackingStartDate'] : '';
    $trackingEndDate     = (isset($reportInfo['post']['TrackingEndDate']))     ? $reportInfo['post']['TrackingEndDate'] : '';
    $trackingUserName    = (isset($reportInfo['post']['TrackingUserName']))    ? $reportInfo['post']['TrackingUserName'] : '';
    $trackingParentTable = (isset($reportInfo['post']['TrackingParentTable'])) ? $reportInfo['post']['TrackingParentTable'] : '';

    $sql =
        "SELECT EventID, EventString, EventTime, SessionUser as User, ParentTable, ParentID, ParentHint, TableName, ChangesMade\n".
        "FROM   TrackInfo\n".
        "WHERE  EventTime >= ". $connection->SqlQuote($trackingStartDate . ' 00:00:00')."\n".
        "AND    EventTime <= ". $connection->SqlQuote($trackingEndDate . ' 23:59:59')."\n";
    if (trim($trackingUserName)) {
        $sql .=
        "AND    SessionUser = " . $connection->SqlQuote($trackingUserName)."\n";
    }
    if (trim($trackingParentTable)) {
        $sql .=
            "AND    ParentTable LIKE " . $connection->SqlQuote($trackingParentTable)."\n";
    }
    $sql .=
        "ORDER BY EventID DESC";

    return $sql;
}


/**
 * Get age variables from POST dropdown, fromAge and toAge fields
 *
 * @param  int   $dropDown optional
 * @param  int   $highAge  optional            e.g. 0  (high as in closer to the present)
 * @param  int   $lowAge   optional            e.g. 10 (low  as in further in the past)
 * @return array $ageVars                      array('active'=>0/1, 'findInvalid'=>0/1, 'lowAge'=>..., 'highAge'=>...)
 */
function calculateAgeFilterYears($dropDown=null, $high=null, $low=null) {
    $active      = 0;
    $findInvalid = 0;
    $lowAge      = 0;
    $highAge     = 0;

    if ((isset($dropDown)) && ($dropDown > 0) && ($dropDown <= 13 )) {
        if ($dropDown == 13) {
            $active      = 1;
            $findInvalid = 1;
        } else {
            $active      = 1;
            $ageGroup    = $dropDown;
            $lowAge      = ($ageGroup)    * 10;
            $highAge     = ($ageGroup -1) * 10;
        }
    }
    
    //From/To fields
    $high = (isset($high)) ? $high : 0;
    $low  = (isset($low))  ? $low  : 0;
    if ((!is_numeric($high)) || ($high < 0))   { $high = 0; }
    if ((!is_numeric($low))  || ($low  < 0))   { $low  = 0; }
    if ((!is_numeric($high)) || ($high > 150)) { $high = 150; }
    if ((!is_numeric($low))  || ($low  > 150)) { $low  = 150; }
    if ($high > $low) { $tmp = $low; $low = $high; $high = $tmp; }
    if (($low) || ($high)) {
        $active  = 1;
        $lowAge  = $low;
        $highAge = $high;
    }

    return array('active' => $active, 'findInvalid' => $findInvalid, 'highAge' => $highAge, 'lowAge' => $lowAge);
}


/**
 * Gathers the data rows for the report
 *
 * @param  object    $connection
 * @param  array ref $reportInfo                Array Ref!  Will be modified (from, haveMoreRows)
 * @param  int       $from                      for LIMIT clause
 * @param  int       $size                      for LIMIT clause
 * @return array     array($rows, $xrows, $haveMoreRows, $from, $error)
 */
function queryLibraryGetReportRowSlice($connection, &$reportInfo, $from=0, $size=0) {
    $rows         = array();
    $xrows        = array();
    $haveMoreRows = 0;
    $error        = '';
    
    $isFirstRow   = 1;
    $sizePlusOne  = ($size + 1);

    $limitClause  = (($from) || ($size))  ?  "LIMIT $from,$sizePlusOne\n" : '';
    
    $sql = str_replace("-- (LIMIT_CLAUSE)\n", $limitClause, $reportInfo['sql']);
  
	$result = $connection->RunQuery($sql);
	while($row = $result->fetch_array(MYSQLI_ASSOC)){
	    if (count($xrows) < $size) {
	        //Add the row as long as we have less than the 'size' number of rows...
	        $row['x_is_first_row'] = 0;
	        $row['x_is_last_row']  = 0;
	        if (($isFirstRow) && ($from==0)) {
	            $row['x_is_first_row'] = 1;
	            $isFirstRow            = 0;
	        }
	        
	        $xrows[] = $row;
	        $from++;
	    } else {
	        //We have equal or more than the requested 'size' number of rows...
	        $haveMoreRows = 1;
	        break;
	    }
	}

    $reportInfo['from']         = $from;
	$reportInfo['haveMoreRows'] = $haveMoreRows;
    $reportInfo['sliceSql']     = $sql;
    
    if ((!$haveMoreRows) && (count($xrows)>0)) {
        $xrows[(count($xrows)-1)]['x_is_last_row'] = 1;
    }

    //Remove all columns that start with x_...
    if (count($xrows)) {
        $rows = queryLibraryRemoveXColumnsInRows($xrows);
    }

    return array($rows, $xrows, $haveMoreRows, $from, $error);
}


function queryLibraryRemoveXColumnsInRows($xrows=array()) {
    $rows = array();
    foreach($xrows as $row) {
        foreach($row as $key=>$val) {
            if (substr($key,0,2)=='x_') {
                unset($row[$key]);
            }
        }
        reset($row);
        $rows[] = $row;
    }
    reset($xrows);
    
    return $rows;
}



    //Determine our SQL query (without LIMIT clause)
    //$sql = "SELECT * FROM DoctorSpecialty"; 

    /*
 	Hey all

Reinhard asked me to send out a list of the table/fieldnames that are used on the reports, so here it goes!

Credits Report
Protocol - Trial.TrialProtocolNumber
Accruals - Patient.TrialKeyID (Total of those that match to Trial.TrialProtocolNumber)
Credit Value - Patient.PatientTrialCredit
Total Credits - Product of �Accruals" X "Credit Value"
Additional Switches Columns
Sex - Patient.PatientSex
Race - Patient.PatientRace and/or Patient.MultiRacial
Ethnicity - Patient.PatientEnthnicity

Patient Registration Report
Protocol - Trial.TrialProtocolNumber
Doctor Last Name - Doctor.DoctorLastName
Patient LI - Patient.LastInit
Patient FI - Patient.FirstInit
Patient Registration Date - Patient.
Patient Credit - Patient.PatientTrialCredit
Patient Protocol Phase - Trial.TrialPhase

Doctor Accruals
Site code - Site.SiteCode
Component - Comp.CompName
Site Name - Site.SiteName
Treatment Accruals Total - Trial.TrialType (Total of those that match �Prevention�)
Cancer Control Accruals Total - Trial.TrialType (Total of those that match �Control�)
Total Accruals - Sum of Treatment and Cancer Control Accruals

Doctor Accruals by Site
Doctor Last Name - Doctor.DoctorLastName
Doctor First Name - Doctor.DoctorFirstName
Doctor�s Treatment Accruals Total - Trial.TrialType (Total of those that match �Prevention� for specific Doctor)
Doctor�s Cancer Control Accruals Total - Trial.TrialType (Total of those that match �Control� for Specific Doctor)
Doctor�s Total Accruals - Sum of Treatment and Cancer Control Accruals

I�m sure there will be questions, but this should cover it for the most part.

Robert

The credit report looks like it may just be done on the patient table joined with sex, race and ethnicity tables:
SELECT
    *
FROM
    (( Patient INNER JOIN PatientSex ON
        Patient.PatientSex = PatientSex.PatientSexKeyID)
     INNER JOIN PatientRace ON
        Patient.PatientRace = PatientRace.PatientRaceKeyID)
     INNER JOIN PatientEthnicity ON
        Patient.PatientEthnicity = PatientEthnicity.PatientEthnicityKeyID


Here is an SQL joining Components, Sites and Patients (possibly for patient registrations by component)
SELECT DISTINCT
    *
FROM
    (Comp INNER JOIN Site ON
        Comp.CompKeyID = Site.CompKeyID)
     INNER JOIN Patient ON
        Site.SiteKeyID = Patient.SiteKeyID


Here is SQL to join Trials, Sites and Doctors (possibly for accruals)
SELECT
    *
FROM
    (((Trial INNER JOIN SiteTrialsOpenJoin ON
        Trial.TrialKeyID = SiteTrialsOpenJoin.TrialKeyID)
     INNER JOIN Site ON
        SiteTrialsOpenJoin.SiteKeyID = Site.SiteKeyID)
     INNER JOIN DoctorSiteJoin ON
        Site.SiteKeyID = DoctorSiteJoin.SiteKeyID)
     INNER JOIN Doctor ON
        DoctorSiteJoin.DoctorKeyID = Doctor.DoctorKeyID

Here is SQL to join Trials, Sites, Comps and Doctors (possibly for accruals including Components)
SELECT
    *
FROM
    ((((Trial INNER JOIN SiteTrialsOpenJoin ON
        Trial.TrialKeyID = SiteTrialsOpenJoin.TrialKeyID)
     INNER JOIN Site ON
        SiteTrialsOpenJoin.SiteKeyID = Site.SiteKeyID)
     INNER JOIN Comp ON
        Site.CompKeyID = Comp.CompKeyID)
     INNER JOIN DoctorSiteJoin ON
        Site.SiteKeyID = DoctorSiteJoin.SiteKeyID)
     INNER JOIN Doctor ON
        DoctorSiteJoin.DoctorKeyID = Doctor.DoctorKeyID

Here is a sample of Patient joined with their Site, Comp, Trial and Doctor, along with the Doctor's Specialty and the Patient Sex/Ethnicity/Race attributes (for general review or usage)
SELECT
    *
FROM
    ((((((Patient INNER JOIN Doctor ON
        Patient.DoctorKeyID = Doctor.DoctorKeyID)
     INNER JOIN Trial ON
        Patient.TrialKeyID = Trial.TrialKeyID)
     INNER JOIN PatientSex ON
        Patient.PatientSex = PatientSex.PatientSexKeyID)
     INNER JOIN PatientRace ON
        Patient.PatientRace = PatientRace.PatientRaceKeyID)
     INNER JOIN PatientEthnicity ON
        Patient.PatientEthnicity = PatientEthnicity.PatientEthnicityKeyID)
     INNER JOIN Site ON
        Patient.SiteKeyID = Site.SiteKeyID)
     INNER JOIN Comp ON
        Site.CompKeyID = Comp.CompKeyID

    */



/*
SELECT Trial.TrialProtocolNumber AS Protocol,
       CONCAT(  'proto=', Trial.TrialProtocolNumber
               ,' credit=',Patient.PatientTrialCredit
               ,' sex=', PatientSex.PatientSexText
               ,' race=', (IF (Patient.PatientMultiRacial, 'Multi-Racial', PatientRace.PatientRaceText))
               ,' ethn=', PatientEthnicity.PatientEthnicityText
              ) AS groupkey
       , COUNT(*) AS Accruals,
       Patient.PatientTrialCredit AS Credits
FROM Trial
LEFT JOIN Patient ON Trial.TrialKeyID = Patient.TrialKeyID
LEFT JOIN PatientSex ON Patient.PatientSex = PatientSex.PatientSexKeyID
LEFT JOIN PatientRace ON Patient.PatientRace = PatientRace.PatientRaceKeyID
LEFT JOIN PatientEthnicity ON Patient.PatientEthnicity = PatientEthnicity.PatientEthnicityKeyID
WHERE Patient.PatientRegistrationDate >= '2014-01-01'
AND   Patient.PatientRegistrationDate <= '2014-12-31'
GROUP BY groupkey
ORDER BY Protocol ASC, Credits DESC
*/

