<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Comp.class.php');
require_once(ROOT_PATH.'/lib/States.class.php');
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/TrialCompJoin.class.php');
require_once(ROOT_PATH.'/lib/SiteTrialsOpenJoin.class.php');
require_once(ROOT_PATH.'/lib/Comments.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_sites_0';
    $_REQUEST['order_direction_0'] = 'asc';
    $_REQUEST['order_field_1']     = 'order_trial_protocol_1';
    $_REQUEST['order_direction_1'] = 'asc';
}

//create required objects
$permissions = new Permissions();
$comp        = new Comp();
$states      = new States();
$site        = new Site();
$trial       = new Trial();
$tcj         = new TrialCompJoin();
$comment     = new Comments();
$stoj        = new SiteTrialsOpenJoin();

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'default.pagination.size');

//searchFilter setup
$sFilter  = new SearchFilter('trials');  //----!!! should not be trials
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, 0, $cfg);

//Check permissions
$canEditRows = checkPermissionString('AddEditComponents');
$canViewRows = checkPermissionString('ViewComponents');
if ((!$canEditRows) && (!$canViewRows)) {
    header('location: login.php');
    exit;
}

$pageURL = getPageUrl();

//define required variables
$msg = '';


try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        //if(preg_match('/.+_trial$/',$action)){
        if (!empty($_POST['tcj']['TrialCompKeyID'])) {
            $tcj->Load_from_key($_POST['tcj']['TrialCompKeyID']);
        }
        foreach ($_POST['tcj'] as $key => $val) {
            $tcj->$key = $val;
        }
        //}
        if ($action == 'insert') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'add component', $action, $_POST);
            // For each site checked, add a row in SiteTrialsOpenJoin table to join sites to trial.
            foreach ($_POST['siteCheck'] as $key => $siteCheck) {
                // load site object
                $site->Load_from_key($siteCheck);
                // load trial object
                $trial->Load_from_key($_POST['tcj']['TrialKeyID']);
                $stoj->setSiteKeyID($site->SiteKeyID);
                $stoj->setTrialKeyID($_POST['tcj']['TrialKeyID']);
                // $stoj->setSiteTrialOpenApprovalDate($_POST['tcj']['TrialCompApprovalDate']);
                $stoj->setSiteTrialOpenApprovalDate($_POST['tcj']['TrialCompRecentRenewalDate']);
                $stoj->setSiteTrialOpenCloseDate($_POST['tcj']['TrialCompCloseDate']);
                $stoj->setSiteTrialOpenClose($trial->TrialClose);
                $stoj->setSiteTrialOpenTerminationDate($trial->TrialTerminationDate);
                $stoj->setSiteTrialOpenTermination($trial->TrialTermination);
                $stoj->Save_Active_Row_as_New();
            }

            // This line adds row in TrialCompJoin table to join trial to component.
            $tcj->Save_Active_Row_as_New();
            TrackChanges::endUserAction();
        }

        if ($action == 'update') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'update component', $action, $_POST);
            // For each site checked, update row in SiteTrialsOpenJoin table to join sites to trial.
            foreach ($_POST['siteCheck'] as $key => $siteCheck) {
                // load site object
                $site->Load_from_key($siteCheck);
                // load trial object
                $trial->Load_from_key($_POST['tcj']['TrialKeyID']);
                // load stoj object
                $stoj->Load_from_trial_and_site_id($_POST['tcj']['TrialKeyID'], $site->SiteKeyID);

                $stoj->setSiteKeyID($site->SiteKeyID);
                $stoj->setTrialKeyID($_POST['tcj']['TrialKeyID']);
                // $stoj->setSiteTrialOpenApprovalDate($_POST['tcj']['TrialCompApprovalDate']);
                $stoj->setSiteTrialOpenApprovalDate($_POST['tcj']['TrialCompRecentRenewalDate']);
                $stoj->setSiteTrialOpenCloseDate($_POST['tcj']['TrialCompCloseDate']);
                $stoj->setSiteTrialOpenClose($trial->TrialClose);
                $stoj->setSiteTrialOpenTerminationDate($trial->TrialTerminationDate);
                $stoj->setSiteTrialOpenTermination($trial->TrialTermination);
                $stoj->Save_Active_Row();

                // echo '<pre>';
                // var_dump($stoj);
                // echo '</pre>';
                // echo $stoj->SiteTrialOpenKeyID . ' -';
                // echo $site->SiteKeyID . ' ';
                // echo $_POST['tcj']['TrialKeyID']. ' ';
                // echo $_POST['tcj']['TrialCompApprovalDate'];
                // exit();
                // Update sites
                // $stoj->GetKeysWhereOrderBy('*', 'TrialKeyID = \'' . $_POST['tcj']['TrialKeyID'] . '\'' . 'AND SiteKeyID = \'' . $site->SiteKeyID . '\'', 'SiteTrialOpenKeyID', 'ASC');
                // select sitetrialopenkeyid where trialkeyid = $_POST['tcj']['TrialKeyID'] and sitekeyid;
                // Close checked sites
                // $site->setSiteOpenDate($_POST['tcj']['TrialCompApprovalDate']);
                // $site->Save_Active_Row();
            }
            // This line updates row in TrialCompJoin table to join trial to component.
            $tcj->Save_Active_Row();
            TrackChanges::endUserAction();
        }

        if ($action == 'close') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'close component', $action, $_POST);
            $tcj->Save_Active_Row();
            $comment->TrialKeyID = $tcj->TrialKeyID;
            $comment->Comment = $_POST['comment'];
            $comment->CompKeyID = $tcj->CompKeyID;
            $comment->Save_Active_Row_as_New();

            $trial->Load_from_key($_POST['tcj']['TrialKeyID']);
            foreach ($_POST['siteCheck'] as $key => $siteCheck) {
                // load site object
                $site->Load_from_key($siteCheck);

                // load stoj object
                $stoj->Load_from_trial_and_site_id($_POST['tcj']['TrialKeyID'], $site->SiteKeyID);
                // echo '<pre>';
                // var_dump($stoj);
                // echo '</pre>';

                // Update stoj
                if ($stoj->SiteTrialOpenClose != 1) {
                    $stoj->setSiteTrialOpenClose('1');
                } else {
                    $stoj->setSiteTrialOpenClose('0');
                }
                $stoj->setSiteKeyID($site->SiteKeyID);
                $stoj->setTrialKeyID($_POST['tcj']['TrialKeyID']);
                $stoj->setSiteTrialOpenCloseDate($_POST['tcj']['TrialCompCloseDate']);
                $stoj->setSiteTrialOpenTerminationDate($trial->TrialTerminationDate);
                $stoj->setSiteTrialOpenTermination($trial->TrialTermination);
                $stoj->Save_Active_Row();
            }
            TrackChanges::endUserAction();
        }

        if ($action == 'delete') {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete component', $action, $_POST);
            $tcj->Delete_row_from_key('TrialCompKeyID', $tcj->TrialCompKeyID);
            TrackChanges::endUserAction();
        }

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }//if !empty action

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


$comp->Load_from_key($_GET['id']);

$states->Load_from_key($comp->CompCRAState);

$siteList = array();
$i = 0;
$site_keys = $site->GetKeysWhereOrderBy('CompKeyID = \'' . $_GET['id'] . '\'', 
                                        'SiteKeyID',
                                        'ASC',
                                        0,
                                        25,
                                        $sClauses);
if (count($site_keys) > 0) {
    foreach ($site_keys as $SiteKeyID) {
        $site->Load_from_key($SiteKeyID);
        $status = ($site->SiteStatus == 1) ? 'Active' : 'Inactive';
        $siteList[$i] = "<tr>\n".
                        "<td class=\"left\">" . $site->SiteName . "&emsp;</td>\n".
                        "<td class=\"left\">" . $site->SiteCode . "&emsp;</td>\n".
                        "<td class=\"left\"><span class=\"phone\">" . $site->SitePhone . "</span>&emsp;</td>\n".
                        "<td class=\"left\">" . $site->SiteOpenDate . "</td><td class=\"left\">" . $status . "&emsp;</td>\n".
                        "</tr>\n";
        $i++;
    }
}

$trialList = array();
$i = 0;
$trial_keys = $tcj->GetKeysWhereOrderBy('TrialCompKeyID',
                                        'CompKeyID = \'' . $_GET['id'] . '\'',
                                        'TrialCompKeyID',
                                        'ASC', $sClauses);
if (count($trial_keys) > 0) {
    if (checkPermissionString('AddEditComponents')) {
        foreach ($trial_keys as $TrialCompKeyID) {
            $tcj->Load_from_key($TrialCompKeyID);
            $trial->Load_from_key($tcj->TrialKeyID);
            $trialList[$i] = "<tr>\n".
                             "<td class=\"left\">" . $trial->TrialProtocolNumber . "&emsp;</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompApprovalDate . "</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompRecentRenewalDate . "</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompNextRenewalDate . "</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompCloseDate . "</td>\n".
                             "<td class=\"left\"><a class=\"colorbox\" href=\"compTrial.php?id=" . $_GET['id'] . "&tid=" . $tcj->TrialKeyID . "&tcjid=" . $tcj->TrialCompKeyID . "&url=" . $pageURL . "&action=update\">Edit</a></td>\n".
                             "</tr>\n";
            $i++;
        }
    } else {
        foreach ($trial_keys as $TrialCompKeyID) {
            $tcj->Load_from_key($TrialCompKeyID);
            $trial->Load_from_key($tcj->TrialKeyID);
            $trialList[$i] = "<tr>\n".
                             "<td class=\"left\">" . $trial->TrialProtocolNumber . "&emsp;</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompApprovalDate . "</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompRecentRenewalDate . "</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompNextRenewalDate . "</td>\n".
                             "<td class=\"left\">" . $tcj->TrialCompCloseDate . "</td>\n".
                             "<td class=\"left\"></td>\n".
                             "</tr>\n";
            $i++;
        }     
    }
}


?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html" />
        <meta name="author" content="Cliff Garrett" />
        <title>
            Component View/Edit
        </title>
        <?php require_once ("lib/common.includes.php"); ?>
        <script type="text/javascript">
            $(document).ready(function(){
               $('#select_all').change(function() {
                    alert('checked');
                    if($(this).is(":checked")) {
                        $("[name*='siteCheck']").prop('checked', true);
                    } else {
                        $("[name*='siteCheck']").prop('checked', false);
                    }
                });

               $('select').addClass('fullwidth');
               $("a.confirm-delete").colorbox({inline:true});
               $("#site-button, a.colorbox").colorbox();
               <?php if (!empty($_GET['id']))
{ ?>
               $('#action').val('update');
               <?php } else
{ ?>
               $('#action').val('insert');
               <?php } ?>
            });
        </script>
    </head>
    <body>
        <form id='main_form' name='main_form' method='POST'>
<?php
        print $pg->generateFormVariables($pgInfo);
?>
        <div class="wrapper">
            <div class="logo">
            </div>
            <div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('components');
                ?>
                <div id="tabs-1" class="ui-tabs-panel">
                    <div class="subhead">
                        Component View/Edit
<?php                   if ($canEditRows) { ?>
                            <a id="main-button" class="button" href="compAddEdit.php">Add Component</a>
                            <a id="main-button" class="button" href="compAddEdit.php?id=<?php echo $_GET['id']; ?>">Edit Component</a>
<?php                   } ?>
                        <a href="components.php" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                    <hr />
                    <div class="msg"><?php echo $msg; ?></div>
                    <table border="0" class="fullwidth">
                        <tr>
                            <td class="left"><strong>Component Name:</strong> <?php echo (!empty($comp->CompName)) ? $comp->CompName : ''; ?></td>
                            <td class="left"><strong>Term Date:</strong> <?php echo ((!empty($comp->CompTerminationDate))&&($comp->CompTerminationDate != '0000-00-00')) ? $comp->CompTerminationDate : ''; ?></td>
                            <td><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2"><strong>CRA Name:</strong> <?php echo (!empty($comp->CompCRAName)) ? $comp->CompCRAName : ''; ?></td>
                            <td><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div><div class="spacer10">&nbsp;</div></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2"><strong>Address 1:</strong> <?php echo (!empty($comp->CompCRAAddress1)) ? $comp->CompCRAAddress1 : ''; ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2"><strong>Address 2:</strong> <?php echo (!empty($comp->CompCRAAddress2)) ? $comp->CompCRAAddress2 : ''; ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="left"><strong>City:</strong> <?php echo (!empty($comp->CompCRACity)) ? $comp->CompCRACity : ''; ?></td>
                            <td class="left"><strong>State:</strong> <?php echo $states->StateName; ?></td>
                            <td class="left"><strong>Zip:</strong> <span class="zip"><?php echo (!empty($comp->CompCRAZip)) ? $comp->CompCRAZip : ''; ?></span></td>
                        </tr>
                        <tr>
                            <td class="left"><strong>Phone Number:</strong> <span class="phone"><?php echo (!empty($comp->CompCRAPhone)) ? $comp->CompCRAPhone : ''; ?></span></td>
                            <td class="left"><strong>Email Address:</strong> <?php echo (!empty($comp->CompCRAEmail)) ? $comp->CompCRAEmail : ''; ?></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="left" colspan="2"><strong>Status: </strong><?php echo ($comp->CompStatus == 1) ? 'Active' : 'Inactive'; ?></td>
                            <td></td>
                        </tr>
                    </table>
                    <hr />
                    <div class="clear"></div>
                    <div class="subhead"></div>
                    <div class="vertical-padding-light"><strong>Sites</strong></div><br />
<?php
                    if (count($siteList) > 0) {
?>
                        <a name="tb_0"></a>
                        <table class="fullwidth zebra sort">
                            <thead>
                                <tr class='thead_unselected'>
<?php
                                    print
                                    $pg->generateTheadLine('order_sites_0',                 'Site Name', array('isdefault'=>1)).
                                    $pg->generateTheadLine('order_site_code_0',             'Site Code').
                                    $pg->generateTheadLine('order_phone_number_0',          'Site Phone').
                                    $pg->generateTheadLine('order_open_date_0',             'Join Date').
                                    $pg->generateTheadLine('order_order_active_inactive_0', 'Status');
?>
                                </tr>
                            <tbody>
<?php
                                foreach ($siteList as $siteRow) {
                                    echo $siteRow;
                                }
?>
                            </tbody>
                        </table>
<?php
                    }
?>
                    <hr />
                    <div class="clear"></div>
                    <div class="subhead">
<?php
                        if (checkPermissionString('AddEditTrials')) { ?>
                            <a id="site-button" class="button" href="compTrial.php?id=<?php echo $_GET['id']; ?>&url=<?php echo $pageURL; ?>&action=insert">Add Trial</a>
<?php
                        }
?>
                    </div>
<?php
                    if (count($trialList) > 0) {
?>
                        <div class="vertical-padding-light"><strong>Trials</strong></div><br />

                        <a name='tb_1'></a>
                        <table class="fullwidth zebra sort">
                            <thead>
                                <tr class='thead_unselected'>
<?php
                                    print
                                    $pg->generateTheadLine('order_trial_protocol_1',            'Trial Protocol', array('isdefault'=>1)).
                                    $pg->generateTheadLine('order_approval_date_1',             'Approved Date').
                                    $pg->generateTheadLine('order_recent_renewal_date_1',       'Recent Renewal Date').
                                    $pg->generateTheadLine('order_next_renewal_date_1',         'Next Renewal Date').
                                    $pg->generateTheadLine('order_close_date_1',                'Close Date').
                                    $pg->generateTheadLine('order_trial_action_1',              'Action', array('nosort'=>1));
?>
                                </tr>
                            </thead>
                            <tbody>
<?php
                                foreach ($trialList as $trialRow) {
                                    echo $trialRow;
                                }
?>
                            </tbody>
                        </table>
<?php
                    }
?>
                </div>
            </div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete this Doctor?<br />Doing so will remove all associations with  Sites, Trials and Patients.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            
        </div>
    </body>
</html>
<?php


