<?php
/**
 * Error display page
 */

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/DataBaseMysql.class.php');

//create required objects
$connection        = new DataBaseMysql();

$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
if ((!is_numeric($id)) || ($id < 1)) { $id = 0; }

if ($id) {
    $sql =
        "SELECT *\n" .
        "FROM   `SystemStatus`\n" .
        "WHERE  `systemstatusid`=" . $connection->SqlQuote($id);
    $result = $connection->RunQuery($sql);
    $statusRow = array();
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $statusRow = $row;
    }
    if (!isset($statusRow['systemstatusid'])) {
        $id = 0;
    }
}

if ((!isSysAdmin()) || (!$id)) {
    $errors[] = 'Invalid ID or no access';
}

if ($statusRow['severity'] < 4) {
    $warnings[] = "HTML:<pre>" . print_r($statusRow, true) . "</pre>";
} else {
    $errors[] = "HTML:<pre>" . print_r($statusRow, true) . "</pre>";
}

?>
    <!DOCTYPE HTML>
    <html>
    <head>
        <meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <title>
            Error Display
        </title>
        <?php require_once ("lib/common.includes.php"); ?>
        <script type="text/javascript">
            $(document).ready(function(){
            });
        </script>
    </head>
    <body>
    <form name='main_form' id='main_form' action='errorDisplay.php' method='POST'>
        <div class="wrapper">
            <div class="logo">
            </div>
            <div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('reports');
                ?>
                <div id="tabs-1" class="ui-tabs-panel">
                    <div class="subhead">
                        Error Display for Id <?php print $id; ?>
                    </div>
                    <hr />
                    <?php
                    displaySuccessAndErrors();
                    ?>
                </div>
            </div>
        </div>
    </form>
    </body>
    </html>
<?php

