<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Trial.class.php');
require_once ('lib/TrialGroup.class.php');
require_once ('lib/Patient.class.php');

//create required objects
$trial      = new Trial;
$trialGroup = new TrialGroup;
$patient    = new Patient;

if (!empty($_GET['id'])) {
    $trial->Load_from_key($_GET['id']);
}
if (!empty($_GET['gid'])) {
    $trialGroup->Load_from_key($_GET['gid']);
}
?>
<html>
	<head>
	</head>
	<body>
        <form name="trialGroup" id="trialGroup" method="post" action="<?php echo $_GET['url']; ?>">
        <input type="hidden" name="TrialKeyID" value="<?php echo $_GET['id']; ?>" />
        <input type="hidden" name="TrialGroupKeyID" value="<?php echo $_GET['gid']; ?>" />
        <input type="hidden" id="group_action" name="action" value="<?php echo (!empty($_GET['id'])) ? 'group_update' : 'group_insert'; ?>" />
        <div class="extra-padding">
            <table class="centered fullwidth">
                <tr>
                    <td class="left" class="left extra-padding" colspan="2">Group Title:<br /> <input class="halfwidth" name="TrialGroupTitle" type="text" value="<?php echo $trialGroup->TrialGroupTitle; ?>" /></td>
                </tr>
                <tr>
                    <td class="left" class="left extra-padding" colspan="2">Group Description:<br /><textarea name="TrialGroupDescription" class="nearfullwidth" ><?php echo $trialGroup->TrialGroupDescription; ?></textarea></td>
                </tr>
                <tr>
                    <td class="left extra-padding"><br /><a class="button floatnone" href="javascript:void(0);" onmouseup="$('#trialGroup').submit();">&emsp;Add/Update&emsp;</a></td>
                    <td class="right extra-padding">
                    <?php if (!empty($_GET['id']))
{
    $uses = $patient->GetKeysWhere('TrialGroupKeyID', 'WHERE TrialGroupKeyID = ' . $trialGroup->TrialGroupKeyID);
    if (count($uses = 0))
    { ?>
                    <br /><a class="button floatnone" href="javascript:void(0);" onmouseup="$('div.extra-padding').hide();$('#group-delete').show();window.parent.$.colorbox.resize();" >&emsp;Delete&emsp;</a>
                    <?php }
} ?>
                    </td>
                </tr>
            </table>
        </div>
            <!-- confirm delete -->
            <div id="group-delete" class="colorboxes">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete Group from <?php echo $trial->TrialProtocolNumber; ?>?</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$('#group_action').val('group_delete');$('#trialGroup').submit();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
        </form>
	</body>
</html>
<?php

