<?php
/**
 * A simple application config file
 */

//============================================================== PRODUCTION SETTINGS
global $cfg, $cfgOverridesForAdmins, $prodCfg;
$cfg = array(
    //SQL-check schema. Activates SqlSchemaCheck.class.php
    'sql.checkSchemaActive'                     => 1,    //+++++++++ Set this to 1 temporarily to check/update the DB-schema

    //Global
    'appEnv'                                    => 'production',
    'sysAdminUserName'                          => 'rburns',   //e,g, 'pendingfun'   SysAdminUser has access to behind-the-scenes controls
    'showAdminTab'                              => 0,    // Set this to 1 temporarily to display the admin tab
        // allow/disallow admin processes
        'adminProcessPatientsUrbanRural'        => 0,
        'adminProcessWpOneTimeDataImport'       => 1,
        'adminProcessClinicalTrialsGovImport'   => 1,

    'pagination.size'                           => 25,
    'database.autoquote'                        => 0,         //Set to 1 if GET/POST/REQUEST are already quoted

    //Startdate / Enddate
    'fiscalyear.startdate'                      => (date('md') <= '0801') ? (date('Y')-1).'-08-01' : date('Y').'-08-01',

    //Score production database connection
    'scordb'                                    => array(
        'host'          => 'localhost',
        'dbname'        => 'oris',
        'user'          => 'oris',
        'pass'          => "aqua163bask",
    ),

    //NEW WpPublic production database connection (second implementation with clinicaltrials.gov support)
    'newPublicDb'        => array(
        'host'           => '109.199.99.3', //'localhost',
        'dbname'         => 'south372_scccprod',
        'user'           => 'south372_usr2',
        'pass'           => 'kX()771()tobY',
    ),

    //OLD WpPublic production database connection  (first implementation against the existing private DB)
    'oldPublicdb'        => array(
        'host'           => '109.199.99.3',  // http://southeastcancercontrol.org
        'dbname'         => 'south372_sccc',
        'user'           => 'south372_new',
        'pass'           => 'onyx615\west',
    ),

    //Pagination Sizes
    'default.pagination.size'                   => 25,
    'trials.pagination.size'                    => 25,
    'components.pagination.size'                => 25,
    'sites.pagination.size'                     => 25,
    'doctors.pagination.size'                   => 25,
    'patients.pagination.size'                  => 25,

    //Debug Settings (leave at 0 in production!)
    'debug.patientAjaxDebugMode'                => 0,
    'debug.searchfilter'                        => 0,

    //HOW ERRORS AND WARNINGS ARE DISPLAYED (OR NOT)
    'error.displayErrorsText'                   => 1,    //leave at 1 to display errors to regular users
    'error.displayDetailedInformation'          => 0,    //leave at 0
    'error.hideErrorsFromNonAdmins'             => 1,    //leave at 1 to hide all errors from non-admin users
    'warning.displayWarningsText'               => 0,    //set to 1 to if you want to show warnings to regular users
    'warning.displayWarningsOnlyMinimally'      => 0,    //set to 1 to show warnings but only as tiny texts on the right
    'warning.displayDetailedInformation'        => 0,    //leave at 0
    'warning.hideWarningsFromNonAdmins'         => 1,    //leave at 1 to hide all warnings from non-admin users

    //WHICH EVENTS TO IGNORE
    'ignore.compViewEdit.close.SiteKeyID.nullcheck'             => 1,
    //'ignore.doctors.pageload.Site_class.Load_from_key()'      => 1,

    //WHAT ERRORS AND WARNINGS TO GENERATE
    //Key:      'handleException_functionName', handleException_className or handleException_className_functionName
    //Options:  'asWarning'               will silently log the exception but continue running the program
    //          'asWarningIfEmpty'        will do the same but only if the given data['key'] is empty
    //          no option (empty array)   will log the exception, display a red error, rollback and halt any updates
    'handleException_Load_from_key'             => array('ignore' => 0, 'asWarning' => 1),
    'handleException_Save_Active_Row'           => array('ignore' => 0, 'asWarning' => 0),
    'handleException_Save_Active_Row_as_New'    => array('ignore' => 0, 'asWarning' => 0),

    //Set to '' if you don't want to style closed rows differently
    'styleForClosedTrials'                      => 'font-size:8pt;color:#999999;',
    'styleForClosedComponents'                  => 'font-size:8pt;color:#999999;',
    'styleForClosedSites'                       => 'font-size:8pt;color:#999999;',
    'styleForClosedDoctors'                     => 'font-size:8pt;color:#999999;',

    //Add Patient
    //'addPatientIgnoreInactiveSites'           => 1,

    //Report handling
    'reportDetails' => array(
        //Info:        This is evaluated in reportPrintFunctions.php
        //Key:         Output       must be either 'print' or 'excel'
        //             ReportName   'master'
        //             ColumnTitle  as it appears on the screen
        //
        //Array contents:
        //'attr'       Attribute for both body and head (if bodyAttr or headAttr are not given)
        //'bodyAttr'   Attribute for body (overrides 'attr')
        //'headAttr'   Attribute for head
        //'hide'       Hide a column by specifying 'hide'=>1

        //MASTER REPORT
        //'print|master|Site'  /*Example1*/  => array('hide' => 0, 'maxLen'=>3, 'headAttr' => 'style="color:blue;"', 'bodyAttr'=>'style="color:green;"', 'addDefaults' => 1),
        //'print|master|Site'  /*Example2*/  => array('hide' => 0, 'attr' => 'width="10px" style="color:blue;"', 'addDefaults' => 1),
        //'print|master|Site'  /*Example3*/  => array('hide' => 1, 'attr' => 'width="10px" style="color:blue;"', 'addDefaults' => 1),
        'print|master|Site'                  => array('hide' => 0, 'attr' => 'style="color:black;"'),
        'print|master|Site Code'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Doctor'                => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Doctor ID'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Trial'                 => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|TrialType'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|TrialCategory'         => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Group'                 => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Phase'                 => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Patient'               => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Patient Identifier'    => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Birthdate'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|PatientAge'            => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|RegistrationAge'       => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Sex'                   => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Race'                  => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|MultiRacial'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Ethnicity'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Zip Code'              => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Registration Date'     => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Creation Date'         => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Update Date'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Trial Credit'          => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|QoL'                   => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|QoLC'                  => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Creditor'              => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|TX_Accruals'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|CC_Accruals'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|PX_Accruals'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|CCDR_Accruals'         => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|master|Total_Accruals'        => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        
        //CREDITS REPORT
        'print|credits|Protocol'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|credits|Sex'                  => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|credits|Race'                 => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|credits|Ethnicity'            => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|credits|Accruals'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|credits|Credits'              => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|credits|Total_Credits'        => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),

        //PATIENTS ACCRUALS REPORT
        'print|patient|Protocol'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|Protocol'             => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|MD_Name'              => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|Component_Name'       => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|Patient_Identifier'   => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|Last_Name'            => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|First_Name'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|Date_of_Registration' => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|Credits'              => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|patient|PHASE'                => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),

        //DOCTOR ACCRUALS REPORT
        'print|doctor|IRB_Code'              => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|doctor|IRB'                   => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|doctor|TX_Accruals'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|doctor|CC_Accruals'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|doctor|PX_Accruals'           => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|doctor|CCDR_Accruals'         => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|doctor|Total_Accruals'        => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),

        //DOCTOR ACCRUALS BY SITE
        'print|docbysite|D_No'               => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|docbysite|MD_No'              => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|docbysite|MD_Name'            => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|docbysite|TX_Accruals'        => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|docbysite|CC_Accruals'        => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|docbysite|CCDR_Accruals'      => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|docbysite|PX_Accruals'        => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        'print|docbysite|Total_Accruals'     => array('hide' => 0, 'attr' => 'style="color:black;"', 'addDefaults' => 1),
        ),
);
$prodCfg = $cfg;
//============================================================== PRODUCTION SETTINGS


//============================================================== SYSADMIN PRODUCTION OVERRIDES
$cfgOverridesForAdmins = array(
    //HOW ERRORS AND WARNINGS ARE DISPLAYED (OR NOT)
    'error.displayErrorsText'                   => 1,    //set to 0 if admin should not be bothered with errors
    'error.displayDetailedInformation'          => 0,    //set to 1 to show all the error information
    'warning.displayWarningsText'               => 1,    //set to 0 if admin should not be bothered with warnings
    'warning.displayWarningsOnlyMinimally'      => 0,    //leave at 0
    'warning.displayDetailedInformation'        => 0,    //set to 1 to show all the warning information
);
//============================================================== SYSADMIN PRODUCTION OVERRIDES


//-------------------------------------------------------------- DEVELOPMENT OVERRIDES
if ((isset($_SERVER)) && 
    (isset($_SERVER['HTTP_HOST'])) && 
    (strpos(strtolower($_SERVER['HTTP_HOST']), 'morpheusrising')!==false)) {
    $cfg = array_merge($cfg, array(

        //Global
        'appEnv'                                   => 'development',
        'sysAdminUserName'                         => 'pendingfun',   //e,g, 'pendingfun'   SysAdminUser has access to behind-the-scenes controls
        'showAdminTab'                             => 0,              // Set this to 1 temporarily to display the admin tab
        'showAdminProcessPatientsUrbanRural'       => 0,

        //Score development database connection
        'scordb' => array(
            'host'          => 'localhost',
            'dbname'        => 'morpheu6_SCCC',
            'user'          => 'morpheu6_sccc',
            'pass'          => "aqua163\bask",
        ),

        //NEW WpPublic development database connection (second implementation with clinicaltrials.gov support)
        'newPublicDb'        => array(
            'host'           => '109.199.99.3', //'localhost',
            'dbname'         => 'south372_scccdev',
            'user'           => 'south372_usr2',
            'pass'           => 'kX()771()tobY',
        ),
        //Debug
        'debug.searchfilter'                       => 0,

        //Startdate / Enddate
        'fiscalyear.startdate'                     => (date('md') <= '0801') ? '2000-08-01' : date('Y').'-08-01',

        //HOW ERRORS AND WARNINGS ARE DISPLAYED (OR NOT)
        'error.displayDetailedInformation'         => 1,
        'warning.displayWarningsText'              => 1,
        'warning.displayWarningsOnlyMinimally'     => 0,
        'warning.displayDetailedInformation'       => 0,

        //WHAT ERRORS AND WARNINGS TO GENERATE
        //Key:      'handleException_functionName', handleException_className or handleException_className_functionName
        //Options:  'asWarning'               will silently log the exception but continue running the program
        //          'asWarningIfEmpty'        will do the same but only if the given data['key'] is empty
        //          no option (empty array)   will log the exception, display a red error, rollback and halt any updates
        //'handleException_Load_from_key'                    => array('ignore' => 0, 'asWarning' => 1),
        //'handleException_Save_Active_Row'                  => array('ignore' => 0, 'asWarning' => 0),
        //'handleException_Save_Active_Row_as_New'           => array('ignore' => 0, 'asWarning' => 0),
    ));

    //Activate this to simulate a regular SCOR user (same exception settings as them)
    if (0) {
        //$cfg['sysAdminUserName'] = 'nobodyIsAdmin';
        foreach($prodCfg as $key => $val) {
            if ((substr($key, 0, 15) == 'handleException') ||
                (substr($key, 0, 8) == 'warning.') ||
                (substr($key, 0, 6) == 'error.')) {
                $cfg[$key] = $val;
            }
        }
    }
}
//-------------------------------------------------------------- DEVELOPMENT OVERRIDES


