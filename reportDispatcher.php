<?php
/**
 * Report dispatcher. Calls download-functions and redirects to print-urls.
 *
 */
session_start();
if (empty($_SESSION['UserKeyID'])) { header('location: login.php'); exit; }

require_once('reportCommonFunctions.php');
require_once('reportDialogFunctions.php');
require_once('reportDownload.php');
require_once('reportPrint.php');


$sessionUsername = $_SESSION['UserName'];




//Start our session, extract POST and GET from SESSION
list($connection, $reportInfo) = commonStartNewReport();
if ($reportInfo['error']) { dispatcherDieWithError($reportInfo['error']); }
//print "<pre>"; print_r($reportInfo); print "</pre>"; exit;

/*Debug
echo '<pre>';
print_r($reportInfo);
echo '</pre>';
exit()
*/

if ($reportInfo['format'] == 'excel') {
    //DOWNLOADING EXCEL FILES...
    if ($reportInfo['report'] == 'cred')   {
        downloadCreditReport($connection, $reportInfo);
    } else if ($reportInfo['report'] == 'prr')    {
        downloadPatientRegistrationReport($connection, $reportInfo);
    } else if ($reportInfo['report'] == 'dac')    {
        downloadDoctorAccrualsReport($connection, $reportInfo);
    } else if ($reportInfo['report'] == 'dacbs')  {
        downloadDoctorAccrualsBySiteReport($connection, $reportInfo);
    } else if ($reportInfo['report'] == 'master') {
        downloadMasterReport($connection, $reportInfo);
    } else if ($reportInfo['report'] == 'tracking') {
        downloadTrackingReport($connection, $reportInfo);
    } else {
        dispatcherDieWithError('Invalid download requested');
    }
} else if ($reportInfo['format'] == 'print') {
    //PRINTING REPORTS...
    $_SESSION['post_params'] = $_POST;  //Store our POST and GET params in the session
    $_SESSION['get_params']  = $_GET;   //(good enough for now, needs to be improved later)
    if ($reportInfo['report'] == 'cred')   {
        header('Location: reportPrintCreditReport.php');
    } else if ($reportInfo['report'] == 'prr')    {
        header('Location: reportPrintPatientRegistrationReport.php');
    } else if ($reportInfo['report'] == 'dac')    {
        header('Location: reportPrintDoctorAccrualsReport.php');
    } else if ($reportInfo['report'] == 'dacbs')  {
        header('Location: reportPrintDoctorAccrualsBySiteReport.php');
    } else if ($reportInfo['report'] == 'master') {
        header('Location: reportPrintMasterReport.php');
    } else if ($reportInfo['report'] == 'tracking') {
        header('Location: reportPrintTrackingReport.php');
    } else {
        dispatcherDieWithError('Invalid printout requested');
    }
} else {
    //INVALID METHOD...
    dispatcherDieWithError("View currently not supported");
}

exit;



/**
 * Dies displaying an error
 *
 * Note: Function does NOT return. Exit is called.
 *
 * @param string $error
 */
function dispatcherDieWithError($error) {
    print "ERROR: $error<br />";
    exit;
}





