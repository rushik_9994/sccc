<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest(1);

$patientAjaxDebugMode = ((isset($cfg)) && (is_array($cfg)) &&
                         (isset($cfg['debug.patientAjaxDebugMode'])) &&
                         ($cfg['debug.patientAjaxDebugMode'] > 0))
                            ?  $cfg['debug.patientAjaxDebugMode']  :  0;


//load required classes
require_once ('lib/Trial.class.php');

//create required objects
$trial   = new Trial;

$trialID = $_REQUEST['trial'];
$trial->Load_from_key($trialID);
$res = array();
$res['trialCredit'] = $trial->TrialCredit;
$res['trialQoL']    = $trial->TrialQoL;
$res['trialQoLC']   = $trial->TrialQoLC;

echo json_encode($res);

