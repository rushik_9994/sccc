<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();


switch ($_GET['action']) {
    case 'insert_site':
        $prefix = "Add";
        break;
    case 'update_site':
        $prefix = "Update";
        break;
    default:
        $prefix = "";
}

//load required classes
require_once ('lib/Site.class.php');
require_once ('lib/Comp.class.php');
require_once ('lib/Comp.class.php');
require_once ('lib/Doctor.class.php');
require_once ('lib/DoctorSiteJoin.class.php');

//create required objects
$site       = new Site;
$comp       = new Comp;
$doctor     = new Doctor;
$doctorSite = new DoctorSiteJoin;

$terminated_comp_keys = $comp->GetKeysOrderBy('CompKeyID', 'WHERE CompTermination = 1 or CompStatus = 0', 'CompKeyID', 'ASC');
if ((!empty($terminated_comp_keys)) && (empty($_GET['sid']))) {
    $terminated_comp_keys = join(',', $terminated_comp_keys);
    $terminated_comp_keys = ' AND CompKeyID NOT IN (' . $terminated_comp_keys . ') ';
} else {
    $terminated_comp_keys = '';
}

//$dsj_keys = $doctorSite->GetKeysWhereDoctorKeyIdOrderBy('SiteKeyID', 'DoctorKeyID', $_GET['id'], 'SiteKeyID', 'ASC');

$dsj_keys = $doctorSite->GetKeysWhereOrderBy('SiteKeyID', 'DoctorKeyID = ' . $_GET['id'], 'SiteKeyID', 'ASC');

if ((!empty($dsj_keys)) && (empty($_GET['sid']))) {
    $dsj_keys = join(',', $dsj_keys);
    $dsj_keys = ' AND SiteKeyID NOT IN (' . $dsj_keys . ') ';
} else {
    $dsj_keys = '';
}

$doctor->Load_from_key($_GET['id']);
if (!empty($_GET['dsjid'])) {
    $doctorSite->Load_from_key($_GET['dsjid']);
}
if (!empty($_GET['sid'])) {
    $site->Load_from_key($_GET['sid']);
}
?>
<html>
	<head>
    <script>
    $(document).ready(function(){
        $('#SiteKeyID').addClass('req');
        $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', changeYear: true, yearRange: '<?php echo (date("Y", time()) - 100) . ':' . (date("Y", time()) + 1); ?>'});
        $('#DoctorSiteCloseDate').datepicker("option", "minDate", "<?php echo $doctorSite->DoctorSiteOpenDate; ?>");
        $('#SiteKeyID').attr('name', 'dsj[SiteKeyID]');
    });
    </script>
	</head>
	<body>
        <form name="doctorSite" id="doctorSite" method="post" action="<?php echo $_GET['url']; ?>">
            <input type="hidden" name="dsj[DoctorKeyID]" value="<?php echo $_GET['id']; ?>" />
            <?php if (!empty($_GET['dsjid'])) { ?>
            <input type="hidden" name="dsj[DoctorSiteKeyID]" value="<?php echo $_GET['dsjid']; ?>" />
            <?php } ?>
            <?php if (!empty($_GET['sid'])) { ?>
            <input type="hidden" name="dsj[SiteKeyID\" value="<?php echo $_GET['sid']; ?>" />
            <?php } ?>
            <input type="hidden" id="doctorSite_action" name="action" value="<?php echo $_GET['action']; ?>" />
            <div class="extra-padding">
                <div class="extra-padding">
                    <table class="centered fullwidth" style="width: 680px;">
                        <tr>
                            <td class="left" colspan="2"><strong>Site for <?php echo $doctor->DoctorFirstName . ' ' . $doctor->DoctorLastName; ?></strong><br />&nbsp;</td>
                        </tr>
                        <tr>
                        <?php if (!empty($_GET['sid'])) { ?>
                            <td class="left" colspan="2">
                                <strong>Site:</strong>
                                <input type="hidden" name="dsj[SiteKeyID]" id="SiteKeyID" value="<?php echo $site->SiteKeyID; ?>" readonly="readonly" />
                                <input type="text" name="ignoreThis" id="ignoreThis" value="<?php echo $site->SiteName; ?>" readonly="readonly" />
                            </td>
                        <?php } else { ?>
                            <td class="left" colspan="2">
                                <strong>Site:</strong>
                                <?php print $site->CreateSelect($_GET['sid'], 'WHERE SiteStatus = 1' . $dsj_keys . $terminated_comp_keys); ?>
                            </td>
                        <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Join Date:</strong> <input type="text" name="dsj[DoctorSiteOpenDate]" id="DoctorSiteOpenDate" size="32" class="dpick req" value="<?php echo (!empty($doctorSite->DoctorSiteOpenDate)) ? $doctorSite->DoctorSiteOpenDate : ''; ?>" /></td>
                            <td>
                            <?php if (!empty($doctorSite->DoctorSiteOpenDate)) { ?>
                            <input type="checkbox"  onmouseup="$('div.extra-padding').hide();$('#confirm-close').show();window.parent.$.colorbox.resize();"/>&ensp;<strong><?php echo ($doctorSite->DoctorSiteClose == 0) ? 'Close' : 'Unclose'; ?> Site</strong>
                            <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="right extra-padding" colspan="2"><a class="button floatnone" href="javascript:void(0);" onmouseup="return validate('doctorSite');$('#doctorSite').submit();">&emsp;<?php echo $prefix; ?> Site&emsp;</a>
                            <?php if (!empty($_GET['dsjid'])) { ?>
                            <a class="button floatnone" href="javascript:void(0);" onmouseup="$('div.extra-padding').hide();$('#confirm_delete').show();window.parent.$.colorbox.resize();" >&emsp;Delete&emsp;</a></td>
                            <?php } ?>
                        </tr>
                    </table>
                </div>
            </div>
           <!-- confirm delete -->
            <div id="confirm_delete" class="colorboxes">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete Site for <?php echo $doctor->DoctorFirstName . ' ' . $doctor->DoctorLastName; ?>?</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$('#doctorSite_action').val('delete_site');$('#doctorSite').submit();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
                     <!-- confirm close -->
            <div id="confirm-close" class="colorboxes">
                <table class="centered fullwidth initial">
                    <tr>
                    <?php if ($doctorSitej->DoctorSiteClose == 0) { ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Close <?php echo $site->SiteName; ?>?<br />Doing so will prevent updating of this Site. You must also enter a Close Date for the Site before updating.</td>
                    <?php } else { ?>
                        <td class="center extra-padding" colspan="2">Are you sure you want to Un-Close <?php echo $site->SiteName; ?>?<br />Doing so will enable updating of this Site.</td>
                    <?php } ?>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="$.colorbox({href:'confirm_doctorSite.php?url=<?php echo $_GET['url']; ?>&id=<?php echo $doctor->DoctorKeyID; ?>&dsjid=<?php echo $doctorSite->DoctorSiteKeyID; ?>&action=<?php echo ($doctorSite->DoctorSiteClose == 0) ? 'close' : 'unclose'; ?>&status=<?php echo $doctorSite->DoctorSiteClose; ?>'});">&emsp;Yes&emsp;</button></td>
                        <td class="center extra-padding"><button type="button" class="floatnone" onclick="$.colorbox.close();">&emsp;No&emsp;</button></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm close -->            
   <!-- end confirm delete-->
         </form>
	</body>
</html>
<?php

