<?php
/**
 * Ajax page returning JSON to autocomplete call
 * Note: autocomplete-setup in lib/common.includes.php
 */

defined('ROOT_PATH')      || define('ROOT_PATH', realpath(dirname(__FILE__)));
defined('SEARCH_COMMENT') || define('SEARCH_COMMENT', ' ---- ');

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest(1);

require_once(ROOT_PATH.'/lib/DataBaseMysql.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');

$connection   = new DataBaseMysql();


//Get searchTag and searchText out of referrer and request['term']
list($searchPage, $searchTag, $searchText) = getSearchTagAndText($_SERVER, $_REQUEST);
//print json_encode(array($searchPage, $searchTag, $searchText)); exit; //Debug


//Respect the searchStartDate and searchEndDate (for patients)
$searchStartDate   = $cfg['fiscalyear.startdate'];   //Beginning of this fiscal year
$searchEndDate     = date('Y-m-d');                  //today
$request           = $_REQUEST;
if (isset($request['search_start_date'])) {
    $searchStartDate = $request['search_start_date'];
}
if (isset($request['search_end_date'])) {
    $searchEndDate   = $request['search_end_date'];
}


$searchFilter = new SearchFilter($searchPage);
$searchText   = str_replace('*', '%', $searchText);


$suggestions  = array();
if ($searchPage == 'trials') {

    if ($searchTag == 'name') {
        //trials page: Trial Protocol Number
        $sql = "SELECT TrialProtocolNumber\n".
               "FROM   Trial\n".
               "WHERE  TrialProtocolNumber LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY TrialProtocolNumber ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['TrialProtocolNumber'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'type') {
        //trials page: Trial Type
        $sql = "SELECT TrialTypeText\n".
               "FROM   TrialType\n".
               "WHERE  TrialTypeText LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY TrialTypeText ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['TrialTypeText'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'dis') {
        //trials page: Disease Type
        $sql = "SELECT TrialDiseaseTypeName\n".
               "FROM   TrialDiseaseType\n".
               "WHERE  TrialDiseaseTypeName LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY TrialDiseaseTypeName ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['TrialDiseaseTypeName'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'cirb') {
        //trials page: CIRB
        $suggestions[] = 'TRUE';
        $suggestions[] = 'FALSE';
    }

} else if ($searchPage == 'components') {

    if ($searchTag == 'name') {
        //components page: Trial Protocol Number
        $sql = "SELECT CompName\n".
               "FROM   Comp\n".
               "WHERE  CompName LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY CompName ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['CompName'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'tpno') {
        //components page: Trial Protocol Number
        $sql = "SELECT TrialProtocolNumber\n".
               "FROM   Trial\n".
               "WHERE  TrialProtocolNumber LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY TrialProtocolNumber ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['TrialProtocolNumber'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    }

} else if ($searchPage == 'sites') {

    if ($searchTag == 'sn') {
        //components page: Trial Protocol Number
        $sql = "SELECT SiteName, SiteCode\n".
               "FROM   Site\n".
               "WHERE  SiteName LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY SiteName ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['SiteName'].SEARCH_COMMENT.$row['SiteCode'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'sc') {
        //components page: Trial Protocol Number
        $sql = "SELECT SiteCode, SiteName\n".
               "FROM   Site\n".
               "WHERE  SiteCode LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY SiteCode ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['SiteCode'] . SEARCH_COMMENT .$row['SiteName'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    }

} else if ($searchPage == 'doctors') {

    if ($searchTag == 'na') {
        //doctors page: DoctorName
        $sql = "SELECT CONCAT(`Doctor`.`DoctorFirstName`, ', ', `Doctor`.`DoctorLastName`) as DoctorName, DoctorCity\n".
               "FROM   Doctor\n".
               "WHERE  CONCAT(`Doctor`.`DoctorFirstName`, ', ', `Doctor`.`DoctorLastName`) LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY Doctor.DoctorLastName ASC, Doctor.DoctorFirstName ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['DoctorName'] . SEARCH_COMMENT . $row['DoctorCity'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'sp') {
        //doctors page: Speciality Name
        $sql = "SELECT DoctorSpecialtyText\n".
               "FROM   DoctorSpecialty\n".
               "WHERE  DoctorSpecialty.DoctorSpecialtyText LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY DoctorSpecialty.DoctorSpecialtyText ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['DoctorSpecialtyText'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    }

} else if ($searchPage == 'patients') {
    if ($searchTag == 'id') {
        //patients page: Patient Identifier
        $sql = "SELECT CONCAT(`PatientIdentifier`.`PatientIdentifierText`,`Patient`.`PatientIdentifierAddendum`) as PatientKey,\n".
               "       CONCAT('RegDate ', `Patient`.`PatientRegistrationDate`) as PatientAdditionalDisplay\n".
               "FROM   PatientIdentifier\n".
               "LEFT JOIN Patient ON PatientIdentifier.PatientIdentifierKeyID=Patient.PatientIdentifier\n".
               "WHERE  CONCAT(`PatientIdentifier`.`PatientIdentifierText`,`Patient`.`PatientIdentifierAddendum`) LIKE '".$connection->quote("$searchText%")."'\n".
               "AND    `Patient`.`PatientRegistrationDate` >= '".$connection->quote($searchStartDate)."'\n".
               "AND    `Patient`.`PatientRegistrationDate` <= '".$connection->quote($searchEndDate)."'\n".
               "ORDER BY CONCAT(`PatientIdentifier`.`PatientIdentifierText`,`Patient`.`PatientIdentifierAddendum`) ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            //$suggestions[] = $row['PatientIdentifierName'] . SEARCH_COMMENT . $row['PatientIdentifierText'];
            $suggestions[] = $row['PatientKey']; //. SEARCH_COMMENT . json_encode($_REQUEST);
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'zip') {
        //patients page: PatientZipCode
        $sql = "SELECT DISTINCT(PatientZipCode) AS zip\n".
               "FROM   Patient\n".
               "WHERE  PatientZipCode LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY zip ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['zip'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    } else if ($searchTag == 'prot') {
        //patients page: TrialProtocolNumber
        $sql = "SELECT TrialProtocolNumber\n".
               "FROM   Trial\n".
               "WHERE  TrialProtocolNumber LIKE '".$connection->quote("$searchText%")."'\n".
               "ORDER BY TrialProtocolNumber ASC\n".
               "LIMIT  30";
        $result = $connection->RunQuery($sql);
        while($row = $result->fetch_array(MYSQLI_ASSOC)) {
            $suggestions[] = $row['TrialProtocolNumber'];
        }
        if (count($suggestions) == 30) { $suggestions[] = '--[RESULTS TRUNCATED]--'; }
    }
}


//$suggestions = array(); foreach($_REQUEST as $key=>$val) { $suggestions[] = "$key = $val";} //Debug
//$suggestions = array($searchPage, $searchTag, $searchText);
print json_encode($suggestions);
exit;



/**
 * Try to extract 'stg' (searchTag) and 'stx' (searchText) out of the referrer and override with $request['term']
 *
 * @param  array $server
 * @param  array $request
 * @return array array($searchPage, $searchTag, $searchText)
 */
function getSearchTagAndText($server=array(), $request=array()) {
    $searchPage = '';
    $searchTag  = '';
    $searchText = '';

    $indexText  = '_0'; //----!!! hardcoded!

    $referrer   = (isset($request['search_page'])) ? $request['search_page'] : '';
    if (!$referrer) {
        $referrer   = (isset($server['HTTP_REFERER'])) ? $server['HTTP_REFERER'] : '';
    }
    if ($referrer) {
        $tmp = parse_url($referrer);

        //Get searchPage
        $path = $tmp['path'];
        if (strpos($path,'trials.php') !== false) {
            $searchPage = 'trials';
        } else if (strpos($path,'components.php') !== false) {
            $searchPage = 'components';
        } else if (strpos($path,'sites.php') !== false) {
            $searchPage = 'sites';
        } else if (strpos($path,'doctors.php') !== false) {
            $searchPage = 'doctors';
        } else if (strpos($path,'patients.php') !== false) {
            $searchPage = 'patients';
        }
    }

    if (isset($request['term'])) {
        $searchText = '*'.trim($request['term'],'*').'*';
    } else {
        $searchText = '*';
    }
    $f1 = strpos($searchText, SEARCH_COMMENT);
    if ($f1 !== false) {
        $searchText = trim(substr($searchText,0,$f1));
    }


    if (isset($request['search_tag'.$indexText])) {
        $searchTag = $request['search_tag'.$indexText];
    }

    return array($searchPage, $searchTag, $searchText);
}

