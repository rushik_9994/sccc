<?php
error_reporting(0);
//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Patient.class.php');
require_once ('lib/PatientIdentifier.class.php');
require_once ('lib/Permissions.class.php');
require_once ('lib/Doctor.class.php');
require_once ('lib/DoctorSiteJoin.class.php');
require_once ('lib/Site.class.php');
require_once ('lib/Trial.class.php');
require_once ('lib/Comp.class.php');
require_once ('lib/TrialGroup.class.php');
require_once ('lib/PatientSex.class.php');
require_once ('lib/PatientRace.class.php');
require_once ('lib/PatientEthnicity.class.php');
$pageURL = getPageUrl();

//create required objects
$patients          = new Patient();
$patientIdentifier = new PatientIdentifier();
$permissions       = new Permissions();
$doctors           = new Doctor();
$doctorSiteJoins   = new DoctorSiteJoin();
$sites             = new Site();
$trials            = new Trial();
$comp              = new Comp();
$trialGroup        = new TrialGroup();
$patientSex        = new PatientSex();
$patientRace       = new PatientRace();
$patientEthnicity  = new PatientEthnicity();


//define required variables
$msg = '';

//Enforce user permissions or redirect to login.php
list($UserTypeIDs) = enforceUserPermissions($permissions);

try {
    if (!empty($_POST['action'])) {
        //------------------- transaction start
        $errors     = array();
        $connection = new DataBaseMysql();
        $connection->BeginTransaction();
        //------------------- transaction start

        $action          = (isset($_POST['action'])) ? $_POST['action'] : '';
        $callingSubEvent = $action;

        if (($action == 'update') || ($action == 'insert')) {
            if ($action == 'update') {
                $patients->Load_from_key($_GET['id']);
            }
            foreach ($_POST['patient'] as $key => $val) {
                $patients->$key = $val;
            }
            $patients->TrialGroupKeyID = $_POST['TrialGroupKeyID'];
            $patients->PatientIdentifier = $_POST['PatientIdentifierKeyID'];
            $patients->PatientCreditor = $_POST['PatientCreditor'];
            $patients->TrialKeyID = $_POST['TrialKeyIDOUT'];
            $patients->SiteKeyID = $_POST['SiteKeyID'];
            $patients->DoctorKeyID = $_POST['DoctorKeyID'];
            $patients->PatientZipCode = preg_replace('/\D/', '', $patients->PatientZipCode);
            $patients->PatientUrbanRural = $patients->calculateUrbanRural($patients->PatientZipCode, $patients->PatientRegistrationDate);
            $patients->PatientQoL = ($patients->PatientQoL == 'on') ? 1 : 0;
            if ($action == 'update') {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'update patient', $action, $_POST);
                $patients->Save_Active_Row();
                TrackChanges::endUserAction();
            } else {
                TrackChanges::startUserAction(__FILE__, __LINE__, 'add patient', $action, $_POST);
                $patients->Save_Active_Row_as_New();
                TrackChanges::endUserAction();
            }
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: patientViewEdit.php?id=" . $patients->PatientKeyID);
            }
        }
        if (($action == 'delete') && (!empty($_GET['id']))) {
            TrackChanges::startUserAction(__FILE__, __LINE__, 'delete patient', $action, $_POST);
            $patients->Delete_row_from_key($_GET['id']);
            TrackChanges::endUserAction();
            if (!haveAnyErrors()) {
                $connection->CommitTransaction();
                header("location: patients.php");
            }
        }

        //------------------- transaction commit or throw exception
        //Commit all database changes or roll our transaction back
        if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
        if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
        if (isset($connection)) { $connection->CommitTransaction(); }
        //------------------- transaction commit or throw exception

    }

} catch (\Exception $e) {
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}


if (!empty($_GET['id'])) {
    //editing patient
    $action = 'update';
    $patients->Load_from_key($_GET['id']);
    $patientIdentifier->Load_from_key($patients->PatientIdentifier);
    $patientSex->Load_from_key($patients->PatientSex);
    $patientEthnicity->Load_from_key($patients->PatientEthnicity);
    $patientRace->Load_from_key($patients->PatientRace);
    $sites->Load_from_key($patients->SiteKeyID);
    $doctorWhere = $doctorSiteJoins->GetKeysWhereOrderBy('SiteKeyID', 'SiteKeyID = \'' . $sites->SiteKeyID . '\'', 'DoctorKeyID', 'ASC');
    $doctors->Load_from_key($patients->DoctorKeyID);
    $trials->Load_from_key($patients->TrialKeyID);
    $trialGroup->Load_from_key($patients->TrialGroupKeyID);
} else {
    $action = 'insert';
}

//Create clause to ignore terminated components
$terminated_comp_keys = $comp->GetKeysOrderBy('CompKeyID', 'WHERE CompTermination = 1 or CompStatus = 0', 'CompKeyID', 'ASC');
if (!empty($terminated_comp_keys)) {
    $terminated_comp_keys = join(',', $terminated_comp_keys);
    $terminated_comp_keys = ' AND CompKeyID NOT IN (' . $terminated_comp_keys . ') ';
} else {
    $terminated_comp_keys = '';
}

//Create clause to ignore inactive sites
//$ignoreInactiveSites = '';
//if ((isset($cfg['addPatientIgnoreInactiveSites'])) && ('addPatientIgnoreInactiveSites')) {
//    $ignoreInactiveSites = " Site.SiteStatus<>0 ";
//}

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
		<meta name="author" content="Cliff Garrett" />
		<title>
			Patient
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
        <script type="text/javascript">

            /**
             * JS function to collect various dropdown values
             */
            function getAjaxParams()
            {
                //Always get all dropdown values if we can
                var ajaxSite          = $('#SiteKeyID').val();
                var ajaxDoc           = $('#DoctorKeyID').val();
                var ajaxTrial         = $('#TrialKeyID').val();
                var ajaxTrialKeyIDOUT = $('#TrialKeyIDOUT').val();
                var ajaxGroup         = $('#TrialKeyID').val();
                if (typeof ajaxSite          == 'undefined')  { ajaxSite=0; }
                if (typeof ajaxDoc           == 'undefined')  { ajaxDoc=0; }
                if (typeof ajaxTrial         == 'undefined')  { ajaxTrial=0; }
                if (typeof ajaxTrialKeyIDOUT == 'undefined')  { ajaxTrialKeyIDOUT=0; }
                if (typeof ajaxGroup         == 'undefined')  { ajaxGroup=0; }

                var ajaxParams = '';
                if (ajaxSite)          { ajaxParams = ajaxParams + '&ajaxSiteKeyID='+ajaxSite; }
                if (ajaxDoc)           { ajaxParams = ajaxParams + '&ajaxDocKeyID='+ajaxDoc; }
                if (ajaxTrial)         { ajaxParams = ajaxParams + '&ajaxTrialKeyID='+ajaxTrial; }
                if (ajaxTrialKeyIDOUT) { ajaxParams = ajaxParams + '&ajaxTrialKeyIDOUT='+ajaxTrialKeyIDOUT; }
                if (ajaxGroup)         { ajaxParams = ajaxParams + '&ajaxGroupKeyID='+ajaxGroup; }

                return ajaxParams;
            }
            
            function processingWhenTrialSelectAjaxIsDone()
            {
                $('#TrialKeyIDOUT').change(function() {
                    var trial      = getTrialFromDialogFields('location3');
                    var ajaxParams = getAjaxParams();
                    if (trial != '') {
                        $.getJSON('ajax_patientAddEditTrialCredit.php?trial='+trial+ajaxParams, { }, function(result) {
                            $('#TrialCredit').val(result.trialCredit);
                            $('#PatientQoLC').val(result.trialQoLC);
                            if(result.trialQoL == 1){
                                $('#PatientQoL').prop('checked', true);
                            }else{
                                $('#PatientQoL').prop('checked', false);
                            }
                        });
                        var ajaxParams = getAjaxParams();
                        $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+ajaxParams, { }, function(result) {
                            $('#TrialGroupSelect').html(result);
                        });
                    }else{
                        $('#TrialCredit').val('');
                        $('#PatientQoLC').val('');
                        $('#PatientQoL').prop('checked', false);
                        var ajaxParams = getAjaxParams();
                        $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+ajaxParams, { }, function(result) {
                            $('#TrialGroupSelect').html(result);
                        });
                    }                   
                });
            }

            function getTrialFromDialogFields(displayText)
            {
                //Get our trial
                var trial = $("#TrialKeyIDOUT option:selected").val();
                if ((typeof trial == 'undefined') || (!trial)) {
                    var fallbackTrial = $('#TrialKeyID').val();
                    if (typeof fallbackTrial == 'undefined') { fallbackTrial = 0; }
                    if (fallbackTrial) {
                        trial = fallbackTrial;
                    }
                }

                if ((typeof trial == 'undefined') || (!trial)) {
                    trial = '';
                }

                return trial;
            }
    
            function onTrialKeyIDOUTchange()
            {
                //Get our trial
                var trial      = getTrialFromDialogFields('location1');
                var ajaxParams = getAjaxParams();
                if (trial != ''){
                    var ajaxParams = getAjaxParams();
                    $.getJSON('ajax_patientAddEditTrialCredit.php?trial='+trial+ajaxParams, { }, function(result) {
                        $('#TrialCredit').val(result.trialCredit);
                        $('#PatientQoLC').val(result.trialQoLC);
                        if(result.trialQoL == 1){
                            $('#PatientQoL').prop('checked', true);
                        }else{
                            $('#PatientQoL').prop('checked', false);
                        }
                    });
                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+ajaxParams, { }, function(result) {
                        $('#TrialGroupSelect').html(result);
                    });
                }else{
                    $('#TrialCredit').val('');
                    $('#PatientQoLC').val('');
                    $('#PatientQoL').prop('checked', false);
                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+ajaxParams, { }, function(result) {
                        $('#TrialGroupSelect').html(result);
                    });
                }
            }

        </script>
		<script type="text/javascript">
        	$(document).ready(function()
        	{        	    
        		//$(".ui-tabs").tabs();
               $('#SiteKeyID').change(function()
               {
                    $('#TrialCredit').val('');
                    $('#PatientQoLC').val('');
                    var site       = $('#SiteKeyID').val();
                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditDoctorSelect.php?site='+site+ajaxParams, { }, function(result) {
                        $('#DoctorSelect').html(result);
                        $('#DoctorKeyID').removeClass('req');
                        $('#DoctorKeyID').addClass('req');
                    });
                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditTrialSelect.php?site='+site+ajaxParams, { }, function(result) {
                        $('#TrialSelect').html(result);
                        $('#TrialKeyIDOUT').removeClass('req');
                        $('#TrialKeyIDOUT').addClass('req');
                        processingWhenTrialSelectAjaxIsDone();
                    });

                    var trial      = getTrialFromDialogFields('location2');
                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+ajaxParams, { }, function(result) {
                        $('#TrialGroupSelect').html(result);
                    });

                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditTrialSelect.php?site='+site+ajaxParams, { }, function(result) 
                    {
                        $('#TrialSelect').html(result);
                        processingWhenTrialSelectAjaxIsDone();
                        //$('#TrialKeyIDOUT').change(function()
                        //{
                        //    onTrialKeyIDOUTchange();
                        //});  //end of $('#TrialKeyIDOUT').change(function()
                        $('#TrialKeyIDOUT').addClass('req');
                    });
               
                }); // end of $('#SiteKeyID').change(function()



              $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;'});


 
                <?php if ((!empty($patients->DoctorKeyID)) || ($patients->DoctorKeyID != 0)) { ?>
                        var site = <?php echo $patients->SiteKeyID; ?>;
                        var selected = <?php echo $patients->DoctorKeyID; ?>;
                        var ajaxParams = getAjaxParams();
                        $.get('ajax_patientAddEditDoctorSelect.php?site='+site+'&selected='+selected+ajaxParams, { }, function(result) {
                        $('#DoctorSelect').html(result);
                        $('#DoctorKeyID').addClass('req');
                    });
                <?php } ?>


               
                <?php if ((!empty($patients->TrialKeyID)) || ($patients->TrialKeyID != 0)) { ?>

                    var site = <?php echo $patients->SiteKeyID; ?>;
                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditTrialSelect.php?site='+site+'&selected='+<?php echo $patients->TrialKeyID; ?>+ajaxParams, { }, function(result) {
                        $('#TrialSelect').html(result);
                        processingWhenTrialSelectAjaxIsDone();
                        $('#TrialKeyIDOUT').addClass('req');
                    });

                <?php } else { ?>
                    $('#TrialKeyIDOUT').change(function(){
                        //Get our trial
                        var trial      = getTrialFromDialogFields('location4');
                        var ajaxParams = getAjaxParams();
                        if (trial != '') {
                            $.getJSON('ajax_patientAddEditTrialCredit.php?trial='+trial+ajaxParams, { }, function(result) {
                                $('#TrialCredit').val(result.trialCredit);
                                $('#PatientQoLC').val(result.trialQoLC);
                                if(result.trialQoL == 1){
                                    $('#PatientQoL').prop('checked', true);
                                }else{
                                    $('#PatientQoL').prop('checked', false);
                                }
                            });
                            var ajaxParams = getAjaxParams();
                            $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+ajaxParams, { }, function(result) {
                                $('#TrialGroupSelect').html(result);
                            });
                        }else{
                            $('#TrialCredit').val('');
                            $('#PatientQoLC').val('');
                            $('#PatientQoL').prop('checked', false)
                            var ajaxParams = getAjaxParams();
                            $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+ajaxParams, { }, function(result) {
                                $('#TrialGroupSelect').html(result);
                            });
                        }                   
                    });
                <?php } ?>

                <?php if ((!empty($patients->TrialGroupKeyID)) || ($patients->TrialGroupKeyID != 0)) { ?>
                    var trial      = <?php echo $patients->TrialKeyID; ?>;
                    //var trial    = getTrialFromDialogFields();  //not used here?
                    var ajaxParams = getAjaxParams();
                    $.get('ajax_patientAddEditTrialGroupSelect.php?trial='+trial+'&selected='+<?php echo $patients->TrialGroupKeyID; ?>+ajaxParams, { }, function(result) {
                        $('#TrialGroupSelect').html(result);
                    });
                <?php } ?>
               
                $('#PatientIdentifierKeyID').addClass('req');
                $('#PatientSexKeyID').addClass('req');
                $('#PatientRaceKeyID').addClass('req');
                $('#PatientEthnicityKeyID').addClass('req');
                $('#SiteKeyID').addClass('req');
                $('#DoctorKeyID').addClass('req');
                $('#TrialKeyIDOUT').addClass('req');

                // make patient creditor mirror patient identifier
                $('#PatientIdentifierKeyID').change(function(){
                    if($(this).val()==1){
                        document.getElementById('PatientCreditor').selectedIndex = 0;
                        $('#PatientCreditor').attr('disabled', false);
                    }else {
                        document.getElementById('PatientCreditor').selectedIndex = $(this).val();
                    }                    
                });
                
                $('#PatientCreditor').on('focus',function(){
                    // If #PatientCreditor is set to CTSU (10), enable Creditor dropdown
                    if($('#PatientIdentifierKeyID').val() != 1 && $('#PatientIdentifierKeyID').val() != 10){
                        $(this).attr('disabled', true);
                    }
                })
                $('#PatientCreditor').on('blur',function(){
                    $(this).attr('disabled', false);
                })
       	    }); //end document ready


            $(function() {
                var msg = 'Are you sure you want to delete this Patient?<br />Doing so will remove all associations with Sites, Doctors and Trials.';
                var confirm = '';
                confirm += '<table class="centered">';
                confirm += '<tr><td class="center extra-padding" colspan="2">'+msg+'</td><td></tr>';
                confirm += '<tr><td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$(\'#action\').val(\'delete\'); trashMe();">&emsp;Yes&emsp;</a></td>';
                confirm += '<td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td></tr></table>';
            
                $(".confirm").colorbox({html:confirm});
            });
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('patients');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<form method="post" action="" name="mainform" id="mainform">
                    <input type="hidden" id="action" name="action" class="action" value="<?php echo $action; ?>" />
                        <?php if ($_GET['id'])
{ ?>
                        <input type="hidden" name="patient[PatientKeyID]" id="PatientKeyID" value="<?php echo $_GET['id']; ?>" />
                        <?php } ?>
                    <div class="subhead">
						Patient Add/Edit
                            <a href="<?php echo (!empty($_GET['id'])) ? 'patientViewEdit.php?id=' . $_GET['id'] : 'patients.php'; ?>" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                        <table class="pad-height fullwidth">
                          <tr>
                            <td><strong>Last&nbsp;Init:&nbsp;</strong><input class="req" type="text" name="patient[PatientLastInit]" id="PatientLastInit" size="2" maxlength="1" value="<?php echo (!empty($patients->PatientLastInit)) ? $patients->PatientLastInit : ''; ?>" /></td>
                            <td class="center"><strong>First&nbsp;Init:&nbsp;</strong><input class="req" type="text" name="patient[PatientFirstInit]" id="PatientFirstInit"   size="2" maxlength="1" value="<?php echo (!empty($patients->PatientFirstInit)) ? $patients->PatientFirstInit : ''; ?>" /></td>
                            <td><strong>Mid.&nbsp;Init:&nbsp;</strong><input type="text" size="2" name="patient[PatientMiddleInit]" id="PatientMiddleInit"  maxlength="1" value="<?php echo (!empty($patients->PatientMiddleInit)) ? $patients->PatientMiddleInit : ''; ?>" /></td>
                            <td>
                                <strong>Patient&nbsp;Identifier:</strong>&nbsp;<?php echo $patientIdentifier->CreateSelect($patientIdentifier->PatientIdentifierKeyID); ?> 
                                <input type="text" size="9" name="patient[PatientIdentifierAddendum]" id="PatientIdentifierAddendum" value="<?php echo (!empty($patients->PatientIdentifierAddendum)) ? $patients->PatientIdentifierAddendum : ''; ?>" />
                                &emsp;
                            </td>
                            <td class="right"><strong>&nbsp;Zip:&nbsp;</strong><input type="text" class="zip req" size="9" name="patient[PatientZipCode]" id="PatientZipCode" value="<?php echo (!empty($patients->PatientZipCode)) ? $patients->PatientZipCode : ''; ?>" /></td>
                          </tr>
                          <tr>
                            <td colspan=3><strong>Birthdate:&nbsp;</strong><input type="text" name="patient[PatientBirthDate]" id="PatientBirthDate" size="32" class="dpick req" value="<?php echo (!empty($patients->PatientBirthDate)) ? $patients->PatientBirthDate : ''; ?>" /></td>
                            <td><strong>Registration Date:&nbsp;</strong><input type="text" name="patient[PatientRegistrationDate]" id="PatientRegistrationDate" size="32" class="dpick req" value="<?php echo (!empty($patients->PatientRegistrationDate)) ? $patients->PatientRegistrationDate : ''; ?>" /></td>
                            <td  class="right">
                                <strong>Urban/Rural:</strong> <?php echo $patients->PatientUrbanRural; ?>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="4"><strong>Sex:&nbsp;</strong><?php echo $patientSex->CreateSelect($patientSex->PatientSexKeyID); ?>
                            &emsp;<strong>Race:&nbsp;</strong><?php echo $patientRace->CreateSelect($patientRace->PatientRaceKeyID); ?>
                            &emsp;<strong>Ethnicity:</strong>&nbsp;<?php echo $patientEthnicity->CreateSelect($patientEthnicity->PatientEthnicityKeyID); ?>
                            &emsp;<input type="checkbox" name="patient[PatientMultiRacial]" id="PatientMultiRacial" value="1" <?php echo ($patients->PatientMultiRacial == 0) ? '' : 'checked'; ?> />&nbsp;Multiracial&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="5">&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="5"><strong>Site:&nbsp;</strong>
                                <?php
                                echo $sites->CreateSelect($sites->SiteKeyID, 'WHERE SiteStatus=1 ' . $terminated_comp_keys);
                                ?>
                            </td>
                        </tr>
                          <tr>
                            <td colspan="5"><strong>Doctor:&nbsp;</strong><span id="DoctorSelect"><span class="msg">Select Site First <span class="arrow">&#8593;</span></span></span></td>
                          </tr>
                          <tr>
                            <td colspan="2" nowrap="nowrap"><strong>Trial Protocol:&nbsp;</strong><span id="TrialSelect"><span class="msg">Select Site First <span class="arrow">&#8593;</span></span></td>
                            <td colspan="3"><strong>&emsp;Patient&nbsp;Trial&nbsp;Credit:&nbsp;</strong><input type="text" name="patient[PatientTrialCredit]" id="TrialCredit" maxlength="13" value="<?php echo $patients->PatientTrialCredit; ?>" />
                            <strong>&emsp;&emsp;Patient QoL:</strong>&nbsp;<input type="checkbox" name="patient[PatientQoL]" id="PatientQoL" <?php echo ($patients->PatientQoL == 0) ? '' : 'checked'; ?> />&emsp;<strong>Patient QoLC:</strong>&nbsp;</strong><input type="text" name="patient[PatientQoLC]" id="PatientQoLC" maxlength="13" value="<?php echo $patients->PatientQoLC; ?>" /></td>
                          </tr>
                          <tr>
                            <td colspan="3"><strong>Group:&nbsp;</strong><span id="TrialGroupSelect"><span class="msg" >Select Trial First <span class="arrow">&#8593;</span></span></span></td>
                            <td colspan="2"><strong>Creditor:</strong>&nbsp;<?php echo $patientIdentifier->CreateSelectCreditor($patients->PatientCreditor); ?></td>
                          </tr>
                        </table>
                        <div class="clear"></div>
                        <div class="right formdiv noborder">
                            <button type="button" class="inline" onmouseup="return validate('mainform'); $('#mainform').submit();" onfocus="$('#PatientCreditor').attr('disabled', false);" onblur="$('#PatientCreditor').attr('disabled', true);">Add/Update</button>&emsp;
                            <?php if (!empty($_GET['id']))
{ ?>
                            <button class="inline confirm">Delete</button>
                            <?php } else
{ ?>
                            <a class="button floatnone cancel" href="patients.php">&emsp;Cancel&emsp;</a>
                            <?php } ?>
        				</div>
					</form>
			</div>
		</div>
	</body>
</html>
<?php


/**
 * Enforce user permissions
 * @param $permissions
 * @return array array($UserTypeIDs);
 */
function enforceUserPermissions($permissions)
{
    $permissions->Load_from_action("AddEditPatients");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }
    $permissions->Load_from_action("DeletePatients");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }

    return array($UserTypeIDs);
}
