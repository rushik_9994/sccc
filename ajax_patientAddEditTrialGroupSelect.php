<?php
error_reporting(0);
//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest(1);

//load required classes
require_once ('lib/TrialGroup.class.php');

global $gSqlDebugMode, $gSqlAddKeysToOptions,$gSqlSelectExtraText;
$patientAjaxDebugMode = ((isset($cfg)) && (is_array($cfg)) &&
                         (isset($cfg['debug.patientAjaxDebugMode'])) &&
                         ($cfg['debug.patientAjaxDebugMode'] > 0))
                            ?  $cfg['debug.patientAjaxDebugMode']  :  0;

//create required objects
$trialGroup = new TrialGroup;

try {
    //------------------- transaction start
    $errors     = array();
    $connection = new DataBaseMysql();
    $connection->BeginTransaction();
    //------------------- transaction start

    $callingSubEvent = 'ajaxcall';
    $debugText = '';
    $trial     = (isset($_REQUEST['trial'])) ? $_REQUEST['trial'] : '';
    if ((!$trial) && (isset($_REQUEST['ajaxTrialKeyID'])))    { $trial = $_REQUEST['ajaxTrialKeyID']; }
    if ((!$trial) && (isset($_REQUEST['ajaxTrialKeyIDOUT']))) { $trial = $_REQUEST['ajaxTrialKeyIDOUT']; }
    $selected = $_REQUEST['selected'];

    $select = '';
    if ($patientAjaxDebugMode) {
        $debugText .= "<div style='color:red;font-size:8pt;'>\n".
                   "===================================== ".basename(__FILE__)."<br />".
                   "REQUEST = <pre>".print_r($_REQUEST,true)."</pre>".
                   "===================================== ".basename(__FILE__)."<br />".
                   "</div>";
        //SQL-Debug mode on
        $gSqlDebugMode        = 1;
        $gSqlAddKeysToOptions = 1;
        $gSqlSelectExtraText  = basename(__FILE__);
    }

    //+++++++ BUG: REQUEST is NOT the right thing to look at as it's only updated through pageload.
    //             What needs to happen here is to actually add some jquery code that looks for the trial dropdown
    //             and determines the selected Trial dynamically.
    //             Alternatively    $('#TrialKeyIDOUT').change() function needs to be triggered
    if ((!empty($trial)) || ($trial != '')) {
        $select .= $trialGroup->CreateSelect($selected, 'WHERE TrialKeyID =' . $trial);
    } else {
        $select .= '<span class="msg" >Select Trial First <span class="arrow">&#8593;</span></span>';
    }
    $select .= $debugText;

    //------------------- transaction commit or throw exception
    //Commit all database changes or roll our transaction back
    if (haveAnyErrors()) { throw new \Exception('EncounteredErrors'); }
    if (!haveAnyErrors()) { $success  = 'All changes made successfully'; }
    if (isset($connection)) { $connection->CommitTransaction(); }
    //------------------- transaction commit or throw exception

} catch (\Exception $e) {
    $publicMessage = $e->getMessage();
    $f1 = strpos($publicMessage, '|||');
    if ($f1 !== false) {
        $publicMessage = substr($publicMessage, 0, $f1);
    }
    $select = '<span class="msg" >ERROR: '.htmlentities($publicMessage).'</span>';
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}

//SQL-Debug mode off again
if ($patientAjaxDebugMode) {
    $gSqlDebugMode        = 0; 
    $gSqlAddKeysToOptions = 0;
}

echo $select;

