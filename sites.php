<?php
error_reporting(0);
/**
 * Sites management page
 */


//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Site.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_sites_0';
    $_REQUEST['order_direction_0'] = 'asc';
}

//create required objects
$sites       = new Site;

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'sites.pagination.size');

//searchFilter setup
$sFilter  = new SearchFilter('sites');
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, 0, $cfg);

//define required variables
$msg = '';

//Check permissions
$canEditRows = checkPermissionString('AddEditSites');
$canViewRows = checkPermissionString('ViewSites');

$SiteKeyIDs    = $sites->GetKeysOrderBy('SiteKeyID', 
                                        'ASC', 
                                        $pgInfo[0]['from'], 
                                        $pgInfo[0]['limit'], 
                                        $sClauses);  //pagination added
$totalRowCount = $sites->totalRowCount;

//begin building output
$indexText   = '_0';
$sites_list  = "<a name='tb_0'></a>\n".
               "<table class=\"fullwidth zebra sort\">\n".
               "<thead>\n".
               "    <tr class='thead_unselected'>\n".
               $pg->generateTheadLine('order_sites'.$indexText, 'Sites').
               $pg->generateTheadLine('order_site_code'.$indexText, 'Site Code').
               $pg->generateTheadLine('order_phone_number'.$indexText, 'Phone Number').
               $pg->generateTheadLine('order_open_date'.$indexText, 'Open Date').
               $pg->generateTheadLine('order_active_inactive'.$indexText, 'Active/Inactive');
if (($canViewRows) || ($canEditRows)) {
    $sites_list .= $pg->generateTheadLine('order_action'.$indexText, '&nbsp;&nbsp;&nbsp;Action', array('right'=>1, 'nosort'=>1));
} else {
    $sites_list .= $pg->generateTheadLine('order_blank'.$indexText, '&nbsp;', array('nosort'=>1));
}
$sites_list .= "    </tr>\n".
               "</thead>\n".
               "<tbody>\n";
foreach ($SiteKeyIDs as $key) {
    $sites->Load_from_key($key);

    //Grey out closed sites
    $escStyle = '';
    $siteIsClosed = ($sites->SiteStatus) ? 0 : 1;
    if ((isset($cfg['styleForClosedEntities'])) && ($cfg['styleForClosedSites'])) {
        $escStyle = ($siteIsClosed) ? " style=\"".$cfg['styleForClosedSites']."\"" : '';
    }

    $active = ($sites->SiteStatus == 1) ? 'Active' : 'Inactive';
    $sites_list .= "<tr>\n".
        "    <td class=\"left\"$escStyle>" . $sites->SiteName . "</td>\n".
        "    <td class=\"left\"$escStyle>" . $sites->SiteCode . "</td>\n".
        "    <td class=\"left\"$escStyle><span class=\"phone\">" . $sites->SitePhone . "</span></td>\n".
        "    <td class=\"left\"$escStyle>" . trim(str_replace('00:00:00', '', $sites->SiteOpenDate)) . "</td>\n".
        "    <td class=\"left\"$escStyle>" . $active . "</td>\n";
    if ($canEditRows) {
        $sites_list .= "    <td class=\"center\"$escStyle><a href=\"siteViewEdit.php?id=" . urlencode($key) . "\">View/Edit</a></td>\n";
    } elseif ($canViewRows) {
        $sites_list .= "    <td class=\"center\"$escStyle><a href=\"siteViewEdit.php?id=" . urlencode($key) . "\">View</a></td>\n";
    } else {
        $sites_list .= "    <td class=\"center\">&nbsp;</td>\n";
    }
    $sites_list .= "</tr>\n";
}
$sites_list .= '</tbody>' . "\n";
$sites_list .= '</table>' . "\n";

//add pagination display
$sites_list .= $pg->generatePaginationCode($pgInfo, 0, $sites, $totalRowCount);

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Sites
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
	</head>
	<body>
	    <form name='main_form' id='main_form' action='sites.php' method='POST'>
<?php
        //pagination variables
        print $pg->generateFormVariables($pgInfo);
?>

		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('sites');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<form >
                    <div class="subhead">
						Sites
<?php                   if ($canEditRows) { ?>
                            <a href="siteAddEdit.php"><button type="button" class="button">Add Site</button></a>
<?php                   }
                        displaySuccessAndErrors();
?>
                    </div>
                        <hr />
<?php
                        print $sFilter->generateSearchFilterCode(0);
?>
                        <div class="msg"><?php echo $msg; ?></div>
                        <?php echo $sites_list; ?>
					</form>
				</div>
			</div>
		</div>
	    </form>
	</body>
</html>
<?php

