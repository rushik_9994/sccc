<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once('lib/SiteTrialsOpenJoin.class.php');

//create required objects
$stoj = new SiteTrialsOpenjoin;

$stoj->Load_from_key($_GET['stojid']);

if($_GET['status'] == 1) {
    $prefix = 'Un-';
} else {
    $prefix = '';
}
?>
<html>
	<head>
    <style>
	textarea { width: 100%; padding: 10px; border: 3px solid #999;
   	-webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
	-moz-box-sizing: border-box;    /* Firefox, other Gecko */
	box-sizing: border-box;         /* IE 8+ */
    }
    div.colorbox-frame {
        width: 450px;
    }
</style>
<script>
$(document).ready(function(){
    $('.dpick').datepicker({dateFormat: 'yy-mm-dd', prevText: '&nbsp;<<&nbsp;', nextText: '>>&emsp;', changeYear: true, yearRange: '<?php echo (date("Y",time()) - 100).':'.(date("Y",time()) + 1);  ?>'});
    $('#SiteTrialOpenCloseDate').datepicker("option", "minDate", "<?php echo $stoj->SiteTrialOpenApprovalDate; ?>");
    $('#SiteTrialOpenTerminationDate').datepicker("option", "minDate", "<?php echo $stoj->SiteTrialOpenCloseDate; ?>");
});
</script>
	</head>
	<body>
        <form name="confirm" id="confirm" method="post" action="<?php echo $_GET['url']; ?>">
        <input type="hidden" name="stoj[SiteKeyID]" value="<?php echo $_GET['id']; ?>" />
        <input type="hidden" name="stoj[TrialKeyID]" value="<?php echo $_GET['tid']; ?>" />
        <input type="hidden" name="stoj[SiteTrialOpenKeyID]" value="<?php echo $_GET['stojid']; ?>" />
        <input type="hidden" name="action" value="<?php echo $_GET['action']; ?>" />
            <div class="colorbox-frame">
                <table class="centered fullwidth outer-margin">
                <?php if($_GET['status'] == 0){ ?>
                    <tr>
                        <td>&emsp;<strong><?php echo $prefix.ucfirst($_GET['action']); ?> Date:</strong></td>
                        <?php if(($_GET['action']=='close')||($_GET['action']=='unclose')){ ?>
                            <td><input type="text" name="stoj[SiteTrialOpenCloseDate]" id="SiteTrialOpenCloseDate" size="32" class="dpick req" value="<?php echo (!empty($stoj->SiteTrialOpenCloseDate)) ? $stoj->SiteTrialOpenCloseDate : ''; ?>" /></td>
                        <?php }elseif(($_GET['action']=='terminate')||($_GET['action']=='terminate')){ ?>
                             <td><input type="text" name="stoj[SiteTrialOpenTerminationDate]" id="SiteTrialOpenTerminationDate" size="32" class="dpick req" value="<?php echo (!empty($stoj->SiteTrialOpenTerminationDate)) ? $stoj->SiteTrialOpenTerminationDate : ''; ?>" /></td>
                        <?php } ?>
                    </tr>
                <?php }else{ ?>
                    <tr>
                        <td></td>
                        <?php if(($_GET['action']=='close')||($_GET['action']=='unclose')){ ?>
                        <td><input type="hidden" name="stoj[SiteTrialOpenCloseDate]" size="32" class="" value="" /></td>
                        <?php }elseif(($_GET['action']=='terminate')||($_GET['action']=='terminate')){ ?>
                        <td><input type="hidden" name="stoj[SiteTrialOpenTerminationDate]" size="32" class="" value="" /></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="return validate('confirm');$('#confirm').submit();">&emsp;<?php echo $prefix.ucfirst($_GET['action']); ?> Trial&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;Cancel&emsp;</a></td>
                    </tr>
                </table>
            </div>
        </form>
	</body>
</html>
<?php

