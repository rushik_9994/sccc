<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Trial.class.php');

//create required objects
$trial = new Trial;

$trial->Load_from_key($_GET['id']);
?>
<html>
	<head>
	</head>
	<body onload="">
        <form name="trialCompanion" id="trialCompanion" method="post" action="<?php echo $_GET['url']; ?>">
            <input type="hidden" name="TrialCompanionKeyID" value="<?php echo $_GET['cid']; ?>" />
            <input type="hidden" id="action" name="action" value="delete_companion" />
            <!-- confirm delete -->
            <div id="group-delete" class="">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete the Companion Trial from <?php echo $trial->TrialProtocolNumber; ?>?</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$('#trialCompanion').submit();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
        </form>
	</body>
</html>
<?php

