<?php
/**
 * Module that actually triggers a download (Excel)
 *
 */

include_once('reportCommonFunctions.php');
require_once ('lib/Comp.class.php');

session_start();
if (empty($_SESSION['UserKeyID'])) { header('location: login.php'); exit; }


/**
 * Excel download of the 'Credit Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo
 */
function downloadCreditReport($connection, &$reportInfo) {
    return _downloadsGenerateReportDownload($connection, $reportInfo);
}


/**
 * Excel download of the 'Patient Registration Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo
 */
function downloadPatientRegistrationReport($connection, &$reportInfo) {
    return _downloadsGenerateReportDownload($connection, $reportInfo);
}


/**
 * Excel download of the 'Doctor Accruals Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo
 */
function downloadDoctorAccrualsReport($connection, &$reportInfo) {
    return _downloadsGenerateReportDownload($connection, $reportInfo);
}


/**
 * Excel download of the 'Doctor Accruals By Site Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo
 */
function downloadDoctorAccrualsBySiteReport($connection, &$reportInfo) {
    return _downloadsGenerateReportDownload($connection, $reportInfo);
}


/**
 * Excel download of the 'Master Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo
 */
function downloadMasterReport($connection, &$reportInfo) {
    return _downloadsGenerateReportDownload($connection, $reportInfo);
}


/**
 * Excel download of the 'Tracking Report'
 *
 * @param object    $connection
 * @param array ref $reportInfo
 */
function downloadTrackingReport($connection, &$reportInfo) {
    return _downloadsGenerateReportDownload($connection, $reportInfo);
}


/**
 * Generates reports in downloadable format (currently supported: Excel only)
 */
function _downloadsGenerateReportDownload($connection, &$reportInfo) {
    $from        = 0;
    $size        = 5000;
    $isFirst     = 1;
    $cont        = '';
    $absRowIndex = 0;

    $reportInfo['debugMode']    = 0;

    while($error=='') {

       //Load a slice
       list($rows, $xrows, $haveMoreRows, $from, $error) = queryLibraryGetReportRowSlice($connection, $reportInfo, $from, $size);

       //If we have an error or have no rows (in the first slice) then exit the loop
       if ($error) { _downloadsDieWithError($error); }
       if (count($rows)==0) {
           if ($isFirst) { _downloadsDieWithError('No data rows found for this report'); } else { break; }
       }

       //Start the export (uses the first row for titles)
       if ($isFirst) {
           $error = _downloadsStartTheExport($connection, $reportInfo, $rows[0]);
           if ($error) { _downloadsDieWithError($error); }
           $absRowIndex++;
           $isFirst = 0;
       }

       //Export all rows of this slice of data
       for ($i=0,$maxi=count($rows); $i < $maxi; $i++) {
           $error = _downloadsAddOneExportRow($connection, $reportInfo, $absRowIndex, $rows[$i]);
           $absRowIndex++;
       }

       //Do we have more data? If so then load another slice
       if (!$haveMoreRows) { break; }

    }//next slice

    if ($absRowIndex > 0) {
       $error = _downloadsEndTheReport($connection, $reportInfo);
       if ($error) { _downloadsDieWithError($error); }
    }

}



/**
 * Code built after the Google Excel class
 * https://code.google.com/p/phpexportxlsclass/downloads/detail?name=export-xls.class-v1.01.zip
 * Alternative method:  http://www.the-art-of-web.com/php/dataexport/
 * Another good source: http://webcheatsheet.com/php/create_word_excel_csv_files_with_php.php#excelheader
 */
function _downloadsStartTheExport($connection, &$reportInfo, $firstRow=array()) {
    $error  = '';

    $report    = $reportInfo['report'];
    $format    = $reportInfo['format'];
    $debugMode = $reportInfo['debugMode'];
    $cont      = '';

    if (($format=='excel') && (count($firstRow)>0)) {
        //Create our file name
        list($reportInfo['fileName'], $error) = _downloadsCreateFilenameForDownload($report, 'xls', $reportInfo);
        if ($error == '') {

            //Send Excel headers
            if (!$debugMode) {

        		#send headers
        		header("Pragma: public");
        		header("Expires: 0");
        		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        		header("Content-Type: application/force-download");
        		header("Content-Type: application/octet-stream");
        		header("Content-Type: application/download");
        		header("Content-Disposition: attachment;filename=".$reportInfo['fileName']);
        		header("Content-Transfer-Encoding: binary ");

                #Excel start of file
	            $cont .= pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
            } else {
                print "<br /><b>DEBUG MODE:  fileName=$fileName error=$error</b><br />";
                print "<pre style=\"background-color:#DDDDDD;color:black;\">";
                $cont = str_replace("    ", '[TAB]', $cont);
                print $cont;
                print "</pre>";
            }

            #Excel title row
            $titleRow    = array_keys($firstRow);
            $columnIndex = 0;
            foreach($titleRow as $ignore=>$title) {
                $title = str_replace('__Remove', '', $title);
                $title = str_replace('__', '', $title);
                if (is_numeric($title)) {
        	        $cont .= _downloadsExcelNumFormat($absRowIndex, $columnIndex, $title);
                } else {
                    $cont .= _downloadsExcelTextFormat($absRowIndex, $columnIndex, $title);
                }
                $columnIndex++;
            }

            print $cont;
        }
    }//excel

    return $error;
}


function _downloadsAddOneExportRow($connection, &$reportInfo, $absRowIndex, $row=array()) {
    $error = '';

    $report    = $reportInfo['report'];
    $format    = $reportInfo['format'];
    $debugMode = $reportInfo['debugMode'];
    $cont      = '';

    if ($format=='excel') {
        $columnIndex = 0;
        foreach($row as $ignore=>$field) {
            if (is_numeric($field)) {
    	        $cont .= _downloadsExcelNumFormat($absRowIndex, $columnIndex, $field);
            } else {
                $cont .= _downloadsExcelTextFormat($absRowIndex, $columnIndex, $field);
            }
            $columnIndex++;
        }
    }

    print $cont;

    return $error;
}


function _downloadsEndTheReport($connection, &$reportInfo) {
    $error = '';

    $report    = $reportInfo['report'];
    $format    = $reportInfo['format'];
    $debugMode = $reportInfo['debugMode'];
    $cont      = '';

    if ($format=='excel') {
        //Excel end of file
        $cont .= pack("ss", 0x0A, 0x00);
    }

    print $cont;

    return $error;
}


function _downloadsExcelTextFormat($row, $col, $data) {
	$data   = utf8_decode($data);
	$length = strlen($data);
	$field  = pack("ssssss", 0x204, 8 + $length, $row, $col, 0x0, $length);
	$field .= $data;

	return $field;
}


function _downloadsExcelNumFormat($row, $col, $data) {
	$field  = pack("sssss", 0x203, 14, $row, $col, 0x0);
	$field .= pack("d", $data);

    return $field;
}



function _downloadsCreateFilenameForDownload($report, $fileExtension, $reportInfo=array()) {
    $fileName = '';
    $error    = '';

    $datePostfix    = '';
    $startDate      = '';
    $endDate        = '';
    $post           = $reportInfo['post'];
    $reportCompName = '';

    if ((!isset($post['ReportStartDate'])) || (!isset($post['ReportEndDate']))) {
        $error = 'Start and/or end date for report missing';
    } else {
        $startDate   = str_replace('-', '_', $post['ReportStartDate']);
        $endDate     = str_replace('-', '_', $post['ReportEndDate']);
        $datePostfix = '_'.$startDate.'_to_'.$endDate;
    }

    if ($error=='') {
        list($reportPrefix, $reportTitle, $error) = commonDetermineReportTitleAndPrefix($report);
    }

    if ($error=='') {
        //Is this a Doctor Accruals By Site Report with a chosen component?
        if ((isset($_POST['dacbs_CompKeyID'])) && ($_POST['dacbs_CompKeyID'])) {
            $comp = new Comp();
            $comp->Load_from_key($_POST['dacbs_CompKeyID']);
            $compName = $comp->getCompName();
            $compName = strtolower($compName);
            $compName = preg_replace('/[^a-z,0-9]/', '_', $compName);
            $reportCompName = "_for__".$compName.'__';
        }
    }

    if ($error=='') {
        $fileName = $reportPrefix.$reportCompName.$datePostfix.'.'.str_replace('.','',$fileExtension);
    }

    return array($fileName, $error);
}



/**
 * Dies displaying an error
 *
 * Note: Function does NOT return. Exit is called.
 *
 * @param string $error
 */
function _downloadsDieWithError($error) {
    print "ERROR: $error<br />";
    exit;
}


