<?php

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once ('lib/Patient.class.php');
require_once ('lib/PatientIdentifier.class.php');
require_once ('lib/Permissions.class.php');
require_once ('lib/Doctor.class.php');
require_once ('lib/DoctorSiteJoin.class.php');
require_once ('lib/Site.class.php');
require_once ('lib/Trial.class.php');
require_once ('lib/TrialGroup.class.php');
require_once ('lib/PatientSex.class.php');
require_once ('lib/PatientRace.class.php');
require_once ('lib/PatientEthnicity.class.php');
$pageURL = getPageUrl();

//create required objects
$patients          = new Patient();
$patientIdentifier = new PatientIdentifier();
$permissions       = new Permissions();
$doctors           = new Doctor();
$doctorSiteJoins   = new DoctorSiteJoin();
$sites             = new Site();
$trials            = new Trial();
$trialGroup        = new TrialGroup();
$patientSex        = new PatientSex();
$patientRace       = new PatientRace();
$patientEthnicity  = new PatientEthnicity();


//define required variables
$msg = '';

$canEditRows = checkPermissionString('AddEditPatients');
$canViewRows = checkPermissionString('ViewPatients');
if ((!$canEditRows) && (!$canViewRows)) {
    header('location: login.php');  exit;
}

// $permissions->Load_from_action("AddEditComponents");
// $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
// if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs))
// {
//     header("location: login.php");
// }
// $permissions->Load_from_action("DeleteComponents");
// $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
// if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs))
// {
//     header("location: login.php");
// }

//Enforce user permissions or redirect to login.php
list($UserTypeIDs) = enforceUserPermissions($permissions);

$patients->Load_from_key($_GET['id']);
$sites->Load_from_key($patients->SiteKeyID);
$patientSex->Load_from_key($patients->PatientSex);
$patientEthnicity->Load_from_key($patients->PatientEthnicity);
$patientRace->Load_from_key($patients->PatientRace);
$doctors->Load_from_key($patients->DoctorKeyID);
$trials->Load_from_key($patients->TrialKeyID);
$trialGroup->Load_from_key($patients->TrialGroupKeyID);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
		<meta name="author" content="Cliff Garrett" />
		<title>
			Patient View/Edit
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
		<script type="text/javascript">
        	$(document).ready(function(){
        	});
		</script>
	</head>
	<body>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                    <?php
                    print displayTopRightInfo();
                    print displayTabs('patients');
                    ?>
    				<div id="tabs-1" class="ui-tabs-panel">
    					<form method="post" action="" name="mainform" id="mainform">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="continue" id="continue" />
                        <?php if ($_GET['id']) { ?>
                        <input type="hidden" name="comp[CompKeyID]" id="CompKeyID" value="<?php echo $_GET['id']; ?>" />
                        <?php } ?>
                    <div class="subhead">
    					Patient View/Edit
<?php                   if ($canEditRows) { ?>
    					    <a href="patientAddEdit.php" class="button">Add Patient</a>
                            <a id="main-button" class="button" href="patientAddEdit.php?id=<?php echo $_GET['id']; ?>">Edit Patient</a>
<?php                   } ?>
                        <a href="patients.php" class="button">Back</a>
                        <?php displaySuccessAndErrors(); ?>
                    </div>
                        <hr />
                        <div class="msg"><?php echo $msg; ?></div>
                        <table class="pad-height">
                            <!--
                            <tr>
                                <td>|</td>
                                <td>|</td>
                                <td>|</td>
                                <td>|</td>
                                <td>|</td>
                                <td>|</td>
                                <td>|</td>
                            </tr>
                            -->
                            <tr>
                                <td>
                                    <strong>Last&nbsp;Init:</strong>&nbsp;<?php echo (!empty($patients->PatientLastInit)) ? $patients->PatientLastInit : ''; ?>
                                </td>
                                <td>
                                    &emsp;<strong>First&nbsp;Init:</strong>&nbsp;<?php echo (!empty($patients->PatientFirstInit)) ? $patients->PatientFirstInit : ''; ?>
                                </td>
                                <td>
                                    &emsp;<strong>Mid.&nbsp;Init:</strong>&nbsp;<?php echo (!empty($patients->PatientMiddleInit)) ? $patients->PatientMiddleInit : ''; ?>
                                </td>
                                <td>
                                    &emsp;&emsp;&emsp;&emsp;
                                </td>
                                <td>
                                    <strong>Patient&nbsp;Identifier:</strong>&nbsp;
                                </td>
                                <td>
<?php
                                    if (!empty($patients->PatientIdentifier)) {
                                        $patientIdentifier->Load_from_key($patients->PatientIdentifier);
                                        echo $patientIdentifier->PatientIdentifierText;
                                    }
                                    echo '&emsp;' . $patients->PatientIdentifierAddendum;
?>
                                </td>
                                <td>
                                    &emsp;<strong>Zip&nbsp;Code:</strong> <span class="zip"><?php echo (!empty($patients->PatientZipCode)) ? $patients->PatientZipCode : ''; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Birthdate:</strong>&nbsp;</td><td colspan="2">&emsp;<?php echo str_replace('0000-00-00', '', $patients->PatientBirthDate); ?>
                                </td>
                                <td>
                                    &emsp;&emsp;&emsp;
                                </td>
                                <td>
                                    <strong>Registration&nbsp;Date:</strong>
                                </td>
                                <td>
                                    <?php echo str_replace('0000-00-00', '', $patients->PatientRegistrationDate); ?>
                                </td>
                                <td>
                                    <strong>&nbsp;&nbsp;&nbsp;Urban/Rural:</strong> <?php echo $patients->PatientUrbanRural; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Age Today:</strong>
                                </td>
                                <td colspan="2">
                                    &emsp;<?php echo $patients->getCurrentAge(); ?>
                                </td>
                                <td>
                                    &emsp;&emsp;&emsp;
                                </td>
                                <td>
                                    <strong>Age At Registration:&nbsp;&nbsp;</strong>
                                </td>
                                <td colspan="2">
                                    <?php echo $patients->getAgeAtRegistration(); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Sex:&nbsp;</strong><?php echo $patientSex->PatientSexText; ?>
                                </td>
                                <td>
                                    &emsp;<strong>Race:&nbsp;</strong><?php echo $patientRace->PatientRaceText; ?>
                                </td>
                                <td>
                                    &emsp;<strong>Ethnicity:</strong>&nbsp;<?php echo $patientEthnicity->PatientEthnicityText; ?>
                                </td>
                                <td>
                                    &emsp;&emsp;&emsp;
                                </td>
                                <td>
                                    <?php echo ($patients->PatientMultiRacial == 0) ? '<em>(Not Multiracial)</em>' : '<em>Multiracial</em>'; ?>
                                </td>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Site:&nbsp;</strong>
                                </td>
                                <td colspan="6">
                                    &emsp;<?php echo $sites->SiteName; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Doctor:&nbsp;</strong>
                                </td>
                                <td colspan="6">
                                    &nbsp;&nbsp;<span id="DoctorSelect">
<?php
                                        if ((!empty($doctors->DoctorKeyID)) && ($doctors->DoctorKeyID != 0)) {
                                            echo $doctors->DoctorLastName . ', ' . $doctors->DoctorFirstName;
                                        }
?>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Trial&nbsp;Protocol:</strong>
                                </td>
                                <td>
                                    &emsp;<?php echo $trials->TrialProtocolNumber; ?>
                                </td>
                                <td colspan="2">
                                    <strong>&emsp;Patient&nbsp;Trial&nbsp;Credit:&nbsp;</strong><?php echo (!empty($patients->PatientTrialCredit)) ? $patients->PatientTrialCredit : ''; ?>
                                </td>
                                <td>
                                    <strong>Patient QoL:</strong> <input type="checkbox" name="patient[PatientQoL]" id="PatientQoL" <?php echo ($patients->PatientQoL == 0) ? '' : 'checked'; ?> disabled="disabled" />
                                </td>
                                <td>
                                    <strong>Patient QoLC:</strong>&nbsp;</strong><?php echo $patients->PatientQoLC; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <strong>Group:&nbsp;</strong><?php echo (!empty($trialGroup->TrialGroupTitle)) ? $trialGroup->TrialGroupTitle : ''; ?>
                                </td>
                                <td colspan="2">
                                    <strong>Creditor:&nbsp;</strong>
<?php
                                    if (!empty($patients->PatientCreditor)) {
                                        $patientIdentifier->Load_from_key($patients->PatientCreditor);
                                        echo $patientIdentifier->PatientIdentifierText;
                                    }
?>
                                </td>
                            </tr>
                        </table>
                        <div class="clear"></div>
                        <!--<div class="right formdiv noborder">
                            <button type="button" class="inline" onmouseup="$('#mainform').submit();">Add/Update</button>&emsp;
                            <?php if (!empty($_GET['id']))
{ ?>
                            <a href="#confirm-delete" class="confirm-delete"><button type="button" class="inline">Delete</button></a>
                            <?php } ?>
                        </div>-->
    				</form>
    			</div>
    		</div>
        </div>
        <!-- start colorboxes -->
        <div class="colorboxes">
                    
            <!-- confirm delete -->
            <div id="confirm-delete">
                <table class="centered">
                    <tr>
                        <td class="center extra-padding" colspan="2">Are you sure you want to delete this Component?<br />Doing so will remove all associations with Sites, Trials, Doctors and Patients.</td>
                    </tr>
                    <tr>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="trashMe();">&emsp;Yes&emsp;</a></td>
                        <td class="center extra-padding"><a class="button floatnone" href="javascript:void(0);" onmouseup="$.colorbox.close();" >&emsp;No&emsp;</a></td>
                    </tr>
                </table>
            </div>
            <!-- end confirm delete-->
            
        </div>
	</body>
</html>
<?php


/**
 * Enforce user permissions or redirect to login.php
 *
 * @param $permissions
 * @return array array($UserTypeIDs)
 */
function enforceUserPermissions($permissions) {
    $permissions->Load_from_action("ViewPatients");
    $UserTypeIDs = explode(',', $permissions->UserTypeIDs);
    if (!in_array($_SESSION['UserTypeKeyID'], $UserTypeIDs)) {
        header("location: login.php");
    }
    return array($UserTypeIDs);
}

