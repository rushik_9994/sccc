<?php
error_reporting(0);
//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest(1);

//load required classes
require_once('lib/Doctor.class.php');
require_once('lib/DoctorSiteJoin.class.php');

//create required objects
$doctor     = new Doctor;
$doctorSite = new DoctorSiteJoin;

try {
    //------------------- transaction start
    $errors     = array();
    $connection = new DataBaseMysql();
    $connection->BeginTransaction();
    //------------------- transaction start

    $callingSubEvent = 'ajaxcall';
    $patientAjaxDebugMode = ((isset($cfg)) && (is_array($cfg)) &&
        (isset($cfg['debug.patientAjaxDebugMode'])) &&
        ($cfg['debug.patientAjaxDebugMode'] > 0))
        ? $cfg['debug.patientAjaxDebugMode'] : 0;

    $site = (isset($_REQUEST['site'])) ? $_REQUEST['site'] : '';
    if ((!$site) && (isset($_REQUEST['ajaxSiteKeyID']))) {
        $site = $_REQUEST['ajaxSiteKeyID'];
    }

    $selected = $_REQUEST['selected'];
    if ((!empty($site)) || ($site != '')) {
        $doctorIDsArray = $doctorSite->GetKeysWhereOrderBy('DoctorKeyID', 'SiteKeyID = \'' . $site . '\' AND DoctorSiteClose = 0', 'DoctorKeyID', 'ASC');
        $doctorIDs = join(',', $doctorIDsArray);
        if ($doctorIDs == ',') {
            $doctorIDs = '';
        } else {
            if (!$doctorIDs) {
                $doctorIDs = '-1';
            } //Bugfix: 2017-06-07
            $doctorIDs = 'WHERE DoctorKeyID IN (' . $doctorIDs . ')';
        }
        $select = $doctor->CreateSelect($selected, $doctorIDs);
    } else {
        $select = '<span class="msg" >Select Site First <span class="arrow">&#8593;</span></span>';
    }

    //------------------- transaction commit or throw exception
    //Commit all database changes or roll our transaction back
    if (haveAnyErrors())    { throw new \Exception('EncounteredErrors'); }
    if (!haveAnyErrors())   { $success  = 'All changes made successfully'; }
    if (isset($connection)) { $connection->CommitTransaction(); }
    //------------------- transaction commit or throw exception

} catch (\Exception $e) {
    $publicMessage = $e->getMessage();
    $f1 = strpos($publicMessage, '|||');
    if ($f1 !== false) {
        $publicMessage = substr($publicMessage, 0, $f1);
    }
    $select = '<span class="msg" >ERROR: '. htmlentities($publicMessage).'</span>';
    //------------------- transaction roll back
    //Roll the transaction back
    $connection->RollbackTransaction();
    if ($e->getMessage() != 'EncounteredErrors') { logException($e); }
    $success = '';
    //------------------- transaction roll back
}

echo $select;

