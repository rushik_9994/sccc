<?php
error_reporting(0);
/**
 * Patient management page
 */

//Initialize page
global $cfg, $errors, $warnings, $success, $callingEvent, $callingSubEvent;
defined('ROOT_PATH') || define('ROOT_PATH', realpath(dirname(__FILE__)));
require_once(ROOT_PATH.'/lib/commonBase.php');  //startRequest, checkSchema, session_start, check authentication etc.
list($cfg, $errors, $warnings, $success) = startRequest();

//load required classes
require_once(ROOT_PATH.'/lib/Patient.class.php');
require_once(ROOT_PATH.'/lib/Trial.class.php');
require_once(ROOT_PATH.'/lib/PatientIdentifier.class.php');
require_once(ROOT_PATH.'/lib/Permissions.class.php');
require_once(ROOT_PATH.'/lib/Pagination.class.php');
require_once(ROOT_PATH.'/lib/SearchFilter.class.php');

//Set some defaults
if ((!isset($_REQUEST['order_field_0'])) || (!$_REQUEST['order_field_0'])) {
    $_REQUEST['order_field_0']     = 'order_initials_0';
    $_REQUEST['order_direction_0'] = 'asc';
}

//create required objects
$patients          = new Patient;
$patientIendtifier = new PatientIdentifier;
$trials            = new Trial;

//define required variables
$msg = '';

//Check permissions
$canEditRows = checkPermissionString('AddEditPatients');
$canViewRows = checkPermissionString('ViewPatients');

//pagination setup
$pg     = new Pagination();
$pgInfo = $pg->getPaginationVarsFromRequest($_REQUEST, 'patients.pagination.size');

//searchFilter setup
$sFilter  = new SearchFilter('patients');
$sClauses = $sFilter->analyzeSearchFilter($_REQUEST, 0, $cfg);

$PatientKeyIDs = $patients->GetKeysOrderBy('PatientKeyID',
                                           'ASC', 
                                           $pgInfo[0]['from'], 
                                           $pgInfo[0]['limit'], 
                                           $sClauses);  //pagination added
$totalRowCount = $patients->totalRowCount;

//begin building output
$indexText = '_0';
$patients_list  = "<a name='tb_0'></a>\n".
                  "<table class=\"fullwidth zebra sort\">\n".
                  "<thead>\n".
                  "    <tr class='thead_unselected'>\n".
                  $pg->generateTheadLine('order_initials'.$indexText, 'Initials').
                  $pg->generateTheadLine('order_protocol_number'.$indexText, 'Protocol Number').
                  $pg->generateTheadLine('order_identifier'.$indexText, 'Identifier').
                  $pg->generateTheadLine('order_zip_code'.$indexText, 'Zip Code').
                  $pg->generateTheadLine('order_urban_rural'.$indexText, 'Urban/Rural').
                  $pg->generateTheadLine('order_birthdate'.$indexText, 'Birthdate').
                  $pg->generateTheadLine('order_reg_date'.$indexText, 'Reg. Date');
if (($canViewRows) || ($canEditRows)) {
    $patients_list .= $pg->generateTheadLine('order_action'.$indexText, '&nbsp;&nbsp;&nbsp;Action', array('right'=>1, 'nosort'=>1));
} else {
    $patients_list .= $pg->generateTheadLine('order_blank'.$indexText, '&nbsp;', array('nosort'=>1));
}
$patients_list .= "     </tr>\n".
                  "</thead>\n".
                  "<tbody>\n";
foreach ($PatientKeyIDs as $key) {
    $patients->Load_from_key($key);
    $patientIendtifier->Load_from_key($patients->PatientIdentifier);

    $trials->Load_from_key($patients->TrialKeyID);

    $patients_list .= "<tr>\n".
                      "<td class=\"left\">" . $patients->PatientFirstInit . $patients->PatientMiddleInit . $patients->PatientLastInit . "</td>\n".
                      "<td class=\"left\">" . $trials->TrialProtocolNumber . "</td>\n".
                      "<td class=\"left\">" . $patientIendtifier->PatientIdentifierText . '&nbsp;' . $patients->PatientIdentifierAddendum . "</td>\n".
                      "<td class=\"left\"><span class=\"zip\">" . $patients->PatientZipCode . "</span></td>\n".
                      "<td class=\"left\">"  . $patients->PatientUrbanRural . "</td>\n".
                      "<td class=\"left\">" . trim(str_replace('0000-00-00', '', $patients->PatientBirthDate)) . "</td>\n".
                      "<td class=\"left\">" . trim(str_replace('0000-00-00', '', $patients->PatientRegistrationDate)) . "</td>\n";
    if ($canEditRows) {
        $patients_list .= "<td class=\"center\"><a href=\"patientViewEdit.php?id=".urlencode($key)."\">View/Edit</a></td>\n";
        $colspan = 6;
    } elseif ($canViewRows) {
        $patients_list .= "<td class=\"center\"><a href=\"patientViewEdit.php?id=".urlencode($key)."\">View</a></td>\n";
        $colspan = 6;
    } else {
        $patients_list .= "<td class=\"center\">&nbsp;</td>\n";
        $colspan = 6;
    }
    $patients_list .= "</tr>\n";
}

$patients_list .= '</tbody>' . "\n";
$patients_list .= '</table>' . "\n";

//add pagination display
$patients_list .= $pg->generatePaginationCode($pgInfo, 0, $patients, $totalRowCount);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
		<title>
			Patients
		</title>
        <?php require_once ("lib/common.includes.php"); ?>
	</head>
	<body>
	    <form name='main_form' id='main_form' action='patients.php' method='POST'>
<?php
        //pagination variables
        print $pg->generateFormVariables($pgInfo);
?>
		<div class="wrapper">
			<div class="logo">
			</div>
			<div class="ui-tabs">
                <?php
                print displayTopRightInfo();
                print displayTabs('patients');
                ?>
				<div id="tabs-1" class="ui-tabs-panel">
					<form>
                    <div class="subhead">
						Patients
<?php                   if ($canEditRows) { ?>
                            <a href="patientAddEdit.php"><button type="button" class="button">Add Patient</button></a>
<?php                   }
                        displaySuccessAndErrors();
?>
                    </div>
                        <hr />
<?php
                        print $sFilter->generateSearchFilterCode(0);
?>
                        <div class="msg"><?php echo $msg; ?></div>
                        <?php echo $patients_list; ?>
					</form>
				</div>
			</div>
		</div>
		</form>
	</body>
</html>
<?php


