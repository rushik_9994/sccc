<?php

$connection = new DataBaseMysql();

class ChartReport{

	public function getReportData($chart_form_data){
		// echo "<pre>chart_form_data";
		// print_r($chart_form_data);
		// exit();
		$titleBlock				= trim($_POST["titleBlock"]);
	    $ReportStartDate		= trim($_POST["ReportStartDate"]);
	    $ReportEndDate			= trim($_POST["ReportEndDate"]);
	    $chart_data				= trim($_POST["chart_data"]);
	    $TrialTypeKeyID			= trim($_POST["TrialTypeKeyID"]);
	    $TrialCategoryKeyID		= trim($_POST["TrialCategoryKeyID"]);
	    $TrialDiseaseTypeKeyID	= trim($_POST["TrialDiseaseTypeKeyID"]);
	    $ReportType				= trim($_POST["ReportType"]);
	    $detailsOption			= trim($_POST["detailsOption"]);
	    $nciQuota				= trim($_POST["nciQuota"]);

		// $condition_params = "Select * from Patient where ";
		$condition_params = [];
		$column = [];
		$leftjoin_data = [];
		$group_by_data = [];
		// Start Date
		if(!empty($ReportStartDate)){
			$ReportStartDate = $date = date('Y-m-d',strtotime($ReportStartDate));
	        $condition_params[] =  "Patient.PatientRegistrationDate >= '" . $ReportStartDate . "'";
	    }
	    // End Date
		if(!empty($ReportEndDate)){
			$ReportEndDate = $date = date('Y-m-d',strtotime($ReportEndDate));
	        $condition_params[] =  " Patient.PatientRegistrationDate <= '" . $ReportEndDate . "'";
	    }

	    // for Chart Type and value is credits
	    if(!empty($chart_data) && strtolower($chart_data) == "credits"){
	    	// sum of PatientTrialCredit and PatientQoLC
	    	$column[] = "(Patient.PatientTrialCredit + Patient.PatientQoLC) AS credit_qolc_total";
	    }
	    // for Chart Type and value is Accruals
	    if(!empty($chart_data) && strtolower($chart_data) == "accruals"){
	    	// count of the query
	    	$column[] = "count(*)";
	    }

	    if(!empty($TrialTypeKeyID)){
	    	// if in Patient table TrialKeyID has not null then that data has to get other data has to remove
	    	// Patient table has to make innerjoin with Tire table in that that data has to get which Tire.TrialTypeKeyID is match with getted $TrialTypeKeyID
	    	$leftjoin_data[] = "INNER JOIN Trial ON Patient.TrialKeyID = Trial.TrialKeyID";
	    	$condition_params[] = "Trial.TrialTypeKeyID = " . $TrialTypeKeyID;
	    }
	    if(!empty($TrialCategoryKeyID)){
	    	// has to put leftjoin with Trial
	    	// if in Patient table TrialKeyID has not null then that data has to get other data has to remove
	    	$condition_params[] = "Trial.TrialCategoryKeyID = " . $TrialCategoryKeyID;
	    }
	    if(!empty($TrialDiseaseTypeKeyID)){
	    	// has to put leftjoin with TrialDiseaseTypeJoin
	    	// if in Patient table TrialDiseaseTypeKeyID has not null then that data has to get other data 
	    	$leftjoin_data[] = "INNER JOIN TrialTrialDiseaseTypeJoin ON Patient.TrialKeyID = TrialTrialDiseaseTypeJoin.TrialKeyID";
	    	$condition_params[] = "TrialTrialDiseaseTypeJoin.TrialDiseaseTypeKeyID = " . $TrialDiseaseTypeKeyID;
	    }

	    if(!empty($ReportType) && strtolower($ReportType) == "per_component"){
	    	$column[] = "Comp.CompName";
	    	$leftjoin_data[] = "INNER JOIN Site ON Patient.SiteKeyID = Site.SiteKeyID";
	    	$leftjoin_data[] = "INNER JOIN Comp ON Site.CompKeyID = Comp.CompKeyID";
	    	$condition_params[] = "Comp.CompStatus = 0";
	    }
	    if(!empty($ReportType) && strtolower($ReportType) == "per_month" ){
	    	$column[] = "YEAR(`PatientRegistrationDate`), MONTHNAME(`PatientRegistrationDate`), COUNT(*)";
	    	$group_by_data[] = "MONTH(PatientRegistrationDate), YEAR(`PatientRegistrationDate`)";
	    }
	    if(!empty($ReportType) && strtolower($ReportType) == "nci_quota" ){
	    	$column[] = "YEAR(`PatientRegistrationDate`), MONTHNAME(`PatientRegistrationDate`) ";
	    	$group_by_data[] = "MONTH(PatientRegistrationDate), YEAR(`PatientRegistrationDate`)";
	    }

	    if(!empty($detailsOption) && strtolower($detailsOption) == "minority"){
	    	$column[] = "SUM(PatientRace) as minority_value";
	    	$leftjoin_data[] = "INNER JOIN PatientRace ON Patient.PatientRace = PatientRace.PatientRaceKeyID";
	    	$condition_params[] = "PatientRace.PatientRaceText != 'White'";
	    }
	    if(!empty($detailsOption) && strtolower($detailsOption) == "aya"){
	    	$column[] = "SUM(ABS(DATEDIFF(PatientBirthDate,PatientRegistrationDate))) AS diff_BR_date";
	    	$condition_params[] = "ABS(DATEDIFF(PatientBirthDate,PatientRegistrationDate)) >= '15' AND ABS(DATEDIFF(PatientBirthDate,PatientRegistrationDate)) < '40'";
	    }
	    if(!empty($detailsOption) && strtolower($detailsOption) == "rural"){
	    	$column[] = "COUNT(Patient.PatientUrbanRural) as total_rulal_value";
	    	$condition_params[] = "Patient.PatientUrbanRural != 'urban'";
	    }
	    if(!empty($detailsOption) && strtolower($detailsOption) == "ethnicity"){
	    	$column[] = "COUNT(Patient.PatientEthnicity) as total_ethnicity_value";
	    	$leftjoin_data[] = "INNER JOIN PatientEthnicity ON Patient.PatientEthnicity = PatientEthnicity.PatientEthnicityKeyID";
	    	$condition_params[] = "PatientEthnicity.PatientEthnicityText != 'Non-Hispanic'";
	    }

	    $query_array = [
	    	'column' => $column,
			'leftjoin' => $leftjoin_data,
			'condition_params' => $condition_params,
			'group_by_data' => $group_by_data
	    ];
	    $this->genarateQuery($query_array);
	}

	public function genarateQuery($query_array){
		$column = $query_array['column'];
		$leftjoin_data = $query_array['leftjoin'];
		$conditional_params = $query_array['condition_params'];
		$group_by_data = $query_array['group_by_data'];

		$where_data = "";
		$left_data = "";
		$column_data = "";
		$group_data = "";
		foreach ($column as $c_key => $c_value) {
			if($c_key != 0){
				$column_data .= ", ";
			}
			$column_data .= $c_value;
		}
		foreach ($leftjoin_data as $left_key => $left_value) {
			if($left_key != 0){
				$left_data .= " ";
			}
			$left_data .= $left_value;
		}
		foreach ($conditional_params as $key => $value) {
			if($key == 0){
				$where_data .= " WHERE ";
			}else{
				$where_data .= " AND ";
			}
			$where_data .= $value;
		}
		foreach ($group_by_data as $g_key => $g_value) {
			if($g_key != 0){
				$group_data .= ", ";
			}
			$group_data .= $g_value;
		}
		$final_query = "SELECT " . $column_data . " FROM Patient " . $left_data . $where_data;
		if($group_data != ""){
			$final_query .= " GROUP BY " . $group_data;
		}

		$connection = new DataBaseMysql();
		

		$result = $connection->RunQuery($final_query);
		
		$final_data = [];
		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            
            // print_r($row);
            // echo '<br>';
            array_push($final_data, $row);
        }
		$chart_data['chart_data'] = $chart_data;
		$chart_data['post_Data'] = $_POST;
		
		$_SESSION['chart_data']  = $chart_data;
	}
}
?>